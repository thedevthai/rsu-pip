﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="PracticeHistoryModify.aspx.vb" Inherits=".PracticeHistoryModify" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
      
</asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <section class="content-header">
      <h1>จัดการประวัติการฝึกปฏิบัติวิชาชีพฯ</h1>   
    </section>

<section class="content">  
      <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-search"></i>

              <h3 class="box-title">เพิ่ม/แก้ไข</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                                 
            </div>
            <div class="box-body"> 

                  <div class="row">
                        <div class="col-md-1">
          <div class="form-group">
            <label>ID</label>
               <asp:TextBox ID="txtUID" runat="server"  cssclass="form-control text-center" ReadOnly="true" BackColor="White"></asp:TextBox> 
          </div>

        </div>

           <div class="col-md-2">
          <div class="form-group">
            <label>ปีการศึกษา</label>
                <asp:DropDownList ID="ddlYear" runat="server" CssClass="form-control select2" AutoPostBack="True">
                                                        </asp:DropDownList>
          </div>

        </div>
              <div class="col-md-2">
          <div class="form-group">
            <label>รหัสนักศึกษา</label>
               <asp:TextBox ID="txtCode" runat="server"  cssclass="form-control text-center" ReadOnly="true" BackColor="White"></asp:TextBox> 
          </div>

        </div>         
    <div class="col-md-7">
          <div class="form-group">
            <label>ชื่อ-สกุล</label>
               <asp:TextBox ID="txtName" runat="server"  cssclass="form-control text-center" ReadOnly="true" BackColor="White"></asp:TextBox> 
          </div>

        </div>      </div>
                  <div class="row">
                        <div class="col-md-4">
          <div class="form-group">
            <label>แหล่งฝึก</label>
               <asp:DropDownList ID="ddlLocation" runat="server"  cssclass="form-control select2"  placeholder="เลือกแหล่งฝึก">
      </asp:DropDownList>
          </div>

        </div>
                    

      <div class="col-md-4">
          <div class="form-group">
            <label>งานที่ฝึก</label>
              <asp:DropDownList ID="ddlSkill" runat="server" cssclass="form-control select2"  placeholder="เลือกงานที่ฝึก">                                                      </asp:DropDownList>      
          </div>

        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label>ผลัดฝึก</label>
               <asp:DropDownList ID="ddlPhase" runat="server"  cssclass="form-control select2"  placeholder="เลือกผลัดฝึก">
      </asp:DropDownList>
          </div>

        </div>
          </div>
<div class="row">
       <div class="col-md-5">
          <div class="form-group">
            <label>รายวิชา</label>
              <asp:DropDownList ID="ddlSubject" runat="server" cssclass="form-control select2"  placeholder="ผูกรายวิชา">                                                      </asp:DropDownList>      
          </div>

        </div>
     <div class="col-md-2">
          <div class="form-group">
            <label>จำนวนชั่วโมงฝึก</label>
                <asp:TextBox ID="txtAccumulatedHours" runat="server"  cssclass="form-control text-center"  ></asp:TextBox>
          </div>

        </div>

</div>
                                    
</div>
            <div class="box-footer text-center clearfix">
            <asp:Button ID="cmdSave" runat="server"  Text="บันทึก" CssClass="btn btn-primary" Width="100px"/> 
               
          &nbsp;<asp:Button ID="cmdBack" runat="server"  Text="Back" CssClass="btn btn-default" Width="100px"/> 
            </div>
          </div>
    
    </section>
</asp:Content>
