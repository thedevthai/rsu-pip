﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="StudentPrintBio.aspx.vb" Inherits=".StudentPrintBio" %>
<%@ Register assembly="FUA" namespace="Subgurim.Controles" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
     <!-- bootstrap select -->  
    <%--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"> --%>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script> 
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   
     <section class="content-header">
      <h1><asp:Label ID="lblReportTitle" runat="server" Text="พิมพ์ประวัตินักศึกษา"></asp:Label></h1>   
    </section>

<section class="content">  

     <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-search"></i>

              <h3 class="box-title">ค้นหานักศึกษา</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                                 
            </div>
            <div class="box-body"> 

                  <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label>สาขาวิชา</label>
                   <asp:DropDownList ID="ddlMajor" runat="server" 
                AutoPostBack="True" CssClass="form-control select2">            </asp:DropDownList>  
                </div>
              </div>

                      <div class="col-md-3">
                <div class="form-group">
                  <label>อ.ที่ปรึกษา</label>
                   <asp:DropDownList cssclass="form-control select2"  title="--ไม่ระบุ--" ID="ddlAdvisor" runat="server"> </asp:DropDownList> 
                </div>
              </div>
                      <div class="col-md-2">
                <div class="form-group">
                  <label>ชั้นปีที่</label>
                    <asp:DropDownList ID="ddlLevel" runat="server" CssClass="form-control select2">
                 <asp:ListItem Selected="True" Value="0">--ไม่ระบุ--</asp:ListItem>
                 <asp:ListItem>1</asp:ListItem>
                 <asp:ListItem>2</asp:ListItem>
                 <asp:ListItem>3</asp:ListItem>
                 <asp:ListItem>4</asp:ListItem>
                 <asp:ListItem>5</asp:ListItem>
                 <asp:ListItem>6</asp:ListItem>
             </asp:DropDownList>
                </div>
              </div>

                      <div class="col-md-2">
                <div class="form-group">
                  <label>ค้นหา</label>
                   <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control" placeholder="รหัสนักศึกษา , ชื่อ-สกุล" ></asp:TextBox>
                </div>
              </div>
                      <div class="col-md-2">
                <div class="form-group">
                  <label></label>
                    <br />
                   <asp:Button ID="cmdFind" runat="server" CssClass="btn btn-find" Width="100px" Text="ค้นหา"></asp:Button>
                </div>
              </div>

                    </div>                              
</div>
            <div class="box-footer clearfix">
           <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="0">
             <ProgressTemplate>
<img alt="" src="images/progress_bar.gif" height="25" />             </ProgressTemplate>
         </asp:UpdateProgress>  
            </div>
          </div>
     <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-list"></i>

              <h3 class="box-title">รายชื่อนักศึกษา</h3> <small>&nbsp; <asp:Label ID="lblStudentCount" runat="server"></asp:Label> </small>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                                 
            </div>
            <div class="box-body mailbox-messages"> 
<asp:GridView ID="grdData"  runat="server" CellPadding="0" 
                                                        GridLines="None" 
             AllowPaging="True" CssClass="table table-hover" 
                             Font-Bold="False" Width="100%" AutoGenerateColumns="False" DataKeyNames="Student_ID" PageSize="20">
                        <RowStyle BackColor="#F7F7F7" />
                        <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />                     
                        <pagerstyle HorizontalAlign="Center" 
                             CssClass="dc_pagination dc_paginationC dc_paginationC11" />                     
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <headerstyle CssClass="th" Font-Bold="True" 
                                                            HorizontalAlign="Center" />                     
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkSelect" runat="server" CssClass="mailbox-messages" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="Student_Code" HeaderText="รหัสนักศึกษา" >
                            <ItemStyle Width="90px" HorizontalAlign="Center" />                            </asp:BoundField>
                            <asp:BoundField DataField="FirstName" HeaderText="ชื่อนักศึกษา" >
                            <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="LastName" HeaderText="นามสกุล" />
                            <asp:BoundField DataField="MajorName" HeaderText="สาขาวิชา" />
                           
                            <asp:BoundField DataField="ClassName" HeaderText="ชั้นปีที่">
                            <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField>
                            <ItemStyle HorizontalAlign="Center" Width="30px" />
                            </asp:BoundField>
                        </Columns>
                        <EditRowStyle BackColor="#2461BF" />
                        <AlternatingRowStyle BackColor="White" />
                     </asp:GridView>    
</div>
            <div class="box-footer clearfix text-center">
           <asp:LinkButton ID="lnkPrintAll" runat="server" CssClass="btn btn-find">พิมพ์จากผลการค้นหาทั้งหมด</asp:LinkButton>
&nbsp;<asp:LinkButton ID="lnkPrintSelect" runat="server" CssClass="btn btn-find">พิมพ์ที่เลือกทั้งหมด</asp:LinkButton> 
            </div>
          </div> 

    </section>
</asp:Content>
