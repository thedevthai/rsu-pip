﻿
Public Class AssessmentModify
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlAss As New AssessmentController
    Dim ctlSk As New SkillController
    Dim acc As New UserController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("UserLogin").Value) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If
        If Not IsPostBack Then
            LoadAssessment(Request("id"))
        End If
    End Sub

    Private Sub LoadLocationToDDL()
        ddlLocation.Items.Clear()
        Dim ctlL As New LocationController
        Dim dtL As New DataTable
        dtL = ctlL.LocationAssessment_Get(StrNull2Zero(txtYear.Text))

        If dtL.Rows.Count > 0 Then
            ddlLocation.Items.Clear()
            With ddlLocation
                .Items.Add("")
                .Items(0).Value = 0

                For i = 0 To dtL.Rows.Count - 1
                    .Items.Add("" & dtL.Rows(i)("LocationName"))
                    .Items(i + 1).Value = dtL.Rows(i)("LocationID")
                Next
            End With
        End If

    End Sub
    Private Sub LoadSkillToDDL()
        dt = ctlSk.Skill_GetByLocation(StrNull2Zero(txtYear.Text), ddlLocation.SelectedValue)
        If dt.Rows.Count > 0 Then
            With ddlSkill
                .Enabled = True
                .DataSource = dt
                .DataTextField = "SkillName"
                .DataValueField = "SkillUID"
                .DataBind()
                .SelectedIndex = 0
            End With
        End If
        dt = Nothing
    End Sub
    Private Sub LoadSkillPhase()
        ddlPhase.Items.Clear()
        Dim ctlTP As New REQcontroller
        Dim dtP As New DataTable
        Dim k As Integer
        k = 0
        dtP = ctlTP.Requirements_GetByLocationSkill(StrNull2Zero(txtYear.Text), StrNull2Zero(ddlLocation.SelectedValue), StrNull2Zero(ddlSkill.SelectedValue))
        'dtP = ctlTP.TurnPhase_GetActiveByYear(StrNull2Zero(ddlYear.SelectedValue))
        If dtP.Rows.Count > 0 Then
            For i = 0 To dtP.Rows.Count - 1
                If (DBNull2Zero(dtP.Rows(i)("NoSpec")) + DBNull2Zero(dtP.Rows(i)("Man")) + DBNull2Zero(dtP.Rows(i)("Women"))) > 0 Then
                    ddlPhase.Items.Add(dtP.Rows(i)("PhaseNo") & " : " & dtP.Rows(i)("PhaseName") & "  (ช=" & DBNull2Zero(dtP.Rows(i)("Man")) & "|ญ=" & DBNull2Zero(dtP.Rows(i)("Women")) & "|ม=" & DBNull2Zero(dtP.Rows(i)("NoSpec")) & ")")
                    ddlPhase.Items(k).Value = dtP.Rows(i)("SkillPhaseID")
                    k = k + 1
                End If
            Next
        Else
            ddlPhase.DataSource = dtP
            ddlPhase.DataBind()
        End If
        dtP = Nothing
    End Sub
    Private Sub LoadAssessment(AssessmentID As Integer)
        Dim dtAsm As New DataTable
        dtAsm = ctlAss.Assessment_GetByUID(AssessmentID)
        If dtAsm.Rows.Count > 0 Then
            With dtAsm.Rows(0)
                txtYear.Text = .Item("PYear")
                LoadLocationToDDL()
                ddlLocation.SelectedValue = .Item("LocationID")
                LoadSkillToDDL()
                ddlSkill.SelectedValue = .Item("SkillUID")
                LoadSkillPhase()
                ddlPhase.SelectedValue = .Item("SkillPhaseID")

                txtUID.Text = .Item("AssessmentID")
                txtCode.Text = .Item("Student_Code")
                txtName.Text = .Item("StudentName")

            End With
        End If
        dtAsm = Nothing
    End Sub
    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click

        ctlAss.Assessment_Update(txtUID.Text, txtYear.Text, txtCode.Text, ddlLocation.SelectedValue, ddlPhase.SelectedValue, ddlSkill.SelectedValue, Request.Cookies("UserLoginID").Value)

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)
    End Sub

    Protected Sub cmdBack_Click(sender As Object, e As EventArgs) Handles cmdBack.Click

        Response.Redirect("ReportAssesmentByStudent?ActionType=asg&ItemType=find")
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocation.SelectedIndexChanged
        LoadSkillToDDL()
        LoadSkillPhase()
    End Sub
End Class

