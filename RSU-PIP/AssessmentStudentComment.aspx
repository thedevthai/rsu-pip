﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="AssessmentStudentComment.aspx.vb" Inherits=".AssessmentStudentComment" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    
    
</asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <section class="content-header">
      <h1>ประเมินจุดเด่น/จุดด้อย นักศึกษา (ไม่มีคะแนน)</h1>   
    </section>

<section class="content">  
<div class="row">  
    <section class="col-lg-12 connectedSortable">
     <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-user-circle"></i>

              <h3 class="box-title">ข้อมูลนักศึกษา</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                                 
            </div>
            <div class="box-body"> 
                <table width="100%" border="0" cellPadding="1" cellSpacing="1" >
<tr>
                                                    <td width="80" class="texttopic">
                                                        รหัสนักศึกษา</td>
                                                    <td>
                                                        <asp:Label ID="lblStudentCode" runat="server"></asp:Label>
                                                    </td>                                                  
                                                    <td width="80" class="texttopic">ชื่อ - นามสกุล</td>
                                                    <td>
                                                        <asp:Label ID="lblStudentName" runat="server"></asp:Label>
                                                    </td>
</tr>
<tr>
  <td class="texttopic">ชื่อเล่น</td>
  <td><asp:Label ID="lblNickName" runat="server"></asp:Label></td>
  <td class="texttopic">สาขา</td>
  <td><asp:Label ID="lblMajorName" runat="server"></asp:Label></td>
</tr>
<tr>
  <td class="texttopic">ปีการศึกษา </td>
  <td>
                                                        <asp:Label ID="lblYear" runat="server"></asp:Label>
                                                    </td>
 

     <td class="texttopic">ผลัดที่ </td>
                                                    <td>
                                                        <asp:Label ID="lblPhaseNo" runat="server"></asp:Label>
                                                    </td>

</tr>

<tr>
  <td class="texttopic">ผู้ประเมิน</td>
  <td>
                                                        
                                                        <asp:Label ID="lblAssessorName" runat="server"></asp:Label>
                                                    </td>
      <td class="texttopic">&nbsp;</td>
                                                    <td>
                                                        
                                                        &nbsp;</td>
</tr>
</table>  
                         
</div>
            <div class="box-footer clearfix">
           
            </div>
          </div>
 </section>
        </div>
<div class="row">
 <section class="col-lg-5 connectedSortable">
    <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-thumbs-up"></i>

              <h3 class="box-title">ประเมินจุดเด่น</h3>
            </div>
            <div class="box-body mailbox-messages">       
                       <asp:CheckBoxList ID="chkPros" runat="server" CssClass="mailbox-messages">
                          </asp:CheckBoxList>
</div>
            <div class="box-footer text-center">
            </div>
          </div>
</section>
 <section class="col-lg-5 connectedSortable">
    <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-thumbs-down"></i>

              <h3 class="box-title">ประเมินจุดจุดด้อย</h3>
            </div>
            <div class="box-body mailbox-messages">       
                        <asp:CheckBoxList ID="chkCons" runat="server" CssClass="mailbox-messages">
                          </asp:CheckBoxList>
</div>
            <div class="box-footer text-center">
            </div>
          </div>
</section>
    </div>
<div class="row">
    <section class="col-lg-12 connectedSortable">
    <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-star"></i>

              <h3 class="box-title">ท่านมีความมั่นใจที่จะจ้างนักศึกษาท่านนี้มาเป็นเภสัชประจำร้าน 0-100%?</h3>
            </div>
            <div class="box-body">  
                <table width="100%">
                    <tr>
                        <td  width="70"> ระบุ 0-100 </td>
                        <td>
              <asp:TextBox ID="txtSure" runat="server" Width="50px"></asp:TextBox>
                                    </td>
                    </tr>
                    <tr>
                        <td colspan="2">ระบุเหตุผล</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                                    <asp:TextBox ID="txtReason" runat="server" Height="100px" TextMode="MultiLine" Width="100%" BackColor="#FFE1E1" CssClass="Objcontrol"></asp:TextBox>
                       
                        </td>
                    </tr>
                </table>
                       
</div>
           
          </div>
    
  <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-commenting-o"></i>

              <h3 class="box-title">คำติชมหรือข้อที่ต้องปรับปรุง อื่นๆ</h3>
            </div>
            <div class="box-body">       
                       
                                    <asp:TextBox ID="txtComment" runat="server" Height="100px" TextMode="MultiLine" Width="100%" BackColor="#FFFFC1" CssClass="Objcontrol"></asp:TextBox>
                       
</div>
           
          </div>
    <div align="center"> <asp:Button ID="cmdSave" runat="server" Text="บันทึก" CssClass="btn btn-save" Width="100px" /> 
                <asp:Button ID="cmdBack" runat="server" Text="<< กลับหน้ารายชื่อนักศึกษา" CssClass="btn btn-find" />

               
        


     </div>
        <br />
  <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-info"></i>

              <h3 class="box-title">อภิปราย</h3>
            </div>
            <div class="box-body">    
                1. แบบประเมินนี้เป็นเพียงกรอบคร่าวๆ ของการประเมินนักศึกษาเบื่องต้น เพื่อแนะนำนักศึกษาถึงข้อผิดพลาดในประเด็นต่างๆ ของแต่ละสัปดาห์ เพื่อให้นักศึกษามีการพัฒนาการฝึกงานร้านยาได้อย่างมีประสิทธิภาพ 
                <br />
                2. คะแนนของแบบประเมินนี้ไม่มีผลต่อแบบประเมินนักศึกษา
                <br />
                3. เภสัชกรแหล่งฝึกสามารถประเมินนนักศึกษารายบุคล โดยจะคุยเป็นรายบุคคลหรือคุยร่วมกับนักศึกษาอื่นๆ ก็ได้ ขึ้นอยู่กับ ดุลยพินิจของแต่ละท่านตามความเหมาะสม
ข้อสังเกต: หากเป็นเรื่องบุคลิกภาพ หรือเรื่องที่คิดว่ากระทบต่อจิตใจนักศึกษา เภสัชกรแหล่งฝึกควรคุยเป็นรายบุคคล
                <br />
                4. ระวังเรื่อง การพูด negative ต่อนักศึกษา
                <br />
                5. เภสัชกรแหล่งฝึกสามารถประเมินนักศึกษาทุกสัปดาห์ หรือตามดุลยพินิจและความเหมาะสมแต่ละร้าน

    </div>
          
          </div>
</section>
</div>
</section>    
</asp:Content>
