﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="StudentLocation.aspx.vb" Inherits=".StudentLocation" %>

<%@ Import Namespace="System.Data" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  
<section class="content-header">
      <h1>เลือกแหล่งฝึก</h1>   
    </section>

<section class="content">  

     <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-filter"></i>

              <h3 class="box-title">ค้นหา</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                                 
            </div>
            <div class="box-body"> 
                  <div class="row">
           <div class="col-md-2">
          <div class="form-group">
            <label>ปีการศึกษา</label>
             <asp:DropDownList ID="ddlYear" runat="server" AutoPostBack="True" CssClass="form-control select2"></asp:DropDownList>  
          </div>

        </div>

        <div class="col-md-4">
          <div class="form-group">
            <label>รายวิชา</label>
  <asp:DropDownList ID="ddlCourse" runat="server" AutoPostBack="True" CssClass="form-control select2"></asp:DropDownList>  
     
          </div>

        </div>
      
        <div class="col-md-4">
          <div class="form-group">
            <label>งานที่ต้องการฝึก</label>
  <asp:DropDownList ID="ddlSkill" runat="server" AutoPostBack="True" CssClass="form-control select2"></asp:DropDownList>  
     
          </div>

        </div>
                      <div class="col-md-2"><br />
  <asp:Button ID="cmdFindStd" runat="server" CssClass="btn btn-find" Width="100px" Text="ค้นหา"> </asp:Button>
                           </div>
      </div>              
</div>           
          </div>
  

    <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-list"></i>

              <h3 class="box-title">รายชื่อแหล่งฝึกในรายวิชานี้ทั้งหมด&nbsp; 
                <asp:Label ID="lblCount" runat="server"></asp:Label>
           &nbsp;แหล่ง</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                                 
            </div>
            <div class="box-body">
                
                  <table id="tbdata" class="table table-bordered table-striped">
                <thead>
                <tr>
                 <th class="sorting_asc_disabled"></th>    
                    <th>ชื่อแหล่งฝึก</th>
                  <th>จังหวัด</th>
                  <th>ประเภท</th> 
                </tr>
                </thead>
                <tbody>
            <% For Each row As DataRow In dtLoc.Rows %>
                <tr>
                 <td width="30px" class="text-center"><a class="editemp" href="LocationDetail.aspx?ActionType=loc&ItemType=l&y=<% =ddlYear.SelectedValue %>&lid=<% =String.Concat(row("LocationID")) %>&cid=<% =ddlCourse.SelectedValue %>&sk=<% =ddlSkill.SelectedValue %>"><img src="images/view.png"/></a>
                    </td>
                  <td><% =String.Concat(row("LocationName")) %></td>
                  <td class="text-center"><% =String.Concat(row("ProvinceName")) %>    </td>
                  <td class="text-center"><% =String.Concat(row("LocationGroupName")) %></td>                  
                   
                </tr>
            <%  Next %>
                </tbody>               
              </table>              



              <asp:Label ID="lblNo" runat="server" CssClass="alert alert-error show"  
                  
                  Text="ยังไม่พบแหล่งฝึกที่กำหนดให้ในวิชานี้ "></asp:Label>               
</div>
            <div class="box-footer">
           
            </div>
          </div>
                       
    </section> 
             
            
  
    
</asp:Content>
