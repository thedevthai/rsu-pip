﻿
Public Class ReportCondition
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlCs As New CourseController
    Dim ctlLtg As New LocationGroupController


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("UserLogin").Value) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If
        If Not IsPostBack Then
            lblResult.Visible = False
            LoadYearToDDL()
            LoadGroupToDDL()
            LoadMajorToDDL()
            LoadLocationToDDL()

            Select Case Request("m")
                Case "docstdlist"
                    lblTitle.Text = "ใบแจ้งผลัดฝึก"
                Case "etm"
                    lblTitle.Text = "รายงานการรับนักศึกษาแต่ละแหล่งฝึก"
                Case Else
                    lblTitle.Text = "รายงานนักศึกษาแต่ละแหล่งฝึก"
            End Select


            'Thread.Sleep(500)
        End If
    End Sub
    Private Sub LoadGroupToDDL()
        dt = ctlLtg.LocationGroup_Get
        If dt.Rows.Count > 0 Then
            ddlType.Items.Clear()
            ddlType.Items.Add("---ทั้งหมด---")
            ddlType.Items(0).Value = 0
            For i = 1 To dt.Rows.Count
                With ddlType
                    .Items.Add("" & dt.Rows(i - 1)("Name"))
                    .Items(i).Value = dt.Rows(i - 1)("Code")
                End With
            Next

        End If
        dt = Nothing
    End Sub

    Private Sub LoadYearToDDL()
        Dim y As Integer = StrNull2Zero(DisplayYear(ctlCs.GET_DATE_SERVER))
        Dim LastRow As Integer
        dt = ctlCs.Courses_GetYear
        LastRow = dt.Rows.Count - 1

        If dt.Rows.Count > 0 Then
            With ddlYear
                .Enabled = True
                .DataSource = dt
                .DataTextField = "CYear"
                .DataValueField = "CYear"
                .DataBind()

                If dt.Rows(LastRow)(0) = y Then
                    ddlYear.Items.Add(y + 1)
                    ddlYear.Items(LastRow + 1).Value = y + 1
                ElseIf dt.Rows(LastRow)(0) > y Then
                    'ddlYear.Items.Add(y + 2)
                    'ddlYear.Items(LastRow + 1).Value = y + 2
                ElseIf dt.Rows(LastRow)(0) < y Then
                    ddlYear.Items.Add(y)
                    ddlYear.Items(LastRow + 1).Value = y
                    ddlYear.Items.Add(y + 1)
                    ddlYear.Items(LastRow + 2).Value = y + 1
                End If
                .SelectedIndex = 0
            End With
        Else
            ddlYear.Items.Add(y)
            ddlYear.Items(0).Value = y
            ddlYear.Items.Add(y + 1)
            ddlYear.Items(1).Value = y + 1
            ddlYear.SelectedIndex = 0
        End If
        ddlYear.SelectedValue = Request.Cookies("EDUYEAR").Value
        dt = Nothing
    End Sub
    Private Sub LoadMajorToDDL()
        Dim dtMajor As New DataTable
        Dim ctlFct As New FacultyController
        dtMajor = ctlFct.Major_GetForSearch
        If dtMajor.Rows.Count > 0 Then
            With ddlMajor
                .Enabled = True
                .DataSource = dtMajor
                .DataTextField = "MajorName"
                .DataValueField = "MajorID"
                .DataBind()
                .SelectedIndex = 0
            End With
        Else

        End If
        dtMajor = Nothing
    End Sub
    Private Sub LoadLocationToDDL()
        Dim ctlL As New LocationController
        If ddlType.SelectedValue = 0 Then

            dt = ctlL.Location_GetActive()
        Else
            dt = ctlL.Location_GetByGroupID(ddlType.SelectedValue)
        End If

        ddlLocation.Items.Clear()
        With ddlLocation
            '.DataSource = dt
            '.DataTextField = "LocationName"
            '.DataValueField = "LocationID"
            '.DataBind()

            .Items.Add("---ทั้งหมด---")
            .Items(0).Value = 0
            If dt.Rows.Count > 0 Then
                .Visible = True
                For i = 0 To dt.Rows.Count - 1
                    .Items.Add(dt.Rows(i)("LocationName"))
                    .Items(i + 1).Value = dt.Rows(i)("LocationID")
                Next
                .SelectedIndex = 0
            End If
        End With
    End Sub

    Private Sub GenDataTable()
        Dim dtReq As New DataTable
        Dim dtAcc As New DataTable

        Dim ctlREQ As New REQcontroller
        Dim sRemark As String = ""

        ctlREQ.tm_StudentRegister_Summary_Delete()
        dtReq = ctlREQ.Requirements_GetLocation(ddlYear.SelectedValue, ddlType.SelectedValue)
        If dtReq.Rows.Count > 0 Then
            For i = 0 To dtReq.Rows.Count - 1
                With dtReq.Rows(i)
                    If .Item("isSameGender") = "Y" Then
                        sRemark = "เพศเดียวกัน"
                    Else
                        sRemark = ""
                    End If
                    ctlREQ.Tmp4ReportLocation_Add(.Item("REQYEAR"), .Item("LocationID"), .Item("LocationName"), .Item("LocationGroupID"), sRemark)

                    dtAcc = ctlREQ.Requirements_Get4Report(.Item("REQYEAR"), .Item("LocationID"))
                End With

                For n = 0 To dtAcc.Rows.Count - 1
                    With dtAcc.Rows(n)
                        ctlREQ.Tmp4ReportLocation_Update(dtReq.Rows(i).Item("REQYEAR"), .Item("TimePhaseID"), dtReq.Rows(i).Item("LocationID"), .Item("LocationGroupName"), .Item("TimeDesc"), .Item("Man"), .Item("Women"), .Item("NoSpec"))
                    End With
                Next
            Next
        End If

    End Sub

    Protected Sub cmdView_Click(sender As Object, e As EventArgs) Handles cmdView.Click

        Select Case Request("m")
            Case "docstdlist"
                ReportName = "docStudentList"
                FagRPT = "DOCSTDLIST"
                Reportskey = "PDF"
                Response.Redirect("ReportServerViewer.aspx?y=" & ddlYear.SelectedValue & "&lv=" & ddlLevelClass.SelectedValue & "&mj=" & ddlMajor.SelectedValue & "&lg=" & ddlType.SelectedValue & "&l=" & ddlLocation.SelectedValue)
        End Select
    End Sub

    Protected Sub ddlType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlType.SelectedIndexChanged
        LoadLocationToDDL()
    End Sub
End Class

