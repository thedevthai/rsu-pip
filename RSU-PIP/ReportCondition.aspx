﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="ReportCondition.aspx.vb" Inherits=".ReportCondition" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    
</asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  
    
<section class="content-header">
  <h1 class="box-title"><asp:Label ID="lblTitle" runat="server" Text=""></asp:Label></h1>  
    </section>

<section class="content">  
       <div class="row"> 
        <section class="col-lg-2 connectedSortable"></section>
          <section class="col-lg-8 connectedSortable">         
         <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-filter"></i>
              <h3 class="box-title">เลือกเงื่อนไขรายงาน</h3>                                               
            </div>
            <div class="box-body"> 
                 <div class="row">
                <div class="col-md-2">
          <div class="form-group">
            <label>ปีการศึกษา</label>
               <asp:DropDownList ID="ddlYear" runat="server" CssClass="form-control select2"></asp:DropDownList>   
          </div>
        </div>                     
     <div class="col-md-2">
          <div class="form-group">
            <label>ชั้นปี</label>
               <asp:DropDownList ID="ddlLevelClass" runat="server" CssClass="form-control select2">
                   <asp:ListItem Selected="True" Value="0">ทั้งหมด</asp:ListItem>
                   <asp:ListItem Value="4">ปี 4</asp:ListItem>
                   <asp:ListItem Value="6">ปี 6</asp:ListItem>
              </asp:DropDownList>   
          </div>
        </div>  
  <div class="col-md-8">
          <div class="form-group">
            <label>สาขา</label>
               <asp:DropDownList ID="ddlMajor" runat="server" CssClass="form-control select2"></asp:DropDownList>   
          </div>
        </div>  
                 </div>
                         <div class="row">
                      <div class="col-md-4">
          <div class="form-group">
            <label>ประเภทแหล่งฝึก</label>
               <asp:DropDownList ID="ddlType" runat="server" CssClass="form-control select2" AutoPostBack="True"></asp:DropDownList>   
          </div>
        </div>  
                      <div class="col-md-8">
          <div class="form-group">
            <label>แหล่งฝึก</label>
               <asp:DropDownList ID="ddlLocation" runat="server" CssClass="form-control select2"></asp:DropDownList>   
          </div>
        </div>  
                  </div>
                         <div class="row">
                     <div class="col-md-12 text-center">
          <div class="form-group">
              <asp:Button ID="cmdView" runat="server" CssClass="btn btn-find" Text="ดูรายงาน" Width="120px" />
          </div>
        </div>  

</div>
           
</div>
            <div class="box-footer clearfix">
             <asp:Label ID="lblResult" runat="server" CssClass="alert alert-error show" Width="95%"></asp:Label>
            </div>
          </div>
      </section>
          <section class="col-lg-2 connectedSortable"></section>
    </div>
     </section>
</asp:Content>
