﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Skill.aspx.vb" Inherits=".Skill" %>
<%@ Register Assembly="DevExpress.Web.v17.2, Version=17.2.13.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">     
</asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
      <section class="content-header">
      <h1>จัดการงานที่ฝึก</h1>   
    </section>

<section class="content">  

       <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">  

     <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-plus-circle"></i>

              <h3 class="box-title">เพิ่ม/แก้ไข งานที่ฝึก</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                                 
            </div>
            <div class="box-body"> 
                
                      <div class="row">
            <div class="col-md-1">
          <div class="form-group">
            <label>รหัส : <asp:Label ID="lblID" runat="server"></asp:Label></label>
                <asp:TextBox ID="txtCode" runat="server"  CssClass="form-control text-center"></asp:TextBox>   
          </div>
        </div>

                 <div class="col-md-4">
          <div class="form-group">
            <label> ชื่อภาษาไทย</label>              
              <asp:TextBox ID="txtNameTH" runat="server"         Width="100%" ></asp:TextBox>
          </div>

        </div> 

                          <div class="col-md-4">
          <div class="form-group">
            <label> ชื่อภาษาอังกฤษ</label>              
                  <asp:TextBox ID="txtNameEN" runat="server" Width="100%"></asp:TextBox>   
          </div>

        </div>                          

            <div class="col-md-2">
          <div class="form-group">  
              <br /><br />
            <label>Status</label>              
                   <asp:CheckBox ID="chkStatus" runat="server" Text="Active" 
                                                            Checked="True" />
          </div>

        </div> 
          
    </div>
                                                                        
</div>
            <div class="box-footer clearfix text-center">       
              
           
            </div>
          </div>
            </section>
           </div>

<div align="center">
  <asp:Button runat="server" Text="บันทึก" CssClass="btn btn-save" Width="100px" ID="cmdSave"></asp:Button>
                <asp:Button runat="server" Text="ยกเลิก" CssClass="btn btn-save" Width="100px" ID="cmdClear"></asp:Button>
</div>
    <br />
  <div class="row"> 
        <section class="col-lg-12 connectedSortable">  

    <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-book"></i>

              <h3 class="box-title">งานที่ฝึก</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                                 
            </div>
            <div class="box-body"> 
       
    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
       
        <tr>
          <td  align="left" valign="top">
              
              <table border="0" cellspacing="1" cellpadding="0">
            <tr>
              <td width="50" >ค้นหา</td>
              <td >
                  <asp:TextBox ID="txtSearch" runat="server" Width="300px"></asp:TextBox>                </td>
              <td >
                  <asp:Button ID="cmdFind" runat="server" CssClass="btn btn-find" Text="ค้นหา" />                  
                  <asp:Button ID="cmdAll" runat="server" CssClass="btn btn-find" Text="ดูทั้งหมด" />                </td>
          
                  <td colspan="3" class="text12_nblue"><strong>&nbsp;*</strong>คำค้นหาสามารถค้นหาได้จาก ชื่อ หรือ รหัสงานที่ฝึก</td>
              </tr>
          </table>

          </td>
      </tr>    
        <tr>
          <td align="center" valign="top">
              <asp:GridView ID="grdData" 
                             runat="server" CellPadding="0" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" Width="100%" CssClass="table table-hover">
            <RowStyle BackColor="#F7F7F7" HorizontalAlign="Center" />
            <columns>
            <asp:BoundField HeaderText="No." DataField="nRow">
              <itemstyle HorizontalAlign="Center" VerticalAlign="Top" Width="30px" />                      </asp:BoundField>
            <asp:BoundField DataField="UID" HeaderText="รหัส">
              <headerstyle HorizontalAlign="Center" />          
              <itemstyle HorizontalAlign="Center" VerticalAlign="Top" Width="50px" />                      </asp:BoundField>
                <asp:BoundField DataField="Name" HeaderText="ชื่องานที่ฝึก">
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>

                
<asp:BoundField DataField="AliasName" HeaderText="Alias Name">
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
            <asp:TemplateField HeaderText="Active">
              <itemtemplate>
                <asp:Image ID="imgStatus" runat="server" ImageUrl="images/icon-ok.png" 
                                    Visible='<%# ConvertYN2Boolean(DataBinder.Eval(Container.DataItem, "StatusFlag")) %>' />                        </itemtemplate>
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />          
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Edit">
              <itemtemplate>
                <asp:ImageButton ID="imgEdit" runat="server" ImageUrl="images/icon-edit.png" 
                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID") %>' />                        </itemtemplate>
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="40px" />          
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Delete">
              <itemtemplate>
                <asp:ImageButton ID="imgDel" runat="server" 
                                    ImageUrl="images/delete.png" 
                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID") %>' />                        </itemtemplate>
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="40px" />          
            </asp:TemplateField>
            </columns>
            <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />          
            <pagerstyle CssClass="dc_pagination dc_paginationC dc_paginationC11" HorizontalAlign="Center" />          
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <headerstyle CssClass="th" Font-Bold="True" HorizontalAlign="Center" 
                      VerticalAlign="Middle" />          
            <EditRowStyle BackColor="#2461BF" />
            <AlternatingRowStyle BackColor="White" />
          </asp:GridView></td>
      </tr>
       
    </table>
                                                
</div> 
        <div class="box-footer clearfix">           
                
            </div>
          </div> 
 </section>
      </div>
    </section> 
</asp:Content>
