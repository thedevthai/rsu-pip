﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports Subgurim.Controles


Public Class Teacher_Reg
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlDept As New DepartmentController
    Dim ctlFct As New FacultyController
    Dim ctlbase As New ApplicationBaseClass

    Dim ctlTch As New PersonController
    Dim objTch As New PersonInfo


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Not IsPostBack Then
            LoadPrefixToDDL()
            LoadDepartmentToDDL()
            LoadPositionToDDL()
            LoadProvinceToDDL()
            LoadTeacherData()

        End If

        If (FileUploaderAJAX1.IsPosting) Then
            UploadFile()
        End If

    End Sub

    Private Sub LoadTeacherData()
        dt = ctlTch.Person_GetByID(Request.Cookies("ProfileID").Value)

        If dt.Rows.Count > 0 Then
            With dt.Rows(0)

                lblStdCode.Text = .Item(objTch.tblField(objTch.fldPos.f00_PersonID).fldName)

                If Not IsDBNull(.Item(objTch.tblField(objTch.fldPos.f01_Prefix).fldName)) Then
                    ddlPrefix.SelectedValue = .Item(objTch.tblField(objTch.fldPos.f01_Prefix).fldName)
                End If

                txtFirstName.Text = DBNull2Str(.Item(objTch.tblField(objTch.fldPos.f02_FirstName).fldName))
                txtLastName.Text = DBNull2Str(.Item(objTch.tblField(objTch.fldPos.f03_LastName).fldName))
                txtNickName.Text = DBNull2Str(.Item(objTch.tblField(objTch.fldPos.f10_NickName).fldName))

                If Not IsDBNull(.Item(objTch.tblField(objTch.fldPos.f06_Gender).fldName)) Then
                    optGender.SelectedValue = .Item(objTch.tblField(objTch.fldPos.f06_Gender).fldName)
                End If

                txtEmail.Text = DBNull2Str(.Item(objTch.tblField(objTch.fldPos.f07_Email).fldName))
                txtTelephone.Text = DBNull2Str(.Item(objTch.tblField(objTch.fldPos.f08_Telephone).fldName))
                txtMobile.Text = DBNull2Str(.Item(objTch.tblField(objTch.fldPos.f09_MobilePhone).fldName))

                ddlPosition.SelectedValue = .Item("PositionID")
                ddlDepartment.SelectedValue = .Item("DepartmentUID")

                txtAddress.Text = DBNull2Str(.Item(objTch.tblField(objTch.fldPos.f11_Address).fldName))
                txtDistrict.Text = DBNull2Str(.Item(objTch.tblField(objTch.fldPos.f12_District).fldName))
                txtCity.Text = DBNull2Str(.Item(objTch.tblField(objTch.fldPos.f13_City).fldName))

                If Not IsDBNull(.Item(objTch.tblField(objTch.fldPos.f14_ProvinceID).fldName)) Then
                    ddlProvince.SelectedValue = .Item(objTch.tblField(objTch.fldPos.f14_ProvinceID).fldName)
                End If

                txtZipCode.Text = DBNull2Str(.Item(objTch.tblField(objTch.fldPos.f16_ZipCode).fldName))

                If DBNull2Str(.Item(objTch.tblField(objTch.fldPos.f17_PicturePath).fldName)) <> "" Then
                    picStudent.ImageUrl = "~/" & stdPic & "/" & .Item(objTch.tblField(objTch.fldPos.f17_PicturePath).fldName)
                End If


            End With

        End If


    End Sub
    
    Private Sub LoadProvinceToDDL()
        dt = ctlbase.Province_GetActive
        If dt.Rows.Count > 0 Then
            With ddlProvince
                .Enabled = True
                .DataSource = dt
                .DataTextField = "ProvinceName"
                .DataValueField = "ProvinceID"
                .DataBind()
                '.SelectedIndex = 0
            End With
        End If
        dt = Nothing
    End Sub

    Private Sub LoadPrefixToDDL()
        dt = ctlbase.Prefix_GetForTeacher
        If dt.Rows.Count > 0 Then
            With ddlPrefix
                .Enabled = True
                .DataSource = dt
                .DataTextField = "PrefixName"
                .DataValueField = "PrefixID"
                .DataBind()
                '.SelectedIndex = 0
            End With
        End If
        dt = Nothing
    End Sub
    Private Sub LoadPositionToDDL()
        dt = ctlbase.Position_GetActive
        If dt.Rows.Count > 0 Then
            With ddlPosition
                .Enabled = True
                .DataSource = dt
                .DataTextField = "PositionName"
                .DataValueField = "PositionID"
                .DataBind()
                '.SelectedIndex = 0
            End With
        End If
        dt = Nothing
    End Sub

    Private Sub LoadDepartmentToDDL()
        dt = ctlDept.Department_GetActive
        If dt.Rows.Count > 0 Then
            With ddlDepartment
                .Enabled = True
                .DataSource = dt
                .DataTextField = "DepartmentName"
                .DataValueField = "DepartmentUID"
                .DataBind()
                '.SelectedIndex = 0
            End With
        End If
        dt = Nothing
    End Sub

    Protected Sub cmdClear_Click(sender As Object, e As EventArgs) Handles cmdClear.Click
        lblStdCode.Text = ""
        ddlPrefix.SelectedIndex = 0
        txtFirstName.Text = ""
        txtLastName.Text = ""
        txtNickName.Text = ""
        optGender.SelectedIndex = 0
        txtNickName.Text = ""
        txtEmail.Text = ""
        txtTelephone.Text = ""
        txtMobile.Text = ""

        txtAddress.Text = ""
        txtDistrict.Text = ""
        txtCity.Text = ""
        ddlProvince.SelectedIndex = 0
        txtZipCode.Text = ""


    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click


        ctlTch.Person_Update(lblStdCode.Text, ddlPrefix.SelectedValue, txtFirstName.Text, txtLastName.Text, txtNickName.Text, optGender.SelectedValue, StrNull2Zero(ddlPosition.SelectedValue), ddlPosition.SelectedItem.Text, StrNull2Zero(ddlDepartment.SelectedValue), ddlDepartment.SelectedItem.Text, txtEmail.Text, txtTelephone.Text, txtMobile.Text, txtAddress.Text, txtDistrict.Text, txtCity.Text, StrNull2Zero(ddlProvince.SelectedValue), ddlProvince.SelectedItem.Text, txtZipCode.Text, Request.Cookies("UserLogin").Value & ".jpg", Request.Cookies("UserLogin").Value)


        Dim objuser As New UserController
        If txtEmail.Text <> "" Then
            objuser.User_UpdateMail(Request.Cookies("UserLogin").Value, txtEmail.Text)
        End If

        objuser.User_GenLogfile(Request.Cookies("UserLogin").Value, ACTTYPE_UPD, "Teacher", "บันทึก/แก้ไข ประวัติอาจารย์ :" & lblStdCode.Text, "")

        DisplayMessage(Me.Page, "บันทึกข้อมูลเรียบร้อย")
    End Sub

    Private Sub UploadFile()

        Dim pf As HttpPostedFileAJAX = FileUploaderAJAX1.PostedFile


        'กรณีต้องการต้องสอบชนิดและขนาดไฟล์
        If ((pf.ContentType.Equals("image/jpeg") Or pf.ContentType.Equals("image/jpg")) And pf.ContentLength <= 500 * 1024) Then

            FileUploaderAJAX1.SaveAs("~/" & stdPic, Request.Cookies("UserLogin").Value & ".jpg")


        Else
            DisplayMessage(Me.Page, "ไม่สามารถอัปโหลดรูปท่านได้ เนื่องจากขนาดรูปท่านใหญ่เกิน 500K กรุณาลองใหม่ภายหลัง")
        End If

    End Sub

End Class