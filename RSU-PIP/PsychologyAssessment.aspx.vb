﻿Public Class PsychologyAssessment
    Inherits System.Web.UI.Page

    Dim ctlLG As New AssessmentController
    Public dtPsy As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("UserLogin").Value) Then
            Response.Redirect("Default.aspx")
        End If

        If Not IsPostBack Then
            'lblYear.Text = Request.Cookies("ASSMYEAR").Value
            LoadAssesseeListToGrid()
        End If

    End Sub
    Private Sub LoadAssesseeListToGrid()
        dtPsy = ctlLG.StudentPsychologyAssessment_Get(StrNull2Zero(Request.Cookies("LocationID").Value))
    End Sub
End Class

