﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="MediaAdd.aspx.vb" Inherits=".MediaAdd" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
     <script type="text/javascript">
        function openModals(sender, title, message) {
            $("#spnTitle").text(title);
            $("#spnMsg").text(message);
            $('#modalPopUp').modal('show');
            $('#btnConfirm').attr('onclick', "$('#modalPopUp').modal('hide');setTimeout(function(){" + $(sender).prop('href') + "}, 50);");
            return false;
        }      

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
<section class="content-header">
      <h1>เอกสารดาวน์โหลด</h1>   
    </section>

<section class="content">  

         <div class="box box-pink">

            <div class="box-header">
              <i class="fa fa-plus-square"></i>

              <h3 class="box-title">เพิ่ม/แก้ไข เอกสารดาวน์โหลด</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                                 
            </div>
            <div class="box-body"> 
<table width="100%"> 
  <tr>
    <td>        
        <table width="100%">
      <tr>
        <td width="120">ID :</td>
        <td><table >
              <tr>
                <td width="100"><asp:Label ID="lblID" runat="server">0</asp:Label></td>
                <td width="50" align="center">ลำดับ</td>
                <td>
                    <asp:TextBox ID="txtOrder" runat="server" Width="50px">0</asp:TextBox>
                  </td>
              </tr>
            </table></td>
      </tr>
      <tr>
        <td>ชื่อเอกสาร :</td>
        <td>
            <asp:TextBox ID="txtTitle" runat="server" Width="100%"></asp:TextBox>
          </td>
      </tr>
      <tr>
        <td>สำหรับ นศ.</td>
        <td>
            <asp:DropDownList ID="ddlLevel" runat="server" CssClass="form-control select2"  Width="50%">
                <asp:ListItem Selected="True" Value="0">ทุกชั้นปี</asp:ListItem>
                <asp:ListItem Value="4">ปี 4</asp:ListItem>
                <asp:ListItem Value="6">ปี 6</asp:ListItem>
            </asp:DropDownList>
          </td>
      </tr>
      <tr>
        <td>สาขา</td>
        <td>
            <asp:DropDownList ID="ddlMajor" runat="server" CssClass="form-control select2"  Width="50%">
                <asp:ListItem Selected="True" Value="0">ทุกสาขา</asp:ListItem>
                <asp:ListItem Value="1">เภสัชกรรมอุตสาหการ</asp:ListItem>
                <asp:ListItem Value="2">การบริบาลทางเภสัชกรรม</asp:ListItem>
                
            </asp:DropDownList>
          </td>
      </tr>
      <tr>
        <td>หมวดเอกสาร</td>
        <td>
            <asp:DropDownList ID="ddlGroup" runat="server" CssClass="form-control select2" Width="50%">
                <asp:ListItem Value="1">คู่มือฝึกงาน</asp:ListItem>
                <asp:ListItem Value="2">แบบประเมินลับ</asp:ListItem>
                <asp:ListItem Value="4">แบบบันทึกกิจกรรม</asp:ListItem>
                <asp:ListItem Selected="True" Value="3">เอกสารอื่นๆ</asp:ListItem>
            </asp:DropDownList>
          </td>
      </tr>
       <tr>
        <td>ประเภท :</td>
        <td class="mailbox-messages">
            <asp:RadioButtonList ID="optType" runat="server" RepeatDirection="Horizontal" 
                AutoPostBack="True">
                <asp:ListItem Value="URL">URL Link</asp:ListItem>
                <asp:ListItem Value="UPL">File Upload</asp:ListItem>
            </asp:RadioButtonList>
          </td>
      </tr>     
      </table></td>
  </tr>
  <tr>
    <td>
        <asp:Panel ID="pnURL" runat="server">
       
    
    <table width="100%">
      <tr>
        <td width="120">URL : </td>
        <td>
            <asp:TextBox ID="txtURL" runat="server" Width="100%"></asp:TextBox>
          </td>
      </tr>
    </table>
     </asp:Panel>
        <asp:Panel ID="pnFile" runat="server">
     
    <table width="100%" >
      <tr>
        <td width="120">Link File : </td>
        <td>
            <asp:HyperLink ID="hlnkNews" runat="server" Target="_blank" ForeColor="#125acd">[hlnkNews]</asp:HyperLink>
          </td>
      </tr>
      <tr>
        <td>Upload File :</td>
        <td>
            <asp:FileUpload ID="FileUpload" runat="server" Width="300px" />
            &nbsp; (ห้ามตั้งชื่อไฟล์เป็นภาษาไทย สามารถเป็นภาษาอังกฤษหรือตัวเลขได้ และห้ามมีเว้นวรรค)</td>
      </tr>
    </table>
       </asp:Panel>    
  
    </td>
  </tr> <tr>
        <td class="mailbox-messages">
            <asp:CheckBox ID="chkFistPage" runat="server" Text="แสดงไว้ที่หน้าแรก" Checked="True" />
            <asp:CheckBox ID="chkStatus" runat="server" Checked="True" 
                Text="Public/Active (เปิดใช้งาน)" />
          </td>
        </tr>     
  <tr>
    <td align="center">
       <asp:Button ID="cmdSave" runat="server" CssClass="btn btn-save" Width="100" Text="บันทึก"></asp:Button>&nbsp;<asp:Button ID="cmdCancel" runat="server" CssClass="btn btn-default" Width="100" Text="ยกเลิก"></asp:Button></td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">
        <asp:LinkButton ID="LinkButton2" runat="server" CssClass="btn btn-find" 
            PostBackUrl="MediaList.aspx?m=media&amp;p=media">หน้ารายการเอกสาร</asp:LinkButton>
      </td>
  </tr>
 
</table>

</div>
            <div class="box-footer clearfix">
           
            </div>
          </div>

                    
    </section>



 <div class="modal fade" id="modalPopUp" data-backdrop="static">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><span id="spnTitle"></span></h4>
              </div>
              <div class="modal-body">
                <p><span id="spnMsg"></span>&hellip;</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

 <!-- Modal HTML -->
        <div id="modalPopUp2" class="modal fade" role="dialog">
            <div class="modal-dialog modal-confirm">
		<div class="modal-content">
			<div class="modal-header">
				<div class="icon-box">
					<i class="material-icons">&#xE5CD;</i>
				</div>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body text-center">
				<h4><span id="spnTitle2"></span></h4>	
				<p><span id="spnMsg2"></span>.</p>
				<button class="btn btn-success" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
            </div>
<!--- End Modal --->


</asp:Content>
