﻿
<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="PracticeHistory.aspx.vb" Inherits=".PracticeHistory" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <section class="content-header">
      <h1>ประวัติการฝึกปฏิบัติงานวิชาชีพ</h1>     
    </section>
<section class="content">
      <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-user-md"></i>
              <h3 class="box-title">ข้อมูลนักศึกษา</h3>             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                                 
            </div>
            <div class="box-body"> 

                  <div class="row">         
       
              <div class="col-md-2">
          <div class="form-group">
            <label>รหัสนักศึกษา</label>
               <asp:TextBox ID="txtCode" runat="server"  cssclass="form-control text-center" ReadOnly="true" BackColor="White"></asp:TextBox> 
          </div>

        </div>         
    <div class="col-md-4">
          <div class="form-group">
            <label>ชื่อ-สกุล</label>
               <asp:TextBox ID="txtName" runat="server"  cssclass="form-control text-center" ReadOnly="true" BackColor="White"></asp:TextBox> 
          </div>

        </div>      
                  
                        <div class="col-md-2">
          <div class="form-group">
            <label>ชั้นปีที่</label>
              <asp:TextBox ID="txtLevelClass" runat="server"  cssclass="form-control text-center" ReadOnly="true" BackColor="White"></asp:TextBox> 
          </div>

        </div>
                    
      <div class="col-md-4">
          <div class="form-group">
            <label>สาขาวิชา</label>
               <asp:TextBox ID="txtMajorName" runat="server"  cssclass="form-control text-center" ReadOnly="true" BackColor="White"></asp:TextBox> 
          </div>

        </div>
          </div>
                                   
            <div class="box-footer text-center clearfix">
           
            </div>
          </div>
</div>

    <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-bookmark"></i>

              <h3 class="box-title">การฝึกปฏิบัติวิชาชีพเภสัชกรรม ภาคพื้นฐาน</h3>             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
             
              </div>                                 
            </div>
            <div class="box-body">      
                <asp:Label ID="lblNotF" runat="server" CssClass="alert alert-error show" 
                 Text="ยังไม่มีประวัติการฝึกปฏิบัติงานวิชาชีพเภสัชกรรม ภาคพื้นฐาน" Width="99%"></asp:Label>
       
         <asp:GridView ID="grdFoundation" 
                             runat="server" CellPadding="0" 
                                                        GridLines="None" CssClass="table table-hover" 
                             Font-Bold="False" Width="100%" AutoGenerateColumns="False">
                        <RowStyle BackColor="#F7F7F7" />
                        <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />                     
                        <pagerstyle HorizontalAlign="Center" 
                             CssClass="dc_pagination dc_paginationC dc_paginationC11" />                     
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <headerstyle CssClass="th" Font-Bold="True" HorizontalAlign="Left" 
                                                            />                     
                        <Columns>
                            <asp:BoundField DataField="SkillName" HeaderText="งานที่ฝึกปฏิบัติ" />
                            <asp:BoundField HeaderText="รายวิชา" DataField="SubjectName" />
                            <asp:BoundField HeaderText="แหล่งฝึก" DataField="LocationName" />
                            <asp:BoundField DataField="AccumulatedHours" HeaderText="จำนวน (ชม.)" >
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" Width="100px" />
                            </asp:BoundField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                   <asp:ImageButton ID="imgEdit1" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "AssessmentID") %>' ImageUrl="images/icon-edit.png" /> 
                                    <asp:ImageButton ID="imgDel1" runat="server"  CommandArgument='<%# DataBinder.Eval(Container.DataItem, "AssessmentID") %>' ImageUrl="images/delete.png" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="65px" />
                            </asp:TemplateField>
                        </Columns>
                        <EditRowStyle BackColor="#2461BF" />
                        <AlternatingRowStyle BackColor="White" />
                     </asp:GridView> 
 </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>
      <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-registered"></i>

              <h3 class="box-title">การฝึกปฏิบัติวิชาชีพเภสัชกรรม สาขาการบริบาลทางเภสัชกรรม </h3>             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
             
              </div>                                 
            </div>
            <div class="box-body">   <asp:Label ID="lblNot" runat="server" CssClass="alert alert-error show" 
                 Text="ยังไม่มีประวัติการฝึกปฏิบัติงานวิชาชีพ" Width="99%"></asp:Label> 
         <asp:GridView ID="grdData" 
                             runat="server" CellPadding="0" 
                                                        GridLines="None" CssClass="table table-hover" 
                             Font-Bold="False" Width="100%" AutoGenerateColumns="False">
                        <RowStyle BackColor="#F7F7F7" />
                        <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />                     
                        <pagerstyle HorizontalAlign="Center" 
                             CssClass="dc_pagination dc_paginationC dc_paginationC11" />                     
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <headerstyle CssClass="th" Font-Bold="True" HorizontalAlign="Left" 
                                                            />                     
                        <Columns>
                            <asp:BoundField DataField="EduYear" HeaderText="ปีการศึกษา" >
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" Width="100px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="PhaseNo" HeaderText="ผลัดที่">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" Width="40px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="SkillName" HeaderText="งานที่ฝึกปฏิบัติ" >
                            </asp:BoundField>
                            <asp:BoundField HeaderText="รายวิชา" DataField="SubjectName" />
                            <asp:BoundField HeaderText="แหล่งฝึก" DataField="LocationName" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                   <asp:ImageButton ID="imgEdit2" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "AssessmentID") %>' ImageUrl="images/icon-edit.png" />
                                    
                                     <asp:ImageButton ID="imgDel2" runat="server"  CommandArgument='<%# DataBinder.Eval(Container.DataItem, "AssessmentID") %>' ImageUrl="images/delete.png" /> 
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="65px" />
                            </asp:TemplateField>
                        </Columns>
                        <EditRowStyle BackColor="#2461BF" />
                        <AlternatingRowStyle BackColor="White" />
                     </asp:GridView>   
 </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>

    <div class="row text-center">
        <asp:HyperLink ID="lnkAdd" Target="_blank" CssClass="btn btn-primary"  runat="server" Width="100px">เพิ่มใหม่</asp:HyperLink>
  
    </div>
    </section>
</asp:Content>
