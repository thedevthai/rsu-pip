﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="ReportAssesmentByStudent.aspx.vb" Inherits=".ReportAssesmentByStudent" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    
    <script src="js/jquery-1.10.2.js"></script> 

<link rel="stylesheet" type="text/css" href="css/jquery-ui.css">
   <script src="js/jquery-1.9.1.js"></script>
  <script src="js/jquery-ui.js"></script>
   
</asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <section class="content-header">
      <h1>ผลการจัดสรรแหล่งฝึก</h1>   
    </section>

<section class="content">  
      <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-search"></i>

              <h3 class="box-title">เลือกเงื่อนไขการค้นหา</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                                 
            </div>
            <div class="box-body"> 

                  <div class="row">
           <div class="col-md-2">
          <div class="form-group">
            <label>ปีการศึกษา</label>
               <asp:DropDownList ID="ddlYear" runat="server"  cssclass="form-control select2"  placeholder="เลือกปีการศึกษา" AutoPostBack="True">    </asp:DropDownList> 
          </div>

        </div>
                       <div class="col-md-2">
          <div class="form-group">
            <label>ชั้นปีที่</label>
              <asp:DropDownList ID="ddlLevel" runat="server"  cssclass="form-control select2"        AutoPostBack="True">
                  <asp:ListItem Selected="True" Value="0">ทั้งหมด</asp:ListItem>
                  <asp:ListItem>4</asp:ListItem>
                  <asp:ListItem>6</asp:ListItem>
              </asp:DropDownList>      
          </div>

        </div>

                        <div class="col-md-4">
          <div class="form-group">
            <label>แหล่งฝึก</label>
               <asp:DropDownList ID="ddlLocation" runat="server"  cssclass="form-control select2"  placeholder="เลือกแหล่งฝึก" AutoPostBack="True">
      </asp:DropDownList>
          </div>

        </div>
                       <div class="col-md-4">
          <div class="form-group">
            <label>ผลัดฝึก</label>
               <asp:DropDownList ID="ddlPhase" runat="server"  cssclass="form-control select2"  placeholder="เลือกผลัดฝึก" AutoPostBack="True">
      </asp:DropDownList>
          </div>

        </div>            
            </div>
                  <div class="row">
                      <div class="col-md-3">
          <div class="form-group">
            <label>งานที่ฝึก</label>
              <asp:DropDownList ID="ddlSkill" runat="server" cssclass="form-control select2"  placeholder="เลือกงานที่ฝึก"
          AutoPostBack="True">                                                      </asp:DropDownList>      
          </div>

        </div>

                           <div class="col-md-5">
          <div class="form-group">
               <label> หรือ รายวิชา</label>
                           <asp:DropDownList ID="ddlCourse" runat="server" CssClass="form-control select2" AutoPostBack="True">     </asp:DropDownList> 
               
          </div>

        </div>

                           <div class="col-md-2">
          <div class="form-group">
               <label>ค้นหา</label>
              <asp:TextBox ID="txtStudent" runat="server" CssClass="form-control" placeholder="รหัส นศ./ชื่อ/สกุล"></asp:TextBox>
               
          </div>

        </div>

                       <div class="col-md-2">
          <div class="form-group">
               <label> &nbsp;</label><br />
            <asp:Button ID="cmdFind" runat="server"  Text="ค้นหา" CssClass="btn btn-find" Width="100px"/> 
               
          </div>

        </div>
                   </div>                                   
</div>
            <div class="box-footer text-center clearfix">
           
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="0">
             <ProgressTemplate>
<img alt="" src="images/progress_bar.gif" height="25" />             </ProgressTemplate>
         </asp:UpdateProgress> 
            </div>
          </div>
    
 <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-list"></i>

              <h3 class="box-title">รายชื่อนักศึกษา</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                                 
            </div>
            <div class="box-body"> 
    <asp:GridView ID="grdStudent" 
                             runat="server" CellPadding="0" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" Width="100%" AllowPaging="True" 
                  DataKeyNames="AssessmentID" PageSize="20" CssClass="table table-hover">
            <RowStyle BackColor="#F7F7F7" />
            <columns>
                <asp:BoundField DataField="nRow" HeaderText="No.">
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="Student_Code" HeaderText="รหัสนักศึกษา" >
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="StudentName" HeaderText="ชื่อ-สกุล" />
                <asp:BoundField HeaderText="ผลัดที่" DataField="TimePhaseName" >
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="PhaseName" HeaderText="วันที่" />
                <asp:BoundField DataField="SkillName" HeaderText="งานที่ฝึก" />
            <asp:BoundField HeaderText="ชื่อแหล่งฝึก" DataField="LocationName">

                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />

                </asp:BoundField>
            <asp:BoundField DataField="ProvinceName" HeaderText="จังหวัด">                      
              <itemstyle HorizontalAlign="Left" VerticalAlign="Middle" />                      </asp:BoundField>
                <asp:BoundField DataField="AliasName" HeaderText="รายวิชา" >
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
            <asp:TemplateField>
              <itemtemplate>
                   <asp:ImageButton ID="imgEdit" runat="server" 
                                    ImageUrl="images/icon-edit.png"  CommandArgument='<%# DataBinder.Eval(Container.DataItem, "AssessmentID")  %>' /> 
                <asp:ImageButton ID="imgDel" runat="server" 
                                    ImageUrl="images/delete.png"      CommandArgument='<%# DataBinder.Eval(Container.DataItem, "AssessmentID")  %>' />                        </itemtemplate>
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="60px" />          
            </asp:TemplateField>
            </columns>
            <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />          
            <pagerstyle HorizontalAlign="Center" 
                      CssClass="dc_pagination dc_paginationC dc_paginationC11" />          
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <headerstyle CssClass="th" Font-Bold="True" 
                      VerticalAlign="Middle" />          
            <EditRowStyle BackColor="#2461BF" />
            <AlternatingRowStyle BackColor="White" />
          </asp:GridView>

    
            <div class="box-footer clearfix">
           
                   
                               
            </div>
         
</div>
            <div class="box-footer clearfix"> 
                <asp:Label ID="lblResult" runat="server" CssClass="alert alert-error show" 
                 Text="ไม่มีข้อมูลตามเงื่อนไขที่ท่านค้นหา" Width="100%"></asp:Label>
          
            </div>
          </div>

    </section>
</asp:Content>
