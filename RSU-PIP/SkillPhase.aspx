﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="SkillPhase.aspx.vb" Inherits=".SkillPhase" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

    </asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">


<section class="content-header">
      <h1>กำหนดผลัดฝึกให้งานที่ฝึก</h1>   
    </section>

<section class="content">   
    <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-search-plus"></i>

              <h3 class="box-title">จัดการผลัดฝึก</h3>
             
                 <div class="box-tools pull-right"><asp:Label ID="lblID" runat="server"></asp:Label> 
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                                 
            </div>
            <div class="box-body"> 

<table  border="0"  width="100%">
 <tr>
                                                    <td align="left" width="100"  valign="top">                                                        ปีการศึกษา :    </td>
                                                    <td align="left"> 
                                                        <asp:DropDownList ID="ddlYear" runat="server" AutoPostBack="True" CssClass="form-control select2" Width="100">
                                                        </asp:DropDownList>
                                                     </td>
                                                </tr>
                                                 <tr>
                                                    <td align="left"  valign="top">                                                        งานที่ฝึก : </td>
                                                    <td align="left" >
                                                      <asp:DropDownList ID="ddlSkill" runat="server" CssClass="form-control select2"  AutoPostBack="True">                                                      </asp:DropDownList>                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" valign="top">                                                       ผลัดที่เปิด : 
                                                        </td>
                                                    <td align="left" >
                                                        <asp:CheckBoxList ID="chkPhase" runat="server" CssClass="mailbox-messages"> </asp:CheckBoxList>                                            </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" valign="top">                                                       &nbsp;</td>
                                                    <td align="left" >
                                                         <asp:Button ID="cmdSave" runat="server" CssClass="btn btn-save" Width="100" Text="บันทึก"></asp:Button>
    <asp:Button ID="cmdClear" runat="server" CssClass="btn btn-default" Width="100" Text="ยกเลิก"></asp:Button>
                                     </td>
                                                </tr>
                                                </table>        
 </div> 
        <div class="box-footer clearfix">           
                
            </div>
          </div>

    <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-list"></i>

              <h3 class="box-title">รายการผลัดฝึกในแต่ละงานที่ฝึก</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                                 
            </div>
            <div class="box-body">                                        
 
 
              <asp:GridView ID="grdData" 
                             runat="server" CellPadding="0" ForeColor="#333333" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" Width="100%" DataKeyNames="SkillUID" CssClass="table table-hover">
            <RowStyle BackColor="#F7F7F7" HorizontalAlign="Center" />
            <columns>
            <asp:BoundField HeaderText="No." DataField="nRow">
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />                      </asp:BoundField>
                <asp:BoundField DataField="EduYear" HeaderText="ปี">
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="100px" />
                </asp:BoundField>
                <asp:BoundField HeaderText="ชื่องานที่ฝึก" DataField="SkillName" >
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
            <asp:TemplateField HeaderText="Edit">
              <itemtemplate>
                <asp:ImageButton ID="imgEdit" runat="server" ImageUrl="images/icon-edit.png" 
                                    CommandArgument='<%#  Container.DataItemIndex %>' />                        </itemtemplate>
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />          
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Delete">
              <itemtemplate>
                <asp:ImageButton ID="imgDel" runat="server" 
                                    ImageUrl="images/delete.png" 
                                    CommandArgument='<%#  Container.DataItemIndex %>' />                        </itemtemplate>
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />          
            </asp:TemplateField>
            </columns>
            <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />          
            <pagerstyle CssClass="dc_pagination dc_paginationC dc_paginationC11"  HorizontalAlign="Center" />          
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <headerstyle CssClass="th" Font-Bold="True" HorizontalAlign="Center" 
                      VerticalAlign="Middle" />          
            <EditRowStyle BackColor="#2461BF" />
            <AlternatingRowStyle BackColor="White" />
          </asp:GridView>
 </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>
                       
    </section>    
    
</asp:Content>
