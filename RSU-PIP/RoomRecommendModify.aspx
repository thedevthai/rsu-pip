﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="RoomRecommendModify.aspx.vb" Inherits=".RoomRecommendModify" %>
<%@ Register assembly="DevExpress.Web.ASPxHtmlEditor.v17.2, Version=17.2.13.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxHtmlEditor" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v17.2, Version=17.2.13.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxSpellChecker.v17.2, Version=17.2.13.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxSpellChecker" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     
     <section class="content-header">
      <h1>คำแนะนำจากนักศึกษา
          <small></small>              
      </h1>
     
    </section>

<section class="content">
    <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-registered"></i>

              <h3 class="box-title">คำแนะนำ</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">             
            
<table cellPadding="1" cellSpacing="1" width="99%">

<tr>
  <td align="left" class="texttopic">ID</td>
  <td align="left" class="texttopic">
      <table width="100%">
          <tr>
              <td width="50">
                  <asp:Label ID="lblID" runat="server"></asp:Label>
                                                              </td>
                                                              <td align="right">
                                                                   <asp:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode="Inline">
                                                                      <ContentTemplate>
                                                                  <table align="right"  >
                                                                      <tr>
                                                                          <td>Status :</td>
                                                                          <td align="left" width="70">
                                                                          <asp:Label ID="lblStatus" runat="server" Text="Open"></asp:Label>
                                                                          </td>
                                                                          <td>
                                                                  <asp:ImageButton ID="imgPending" runat="server" ImageUrl="images/statusicon_pending_01.png" Width="20px" />
                                                                          </td>
                                                                          <td>
                                                                  <asp:ImageButton ID="imgApprove" runat="server" ImageUrl="images/statusicon_approve_01.png" Width="20px" />
                                                                          </td>
                                                                          <td>
                                                                  <asp:ImageButton ID="imgReject" runat="server" ImageUrl="images/statusicon_reject_01.png" Width="20px" />
                                                                          </td>
                                                                      </tr>
                                                                  </table>
                                                                          
                                                                      </ContentTemplate>
                                                                      <Triggers>
                                                                          <asp:AsyncPostBackTrigger ControlID="imgPending" EventName="Click" />
                                                                          <asp:AsyncPostBackTrigger ControlID="imgApprove" EventName="Click" />
                                                                          <asp:AsyncPostBackTrigger ControlID="imgReject" EventName="Click" />
                                                                      </Triggers>
                                                                  </asp:UpdatePanel>

                                                              </td>          
                                                          </tr>
                                                      </table>
    </td>
</tr>

<tr>
  <td align="left" class="texttopic">นักศึกษาที่แนะนำ</td>
  <td align="left" class="texttopic">
                                                      [<asp:Label ID="lblStudentCode" runat="server"></asp:Label>
                                                      ]&nbsp;<asp:Label ID="lblStudentName" runat="server"></asp:Label>
    </td>
</tr>

<tr>
  <td align="left" class="texttopic">แหล่งฝึกที่เคยไปฝึกมา </td>
  <td align="left" class="texttopic">
                                                      <asp:DropDownList ID="ddlLocation" runat="server" AutoPostBack="True" 
                                                            CssClass="Objcontrol">                                                      </asp:DropDownList>                                                    
                                                      <asp:Label ID="lblLocation" runat="server"></asp:Label>
    </td>
</tr>

<tr>
  <td align="left" class="texttopic">คำแนะนำสั้นๆ</td>
  <td align="left" class="texttopic">
                                                      <asp:TextBox ID="txtShortDesc" runat="server" Width="400px"></asp:TextBox>
    </td>
</tr>

<tr>
  <td align="left" class="texttopic">รายละเอียด</td>
  <td align="left" class="texttopic">
                                                      &nbsp;</td>
</tr>

<tr>
  <td align="left" colspan="2">
      <dx:ASPxHtmlEditor ID="txtRecommend" runat="server" Width="99%" Theme="Office2010Blue">
      </dx:ASPxHtmlEditor>
    </td>
</tr>

<tr>
  <td align="center" class="texttopic" colspan="2">
      <asp:Button ID="cmdSave" runat="server" CssClass="btn btn-save" Text="บันทึก" Width="120px" />
    </td>
</tr>
  </table>   

                 </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>


</section>
     

</asp:Content>
