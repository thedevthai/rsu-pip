﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="ResultProceptor.aspx.vb" Inherits=".ResultProceptor" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    
    <script src="js/jquery-1.10.2.js"></script> 

<link rel="stylesheet" type="text/css" href="css/jquery-ui.css">
   <script src="js/jquery-1.9.1.js"></script>
  <script src="js/jquery-ui.js"></script>
  
  <script> $(function() {
       $( "#dialog-message" ).dialog({
      autoOpen: false,
      height: 600,
      width: 600,
      modal: true,
      buttons: {
        Close: function() {
          $( this ).dialog( "close" );
        }
      },
      close: function() {
        
      }
    });
 
    $( "#user-login" )
      .button()
      .click(function() {
        $( "#dialog-message" ).dialog( "open" );
      });
  });
  </script>
</asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <section class="content-header">
      <h1>รายชื่อนักศึกษาที่เข้ามาฝึกปฏิบัติวิชาชีพ</h1>   
    </section>

<section class="content">  

     <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-filter"></i>

              <h3 class="box-title">เงื่อนไขการค้นหา</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                                 
            </div>
            <div class="box-body"> 
                 <div class="row">
           <div class="col-md-2">
          <div class="form-group">
            <label>ปีการศึกษา</label>
               <asp:DropDownList ID="ddlYear" runat="server"  cssclass="form-control select2"  placeholder="เลือกปีการศึกษา" AutoPostBack="True">    </asp:DropDownList> 
          </div>

        </div>
                       <div class="col-md-1">
          <div class="form-group">
            <label>ชั้นปีที่</label>
              <asp:DropDownList ID="ddlLevel" runat="server"  cssclass="form-control select2"        AutoPostBack="True">
                  <asp:ListItem Value="0">ทั้งหมด</asp:ListItem>
                  <asp:ListItem>4</asp:ListItem>
                  <asp:ListItem>6</asp:ListItem>
              </asp:DropDownList>      
          </div>

        </div>
                       <div class="col-md-3">
          <div class="form-group">
            <label>ผลัดฝึก</label>
               <asp:DropDownList ID="ddlPhase" runat="server"  cssclass="form-control select2"  placeholder="เลือกผลัดฝึก" AutoPostBack="True">
      </asp:DropDownList>
          </div>

        </div>
                       <div class="col-md-4">
          <div class="form-group">
            <label>งานที่ฝึก</label>
              <asp:DropDownList ID="ddlSubject" runat="server" cssclass="form-control select2"  placeholder="เลือกงานที่ฝึก"
          AutoPostBack="True">                                                      </asp:DropDownList>      
          </div>

        </div>

                       <div class="col-md-2">
          <div class="form-group">
               <label> &nbsp;</label><br />
            <asp:Button ID="cmdFind" runat="server"  Text="ค้นหา" CssClass="btn btn-find" Width="100px"/> 
               
          </div>

        </div>


                   </div>
                          
                               
</div>
            <div class="box-footer clearfix text-center">
           
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="0">
             <ProgressTemplate>
<img alt="" src="images/progress_bar.gif" height="25" />             </ProgressTemplate>
         </asp:UpdateProgress>    
            </div>
          </div>

 <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-list"></i>               
              <h3 class="box-title">รายชื่อนักศึกษา</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                                 
            </div>
            <div class="box-body"> 
              <asp:GridView ID="grdData" 
                             runat="server" CellPadding="0" ForeColor="#333333" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" Width="100%" DataKeyNames="Student_ID" CssClass="table table-hover">
            <RowStyle BackColor="#F7F7F7" HorizontalAlign="Center" />
            <columns>
                <asp:BoundField >
                <ItemStyle Width="60px" />
                </asp:BoundField>
            <asp:BoundField HeaderText="No." DataField="nRow">
              <itemstyle HorizontalAlign="Center" VerticalAlign="Top" Width="30px" />                      </asp:BoundField>
            <asp:BoundField DataField="Student_Code" HeaderText="รหัสนักศึกษา">
              <headerstyle HorizontalAlign="Center" />          
              <itemstyle HorizontalAlign="Center" VerticalAlign="Top" Width="90px" />                      </asp:BoundField>
                <asp:BoundField HeaderText="ชื่อ-สกุล" DataField="StudentName">
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                </asp:BoundField>
                <asp:BoundField HeaderText="งานที่ฝึก" DataField="SkillName" >
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="SkillPhaseName" HeaderText="ผลัดฝึก" />
                <asp:BoundField HeaderText="วันที่ฝึกปฏิบัติ" DataField="SkillPhaseDesc" />
            </columns>
            <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />          
            <pagerstyle CssClass="dc_pagination dc_paginationC dc_paginationC11"  HorizontalAlign="Center" />          
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <headerstyle CssClass="th" Font-Bold="True" HorizontalAlign="Center" 
                      VerticalAlign="Middle" />          
            <EditRowStyle BackColor="#2461BF" />
            <AlternatingRowStyle BackColor="White" />
          </asp:GridView>


             
                                   
</div>
            <div class="box-footer clearfix">
           <asp:Label ID="lblResult" runat="server" CssClass="OptionPanels" 
                 Text="ยังไม่ได้รับคัดเลือกแหล่งฝึกให้" Width="95%"></asp:Label>
            </div>
          </div>
</section>         
</asp:Content>
