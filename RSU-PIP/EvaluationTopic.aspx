﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="EvaluationTopic.aspx.vb" Inherits=".EvaluationTopic" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">  
</asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<section class="content-header">
      <h1>จัดการหัวข้อการประเมิน</h1>   
    </section>

<section class="content">  

         <div class="box box-pink">

            <div class="box-header">
              <i class="fa fa-filter"></i>

              <h3 class="box-title">เพิ่ม/แก้ไข หัวข้อการประเมิน</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                                 
            </div>
            <div class="box-body"> 
         <table cellSpacing="1" cellPadding="1" border="0" width="98%">
                                                <tr>
                                                    <td align="left" width="100" >
                                                        รหัส :</td>
                                                    <td align="left" > 
                                                        <asp:Label ID="lblID" runat="server"></asp:Label>                                                    &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td align="left" valign="top">
                                                        หัวข้อ:</td>
                                                    <td align="left" ><asp:TextBox ID="txtName" runat="server" 
                                                            Width="100%" CssClass="Objcontrol" Height="52px" TextMode="MultiLine" MaxLength="1000"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td align="left" >น้ำหนักคะแนน:</td>
                                                    <td align="left" >
                                                        <asp:RadioButtonList ID="optWeight" runat="server" RepeatDirection="Horizontal">
                                                            <asp:ListItem Selected="True" Value="1">Default</asp:ListItem>
                                                            <asp:ListItem Value="2">คะแนน x 2</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" valign="top" >Remark :</td>
                                                    <td align="left" >
                                                        <asp:TextBox ID="txtRemark" runat="server" 
                                                            Width="100%" CssClass="Objcontrol" Height="70px" TextMode="MultiLine" MaxLength="4000"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" >Status : </td>
                                                    <td align="left" class="mailbox-messages" ><asp:CheckBox ID="chkStatus" runat="server" Text="Active" 
                                                            Checked="True" /></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" align="center" vAlign="top" >
                                                       
                                                        <asp:Button ID="cmdSave" runat="server" CssClass="btn btn-save" Text="บันทึก" Width="80px" />
&nbsp;<asp:Button ID="cmdClear" runat="server" CssClass="btn btn-default" Text="ยกเลิก" Width="80px" />
                                                                                          </td>
                                                </tr>
  </table>         
</div>
            <div class="box-footer clearfix">
           
            </div>
          </div>

   
    <div class="box box-success">
            <div class="box-header">
              <i class="fa fa-list"></i>

              <h3 class="box-title">หัวข้อการประเมิน</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                                 
            </div>
            <div class="box-body"> 
    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
        
       <tr>
          <td align="center" valign="top">&nbsp;</td>
      </tr>
        <tr>
          <td  align="left" valign="top">
              
              <table border="0" cellspacing="1" cellpadding="0">
            <tr>
              <td width="50" >ค้นหา</td>
              <td >
                  <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>                </td>
              <td >
                  <asp:Button ID="cmdFind" runat="server" CssClass="btn btn-find" Text="ค้นหา" />
&nbsp;<asp:Button ID="cmdAll" runat="server" CssClass="btn btn-find" Text="ดูทั้งหมด" />
                </td>
            </tr>
            </table></td>
      </tr>

        <tr>
          <td align="center" valign="top">
              <asp:GridView ID="grdData" 
                             runat="server" CellPadding="0" ForeColor="#333333" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" Width="100%" AllowPaging="True" PageSize="15">
            <RowStyle BackColor="#F7F7F7" HorizontalAlign="Center" VerticalAlign="Top" />
            <columns>
            <asp:BoundField HeaderText="No." DataField="nRow">
              <itemstyle HorizontalAlign="Center" VerticalAlign="Top" />                      </asp:BoundField>
                <asp:BoundField DataField="TopicName" HeaderText="หัวข้อ">
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" />
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
            <asp:TemplateField HeaderText="Active">
              <itemtemplate>
                <asp:Image ID="imgStatus" runat="server" ImageUrl="images/icon-ok.png" 
                                    Visible='<%# ConvertStatusFlag2CHK(DataBinder.Eval(Container.DataItem, "StatusFlag")) %>' />                        </itemtemplate>
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />          
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Edit">
              <itemtemplate>
                <asp:ImageButton ID="imgEdit" runat="server" ImageUrl="images/icon-edit.png" 
                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID") %>' />                        </itemtemplate>
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />          
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Delete">
              <itemtemplate>
                <asp:ImageButton ID="imgDel" runat="server" 
                                    ImageUrl="images/delete.png" 
                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID") %>' />                        </itemtemplate>
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />          
            </asp:TemplateField>
            </columns>
            <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />          
            <pagerstyle HorizontalAlign="Center" CssClass="dc_pagination dc_paginationC dc_paginationC11" />          
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <headerstyle CssClass="th" Font-Bold="True" HorizontalAlign="Center" 
                      VerticalAlign="Middle" />          
            <EditRowStyle BackColor="#2461BF" />
            <AlternatingRowStyle BackColor="White" />
          </asp:GridView></td>
      </tr>
       
    </table>
</div>
            <div class="box-footer clearfix">
           
            </div>
          </div>

   </section>   
</asp:Content>
