﻿
Public Class Skill
    Inherits System.Web.UI.Page

    Dim ctlSk As New SkillController
    Dim dt As New DataTable
    Dim acc As New UserController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("UserLogin").Value) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If
        If Not IsPostBack Then
            ClearData()
            grdData.PageIndex = 0
            LoadSkillToGrid()
        End If

    End Sub
    Private Sub LoadSkillToGrid()

        If Trim(txtSearch.Text) <> "" Then
            dt = ctlSk.Skill_GetBySearch(txtSearch.Text)
        Else
            dt = ctlSk.Skill_Get
        End If

        With grdData
            .Visible = True
            .DataSource = dt
            .DataBind()
        End With

    End Sub

    Private Sub grdData_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdData.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit"
                    EditData(e.CommandArgument())
                Case "imgDel"
                    If ctlSk.Skill_Delete(e.CommandArgument) Then

                        acc.User_GenLogfile(Request.Cookies("UserLogin").Value, ACTTYPE_DEL, "Skill", "ลบงานที่ฝึก:" & txtCode.Text & ">>" & txtNameTH.Text, "")
                        DisplayMessage(Me, "ลบข้อมูลเรียบร้อย")
                        grdData.PageIndex = 0
                        LoadSkillToGrid()
                    Else
                        DisplayMessage(Me, "ไม่สามารถลบข้อมูลได้ กรุณาตรวจสอบและลองใหม่อีกครั้ง")
                    End If
            End Select


        End If
    End Sub
    Private Sub EditData(ByVal pID As String)
        dt = ctlSk.Skill_GetByCode(pID)

        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                Me.lblID.Text = DBNull2Str(dt.Rows(0)("UID"))
                Me.txtCode.Text = DBNull2Str(dt.Rows(0)("UID"))
                txtNameTH.Text = DBNull2Str(dt.Rows(0)("Name"))
                txtNameEN.Text = DBNull2Str(dt.Rows(0)("Name2"))
                'txtAliasName.Text = DBNull2Str(dt.Rows(0)("AliasName"))
                chkStatus.Checked = ConvertStatusFlag2CHK(String.Concat(dt.Rows(0)("StatusFlag")))

            End With
        End If
        dt = Nothing
    End Sub
    Private Sub ClearData()
        Me.lblID.Text = ""
        txtCode.Text = ""
        txtNameTH.Text = ""
        txtNameEN.Text = ""
        'txtAliasName.Text = ""
        chkStatus.Checked = True
    End Sub

    Private Sub grdData_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdData.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then

            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(6).FindControl("imgDel")
            imgD.Attributes.Add("onClick", scriptString)

        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#ffdfef';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If
    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click
        If txtNameTH.Text = "" Or StrNull2Zero(txtCode.Text) = 0 Then
            DisplayMessage(Me, "กรุณากรอกข้อมูลให้ครบถ้วน")
            Exit Sub
        End If

        Dim item As Integer
        If lblID.Text = "" Then
            If ctlSk.Skill_CheckDup(StrNull2Zero(txtCode.Text)) Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ท่านกำหนดรหัสซ้ำ กรุณาตรวจสอบ');", True)
                Exit Sub
            End If

            item = ctlSk.Skill_Add(StrNull2Zero(txtCode.Text), txtNameTH.Text, txtNameEN.Text, "", ConvertBoolean2StatusFlag(chkStatus.Checked))

            acc.User_GenLogfile(Request.Cookies("UserLogin").Value, ACTTYPE_ADD, "Skill", "เพิ่มใหม่ งานที่ฝึก:" & txtCode.Text, "Result:" & item)

        Else
            item = ctlSk.Skill_Update(StrNull2Zero(lblID.Text), StrNull2Zero(txtCode.Text), txtNameTH.Text, txtNameEN.Text, "", ConvertBoolean2StatusFlag(chkStatus.Checked))

            acc.User_GenLogfile(Request.Cookies("UserLogin").Value, ACTTYPE_UPD, "Skill", "แก้ไข งานที่ฝึก:" & txtCode.Text, "Result:" & item)

        End If


        grdData.PageIndex = 0
        LoadSkillToGrid()
        ClearData()
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)

    End Sub

    Protected Sub cmdClear_Click(sender As Object, e As EventArgs) Handles cmdClear.Click
        ClearData()
    End Sub

    Protected Sub cmdFind_Click(sender As Object, e As EventArgs) Handles cmdFind.Click
        grdData.PageIndex = 0
        LoadSkillToGrid()
    End Sub
    Protected Sub cmdAll_Click(sender As Object, e As EventArgs) Handles cmdAll.Click
        txtSearch.Text = ""
        grdData.PageIndex = 0
        LoadSkillToGrid()
    End Sub

    Protected Sub grdData_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdData.PageIndexChanging
        grdData.PageIndex = e.NewPageIndex
        LoadSkillToGrid()
    End Sub
End Class

