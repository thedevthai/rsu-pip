﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class GradeEdit
    
    '''<summary>
    '''lblStudentCode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblStudentCode As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblStudentName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblStudentName As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblMajorName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMajorName As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblEditorName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblEditorName As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblUID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblUID As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblYear control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblYear As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblSubjectCode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblSubjectCode As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblSubjectName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblSubjectName As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''txtScore control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtScore As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''hdScore control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdScore As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''txtGrade control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtGrade As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''hdGrade control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdGrade As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''txtReason control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtReason As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''cmdSave control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cmdSave As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''cmdBack control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cmdBack As Global.System.Web.UI.WebControls.Button
End Class
