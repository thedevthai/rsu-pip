﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports Subgurim.Controles


Public Class Student_Reg
    Inherits System.Web.UI.Page
    Dim dt As New DataTable

    Dim ctlFct As New FacultyController
    Dim ctlbase As New ApplicationBaseClass

    Dim ctlStd As New StudentController
    Dim objStd As New StudentInfo

    Dim ctlPsn As New PersonController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            cmdPrint.Visible = False
            cmdDelete.Visible = False
            BindDayToDDL()
            BindMonthToDDL()
            BindYearToDDL()
            LoadMajorToDDL()
            LoadAdvisorToDDL()

            LoadPayorToDDL()
            LoadProvinceToDDL()

            LoadStudentData()

        End If

        If (FileUploaderAJAX1.IsPosting) Then
            UploadFile()
        End If

        'picStudent.ImageUrl = "~/" & stdPic & "/" & Request.Cookies("ProfileID").Value.Value & ".jpg"

        btnOK.OnClientClick = String.Format("fnClickOK('{0}','{1}')", btnOK.UniqueID, "")


        cmdPrint.Attributes.Add("onClick", "window.open('" + ResolveUrl("ReportViewerStudentBio.aspx?id=" + lblStdCode.Text) + "', 'windowname', 'width=800,height=600,scrollbars=yes')")

        txtGPAX.Attributes.Add("OnKeyPress", "return AllowOnlyDouble();")
        txtWL.Attributes.Add("OnKeyPress", "return AllowOnlyDouble();")
        txtHL.Attributes.Add("OnKeyPress", "return AllowOnlyDouble();")
        txtCardID.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")

        cmdDelete.Attributes.Add("onClick", "javascript:return confirm(""ต้องการส่งข้อมูลนักศึกษารายนี้ออกจากระบบใช่หรือไม่?"");")

        If StrNull2Boolean(Request.Cookies("ROLE_ADM").Value) = True Or StrNull2Boolean(Request.Cookies("ROLE_SPA").Value) = True Then
            RequiredFieldValidator1.Enabled = False
            RequiredFieldValidator2.Enabled = False
            RequiredFieldValidator3.Enabled = False
            RequiredFieldValidator4.Enabled = False
            RequiredFieldValidator5.Enabled = False
            RequiredFieldValidator6.Enabled = False
            RequiredFieldValidator7.Enabled = False
            RequiredFieldValidator8.Enabled = False
            RequiredFieldValidator9.Enabled = False
            RequiredFieldValidator10.Enabled = False
            RequiredFieldValidator11.Enabled = False
            RequiredFieldValidator12.Enabled = False
            RequiredFieldValidator13.Enabled = False
            RequiredFieldValidator14.Enabled = False
            RequiredFieldValidator15.Enabled = False
            RequiredFieldValidator16.Enabled = False
            RequiredFieldValidator17.Enabled = False
            RequiredFieldValidator18.Enabled = False
            RequiredFieldValidator19.Enabled = False
            RequiredFieldValidator20.Enabled = False
            RequiredFieldValidator21.Enabled = False
            RequiredFieldValidator22.Enabled = False
            RequiredFieldValidator23.Enabled = False
            RequiredFieldValidator24.Enabled = False
            RequiredFieldValidator25.Enabled = False
            RequiredFieldValidator26.Enabled = False
            RequiredFieldValidator27.Enabled = False
            RequiredFieldValidator28.Enabled = False
            RequiredFieldValidator29.Enabled = False
            RequiredFieldValidator30.Enabled = False
            RequiredFieldValidator31.Enabled = False
            RequiredFieldValidator32.Enabled = False
            RequiredFieldValidator33.Enabled = False
        End If

    End Sub

    Private Sub LoadAdvisorToDDL()

        dt = ctlPsn.Person_GetActiveByType("T")

        ddlAdvisor.Items.Clear()
        If dt.Rows.Count > 0 Then
            With ddlAdvisor
                .Enabled = True
                .Items.Add("")
                .Items(0).Value = 0

                For i = 0 To dt.Rows.Count - 1
                    .Items.Add("" & dt.Rows(i)("PersonName"))
                    .Items(i + 1).Value = dt.Rows(i)("PersonID")
                Next
                .SelectedIndex = 0

            End With

        End If
    End Sub
    Private Sub LoadMajorToDDL()
        Dim dtMajor As New DataTable
        dtMajor = ctlFct.GetMajor
        If dtMajor.Rows.Count > 0 Then
            With ddlMajor
                .Enabled = True
                .DataSource = dtMajor
                .DataTextField = "MajorName"
                .DataValueField = "MajorID"
                .DataBind()
                .SelectedIndex = 0
            End With
        Else

        End If
        dtMajor = Nothing
    End Sub
    Private Sub LoadStudentData()

        If Request("stdid") Is Nothing Then
            dt = ctlStd.GetStudent_ByID(Request.Cookies("ProfileID").Value)
        Else
            dt = ctlStd.GetStudent_ByID(Request("stdid"))
        End If



        If dt.Rows.Count > 0 Then
            With dt.Rows(0)

                hdStudentID.Value = String.Concat(.Item("Student_ID"))
                Session("stdcode") = String.Concat(.Item("Student_Code"))
                lblStdCode.Text = .Item(objStd.tblField(objStd.fldPos.f01_Student_Code).fldName)

                If Not IsDBNull(.Item(objStd.tblField(objStd.fldPos.f02_Prefix).fldName)) Then
                    ddlPrefix.SelectedValue = .Item(objStd.tblField(objStd.fldPos.f02_Prefix).fldName)
                End If

                txtFirstNameTH.Text = DBNull2Str(.Item(objStd.tblField(objStd.fldPos.f03_FirstName).fldName))
                txtLastNameTH.Text = DBNull2Str(.Item(objStd.tblField(objStd.fldPos.f04_LastName).fldName))
                txtNickName.Text = DBNull2Str(.Item(objStd.tblField(objStd.fldPos.f05_NickName).fldName))

                If Not IsDBNull(.Item(objStd.tblField(objStd.fldPos.f06_Gender).fldName)) Then
                    optGender.SelectedValue = .Item(objStd.tblField(objStd.fldPos.f06_Gender).fldName)
                End If

                txtEmail.Text = DBNull2Str(.Item(objStd.tblField(objStd.fldPos.f07_Email).fldName))
                txtTelephone.Text = DBNull2Str(.Item(objStd.tblField(objStd.fldPos.f08_Telephone).fldName))
                txtMobile.Text = DBNull2Str(.Item(objStd.tblField(objStd.fldPos.f09_MobilePhone).fldName))

                If DBNull2Str(.Item(objStd.tblField(objStd.fldPos.f10_BirthDate).fldName)) <> "" Then
                    ddlDay.SelectedValue = Right(.Item(objStd.tblField(objStd.fldPos.f10_BirthDate).fldName), 2)
                    ddlMonth.SelectedValue = Mid(.Item(objStd.tblField(objStd.fldPos.f10_BirthDate).fldName), 5, 2)
                    ddlYear.SelectedValue = Left(.Item(objStd.tblField(objStd.fldPos.f10_BirthDate).fldName), 4)
                End If


                If Not IsDBNull(.Item(objStd.tblField(objStd.fldPos.f11_BloodGroup).fldName)) Then
                    ddlBloodGroup.SelectedValue = .Item(objStd.tblField(objStd.fldPos.f11_BloodGroup).fldName)
                End If
                ddlMajor.SelectedValue = String.Concat(.Item("MajorID"))

                'lblMajorName.Text = String.Concat(.Item("MajorName"))
                'lblMinorName.Text = String.Concat(.Item("MinorName"))
                'lblSubMinorName.Text = String.Concat(.Item("SubMinorName"))

                ddlAdvisor.SelectedValue = String.Concat(.Item("AdvisorID"))
                'lblAdvisorName.Text = String.Concat(.Item("PrefixName")) & String.Concat(.Item("Ad_Fname")) & " " & String.Concat(.Item("Ad_Lname"))

                txtWL.Text = String.Concat(.Item("WL"))
                txtHL.Text = String.Concat(.Item("HL"))
                txtCardID.Text = String.Concat(.Item("CardID"))

                txtFather_FirstName.Text = DBNull2Str(.Item(objStd.tblField(objStd.fldPos.f16_Father_FirstName).fldName))
                txtFather_LastName.Text = DBNull2Str(.Item(objStd.tblField(objStd.fldPos.f17_Father_LastName).fldName))
                txtFather_Career.Text = DBNull2Str(.Item(objStd.tblField(objStd.fldPos.f18_Father_Career).fldName))
                txtFather_Tel.Text = DBNull2Str(.Item(objStd.tblField(objStd.fldPos.f19_Father_Tel).fldName))
                txtMother_FirstName.Text = DBNull2Str(.Item(objStd.tblField(objStd.fldPos.f20_Mother_FirstName).fldName))
                txtMother_LastName.Text = DBNull2Str(.Item(objStd.tblField(objStd.fldPos.f21_Mother_LastName).fldName))
                txtMother_Career.Text = DBNull2Str(.Item(objStd.tblField(objStd.fldPos.f22_Mother_Career).fldName))
                txtMother_Tel.Text = DBNull2Str(.Item(objStd.tblField(objStd.fldPos.f23_Mother_Tel).fldName))
                txtSibling.Text = DBNull2Str(.Item(objStd.tblField(objStd.fldPos.f24_Sibling).fldName))
                txtChildNo.Text = DBNull2Str(.Item(objStd.tblField(objStd.fldPos.f25_ChildNo).fldName))

                txtAddressLive.Text = DBNull2Str(.Item(objStd.tblField(objStd.fldPos.f57_AddressLive).fldName))
                txtTelLive.Text = DBNull2Str(.Item(objStd.tblField(objStd.fldPos.f58_TelLive).fldName))


                txtAddressCard.Text = DBNull2Str(.Item(objStd.tblField(objStd.fldPos.f26_Address).fldName))
                'txtDistrict.Text = DBNull2Str(.Item(objStd.tblField(objStd.fldPos.f27_District).fldName))
                'txtCity.Text = DBNull2Str(.Item(objStd.tblField(objStd.fldPos.f28_City).fldName))

                If Not IsDBNull(.Item(objStd.tblField(objStd.fldPos.f29_ProvinceID).fldName)) Then
                    ddlProvince.SelectedValue = .Item(objStd.tblField(objStd.fldPos.f29_ProvinceID).fldName)
                End If

                txtZipCode.Text = DBNull2Str(.Item(objStd.tblField(objStd.fldPos.f31_ZipCode).fldName))
                txtCongenitalDisease.Text = DBNull2Str(.Item(objStd.tblField(objStd.fldPos.f32_CongenitalDisease).fldName))
                txtMedicineUsually.Text = DBNull2Str(.Item(objStd.tblField(objStd.fldPos.f33_MedicineUsually).fldName))
                txtMedicalHistory.Text = DBNull2Str(.Item(objStd.tblField(objStd.fldPos.f34_MedicalHistory).fldName))
                txtHobby.Text = DBNull2Str(.Item(objStd.tblField(objStd.fldPos.f35_Hobby).fldName))
                txtTalent.Text = DBNull2Str(.Item(objStd.tblField(objStd.fldPos.f36_Talent).fldName))
                txtExpectations.Text = DBNull2Str(.Item(objStd.tblField(objStd.fldPos.f37_Expectations).fldName))
                txtPrimarySchool.Text = DBNull2Str(.Item(objStd.tblField(objStd.fldPos.f38_PrimarySchool).fldName))
                txtPrimaryProvince.Text = DBNull2Str(.Item(objStd.tblField(objStd.fldPos.f39_PrimaryProvince).fldName))
                txtPrimaryYear.Text = Zero2StrNull(.Item(objStd.tblField(objStd.fldPos.f40_PrimaryYear).fldName))
                txtSecondarySchool.Text = DBNull2Str(.Item(objStd.tblField(objStd.fldPos.f41_SecondarySchool).fldName))
                txtSecondaryProvince.Text = DBNull2Str(.Item(objStd.tblField(objStd.fldPos.f42_SecondaryProvince).fldName))
                txtSecondaryYear.Text = Zero2StrNull(.Item(objStd.tblField(objStd.fldPos.f43_SecondaryYear).fldName))
                txtHighSchool.Text = DBNull2Str(.Item(objStd.tblField(objStd.fldPos.f44_HighSchool).fldName))
                txtHighProvince.Text = DBNull2Str(.Item(objStd.tblField(objStd.fldPos.f45_HighProvince).fldName))
                txtHighYear.Text = Zero2StrNull(.Item(objStd.tblField(objStd.fldPos.f46_HighYear).fldName))
                txtContactName.Text = DBNull2Str(.Item(objStd.tblField(objStd.fldPos.f47_ContactName).fldName))
                txtContactAddress.Text = DBNull2Str(.Item(objStd.tblField(objStd.fldPos.f48_ContactAddress).fldName))
                txtContactTel.Text = DBNull2Str(.Item(objStd.tblField(objStd.fldPos.f49_ContactTel).fldName))

                txtGPAX.Text = DBNull2Str(.Item(objStd.tblField(objStd.fldPos.f54_GPAX).fldName))
                'lblLevelClass.Text = DBNull2Str(.Item(objStd.tblField(objStd.fldPos.f56_LevelClass).fldName))
                txtContactRelation.Text = DBNull2Str(.Item(objStd.tblField(objStd.fldPos.f55_ContactRelation).fldName))

                Session("picname") = ""
                If DBNull2Str(.Item(objStd.tblField(objStd.fldPos.f50_PicturePath).fldName)) <> "" Then
                    Session("picname") = .Item(objStd.tblField(objStd.fldPos.f50_PicturePath).fldName)
                    picStudent.ImageUrl = "~/" & stdPic & "/" & .Item(objStd.tblField(objStd.fldPos.f50_PicturePath).fldName)
                End If

                ddlLevelClass.SelectedValue = String.Concat(.Item("LevelClass"))
                If .Item("StudentStatus") = "40" Then
                    ddlLevelClass.SelectedValue = 0
                End If

                txtRegion.Text = String.Concat(.Item("Religion"))
                txtFirstNameEN.Text = String.Concat(.Item("FirstNameEN"))
                txtLastNameEN.Text = String.Concat(.Item("LastNameEN"))
                txtMedicalRecommend.Text = String.Concat(.Item("MedicalRecommend"))
                txtHospital.Text = String.Concat(.Item("HospitalName"))
                txtDoctor.Text = String.Concat(.Item("DoctorName"))
                txtCharacter.Text = String.Concat(.Item("Characteristic"))
                txtFavoriteSubject.Text = String.Concat(.Item("FavoriteSubject"))
                txtExperience.Text = String.Concat(.Item("EduExperience"))
                ddlPayor.SelectedValue = String.Concat(.Item("PayorID"))
                txtPayorDesc.Text = String.Concat(.Item("PayorDetail"))


                txtMotto.Text = String.Concat(.Item("Motto"))

                txtFacebook.Text = String.Concat(.Item("Facebook"))
                txtLineID.Text = String.Concat(.Item("LineID"))

                txtLinkCV.Text = String.Concat(.Item("CVLink"))
                chkAccepted.Checked = ConvertYN2Boolean(String.Concat(.Item("IsAccepted")))
                LoadEducation()
            End With

            If (StrNull2Boolean(Request.Cookies("ROLE_ADM").Value) = True) Or (StrNull2Boolean(Request.Cookies("ROLE_SPA").Value) = True) Then
                cmdDelete.Visible = True
            Else
                cmdDelete.Visible = False
            End If
        Else
            cmdDelete.Visible = False
        End If


    End Sub
    Private Sub LoadPayorToDDL()
        dt = ctlbase.Payor_Get
        If dt.Rows.Count > 0 Then
            With ddlPayor
                .Enabled = True
                .DataSource = dt
                .DataTextField = "PayorName"
                .DataValueField = "PayorID"
                .DataBind()
                '.SelectedIndex = 0
            End With
        End If
        dt = Nothing
    End Sub

    Private Sub LoadProvinceToDDL()
        dt = ctlbase.Province_GetActive
        If dt.Rows.Count > 0 Then
            With ddlProvince
                .Enabled = True
                .DataSource = dt
                .DataTextField = "ProvinceName"
                .DataValueField = "ProvinceID"
                .DataBind()
                '.SelectedIndex = 0
            End With
        End If
        dt = Nothing
    End Sub

    Private Sub BindDayToDDL()
        Dim sD As String = ""
        For i = 1 To 31
            If i < 10 Then
                sD = "0" & i
            Else
                sD = i
            End If

            With ddlDay
                .Items.Add(i)
                .Items(i - 1).Value = sD
                .SelectedIndex = 0
            End With
        Next
    End Sub
    Private Sub BindMonthToDDL()
        Dim sD As String = ""

        For i = 1 To 12
            If i < 10 Then
                sD = "0" & i
            Else
                sD = i
            End If

            With ddlMonth
                .Items.Add(DisplayNumber2Month(i))
                .Items(i - 1).Value = sD
                .SelectedIndex = 0
            End With
        Next
    End Sub

    Private Sub BindYearToDDL()
        Dim ctlb As New ApplicationBaseClass
        Dim sDate As Date
        Dim y As Integer

        sDate = ctlb.GET_DATE_SERVER()

        If sDate.Year < 2500 Then
            y = (sDate.Year + 543)
        Else
            y = sDate.Year
        End If
        Dim i As Integer = 0
        Dim n As Integer = y

        For i = 0 To y - 10
            With ddlYear
                .Items.Add(n)
                .Items(i).Value = n
                .SelectedIndex = 0
            End With
            n = n - 1
            If n < 2510 Then
                Exit For
            End If
        Next
    End Sub

    'Protected Sub cmdClear_Click(sender As Object, e As EventArgs) Handles cmdClear.Click
    '    lblStdCode.Text = ""
    '    ddlPrefix.SelectedIndex = 0
    '    txtFirstNameTH.Text = ""
    '    txtLastNameTH.Text = ""
    '    txtNickName.Text = ""
    '    optGender.SelectedIndex = 0
    '    lblMajorName.Text = ""
    '    'lblMinorName.Text = ""
    '    'lblSubMinorName.Text = ""
    '    lblAdvisorName.Text = ""
    '    txtGPAX.Text = ""
    '    lblLevelClass.Text = ""
    '    txtEmail.Text = ""
    '    ddlDay.SelectedIndex = 0
    '    ddlMonth.SelectedIndex = 0
    '    ddlYear.SelectedIndex = 0
    '    ddlBloodGroup.SelectedIndex = 0
    '    txtTelephone.Text = ""
    '    txtMobile.Text = ""
    '    txtCongenitalDisease.Text = ""
    '    txtMedicineUsually.Text = ""
    '    txtMedicalHistory.Text = ""
    '    txtHobby.Text = ""
    '    txtTalent.Text = ""
    '    txtExpectations.Text = ""
    '    txtFather_FirstName.Text = ""
    '    txtFather_LastName.Text = ""
    '    txtFather_Career.Text = ""
    '    txtFather_Tel.Text = ""
    '    txtMother_FirstName.Text = ""
    '    txtMother_LastName.Text = ""
    '    txtMother_Career.Text = ""
    '    txtMother_Tel.Text = ""
    '    txtSibling.Text = ""
    '    txtChildNo.Text = ""
    '    txtAddress.Text = ""
    '    txtDistrict.Text = ""
    '    txtCity.Text = ""
    '    ddlProvince.SelectedIndex = 0
    '    txtZipCode.Text = ""
    '    txtPrimarySchool.Text = ""
    '    txtPrimaryProvince.Text = ""
    '    txtPrimaryYear.Text = ""
    '    txtSecondarySchool.Text = ""
    '    txtSecondaryProvince.Text = ""
    '    txtSecondaryYear.Text = ""
    '    txtHighSchool.Text = ""
    '    txtHighProvince.Text = ""
    '    txtHighYear.Text = ""
    '    txtContactName.Text = ""
    '    txtContactTel.Text = ""
    '    txtContactAddress.Text = ""
    '    txtContactRelation.Text = ""
    '    hdEduUID.Value = "0"
    '    cmdPrint.Visible = False
    '    txtFacebook.Text = ""
    '    txtLineID.Text = ""
    '    txtMotto.Text = ""
    'End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click

        Dim BDate As String = ddlDay.SelectedValue
        BDate = ddlYear.SelectedValue & ddlMonth.SelectedValue & BDate
        If txtGPAX.Text <> "" Then
            If Not IsNumeric(txtGPAX.Text) Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','GPAX ไม่ถูกต้อง');", True)
                Exit Sub
            End If
        End If

        If chkAccepted.Checked = False Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','นักศึกษาต้องอ่านและยืนยันการยอมรับเงื่อนไขในบันทึกข้อตกลงก่อน');", True)
            Exit Sub
        End If

        ctlStd.Student_Update(lblStdCode.Text, ddlPrefix.SelectedValue, txtFirstNameTH.Text, txtLastNameTH.Text, txtNickName.Text, optGender.SelectedValue, txtEmail.Text, BDate, ddlBloodGroup.SelectedValue, txtTelephone.Text, txtMobile.Text, txtCongenitalDisease.Text, txtMedicineUsually.Text, txtMedicalHistory.Text, txtHobby.Text, txtTalent.Text, txtExpectations.Text, txtFather_FirstName.Text, txtFather_LastName.Text, txtFather_Career.Text, txtFather_Tel.Text, txtMother_FirstName.Text, txtMother_LastName.Text, txtMother_Career.Text, txtMother_Tel.Text, StrNull2Zero(txtSibling.Text), StrNull2Zero(txtChildNo.Text), txtAddressCard.Text, "", "", StrNull2Zero(ddlProvince.SelectedValue), ddlProvince.SelectedItem.Text, txtZipCode.Text, txtPrimarySchool.Text, txtPrimaryProvince.Text, StrNull2Zero(txtPrimaryYear.Text), txtSecondarySchool.Text, txtSecondaryProvince.Text, StrNull2Zero(txtSecondaryYear.Text), txtHighSchool.Text, txtHighProvince.Text, StrNull2Zero(txtHighYear.Text), txtContactName.Text, txtContactTel.Text, txtContactAddress.Text, txtContactRelation.Text, Session("picname"), Request.Cookies("UserLoginID").Value, StrNull2Double(txtGPAX.Text), txtAddressLive.Text, txtTelLive.Text, txtFirstNameEN.Text, txtLastNameEN.Text, txtRegion.Text, txtMedicalRecommend.Text, txtHospital.Text, txtDoctor.Text, txtCharacter.Text, txtFavoriteSubject.Text, txtExperience.Text, StrNull2Zero(ddlPayor.SelectedValue), txtPayorDesc.Text, txtLinkCV.Text, txtMotto.Text, txtFacebook.Text, txtLineID.Text, StrNull2Zero(ddlMajor.SelectedValue), StrNull2Zero(ddlAdvisor.SelectedValue), StrNull2Zero(ddlLevelClass.SelectedValue), StrNull2Double(txtWL.Text), StrNull2Double(txtHL.Text), txtCardID.Text)

        Dim objuser As New UserController
        If txtEmail.Text <> "" Then
            objuser.User_UpdateMail(lblStdCode.Text, txtEmail.Text)
        End If

        objuser.User_GenLogfile(Request.Cookies("UserLogin").Value, ACTTYPE_UPD, "Student", "บันทึก/แก้ไข ประวัตินักศึกษา :" & lblStdCode.Text, "")
        cmdPrint.Visible = True

        'lblAlert.Text = "บันทึกข้อมูลเรียบร้อย"

        Response.Redirect("ResultPage.aspx?ActionType=std&ItemType=std&p=complete&stdid=" & lblStdCode.Text)

        'DisplayMessage(Me.Page, "บันทึกข้อมูลเรียบร้อย")
    End Sub

    Private Sub UploadFile()

        Dim pf As HttpPostedFileAJAX = FileUploaderAJAX1.PostedFile

        'กรณีต้องการต้องสอบชนิดและขนาดไฟล์
        If ((pf.ContentType.Equals("image/jpeg") Or pf.ContentType.Equals("image/jpg")) And pf.ContentLength <= 500 * 1024) Then



            Dim objfile As FileInfo = New FileInfo(Server.MapPath("~/" & stdPic & "/" & lblStdCode.Text & ".jpg"))

            If objfile.Exists Then
                objfile.Delete()
            End If

            FileUploaderAJAX1.SaveAs("~/" & stdPic, lblStdCode.Text & ".jpg")

            Session("picname") = lblStdCode.Text & ".jpg"

            ' picStudent.ImageUrl = "~/" & stdPic & "/" & lblStdCode.Text & ".jpg"

        Else

            lblAlert.Text = "ไม่สามารถอัปโหลดรูปท่านได้ เนื่องจากขนาดรูปท่านใหญ่เกิน 500K กรุณาลองใหม่ภายหลัง"

            'DisplayMessage(Me.Page, "ไม่สามารถอัปโหลดรูปท่านได้ เนื่องจากขนาดรูปท่านใหญ่เกิน 500K กรุณาลองใหม่ภายหลัง")
            'ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)
        End If

    End Sub
    Dim ctlEdu As New EducationController

    Private Sub LoadEducation()
        dt = ctlEdu.Education_Get(StrNull2Zero(hdStudentID.Value))
        grdEdu.DataSource = dt
        grdEdu.DataBind()
    End Sub

    Protected Sub cmdAddEdu_Click(sender As Object, e As EventArgs) Handles cmdAddEdu.Click
        If txtCourse.Text = "" Or txtSchool.Text = "" Or txtProvinceEdu.Text = "" Or txtYearEdu.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุข้อมูลให้ครบถ้วนก่อน');", True)
            Exit Sub
        End If

        ctlEdu.Education_Save(StrNull2Zero(hdEduUID.Value), StrNull2Zero(hdStudentID.Value), txtCourse.Text, txtSchool.Text, txtProvinceEdu.Text, txtYearEdu.Text)

        LoadEducation()
        hdEduUID.Value = "0"
        txtCourse.Text = ""
        txtSchool.Text = ""
        txtProvinceEdu.Text = ""
        txtYearEdu.Text = ""
        txtCourse.Focus()
    End Sub

    Private Sub cmdDelete_Click(sender As Object, e As EventArgs) Handles cmdDelete.Click
        ctlStd.Student_Delete(lblStdCode.Text)
        Response.Redirect("ResultPage.aspx?ActionType=std&ItemType=std&p=delete")
    End Sub
End Class