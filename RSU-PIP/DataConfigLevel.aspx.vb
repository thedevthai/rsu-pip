﻿Public Class DataConfigLevel
    Inherits System.Web.UI.Page

    Dim dt As New DataTable
    Dim ctlCfg As New StudentController
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("UserLogin").Value) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If
        If Not IsPostBack Then
            isAdd = True
            txtCode.ReadOnly = True
            LoadCategoryToGrid()
        End If
        '  txtCode.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")
        'txtValues.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")
    End Sub
    Private Sub LoadCategoryToGrid()
        dt = ctlCfg.Student_GetLevelClass
        If dt.Rows.Count > 0 Then
            With grdData
                .Visible = True
                .DataSource = dt
                .DataBind()

            End With
        Else
            grdData.DataSource = Nothing
            grdData.Visible = False
        End If
        dt = Nothing
    End Sub

    Private Sub grdData_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdData.RowCommand

        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit"
                    EditData(e.CommandArgument)
            End Select

        End If


    End Sub
    Private Sub EditData(pIndex As Integer)

        isAdd = False
        'chkPass.Checked = False
        txtCode.ReadOnly = True
        Me.txtCode.Text = grdData.Rows(pIndex).Cells(1).Text

        If IsNumeric(grdData.Rows(pIndex).Cells(1).Text) Then
            ddlLevel2.SelectedValue = grdData.Rows(pIndex).Cells(2).Text
        Else
            ddlLevel2.SelectedIndex = 0
            'chkPass.Checked = True
        End If
        If grdData.Rows(pIndex).Cells(0).Text = "การบริบาลทางเภสัชกรรม" Then
            ddlMajor2.SelectedValue = 2
        ElseIf grdData.Rows(pIndex).Cells(0).Text = "Non-Degree" Then
            ddlMajor2.SelectedValue = 3
        ElseIf grdData.Rows(pIndex).Cells(0).Text = "เภสัชกรรมอุตสาหการ" Then
            ddlMajor2.SelectedValue = 1
        Else
            ddlMajor2.SelectedValue = 0
        End If

    End Sub
     
    Private Sub ClearData()
        txtCode.ReadOnly = True
        Me.txtCode.Text = ""
        ddlLevel2.SelectedIndex = 0
        isAdd = True
    End Sub
    Dim item As Integer


    Private Sub grdData_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdData.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#ffdfef';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If
    End Sub

    Protected Sub lnkSubmitAll_Click(sender As Object, e As EventArgs) Handles lnkSubmitAll.Click
        Try
            item = ctlCfg.StudentLevel_UpdateOneLevel

            LoadCategoryToGrid()
            ClearData()
             ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)
        Catch ex As Exception

            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ไม่สามารถบันทึกข้อมูลได้ กรุณาตรวจสอบและลองใหม่อีกครั้ง');", True)
        End Try
    End Sub

    Protected Sub cmdSaveByChange_Click(sender As Object, e As EventArgs) Handles cmdSaveByChange.Click

        If ddlLevelBegin.Text = "" Or ddlLevelEnd.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาเลือกชั้นปีให้ครบถ้วน');", True)
            Exit Sub
        End If
        If ddlLevelBegin.SelectedValue = ddlLevelEnd.SelectedValue Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาเลือกชั้นปีให้ถูกต้อง');", True)
            Exit Sub
        End If

        Dim A, B, MajorID As Integer

        MajorID = StrNull2Zero(ddlMajor1.SelectedValue)
        A = StrNull2Zero(ddlLevelBegin.SelectedValue)
        B = StrNull2Zero(ddlLevelEnd.SelectedValue)

        ctlCfg.Student_UpdateLevelClass(MajorID, A, B)

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)
        LoadCategoryToGrid()

    End Sub
    Protected Sub cmdSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdSave.Click

        If txtCode.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณากรอกข้อมูลให้ครบถ้วน');", True)

            Exit Sub
        End If
        Try
            Dim isPass As String = "N"
            If ddlLevel2.SelectedValue = 7 Then
                isPass = "Y"
            Else
                isPass = "N"
            End If

            item = ctlCfg.StudentLevel_UpdateByCode(StrNull2Zero(ddlMajor2.SelectedValue), txtCode.Text, StrNull2Zero(ddlLevel2.SelectedValue), isPass)
            LoadCategoryToGrid()
            ClearData()
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)
        Catch ex As Exception
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ไม่สามารถบันทึกข้อมูลได้ กรุณาตรวจสอบและลองใหม่อีกครั้ง');", True)

        End Try
    End Sub


End Class