﻿Public Class AssesseeEvaluationGroup
    Inherits System.Web.UI.Page

    Dim ctlLG As New AssessmentController
    Dim ctlStd As New StudentController
    Dim ctlCs As New Coursecontroller
    Dim dt As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("UserLogin").Value) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If
        If Not IsPostBack Then

            lblYear.Text = Request.Cookies("ASSMYEAR").Value
            LoadStudentInfo()
                LoadCourse()
                LoadEvaluationGroupToGrid()


            LoadAssessmentInfo()
            LoadAssessorInfo()

        End If

    End Sub

    Private Sub LoadAssessorInfo()
        Dim ctlPs As New PersonController
        lblAssessorName.Text = ctlPs.Person_GetName(Session("assessoruid"))
        If Session("assessorroleid") = "P" Then
            lblAssessorRole.Text = "อ.แหล่งฝึก"
        Else
            lblAssessorRole.Text = "อ.ประจำรายวิชา"
        End If

    End Sub
    Private Sub LoadAssessmentInfo()
        dt = ctlStd.GetAssessmentInfo(lblYear.Text, lblCode.Text, lblSubjectCode.Text)
        If dt.Rows.Count > 0 Then
            'lblPhaseNo.Text = ctlStd.GetPhaseNoOfAssessment(lblYear.Text, lblCode.Text, lblSubjectCode.Text)
            lblPhaseNo.Text = dt.Rows(0)("PhaseNo")
            lblLocationID.Text = dt.Rows(0)("LocationID")
            lblLocationName.Text = dt.Rows(0)("LocationName")
        End If

    End Sub
    Private Sub LoadStudentInfo()
        dt = ctlStd.GetStudent_ByID(Request("std"))
        If dt.Rows.Count > 0 Then
            lblCode.Text = String.Concat(dt.Rows(0)("Student_Code"))
            lblStudentName.Text = String.Concat(dt.Rows(0)("StudentName"))
            'lblLevelClass.Text = String.Concat(dt.Rows(0)("LevelClass"))
            lblMajorName.Text = String.Concat(dt.Rows(0)("MajorName"))
            lblNickName.Text = String.Concat(dt.Rows(0)("NickName"))
        End If
        dt = Nothing
    End Sub
    Private Sub LoadCourse()
        dt = ctlCs.Courses_GetSubjectBySubjectCode(Request("Sj"))
        If dt.Rows.Count > 0 Then
            lblSubjectCode.Text = RTrim(dt.Rows(0)("SubjectCode"))
            lblSubjectName.Text = dt.Rows(0)("SubjectName")
        End If
        dt = Nothing
    End Sub

    Private Sub LoadEvaluationGroupToGrid()

        If Session("assessorroleid") = "A" Then
            dt = ctlLG.EvaluationGroup_GetAssessment(StrNull2Zero(lblYear.Text), lblSubjectCode.Text, "A", lblCode.Text, Session("assessoruid"))
        ElseIf Session("assessorroleid") = "P" Then
            dt = ctlLG.EvaluationGroup_GetAssessment(StrNull2Zero(lblYear.Text), lblSubjectCode.Text, "P", lblCode.Text, Session("assessoruid"))
        Else
            Response.Redirect("ResultPage.aspx")
        End If

        If dt.Rows.Count > 0 Then
            With grdData
                .Visible = True
                .DataSource = dt
                .DataBind()
            End With
        Else
            grdData.DataSource = Nothing
        End If


        dt = Nothing
    End Sub

    Private Sub grdData_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdData.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit"
                    Response.Redirect("AssessmentStudent.aspx?p=a&std=" & lblCode.Text & "&guid=" & e.CommandArgument() & "&sj=" & lblSubjectCode.Text)
            End Select
        End If
    End Sub
    Private Sub grdData_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdData.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#ffdfef';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")
        End If
    End Sub

    Protected Sub cmdBack_Click(sender As Object, e As EventArgs) Handles cmdBack.Click
        Response.Redirect("AssesseeList.aspx?p=a&sj=" & lblSubjectCode.Text)
    End Sub
End Class

