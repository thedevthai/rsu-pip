﻿Public Class ResultPage
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        hlnkBack.NavigateUrl = "Student_Reg.aspx?ActionType=std&ItemType=std&stdid=" & Request("stdid")
        hlnkPrint.NavigateUrl = "ReportViewerStudentBio.aspx?ActionType=std&id=" & Request("stdid")
        lblMsg.Text = "บันทึกข้อมูลเรียบร้อย"

        If Request("t") = "closed" Then
            hlnkBack.Visible = False
            hlnkPrint.Visible = False
        ElseIf Request("t") = "p" Then
            hlnkBack.Visible = False
            hlnkPrint.Visible = False
        End If

        Select Case Request("p")
            Case "select"
                lblMsg.Text = "ยังไม่ถึงกำหนด หรือ หมดเขตการเลือกแหล่งฝึกแล้ว <br/> กรุณาติดต่อ ผู้ดูแลระบบ/อาจารย์ผู้ประสานงานรายวิชา"
            Case "result"
                lblMsg.Text = "ยังไม่ถึงกำหนดวันประกาศผล <br/> กรุณาติดต่อ ผู้ดูแลระบบ/อาจารย์ผู้ประสานงานรายวิชา"
            Case "complete"
                lblMsg.Text = "บันทึกข้อมูลเรียบร้อย"
            Case "unrole"
                lblMsg.Text = "ท่านไม่มีสิทธิ์เข้าถึงข้อมูลนี้ <br/> กรุณาติดต่อ ผู้ดูแลระบบ"
                hlnkBack.Visible = False
                hlnkPrint.Visible = False
            Case "assm"
                lblMsg.Text = "หมดเขตประเมินแล้ว <br/> กรุณาติดต่อ ผู้ดูแลระบบ"
            Case "delete"
                lblMsg.Text = "ลบข้อมูลเรียบร้อย"
                lblMsg.ForeColor = System.Drawing.Color.Red
                hlnkBack.NavigateUrl = "Student.aspx?ActionType=std&ItemType=std"
                hlnkBack.Text = "กลับไปหน้ารายชื่อนักศึกษา"
                hlnkBack.Visible = True
                hlnkPrint.Visible = False
            Case Else
                lblMsg.Text = "ท่านไม่มีสิทธิ์เข้าถึงข้อมูลส่วนนี้ <br/> กรุณาติดต่อ ผู้ดูแลระบบ/อาจารย์ผู้ประสานงานรายวิชา"
                hlnkBack.Visible = False
                hlnkPrint.Visible = False
        End Select



    End Sub

End Class