﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Vaccine.aspx.vb" Inherits=".Vaccine" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server"> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <section class="content-header">
      <h1>ข้อมูลประวัติการรับวัคซีน</h1>   
    </section>

<section class="content">  
<div class="row">  
    <section class="col-lg-12 connectedSortable">
     <div class="box box-default">
            <div class="box-header">
              <i class="fa fa-user-circle"></i>

              <h3 class="box-title">ข้อมูลนักศึกษา</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                                 
            </div>
            <div class="box-body"> 
                <table width="100%" border="0" class="table table-hover" cellPadding="1" cellSpacing="1" >
<tr>
                                                    <td width="80" class="texttopic">
                                                        รหัสนักศึกษา</td>
                                                    <td>
                                                        <asp:Label ID="lblStudentCode" runat="server"></asp:Label>
                                                    </td>                                                  
                                                    <td width="100" class="texttopic">ชื่อ - นามสกุล</td>
                                                    <td>
                                                        <asp:Label ID="lblStudentName" runat="server"></asp:Label>
                                                    </td>
</tr>
<tr>
  <td class="texttopic">ชื่อเล่น</td>
  <td><asp:Label ID="lblNickName" runat="server"></asp:Label></td>
  <td class="texttopic">สาขา</td>
  <td><asp:Label ID="lblMajorName" runat="server"></asp:Label></td>
</tr>
</table>  
                         
</div>
            <div class="box-footer clearfix">
           
            </div>
          </div>
 </section>
        </div>
<div class="row">
 <section class="col-lg-6 connectedSortable">                         
    <div class="box box-success">
            <div class="box-header">
              <i class="fa fa-thumbs-up"></i>

              <h3 class="box-title">บันทึกข้อมูลวัคซีนทั่วไป</h3>
                <asp:HiddenField ID="hdGUID" runat="server" />
            </div>
            <div class="box-body">       
                   <div class="row">
                <div class="col-md-3">
          <div class="form-group">
            <label>วันที่</label>
               <asp:TextBox ID="txtGenDate" runat="server"  cssclass="form-control text-center"  autocomplete="off" data-date-format="dd/mm/yyyy"
                    data-date-language="th-th" data-provide="datepicker"  onkeyup="chkstr(this,this.value)"></asp:TextBox> 
          </div>
        </div>  
                      
  <div class="col-md-4">
          <div class="form-group">
            <label>ชื่อวัคซีน</label>
              <asp:DropDownList ID="ddlVaccineGeneral" runat="server" cssclass="form-control select2">
                  <asp:ListItem Value="ไข้หวัดใหญ่">ไข้หวัดใหญ่</asp:ListItem>
                  <asp:ListItem Value="บาดทะยัก">บาดทะยัก</asp:ListItem>
                  <asp:ListItem Value="พิษสุนัขบ้า">พิษสุนัขบ้า</asp:ListItem>
                  <asp:ListItem Value="คอตีบ">คอตีบ</asp:ListItem>
                  <asp:ListItem Value="Hepatitis B">Hepatitis B</asp:ListItem>
                  <asp:ListItem Value="อื่นๆ">อื่นๆ</asp:ListItem> 
              </asp:DropDownList>                
          </div>

        </div>                  
    <div class="col-md-5">
          <div class="form-group">
            <label>ชื่อสถานพยาบาล</label>
               <asp:TextBox ID="txtHospitalGeneral" runat="server"  cssclass="form-control text-center" BackColor="White"></asp:TextBox> 
          </div>

        </div>   
                        </div>  
       <div class="row">    
                        <div class="col-md-3">
          <div class="form-group">
            <label>เข็มที่ (ไม่บังคับ)</label>
               <asp:TextBox ID="txtOrderNoGen" runat="server"  cssclass="form-control text-center" BackColor="White"></asp:TextBox> 
          </div>
        </div>
             <div class="col-md-5">
          <div class="form-group">
            <label> </label>
              <p>
              <asp:Button ID="cmdSaveGeneral" runat="server" Text="บันทึก" CssClass="btn btn-save" Width="80px" />  
                   <asp:Button ID="cmdCancelGeneral" runat="server" Text="ยกเลิก" CssClass="btn btn-default" Width="80px" />  
          </p></div>

        </div>       

                            </div>
         <div class="row text-center">  
              <div class="col-md-12">
  <asp:GridView ID="grdGeneral" 
                             runat="server" CellPadding="0" 
                                                        GridLines="None" CssClass="table table-hover" 
                             Font-Bold="False" Width="100%" AutoGenerateColumns="False">
                        <RowStyle BackColor="#F7F7F7" />
                        <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />                     
                        <pagerstyle HorizontalAlign="Center" 
                             CssClass="dc_pagination dc_paginationC dc_paginationC11" />                     
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <headerstyle CssClass="table table-hover" Font-Bold="True" HorizontalAlign="Center" 
                                                            />                     
                        <Columns>
                            <asp:BoundField DataField="VaccineDTT" HeaderText="วันที่" />
                            <asp:BoundField HeaderText="เข็มที่" DataField="OrderNo" />
                            <asp:BoundField HeaderText="ชื่อวัคซีน" DataField="VaccineName" />
                            <asp:BoundField DataField="LotNo" HeaderText="Lot No." >
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" Width="100px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="HospitalName" HeaderText="สถานบริการ" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                   <asp:ImageButton ID="imgEditG" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID") %>' ImageUrl="images/icon-edit.png" /> 
                                    <asp:ImageButton ID="imgDelG" runat="server"  CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID") %>' ImageUrl="images/delete.png" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="65px" />
                            </asp:TemplateField>
                        </Columns>
                        <EditRowStyle BackColor="#2461BF" />
                        <AlternatingRowStyle BackColor="White" />
                     </asp:GridView> 
</div>

</div>



</div>
            <div class="box-footer">
                
            </div>
          </div>    
 
</section>
 <section class="col-lg-6 connectedSortable"> 
    <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-thumbs-up"></i>

              <h3 class="box-title">บันทึกข้อมูลการรับวัคซีน Covid-19</h3>
                  <asp:HiddenField ID="hdCUID" runat="server" />
            </div>
            <div class="box-body">       
                   <div class="row">
                <div class="col-md-3">
          <div class="form-group">
            <label>วันที่</label>
               <asp:TextBox ID="txtCovidDate" runat="server"  cssclass="form-control text-center"  autocomplete="off" data-date-format="dd/mm/yyyy"
                      data-date-language="th-th" data-provide="datepicker" onkeyup="chkstr(this,this.value)"></asp:TextBox> 
          </div>
        </div>  
                       <div class="col-md-2">
          <div class="form-group">
            <label>เข็มที่</label>
               <asp:DropDownList ID="ddlOrderNo" runat="server" cssclass="form-control select2">
                  <asp:ListItem Value="1">1</asp:ListItem>
                  <asp:ListItem Value="2">2</asp:ListItem>
                  <asp:ListItem Value="3">3</asp:ListItem>
                  <asp:ListItem Value="4">4</asp:ListItem>
              </asp:DropDownList>   
          </div>
        </div>
  <div class="col-md-4">
          <div class="form-group">
            <label>ชื่อวัคซีน</label>
              <asp:DropDownList ID="ddlVaccineCovid" runat="server" cssclass="form-control select2">
                  <asp:ListItem Value="CoronaVac">CoronaVac (Sinovac)</asp:ListItem>
                  <asp:ListItem Value="AstraZeneca">AstraZeneca</asp:ListItem>
                  <asp:ListItem Value="Pfizer">Pfizer</asp:ListItem>
                  <asp:ListItem Value="Moderna">Moderna</asp:ListItem>
                  <asp:ListItem Value="Sinopharm">Sinopharm</asp:ListItem>
                  <asp:ListItem Value="Johnson & Johnson">Johnson &amp; Johnson</asp:ListItem>
                  <asp:ListItem Value="Novavax">Novavax</asp:ListItem>
              </asp:DropDownList>                
          </div>

        </div>         
         
              <div class="col-md-3">
          <div class="form-group">
            <label>Lot No</label>
               <asp:TextBox ID="txtLotNo" runat="server"  cssclass="form-control text-center" BackColor="White"></asp:TextBox> 
          </div>

        </div>  
   </div>  
       <div class="row">          
    <div class="col-md-7">
          <div class="form-group">
            <label>ชื่อสถานพยาบาล</label>
               <asp:TextBox ID="txtHospitalName1" runat="server"  cssclass="form-control text-center" BackColor="White"></asp:TextBox> 
          </div>

        </div>   
        <div class="col-md-5">
          <div class="form-group">
            <label> </label>
              <p>
 <asp:Button ID="cmdSaveCovid" runat="server" Text="บันทึก" CssClass="btn btn-save" Width="80px" />  
                   <asp:Button ID="cmdCancelCovid" runat="server" Text="ยกเลิก" CssClass="btn btn-default" Width="80px" />  
          </p></div>

        </div>                  
           
       </div>      

                    <div class="row text-center">  
                          <div class="col-md-12">
   <asp:GridView ID="grdCovid" 
                             runat="server" CellPadding="0" 
                                                        GridLines="None" CssClass="table table-hover" 
                             Font-Bold="False" Width="100%" AutoGenerateColumns="False">
                        <RowStyle BackColor="#F7F7F7" />
                        <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />                     
                        <pagerstyle HorizontalAlign="Center" 
                             CssClass="dc_pagination dc_paginationC dc_paginationC11" />                     
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <headerstyle CssClass="table table-hover" Font-Bold="True" HorizontalAlign="Center" 
                                                            />                     
                        <Columns>
                            <asp:BoundField DataField="VaccineDTT" HeaderText="วันที่" />
                            <asp:BoundField HeaderText="เข็มที่" DataField="OrderNo" />
                            <asp:BoundField HeaderText="ชื่อวัคซีน" DataField="VaccineName" />
                            <asp:BoundField DataField="LotNo" HeaderText="Lot No." >
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" Width="100px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="HospitalName" HeaderText="สถานบริการ" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                   <asp:ImageButton ID="imgEditC" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID") %>' ImageUrl="images/icon-edit.png" /> 
                                    <asp:ImageButton ID="imgDelC" runat="server"  CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID") %>' ImageUrl="images/delete.png" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="65px" />
                            </asp:TemplateField>
                        </Columns>
                        <EditRowStyle BackColor="#2461BF" />
                        <AlternatingRowStyle BackColor="White" />
                     </asp:GridView> 
                  </div>     
</div>

</div>
            <div class="box-footer">
                
            </div>
          </div>
     
     
    <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-thumbs-up"></i>
              <h3 class="box-title">Vaccine Covid-19 Certificate</h3> 
            </div>
            <div class="box-body">       
                  <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>ไฟล์เอกสาร Certificate (PDF,.jpg)</label>
                                            <asp:FileUpload ID="FileUpload1" runat="server" Width="80%" />
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label></label>
                                            <asp:HyperLink ID="hlnkDoc" runat="server" CssClass="btn btn-success" Target="_blank">[hlnkDoc]
                                            </asp:HyperLink>

                                        </div>
                                    </div>
                                </div>
                 <div class="row">  
                    <div class="col-md-12">
                        <asp:Button ID="cmdUpload" CssClass="btn btn-primary" runat="server" Text="Upload" Width="120px" />                         

                    </div>
                </div>
                </div>
        </div>
 </section> 


</div>
</section>    
</asp:Content>
