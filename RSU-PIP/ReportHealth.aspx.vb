﻿Imports Microsoft.Reporting.WebForms
Public Class ReportHealth
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlH As New HealthController
    Dim ctlCs As New CourseController
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("UserLogin").Value) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If
        If Not IsPostBack Then
            lblResult.Visible = False
            LoadYearToDDL()
            LoadProvinceToDDL()
            LoadLocationToDDL()

        End If
    End Sub
    Private Sub LoadYearToDDL()
        Dim y As Integer = StrNull2Zero(DisplayYear(ctlCs.GET_DATE_SERVER))
        Dim LastRow As Integer
        dt = ctlCs.Courses_GetYear
        LastRow = dt.Rows.Count - 1

        If dt.Rows.Count > 0 Then
            With ddlYear
                .Enabled = True
                .DataSource = dt
                .DataTextField = "CYear"
                .DataValueField = "CYear"
                .DataBind()

                If dt.Rows(LastRow)(0) = y Then
                    ddlYear.Items.Add(y + 1)
                    ddlYear.Items(LastRow + 1).Value = y + 1
                ElseIf dt.Rows(LastRow)(0) > y Then
                    'ddlYear.Items.Add(y + 2)
                    'ddlYear.Items(LastRow + 1).Value = y + 2
                ElseIf dt.Rows(LastRow)(0) < y Then
                    ddlYear.Items.Add(y)
                    ddlYear.Items(LastRow + 1).Value = y
                    ddlYear.Items.Add(y + 1)
                    ddlYear.Items(LastRow + 2).Value = y + 1
                End If
                .SelectedIndex = 0
            End With
        Else
            ddlYear.Items.Add(y)
            ddlYear.Items(0).Value = y
            ddlYear.Items.Add(y + 1)
            ddlYear.Items(1).Value = y + 1
            ddlYear.SelectedIndex = 0
        End If
        ddlYear.SelectedValue = Request.Cookies("EDUYEAR").Value
        dt = Nothing
    End Sub
    Private Sub LoadProvinceToDDL()
        Dim ctlbase As New ApplicationBaseClass
        dt = ctlbase.Province_GetByZoneID(ddlZone.SelectedValue)
        If dt.Rows.Count > 0 Then
            With ddlProvince
                .Enabled = True
                .DataSource = dt
                .DataTextField = "ProvinceName"
                .DataValueField = "ProvinceID"
                .DataBind()
                .SelectedIndex =0
            End With
        Else
            With ddlProvince
                .Items.Add("")
                .Items(0).Value = 0
            End With
        End If
        dt = Nothing
    End Sub
    Private Sub LoadLocationToDDL()
        ddlLocation.Items.Clear()
        Dim ctlL As New LocationController

        dt = ctlL.Location_GetByProvince(StrNull2Zero(ddlProvince.SelectedValue))

        If dt.Rows.Count > 0 Then
            ddlLocation.Items.Clear()

            With ddlLocation
                .Enabled = True
                .DataSource = dt
                .DataTextField = "LocationName"
                .DataValueField = "LocationID"
                .DataBind()
                .SelectedIndex = 0
            End With

            'With ddlLocation
            '    .Items.Add("")
            '    .Items(0).Value = 0

            '    For i = 0 To dt.Rows.Count - 1
            '        .Items.Add("" & dt.Rows(i)("LocationName"))
            '        .Items(i + 1).Value = dt.Rows(i)("LocationID")
            '    Next
            'End With
        Else
            With ddlLocation
                .Items.Add("")
                .Items(0).Value = 0
            End With
        End If

    End Sub
    Private Sub LoadReport()

        System.Threading.Thread.Sleep(1000)
        UpdateProgress1.Visible = True
        UpdatePanel1.Visible = True

        Dim credential As New ReportServerCredentials
        ReportViewer1.Reset()
        ReportViewer1.ServerReport.ReportServerCredentials = credential
        ReportViewer1.ServerReport.ReportServerUrl = credential.ReportServerUrl
        ReportViewer1.ShowPrintButton = True
        ReportViewer1.Height = 500
        'ReportViewer1.ZoomPercent = 100


        'System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("th-TH")
        Dim xParam As New List(Of ReportParameter)


        ReportViewer1.ServerReport.ReportPath = credential.ReportPath(ReportName)


        'Select Case FagRPT

        '    Case "REQACCOUNT"

        xParam.Add(New ReportParameter("UserID", Request.Cookies("UserLoginID").Value))
        'If Request.Cookies("ROLE_STD").Value = True Then
        '    xParam.Add(New ReportParameter("StudentCode", Request.Cookies("UserLogin").Value.ToString()))
        'Else
        '    xParam.Add(New ReportParameter("StudentCode", "0"))
        'End If


        'End Select


        ReportViewer1.ServerReport.SetParameters(xParam)
        ReportViewer1.ServerReport.Refresh()

        UpdateProgress1.Visible = False
    End Sub


    Protected Sub cmdPrint3_Click(sender As Object, e As EventArgs) Handles cmdView.Click
        Select Case Request("ItemType")
            Case "h1"
                ctlH.Gen_StudentVaccineCovid(ddlYear.SelectedValue, ddlLevel.SelectedValue, ddlZone.SelectedValue, ddlProvince.SelectedValue, ddlLocation.SelectedValue, Request.Cookies("UserLoginID").Value)
                ReportName = "StudentVaccineCovid"
            Case "h2"

            Case "h3"

            Case "h4"


        End Select


        LoadReport()
        UpdateProgress1.Visible = False
    End Sub

    Protected Sub ddlZone_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlZone.SelectedIndexChanged
        LoadProvinceToDDL()
    End Sub

    Protected Sub ddlProvince_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlProvince.SelectedIndexChanged
        LoadLocationToDDL()
    End Sub
End Class

