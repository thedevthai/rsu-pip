﻿<%@ Page Title="" Language="vb" AutoEventWireup="false"  MasterPageFile="~/Site.Master" CodeBehind="DataConfigLevel.aspx.vb" Inherits=".DataConfigLevel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server"><title> Category : ระบบฐานข้อมูลงานอนุญาต ตาม พ.ร.บ. การสาธารณสุข พ.ศ. 2535 </title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
<section class="content-header">
      <h1>กำหนดระดับชั้นให้นักศึกษา</h1>   
</section>

<section class="content">  

         <div class="box box-pink">

            <div class="box-header">
              <i class="fa fa-filter"></i>

              <h3 class="box-title">กำหนดทั้งหมดแบบครั้งเดียว</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                                 
            </div>
            <div class="box-body"> 
              <table border="0" cellspacing="2" cellpadding="0">
                  <TR>
				     <TD   class="MenuSt"> 
				     กำหนดระดับชั้นให้กับนักศึกษาแบบเลื่อนชั้นไป 1 ระดับชั้นทุกคน โดยทำการคลิกปุ่ม &quot;เลื่อนระดับชั้น&quot; ด้านล่าง</td>
                       </tr>
                       <tr>
                         <td  class="MenuSt"><strong><u>ข้อควรระวัง</u></strong> ควรกดปุ่มเลื่อนระดับชั้น ปีละครั้งเท่านั้น และควรตรวจสอบความถูกต้องจากข้อมูลในตารางข้างล่าง</td>
                       </tr>
                       <tr>
                         <td><asp:Button ID="lnkSubmitAll" runat="server"  CssClass="buttonModal" text="เลื่อนระดับชั้น" /> 
                           </td>
                       </tr>
                     </table>
 </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>
    
    <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-search-plus"></i>

              <h3 class="box-title">กำหนดแบบระบุชั้นปี</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                                 
            </div>
            <div class="box-body"> 
            
            <TABLE id="TableA"  class="table" width="100%" bgColor="#ffffff" border="0">
                <TR>
				     <TD   class="MenuSt">**คำอธิบาย** เปลี่ยนชั้นปีนักศึกษาทั้งหมดที่อยู่ชั้นปี A ไป B</TD>
				     </TR>

                      <TR>
									<TD align="center" >
                                        <table cellSpacing="1" cellPadding="1" border="0">
											<tr>
												<td align="left">ชั้นปีที่</td>
						              
												<td align="left" >
                                                    <asp:DropDownList ID="ddlLevelBegin" runat="server" CssClass="form-control select2" Width="50px">
                                                        <asp:ListItem>1</asp:ListItem>
                                                        <asp:ListItem>2</asp:ListItem>
                                                        <asp:ListItem>3</asp:ListItem>
                                                        <asp:ListItem>4</asp:ListItem>
                                                        <asp:ListItem>5</asp:ListItem>
                                                        <asp:ListItem>6</asp:ListItem> 
                                                    </asp:DropDownList>
						            </td>
                                                	<td align="left">สาขา</td>						              
												<td align="left">
                                                    <asp:DropDownList ID="ddlMajor1" runat="server" CssClass="form-control select2" Width="200px">
                                                        <asp:ListItem Value="1">เภสัชกรรมอุตสาหการ</asp:ListItem>
                                                        <asp:ListItem Value="2">การบริบาลทางเภสัชกรรม</asp:ListItem>
                                                        <asp:ListItem Value="3">Non-Degree</asp:ListItem>
                                                    </asp:DropDownList>
						            </td>
												<td align="left">เปลี่ยนเป็น ชั้นปีที่</td>
						              
												<td align="left">
                                                    <asp:DropDownList ID="ddlLevelEnd" runat="server" CssClass="form-control select2">
                                                        <asp:ListItem>1</asp:ListItem>
                                                        <asp:ListItem>2</asp:ListItem>
                                                        <asp:ListItem>3</asp:ListItem>
                                                        <asp:ListItem>4</asp:ListItem>
                                                        <asp:ListItem>5</asp:ListItem>
                                                        <asp:ListItem>6</asp:ListItem>
                                                        <asp:ListItem Value="7">สำเร็จการศึกษา</asp:ListItem>
                                                    </asp:DropDownList>                                             </td>
						              															              
												
						                        <td align="left"> <asp:Button ID="cmdSaveByChange" runat="server" CssClass="btn btn-save" Width="100" Text="บันทึก"></asp:Button> </td>
									  </tr>


					</table></TD>
</TR>
				 
                       

</TABLE>	                                       
</div> 
        <div class="box-footer clearfix">           
                
            </div>
          </div>
    <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-search-plus"></i>

              <h3 class="box-title">กำหนดทีละรหัส</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                                 
            </div>
            <div class="box-body"> 
            
            <TABLE id="Table1"  class="table"  width="100%" bgColor="#ffffff" border="0">
                      <TR>
									<TD align="center" >
                                        <table cellSpacing="1" cellPadding="1" border="0">
											<tr>
                                                   	<td align="left">สาขา</td>						              
												<td align="left">
                                                    <asp:DropDownList ID="ddlMajor2" runat="server" CssClass="form-control select2" Width="200px"> 
                                                        <asp:ListItem Value="1">เภสัชกรรมอุตสาหการ</asp:ListItem>
                                                        <asp:ListItem Value="2">การบริบาลทางเภสัชกรรม</asp:ListItem>
                                                           <asp:ListItem Value="3">Non-Degree</asp:ListItem>
                                                    </asp:DropDownList>
						            </td>
												<td align="left">รหัสขึ้นต้น</td>
						              
												<td align="left">
                                                    <asp:TextBox ID="txtCode" runat="server" 
                                                         Width="60px"></asp:TextBox></td>								           
												<td align="left">เปลี่ยนเป็น ชั้นปีที่</td>						              
												<td align="left">
                                                    <asp:DropDownList ID="ddlLevel2" runat="server" CssClass="form-control select2">
                                                        <asp:ListItem>1</asp:ListItem>
                                                        <asp:ListItem>2</asp:ListItem>
                                                        <asp:ListItem>3</asp:ListItem>
                                                        <asp:ListItem>4</asp:ListItem>
                                                        <asp:ListItem>5</asp:ListItem>
                                                        <asp:ListItem>6</asp:ListItem>
                                                        <asp:ListItem Value="7">สำเร็จการศึกษา</asp:ListItem>
                                                    </asp:DropDownList>           </td>					
						              
						                        <td align="left"> <asp:Button ID="cmdSave" runat="server" CssClass="btn btn-save" Width="100" Text="บันทึก"></asp:Button> </td>
									  </tr>


					</table></TD>
</TR>
				<TR>
				     <TD   class="MenuSt">ข้อมูลรหัสนักศึกษาปัจจุบัน</TD>
				     </TR>
                       <TR>
				     <TD >
                         <asp:GridView ID="grdData" runat="server" CellPadding="4" ForeColor="#333333"  GridLines="None" CssClass="table table-hover"  AutoGenerateColumns="False" Width="100%">
                        <RowStyle BackColor="#F7F7F7" HorizontalAlign="Center" />
                        <columns>
                            <asp:BoundField DataField="MajorName" HeaderText="สาขา" />
                        <asp:BoundField HeaderText="รหัส" DataField="stdCode" >
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" /></asp:BoundField>
                            <asp:BoundField DataField="LevelClass"  HeaderText="ชั้นปีที่" >
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" Width="100px" /></asp:BoundField>
                        <asp:TemplateField HeaderText="Edit">
                            <ItemTemplate>
                                <asp:ImageButton ID="imgEdit" runat="server" ImageUrl="images/icon-edit.png" 
                                    CommandArgument='<%# Container.DataItemIndex %>' />                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                        </columns>
                        <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />                     
                        <pagerstyle CssClass="dc_pagination dc_paginationC dc_paginationC11"  HorizontalAlign="Center" />                     
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <headerstyle CssClass="th" Font-Bold="True" HorizontalAlign="Center" />                     
                        <EditRowStyle BackColor="#2461BF" />
                        <AlternatingRowStyle BackColor="White" />
                     </asp:GridView>  </TD>
				     </TR>

</TABLE>	                                       
</div> 
        <div class="box-footer clearfix">           
                
            </div>
          </div>
                           
</section>
                
</asp:Content>
