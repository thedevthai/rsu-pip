﻿Imports System.IO
Imports System.Data.OleDb
Imports System.Data
Imports Subgurim.Controles

Public Class StudentModify
    Inherits System.Web.UI.Page

    Dim dt As New DataTable
    Dim ctlStd As New StudentController
    Dim objUser As New UserController
    Dim ctlFct As New FacultyController
    Dim ctlPsn As New PersonController

    Dim objStd As New StudentInfo


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If IsNothing(Request.Cookies("UserLogin").Value) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If

        If Not IsPostBack Then
            grdData.PageIndex = 0
            lblResult.Visible = False
            ClearData()
            LoadPrefixToDDL()
            'ddlMinor.Enabled = False
            'ddlSubMinor.Enabled = False
            LoadStatusToDDL()

            LoadMajorToDDL()
            LoadAdvisorToDDL()
            LoadStudent()
        End If

        If (FileUploaderAJAX1.IsPosting) Then
            UploadFile()
        End If
    End Sub
    Dim objPsn As New PersonInfo
    Private Sub LoadPrefixToDDL()
        dt = ctlStd.Prefix_GetForStudent
        If dt.Rows.Count > 0 Then
            With ddlPrefix
                .Enabled = True
                .DataSource = dt
                .DataTextField = "PrefixName"
                .DataValueField = "PrefixID"
                .DataBind()
                '.SelectedIndex = 0
            End With
        End If
        dt = Nothing
    End Sub
    Private Sub LoadStatusToDDL()

        dt = ctlStd.StudentStatus_GetAll()

        ddlStudentStatus.Items.Clear()
        If dt.Rows.Count > 0 Then
            With ddlStudentStatus
                .Enabled = True
                For i = 0 To dt.Rows.Count - 1
                    .Items.Add("" & dt.Rows(i)("StatusCode") & " " & dt.Rows(i)("StatusName"))
                    .Items(i).Value = dt.Rows(i)("StatusCode")
                Next

                .SelectedIndex = 0

            End With

        End If
    End Sub

    Private Sub LoadAdvisorToDDL()

        dt = ctlPsn.Person_GetActiveByType("T")

        ddlAdvisor.Items.Clear()
        If dt.Rows.Count > 0 Then
            With ddlAdvisor
                .Enabled = True
                For i = 0 To dt.Rows.Count - 1
                    .Items.Add("" & dt.Rows(i)(objPsn.tblField(objPsn.fldPos.f02_FirstName).fldName) & " " & dt.Rows(i)(objPsn.tblField(objPsn.fldPos.f03_LastName).fldName))
                    .Items(i).Value = dt.Rows(i)(objPsn.tblField(objPsn.fldPos.f00_PersonID).fldName)
                Next
                .SelectedIndex = 0

            End With

        End If
    End Sub
    Private Sub LoadMajorToDDL()
        Dim dtMajor As New DataTable
        dtMajor = ctlFct.GetMajor
        If dtMajor.Rows.Count > 0 Then
            With ddlMajor
                .Enabled = True
                .DataSource = dtMajor
                .DataTextField = "MajorName"
                .DataValueField = "MajorID"
                .DataBind()
                .SelectedIndex = 0
            End With
        Else

        End If
        dtMajor = Nothing
    End Sub

    Private Sub UploadFile()

        Dim pf As HttpPostedFileAJAX = FileUploaderAJAX1.PostedFile

        'กรณีต้องการต้องสอบชนิด file
        ' If pf.ContentType.Equals("application/vnd.ms-excel") Then
        Try
            FileUploaderAJAX1.SaveAs("~/" & tmpUpload, pf.FileName)

            Session("fname") = pf.FileName
        Catch ex As Exception
            DisplayMessage(Me.Page, "ไม่สามารถอัปโหลดรได้ กรุณาลองใหม่ภายหลัง เนื่องจาก " & ex.Message)
        End Try

        ' Else

        ' End If

    End Sub

    Private Sub LoadStudent()

        dt = ctlStd.Student_GetByStatus(ddlLevel.SelectedValue, optStatus.SelectedValue, txtSearch.Text)
        If dt.Rows.Count > 0 Then
            With grdData
                .Visible = True
                .DataSource = dt
                .DataBind()
            End With

            lblStudentCount.Text = "พบรายชื่อนักศึกษาทั้งหมด " & dt.Rows.Count.ToString("#,###") & " คน"
        Else
            grdData.Visible = False
            grdData.DataSource = Nothing
        End If
        dt = Nothing
    End Sub

    Protected Sub cmdImport_Click(sender As Object, e As EventArgs) Handles cmdImport.Click
        System.Threading.Thread.Sleep(3000)
        UpdateProgress1.Visible = True

        Dim connectionString As String = ""
        Try

            lblResult.Visible = False

            Dim fileName As String = Path.GetFileName("~/" & tmpUpload & "/" & Session("fname"))
            Dim fileExtension As String = Path.GetExtension("~/" & tmpUpload & "/" & Session("fname"))

            Dim fileLocation As String = Server.MapPath("~/" & tmpUpload & "/" & fileName)

            'Check whether file extension is xls or xslx

            If fileExtension = ".xls" Then
                connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & fileLocation & ";Extended Properties=""Excel 8.0;HDR=Yes;IMEX=2"""
            ElseIf fileExtension = ".xlsx" Then
                connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & fileLocation & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""
            End If

            'Create OleDB Connection and OleDb Command

            Dim con As New OleDbConnection(connectionString)
            Dim cmd As New OleDbCommand()
            cmd.CommandType = System.Data.CommandType.Text
            cmd.Connection = con
            Dim dAdapter As New OleDbDataAdapter(cmd)
            Dim dtExcelRecords As New DataTable()
            con.Open()
            Dim dtExcelSheetName As DataTable = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
            Dim getExcelSheetName As String = dtExcelSheetName.Rows(0)("Table_Name").ToString()
            cmd.CommandText = "SELECT * FROM [" & getExcelSheetName & "]"
            dAdapter.SelectCommand = cmd
            dAdapter.Fill(dtExcelRecords)
            con.Close()

            Dim k As Integer = 0
            For i = 0 To dtExcelRecords.Rows.Count - 1
                With dtExcelRecords.Rows(i)
                    If .Item(0).ToString <> "" Then
                        ctlStd.Student_AddByImport(.Item(0).ToString, .Item(1), .Item(2), .Item(3), .Item(4), .Item(5), .Item(6), .Item(7), .Item(8), .Item(9), .Item(10))
                        k = k + 1
                    End If
                End With
            Next
            'grdData.DataSource = dtExcelRecords
            'grdData.DataBind()


            acc.User_GenLogfile(Request.Cookies("UserLogin").Value, ACTTYPE_ADD, "Students", "import รายชื่อนักศึกษาใหม่ : " & k & " คน", "จากไฟล์ excel")

            lblResult.Text = "ข้อมูลทั้งหมด " & k & "เรคอร์ด ถูก import เรียบร้อย"
            lblResult.Visible = True
            dtExcelRecords = Nothing

            LoadStudent()
            UpdateProgress1.Visible = False
        Catch ex As Exception
            DisplayMessage(Me.Page, "Error : " & ex.Message)
        End Try
    End Sub
    Dim acc As New UserController

    Function chkDup() As Boolean
        dt = ctlStd.GetStudent_ByID(txtCode.Text)
        If dt.Rows.Count > 0 Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click

        If txtCode.Text = "" Or txtFirstName.Text = "" Or txtLastName.Text = "" Then
            DisplayMessage(Me.Page, "กรุณป้อนข้อมูลให้ครบถ้วน")
            Exit Sub
        End If

        If Not chkDup() Then
            If lblStdID.Text = "" Then
                ctlStd.Student_Add(txtCode.Text, ddlPrefix.SelectedValue.ToString(), txtFirstName.Text, txtLastName.Text, optSex.SelectedValue, StrNull2Zero(ddlMajor.SelectedValue), 0, 0, StrNull2Zero(ddlAdvisor.SelectedValue), StrNull2Double(txtGPAX.Text), StrNull2Zero(txtLevelClass.Text), 2, ddlStudentStatus.SelectedValue.ToString())
                acc.User_GenLogfile(Request.Cookies("UserLogin").Value, ACTTYPE_ADD, "Students", "เพิ่มรายชื่อนักศึกษาใหม่ :" & txtFirstName.Text & " " & txtLastName.Text, "เพิ่มแบบทีละคน")
            Else
                ctlStd.Student_UpdateSmall(StrNull2Zero(lblStdID.Text), txtCode.Text, ddlPrefix.SelectedValue.ToString(), txtFirstName.Text, txtLastName.Text, optSex.SelectedValue, StrNull2Zero(ddlMajor.SelectedValue), 0, 0, StrNull2Zero(ddlAdvisor.SelectedValue), StrNull2Double(txtGPAX.Text), StrNull2Zero(txtLevelClass.Text), Request.Cookies("UserLogin").Value, ddlStudentStatus.SelectedValue.ToString())

                acc.User_GenLogfile(Request.Cookies("UserLogin").Value, ACTTYPE_UPD, "Students", "แก้ไขข้อมูลนักศึกษา :" & txtFirstName.Text & " " & txtLastName.Text, "เพิ่มแบบทีละคน")
            End If

        Else
            ctlStd.Student_UpdateSmall(StrNull2Zero(lblStdID.Text), txtCode.Text, ddlPrefix.SelectedValue.ToString(), txtFirstName.Text, txtLastName.Text, optSex.SelectedValue, StrNull2Zero(ddlMajor.SelectedValue), 0, 0, StrNull2Zero(ddlAdvisor.SelectedValue), StrNull2Double(txtGPAX.Text), StrNull2Zero(txtLevelClass.Text), Request.Cookies("UserLogin").Value, ddlStudentStatus.SelectedValue.ToString())

            acc.User_GenLogfile(Request.Cookies("UserLogin").Value, ACTTYPE_UPD, "Students", "แก้ไขข้อมูลนักศึกษา :" & txtFirstName.Text & " " & txtLastName.Text, "เพิ่มแบบทีละคน")
        End If

        DisplayMessage(Me.Page, "บันทึกข้อมูลเรียบร้อย")
        LoadStudent()
    End Sub
    Private Sub ClearData()
        txtFirstName.Text = ""
        txtLastName.Text = ""
        txtCode.Text = ""
        lblStdID.Text = ""
        txtGPAX.Text = ""
    End Sub
    Private Sub grdData_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdData.PageIndexChanging
        grdData.PageIndex = e.NewPageIndex
        LoadStudent()
    End Sub

    Private Sub grdData_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdData.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgView"
                    'Response.Redirect("Student_Bio.aspx?ActionType=std&std=" & e.CommandArgument())
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Download", "window.location='Student_Bio.aspx?ActionType=std&std=" & e.CommandArgument() & "';", True)

                Case "imgEdit"
                    EditData(e.CommandArgument())

                Case "imgEdit2"
                    Response.Redirect("Student_Reg.aspx?ActionType=std&ItemType=std&stdid=" & e.CommandArgument)
                Case "imgDel"
                    If ctlStd.Student_Delete(e.CommandArgument) Then

                        objUser.User_GenLogfile(Request.Cookies("UserLogin").Value, "DEL", "Student", "ลบนักศึกษา :" & e.CommandArgument, "")

                        DisplayMessage(Me, "ลบข้อมูลเรียบร้อย")
                    Else
                        DisplayMessage(Me, "ไม่สามารถลบข้อมูลได้ กรุณาตรวจสอบและลองใหม่อีกครั้ง")
                    End If

                    LoadStudent()

            End Select

        End If
    End Sub
    Private Sub EditData(ByVal pID As String)

        ClearData()

        dt = ctlStd.GetStudent_ByID(pID)

        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                lblStdID.Text = .Item("Student_ID")
                Me.txtCode.Text = .Item("Student_Code")
                Me.txtFirstName.Text = String.Concat(.Item("FirstName"))
                txtLastName.Text = String.Concat(.Item("LastName"))

                If Not IsDBNull(.Item(objStd.tblField(objStd.fldPos.f12_MajorID).fldName)) Then
                    ddlMajor.SelectedValue = .Item(objStd.tblField(objStd.fldPos.f12_MajorID).fldName)
                End If

                If Not IsDBNull(.Item(objStd.tblField(objStd.fldPos.f15_AdvisorID).fldName)) Then
                    ddlAdvisor.SelectedValue = .Item(objStd.tblField(objStd.fldPos.f15_AdvisorID).fldName)
                End If

                txtGPAX.Text = DBNull2Dbl(.Item(objStd.tblField(objStd.fldPos.f54_GPAX).fldName))


                If Not IsDBNull(.Item(objStd.tblField(objStd.fldPos.f59_StudentStatus).fldName)) Then
                    ddlStudentStatus.SelectedValue = .Item(objStd.tblField(objStd.fldPos.f59_StudentStatus).fldName)

                    If ddlStudentStatus.SelectedValue = "40" Then
                        txtLevelClass.Text = ""
                    Else
                        txtLevelClass.Text = String.Concat(.Item(objStd.tblField(objStd.fldPos.f56_LevelClass).fldName))
                    End If
                End If


            End With

            ' txtCode.ReadOnly = True

        End If

        dt = Nothing
    End Sub

    Private Sub grdData_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdData.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then

            'e.Row.Cells(0).Text = e.Row.RowIndex + 1
            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(7).FindControl("imgDel")
            imgD.Attributes.Add("onClick", scriptString)

        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#ffdfef';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If

    End Sub

    Protected Sub cmdFind_Click(sender As Object, e As EventArgs) Handles cmdFind.Click
        grdData.PageIndex = 0
        LoadStudent()
    End Sub

    Protected Sub cmdClear_Click(sender As Object, e As EventArgs) Handles cmdClear.Click
        ClearData()
    End Sub

    Protected Sub cmdAll_Click(sender As Object, e As EventArgs) Handles cmdAll.Click
        txtSearch.Text = ""
        optStatus.SelectedIndex = 0
        LoadStudent()
    End Sub

End Class