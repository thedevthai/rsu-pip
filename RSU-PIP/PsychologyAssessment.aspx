﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="PsychologyAssessment.aspx.vb" Inherits=".PsychologyAssessment" %>
<%@ Import Namespace="System.Data" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    
  
</asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
      <section class="content-header">
      <h1>ข้อมูลการประเมินด้านจิตเวช</h1>   
    </section>

<section class="content"> 

        <div class="row no-print">
        <div class="col-xs-12">
          <a href="StudentSelect?ActionType=psy"  class="btn btn-success pull-right"><i class="fa fa-plus-circle"></i> เพิ่มการประเมิน</a>
        </div>
      </div>
        <br />
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">รายชื่อนักศึกษาที่ถูกประเมิน</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body"> 
              <table id="tbdata" class="table table-bordered table-striped">
                <thead>
                <tr>
                 <th class="sorting_asc_disabled"></th>   
                    <th>รหัส</th>
                  <th>ชื่อ-นามสกุล</th>
                  <th>สาขา</th>           
                    
                </tr>
                </thead>
                <tbody>
            <% For Each row As DataRow In dtPsy.Rows %>
                <tr>
                     <td width="35px"><a href="PsychologyAssessmentModify?id=<% =String.Concat(row("UID")) %>&std=<% =String.Concat(row("Student_Code")) %>"   ><img src="images/icon-edit.png"/></a>
                    </td>

                  <td><% =String.Concat(row("Student_Code")) %></td>
                  <td><% =String.Concat(row("StudentName")) %>    </td>
                  <td class="text-center"><% =String.Concat(row("MajorName")) %></td>                    
                </tr>
            <%  Next %>
                </tbody>               
              </table>                                    
            </div>
            <!-- /.box-body -->
          </div>
    
</section>    
</asp:Content>
