﻿Imports System.IO
Public Class MediaAdd
    Inherits System.Web.UI.Page
    Dim ctlMedia As New MediaController
    Dim sFile As String = ""
    Dim tmp As Integer
    Dim sContent As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then

            If Not Request("id") Is Nothing Then
                LoadMedia()
            End If
            ShowMediaType()
        End If
    End Sub
    Private Sub LoadMedia()
        Dim dtN As New DataTable

        dtN = ctlMedia.Media_GetByID(Request("id"))
        If dtN.Rows.Count > 0 Then
            With dtN.Rows(0)
                lblID.Text = .Item("MediaID")
                txtTitle.Text = .Item("Title")
                ddlLevel.SelectedValue = DBNull2Zero(.Item("StdLevel"))
                ddlMajor.SelectedValue = DBNull2Zero(.Item("MajorUID"))
                ddlGroup.SelectedValue = DBNull2Zero(.Item("GroupUID"))

                txtURL.Text = String.Concat(.Item("LinkPath"))
                hlnkNews.Text = String.Concat(.Item("LinkPath"))
                hlnkNews.NavigateUrl = DocumentUpload & "/" & String.Concat(.Item("StdLevel")) & "/" & String.Concat(.Item("LinkPath"))
                chkFistPage.Checked = ConvertYN2Boolean(.Item("FirstPage"))
                chkStatus.Checked = ConvertStatusFlag2CHK(.Item("StatusFlag"))
                optType.SelectedValue = String.Concat(.Item("MediaType"))
                txtOrder.Text = String.Concat(.Item("DisplayOrder"))

                ShowMediaType()
            End With

        End If
        dtN = Nothing
    End Sub
    Private Sub SaveMedia()

        Dim OrderNo As Integer = 0

        If txtTitle.Text.Trim = "" Then
            'DisplayMessage(Me, "กรุณากรอกหัวเรื่องที่ประกาศก่อน")
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'Warning!','กรุณากรอกชื่อเอกสารก่อน');", True)
            Exit Sub
        End If
        sContent = ""
        If optType.SelectedValue = "UPL" Then
            If FileUpload.HasFile Then
                sFile = Path.GetFileName(FileUpload.PostedFile.FileName)
            Else
                sFile = hlnkNews.Text
            End If

        ElseIf optType.SelectedValue = "URL" Then
            sFile = txtURL.Text
        Else
            sContent = ""
        End If


        If StrNull2Zero(txtOrder.Text) = 0 Then
            OrderNo = 0
        Else
            OrderNo = StrNull2Zero(txtOrder.Text)
        End If
        If lblID.Text <> 0 Then
            tmp = ctlMedia.Media_Update(lblID.Text, txtTitle.Text, ConvertBoolean2YN(chkFistPage.Checked), optType.SelectedValue, sFile, ConvertBoolean2StatusFlag(chkStatus.Checked), OrderNo, StrNull2Zero(ddlLevel.SelectedValue), StrNull2Zero(ddlMajor.SelectedValue), StrNull2Zero(ddlGroup.SelectedValue))
        Else
            tmp = ctlMedia.Media_Add(txtTitle.Text, ConvertBoolean2YN(chkFistPage.Checked), optType.SelectedValue, sFile, ConvertBoolean2StatusFlag(chkStatus.Checked), OrderNo, StrNull2Zero(ddlLevel.SelectedValue), StrNull2Zero(ddlMajor.SelectedValue), StrNull2Zero(ddlGroup.SelectedValue))
        End If

        If tmp Then
            Try
                If optType.SelectedValue = "UPL" Then
                    UploadFile(FileUpload)
                End If
            Catch ex As Exception
                'DisplayMessage(Me, "บันทึกข้อมูลเรียบร้อย แต่ไม่สามารถ upload ได้")
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'Warning!','บันทึกข้อมูลเรียบร้อย แต่ไม่สามารถ upload ได้');", True)
            End Try


            ' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)

            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'Success','บันทึกข้อมูลเรียบร้อย');", True)


        Else
            'DisplayMessage(Me, "ไม่สามารถบันทึกข้อมูลได้ กรุณาตรวจสอบและลองใหม่อีกครั้ง")

            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'Warning!','ไม่สามารถบันทึกข้อมูลได้ กรุณาตรวจสอบและลองใหม่อีกครั้ง');", True)

        End If

        ClearData()

    End Sub
    Private Sub ClearData()
        lblID.Text = "0"
        txtOrder.Text = "0"
        txtTitle.Text = ""
        txtURL.Text = ""
        hlnkNews.Text = ""


        ddlLevel.SelectedIndex = 0
        ddlMajor.SelectedIndex = 0
        ddlGroup.SelectedIndex = 0
        chkFistPage.Checked = False
        chkStatus.Checked = True
    End Sub
    Sub UploadFile(ByVal Fileupload As Object)
        Dim FileFullName As String = Fileupload.PostedFile.FileName
        Dim FileNameInfo As String = Path.GetFileName(FileFullName)
        Dim filename As String = Path.GetFileName(Fileupload.PostedFile.FileName)
        Dim objfile As FileInfo = New FileInfo(Server.MapPath(DocumentUpload))
        If FileNameInfo <> "" Then
            'Fileupload.PostedFile.SaveAs(objfile.DirectoryName & "\" & filename)
            'sFile = Path.GetExtension(FileFullName)
            Fileupload.PostedFile.SaveAs(objfile.DirectoryName & "\" & DocumentUpload & "\" & ddlLevel.SelectedValue & "\" & sFile)
        End If
        objfile = Nothing
    End Sub


    Protected Sub optType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optType.SelectedIndexChanged
        ShowMediaType()
    End Sub
    Private Sub ShowMediaType()
        pnURL.Visible = False
        pnFile.Visible = False

        Select Case optType.SelectedValue
            Case "URL"
                pnURL.Visible = True
            Case "UPL"
                pnFile.Visible = True
        End Select
    End Sub

    Protected Sub cmdCancel_Click(sender As Object, e As EventArgs) Handles cmdCancel.Click
        ClearData()
    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click
        SaveMedia()
    End Sub
End Class