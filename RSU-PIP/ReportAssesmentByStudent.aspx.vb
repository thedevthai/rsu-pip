﻿
Public Class ReportAssesmentByStudent
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlAss As New AssessmentController
    Dim ctlCs As New CourseController
    Dim ctlSk As New SkillController


    Dim acc As New UserController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("UserLogin").Value) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If
        If Not IsPostBack Then
            lblResult.Visible = False
            LoadYearToDDL()
            LoadCourseToDDL()
            LoadLocationToDDL()
            LoadSkillToDDL()
            LoadSkillPhase()
            LoadAssessmentToGrid()
        End If
    End Sub
    Private Sub LoadCourseToDDL()
        ddlCourse.Items.Clear()
        dt = ctlCs.Courses_GetByYear(ddlYear.SelectedValue)
        If dt.Rows.Count > 0 Then
            ddlCourse.Items.Clear()
            ddlCourse.Items.Add("---ทั้งหมด---")
            ddlCourse.Items(0).Value = 0

            For i = 1 To dt.Rows.Count
                With ddlCourse
                    .Items.Add("" & dt.Rows(i - 1)("SubjectCode") & " : " & dt.Rows(i - 1)("NameTH"))
                    .Items(i).Value = dt.Rows(i - 1)("SubjectCode")
                End With
            Next

        End If

    End Sub
    Private Sub LoadLocationToDDL()
        ddlLocation.Items.Clear()
        Dim ctlL As New LocationController

        dt = ctlL.LocationAssessment_Get(StrNull2Zero(ddlYear.SelectedValue))

        If dt.Rows.Count > 0 Then
            ddlLocation.Items.Clear()
            With ddlLocation
                .Items.Add("---ทั้งหมด---")
                .Items(0).Value = 0

                For i = 0 To dt.Rows.Count - 1
                    .Items.Add("" & dt.Rows(i)("LocationName"))
                    .Items(i + 1).Value = dt.Rows(i)("LocationID")
                Next
            End With
        End If

    End Sub
    Private Sub LoadSkillToDDL()

        dt = ctlSk.Skill_Get4Selection
        If dt.Rows.Count > 0 Then
            With ddlSkill
                .Enabled = True
                .DataSource = dt
                .DataTextField = "Name"
                .DataValueField = "UID"
                .DataBind()
                .SelectedIndex = 0
            End With
        End If
        dt = Nothing
    End Sub

    Private Sub LoadYearToDDL()
        Dim y As Integer = StrNull2Zero(DisplayYear(ctlCs.GET_DATE_SERVER))
        Dim LastRow As Integer
        dt = ctlSk.Skill_GetYear
        LastRow = dt.Rows.Count - 1

        If dt.Rows.Count > 0 Then
            With ddlYear
                .Enabled = True
                .DataSource = dt
                .DataTextField = "EduYear"
                .DataValueField = "EduYear"
                .DataBind()

                If dt.Rows(LastRow)(0) = y Then
                    ddlYear.Items.Add(y + 1)
                    ddlYear.Items(LastRow + 1).Value = y + 1
                ElseIf dt.Rows(LastRow)(0) > y Then
                    'ddlYear.Items.Add(y + 2)
                    'ddlYear.Items(LastRow + 1).Value = y + 2
                ElseIf dt.Rows(LastRow)(0) < y Then
                    ddlYear.Items.Add(y)
                    ddlYear.Items(LastRow + 1).Value = y
                    ddlYear.Items.Add(y + 1)
                    ddlYear.Items(LastRow + 2).Value = y + 1
                End If
                .SelectedIndex = 0
            End With
        Else
            ddlYear.Items.Add(y)
            ddlYear.Items(0).Value = y
            ddlYear.Items.Add(y + 1)
            ddlYear.Items(1).Value = y + 1
            ddlYear.SelectedIndex = 0
        End If
        ddlYear.SelectedValue = Request.Cookies("EDUYEAR").Value
        dt = Nothing
    End Sub

    Private Sub LoadSkillPhase()
        Dim ctlTP As New SkillPhaseController
        'dt = ctlTP.TurnPhase_GetActiveByYear(ddlYear.SelectedValue)
        If ddlLocation.SelectedValue = "0" Then
            dt = ctlTP.PhaseNo_GetByYear(StrNull2Zero(ddlYear.SelectedValue))
        Else
            dt = ctlTP.SkillPhase_GetByLocation(StrNull2Zero(ddlYear.SelectedValue), StrNull2Zero(ddlLocation.SelectedValue))
        End If

        If dt.Rows.Count > 0 Then

            ddlPhase.Items.Clear()
            ddlPhase.Items.Add("---ทั้งหมด---")
            ddlPhase.Items(0).Value = 0

            For i = 1 To dt.Rows.Count
                With ddlPhase
                    .Items.Add("" & dt.Rows(i - 1)("PhaseName"))
                    .Items(i).Value = dt.Rows(i - 1)("PhaseNo")
                End With
            Next


            'With ddlPhase
            '    .Visible = True
            '    .DataSource = dt
            '    .DataTextField = "PhaseName"
            '    .DataValueField = "PhaseID"
            '    .DataBind()
            '    .SelectedIndex = 0
            'End With
        Else
            ddlPhase.DataSource = dt
            ddlPhase.DataBind()
        End If
    End Sub
    Private Sub LoadAssessmentToGrid()

        dt = ctlAss.Assessment_GetByStudentSearch(ddlYear.SelectedValue, ddlLevel.SelectedValue, ddlLocation.SelectedValue, ddlPhase.SelectedValue, ddlSkill.SelectedValue, ddlCourse.SelectedValue, Trim(txtStudent.Text))
        If dt.Rows.Count > 0 Then
            With grdStudent
                .Visible = True
                .DataSource = dt
                .DataBind()
            End With
            lblResult.Visible = False
        Else
            grdStudent.Visible = False
            lblResult.Visible = True
        End If
        dt = Nothing
    End Sub
    Private Sub grdStudent_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdStudent.PageIndexChanging
        grdStudent.PageIndex = e.NewPageIndex
        LoadAssessmentToGrid()
    End Sub

    Private Sub grdStudent_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdStudent.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit"
                    Response.Redirect("AssessmentModify.aspx?ActionType=asg&ItemType=find&id=" & e.CommandArgument)
                Case "imgDel"
                    If ctlAss.Assessment_DeleteByID(e.CommandArgument) Then

                        acc.User_GenLogfile(Request.Cookies("UserLogin").Value, ACTTYPE_DEL, "Assessments", "ลบ ผลการคัดเลือก :" & ddlCourse.SelectedValue, "")

                        DisplayMessage(Me, "ลบข้อมูลเรียบร้อย")
                        grdStudent.PageIndex = 0
                        LoadAssessmentToGrid()
                    Else
                        DisplayMessage(Me, "ไม่สามารถลบข้อมูลได้ กรุณาตรวจสอบและลองใหม่อีกครั้ง")
                    End If

            End Select


        End If
    End Sub

    Private Sub grdStudent_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdStudent.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then

            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(9).FindControl("imgDel")
            imgD.Attributes.Add("onClick", scriptString)

        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#ffdfef';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If

    End Sub

    Protected Sub ddlYear_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlYear.SelectedIndexChanged
        grdStudent.PageIndex = 0
        LoadAssessmentToGrid()
    End Sub

    Protected Sub ddlCourse_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCourse.SelectedIndexChanged
        grdStudent.PageIndex = 0
        LoadAssessmentToGrid()
    End Sub

    Protected Sub cmdFind_Click(sender As Object, e As EventArgs) Handles cmdFind.Click
        grdStudent.PageIndex = 0
        LoadAssessmentToGrid()
    End Sub

    Protected Sub ddlLevel_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLevel.SelectedIndexChanged
        grdStudent.PageIndex = 0
        LoadAssessmentToGrid()
    End Sub

    Protected Sub ddlPhase_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlPhase.SelectedIndexChanged
        grdStudent.PageIndex = 0
        LoadAssessmentToGrid()
    End Sub

    Protected Sub ddlSkill_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSkill.SelectedIndexChanged
        grdStudent.PageIndex = 0
        LoadAssessmentToGrid()
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocation.SelectedIndexChanged
        grdStudent.PageIndex = 0
        LoadSkillPhase()
        LoadAssessmentToGrid()
    End Sub
End Class

