﻿Public Class RoomRecommendModify
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlR As New RecommendController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            LoadLocationToDDL()

            If Not Request("id") Is Nothing Then
                LoadRecommendDetail()
            Else
                lblStudentCode.Text = Request.Cookies("ProfileID").Value
                lblStudentName.Text = Request.Cookies("NameOfUser").Value
            End If

            If Request.Cookies("ROLE_STD").Value = True Then
                imgPending.Enabled = False
                imgApprove.Enabled = False
                imgReject.Enabled = False
                lblLocation.Visible = False
            Else
                imgPending.Enabled = True
                imgApprove.Enabled = True
                imgReject.Enabled = True
                ddlLocation.Visible = False
                lblLocation.Visible = True
            End If
        End If

    End Sub
    Private Sub LoadLocationToDDL()
        ddlLocation.Items.Clear()
        Dim ctlL As New LocationController
        dt = ctlL.Location_GetByStudent(Request.Cookies("UserLogin").Value)
        If dt.Rows.Count > 0 Then
            With ddlLocation
                .DataSource = dt
                .DataTextField = "LocationName"
                .DataValueField = "LocationID"
                .DataBind()
                .SelectedIndex = 0
            End With
        Else
            If Request.Cookies("ROLE_STD").Value = True Then
                DisplayMessage(Me.Page, "นักศึกษายังไม่ได้ไปฝึก จึงไม่สามารถบันทึกคำแนะนำได้")
                Response.Redirect("Home.aspx?actionType=h")
            End If
        End If
    End Sub

    Private Sub LoadRecommendDetail()
        dt = ctlR.RoomRecommend_GetByID(Request("id"))
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                lblID.Text = .Item("UID")
                txtShortDesc.Text = .Item("Title")
                txtRecommend.Html = .Item("Recommend")
                lblLocation.Text = .Item("LocationName")
                lblStudentCode.Text = .Item("StudentCode")
                lblStudentName.Text = .Item("StudentName")
                lblStatus.Text = .Item("StatusName")

                imgPending.ImageUrl = "images/statusicon_pending_01.png"
                imgApprove.ImageUrl = "images/statusicon_approve_01.png"
                imgReject.ImageUrl = "images/statusicon_reject_01.png"

                Select Case .Item("ApproveStatus")
                    Case "P"
                        imgPending.ImageUrl = "images/statusicon_pending_02.png"
                    Case "A"
                        imgApprove.ImageUrl = "images/statusicon_approve_02.png"
                    Case "X"
                        imgReject.ImageUrl = "images/statusicon_reject_02.png"
                End Select

                If Request.Cookies("ROLE_STD").Value = True Then
                    If .Item("ApproveStatus") = "A" Then
                        cmdSave.Visible = False
                    Else
                        cmdSave.Visible = True
                    End If
                Else

                End If
            End With
        End If


    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click
        If txtShortDesc.Text.Trim() = "" Then
            DisplayMessage(Me.Page, "ระบุคำแนะนำสั้นๆก่อน")
            Exit Sub
        End If
        If txtRecommend.Html.Trim() = "" Then
            DisplayMessage(Me.Page, "ระบุคำแนะนำก่อน")
            Exit Sub
        End If

        If Request.Cookies("ROLE_STD").Value = True Then
            ctlR.RoomRecommend_Save(StrNull2Zero(lblID.Text), Request.Cookies("UserLogin").Value, ddlLocation.SelectedValue, txtShortDesc.Text, txtRecommend.Html)
        Else
            ctlR.RoomRecommend_UpdateByAdmin(StrNull2Zero(lblID.Text), txtShortDesc.Text, txtRecommend.Html)
        End If

        DisplayMessage(Me.Page, "บันทึกข้อมูลเรียบร้อย")
        Response.Redirect("RoomRecommend.aspx")
    End Sub

    Private Sub UpdateStatus(Status As String)
        ctlR.RoomRecommend_UpdateStatus(lblID.Text, Status)
        LoadRecommendDetail()
    End Sub

    Protected Sub imgPending_Click(sender As Object, e As ImageClickEventArgs) Handles imgPending.Click
        UpdateStatus("P")
    End Sub

    Protected Sub imgApprove_Click(sender As Object, e As ImageClickEventArgs) Handles imgApprove.Click
        UpdateStatus("A")
    End Sub

    Protected Sub imgReject_Click(sender As Object, e As ImageClickEventArgs) Handles imgReject.Click
        UpdateStatus("X")
    End Sub
End Class