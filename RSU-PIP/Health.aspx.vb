﻿Public Class Health
    Inherits System.Web.UI.Page

    Dim ctlH As New HealthController
    Dim ctlStd As New StudentController
    Dim dt As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("UserLogin").Value) Then
            Response.Redirect("Default.aspx")
        End If
        If Not IsPostBack Then
            hlnkDoc.Visible = False
            LoadStudentInfo()
            hdGUID.Value = ""
            LoadGeneral()
        End If
    End Sub

    Private Sub LoadStudentInfo()
        dt = ctlStd.GetStudent_ByID(Request.Cookies("ProfileID").Value)
        If dt.Rows.Count > 0 Then
            lblStudentCode.Text = String.Concat(dt.Rows(0)("Student_Code"))
            lblStudentName.Text = String.Concat(dt.Rows(0)("StudentName"))
            lblMajorName.Text = String.Concat(dt.Rows(0)("MajorName"))
            lblNickName.Text = String.Concat(dt.Rows(0)("NickName"))
        End If
        dt = Nothing
    End Sub

    Private Sub LoadGeneral()
        dt = ctlH.StudentHealth_Get(lblStudentCode.Text)
        If dt.Rows.Count > 0 Then
            grdGeneral.Visible = True
            grdGeneral.DataSource = dt
            grdGeneral.DataBind()
        Else
            grdGeneral.DataSource = Nothing
            grdGeneral.Visible = False
        End If
        hdGUID.Value = ""
        dt = Nothing
    End Sub

    Private Sub EditGeneral(UID As Integer)
        dt = ctlH.StudentHealth_GetByUID(UID)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                hdGUID.Value = .Item("UID")
                If String.Concat(.Item("AdmitDate")) <> "" Then
                    txtGenDate.Text = DisplayStr2ShortDateTH(String.Concat(.Item("AdmitDate")))
                Else
                    txtGenDate.Text = ""
                End If
                txtDescription.Text = .Item("Descriptions")
                optResult.SelectedValue = .Item("Result")
                txtHospitalGeneral.Text = .Item("HospitalName")

                If String.Concat(dt.Rows(0)("CertPath")) <> "" Then
                    hlnkDoc.Text = "คลิกเพื่อดูใบรับรองแพทย์"
                    hlnkDoc.NavigateUrl = "../Certificate/" & DBNull2Str(dt.Rows(0)("CertPath"))
                    hlnkDoc.Visible = True
                Else
                    hlnkDoc.Visible = False
                End If
            End With
        Else
            hlnkDoc.Visible = False
        End If
    End Sub


    Protected Sub cmdSaveGeneral_Click(sender As Object, e As EventArgs) Handles cmdSaveGeneral.Click

        If txtGenDate.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาป้อนวันที่รับการรักษาก่อน');", True)
            Exit Sub
        End If

        If optResult.SelectedValue = Nothing Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาเลิกผลการรักษาก่อน');", True)
            Exit Sub
        End If


        Dim AdmitDate As Integer
        If txtGenDate.Text <> "" Then
            AdmitDate = CInt(ConvertStrDate2DBString(txtGenDate.Text))
        End If

        ctlH.StudentHealth_Save(StrNull2Zero(hdGUID.Value), lblStudentCode.Text, AdmitDate, txtDescription.Text, optResult.SelectedValue, txtHospitalGeneral.Text, Request.Cookies("UserLoginID").Value)

        txtGenDate.Text = ""
        txtDescription.Text = ""
        optResult.SelectedValue = Nothing
        txtHospitalGeneral.Text = ""


        Dim f_Path As String
        f_Path = ""
        If FileUpload1.HasFile Then
            f_Path = "H-" & lblStudentCode.Text & "-" & hdGUID.Value & System.IO.Path.GetExtension(FileUpload1.PostedFile.FileName)
            ctlH.StudentHealth_UpdateCertificate(StrNull2Zero(hdGUID.Value), lblStudentCode.Text, f_Path, Request.Cookies("UserLoginID").Value)
            UploadFile(FileUpload1, f_Path)
        End If

        LoadGeneral()
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)

    End Sub
    Sub UploadFile(ByVal Fileupload As Object, sName As String)
        Dim filename As String = System.IO.Path.GetFileName(Fileupload.PostedFile.FileName)
        Dim objfile As System.IO.FileInfo = New System.IO.FileInfo(Server.MapPath("Certificate/" & sName))
        If objfile.Exists Then
            objfile.Delete()
        End If

        Fileupload.PostedFile.SaveAs(Server.MapPath("Certificate/" & sName))
        hlnkDoc.Visible = False
    End Sub

    Private Sub grdGeneral_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdGeneral.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then
            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(5).FindControl("imgDel")
            imgD.Attributes.Add("onClick", scriptString)
        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#ded5f2';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If
    End Sub


    Private Sub grdGeneral_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdGeneral.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit"
                    EditGeneral(e.CommandArgument())
                Case "imgDel"
                    ctlH.StudentHealth_Delete(e.CommandArgument())
                    LoadGeneral()
            End Select
        End If
    End Sub

    Protected Sub cmdCancel_Click(sender As Object, e As EventArgs) Handles cmdCancel.Click
        txtGenDate.Text = ""
        txtDescription.Text = ""
        optResult.SelectedValue = Nothing
        txtHospitalGeneral.Text = ""
        hdGUID.Value = ""

    End Sub
End Class

