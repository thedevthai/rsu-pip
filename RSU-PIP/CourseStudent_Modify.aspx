﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="CourseStudent_Modify.aspx.vb" Inherits=".CourseStudent_Modify" %>
<%@ Register assembly="FUA" namespace="Subgurim.Controles" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">       
</asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  

<section class="content-header">
      <h1>จัดการรายวิชาและงานที่ฝึกรายนักศึกษา</h1>   
    </section>

<section class="content">  

     <div class="box box-pink">
            <div class="box-header">
              <i class="fa fa-filter"></i>

              <h3 class="box-title">กำหนดค่า</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                                 
            </div>
            <div class="box-body"> 

                  <div class="row">
           <div class="col-md-2">
          <div class="form-group">
            <label>ปี</label>
             <asp:DropDownList ID="ddlYear" runat="server" AutoPostBack="True" CssClass="form-control select2 text-center"></asp:DropDownList>  
          </div>

        </div>

        <div class="col-md-5">
          <div class="form-group">
            <label>รายวิชา</label>
              <asp:DropDownList ID="ddlCourse" runat="server" AutoPostBack="True" CssClass="form-control select2"></asp:DropDownList>     
          </div>
        </div>    
                      
 <div class="col-md-2">
          <div class="form-group">
            <label>รหัสนักศึกษา</label>
                 <asp:TextBox ID="txtStudentCode" runat="server" CssClass="form-control text-center"></asp:TextBox>
          </div>
        </div> 
                       <div class="col-md-3">
          <div class="form-group">
            <label>ชื่อ-สกุล</label>
                  <asp:Label ID="lblStudentName" runat="server" CssClass="form-control text-center"></asp:Label>
          </div>
        </div> 
          </div>   
        
                  <div class="row">              
                      <div class="col-md-6">
          <div class="form-group">
            <label>งานที่ฝึก</label>
              <asp:DropDownList ID="ddlSkill" runat="server" AutoPostBack="True" CssClass="form-control select2"></asp:DropDownList>  
     
          </div>

        </div>
       
           <div class="col-md-6">
                <div class="form-group">
                    <br />
               <asp:Button ID="cmdSave" runat="server" CssClass="btn btn-save" Width="100" Text="บันทึก"></asp:Button>
    <asp:Button ID="cmdClear" runat="server" CssClass="btn btn-default" Width="100" Text="ยกเลิก"></asp:Button>    
              </div>  </div>
          </div>

</div>
            <div class="box-footer clearfix">
           
            </div>
          </div>
    

 <asp:Panel ID="Panel1" runat="server">  
    <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-search-plus"></i>

              <h3 class="box-title">รายชื่อนักศึกษาที่กำหนดรายวิชา/งานแล้ว ทั้งหมด&nbsp; 
                <asp:Label ID="lblCount" runat="server"></asp:Label>
           &nbsp;คน</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                                 
            </div>
            <div class="box-body">     
        <table border="0" cellspacing="2" cellpadding="0">
            <tr>
              <td>ค้นหา</td>
              <td width="150">
                  <asp:TextBox ID="txtSearchStd" runat="server" Width="150px"></asp:TextBox>
                  </td>
              <td><asp:Button ID="cmdFindStd" runat="server" CssClass="btn btn-find" Width="70" Text="ค้นหา"></asp:Button>              </td>
            </tr>
           
          </table>
 <asp:GridView ID="grdStudent" 
                             runat="server" CellPadding="0" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" Width="100%" AllowPaging="True" CssClass="table table-hover" PageSize="20">
            <RowStyle BackColor="#F7F7F7" />
            <columns>
            <asp:BoundField DataField="Student_Code" HeaderText="รหัสนักศึกษา">                      
                <HeaderStyle HorizontalAlign="Center" />
              <itemstyle HorizontalAlign="Center" Width="120px"/>

            </asp:BoundField>

                <asp:BoundField DataField="StudentName" HeaderText="ชื่อนักศึกษา">
                <ItemStyle HorizontalAlign="Left"/>
                </asp:BoundField>
            <asp:BoundField HeaderText="สาขา" DataField="MajorName">

                <HeaderStyle HorizontalAlign="Left" />
                </asp:BoundField>

                <asp:BoundField DataField="LevelClass" HeaderText="ชั้นปี">
                 <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Center" />                </asp:BoundField>
                <asp:BoundField DataField="SubjectName" HeaderText="รายวิชา">
                <itemstyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                </asp:BoundField>
                <asp:BoundField DataField="SkillName" HeaderText="งาน" />
            <asp:TemplateField HeaderText="ลบ">
              <itemtemplate>
                <asp:ImageButton ID="imgEdit" runat="server" ImageUrl="images/icon-edit.png"     CommandArgument='<%# DataBinder.Eval(Container.DataItem, "itemID") %>' />                        </itemtemplate>
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />          
            </asp:TemplateField>
            </columns>
            <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />          
            <pagerstyle HorizontalAlign="Center" 
                      CssClass="dc_pagination dc_paginationC dc_paginationC11" />          
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <headerstyle CssClass="th" Font-Bold="True" 
                      VerticalAlign="Middle" HorizontalAlign="Left" />          
            <EditRowStyle BackColor="#2461BF" />
            <AlternatingRowStyle BackColor="White" />
          </asp:GridView>

<asp:Label ID="lblNo" runat="server" CssClass="alert alert-error show" Text="No data found." Width="100%"></asp:Label>
  
</div> 
        <div class="box-footer clearfix">           
                
            </div>
          </div>

 </asp:Panel>   
  
      <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-upload"></i>

              <h3 class="box-title">Update from Excel file</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                                 
            </div>
            <div class="box-body"> 

                              <table width="100%" border="0" cellspacing="2" cellpadding="0">
       <tr>
         <td height="30">
            <cc1:FileUploaderAJAX ID="FileUploaderAJAX1" runat="server" />           </td>
         </tr>
       <tr>
         <td>
             <asp:Button ID="cmdImport" runat="server" CssClass="btn btn-find" Text="Update" Width="100px" />           </td>
         </tr>
       <tr>
         <td>
             <asp:Label ID="lblResult" runat="server" CssClass="OptionPanels" Width="100%"></asp:Label>           </td>
         </tr>
     </table>             
</div>
            <div class="box-footer clearfix">
             <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="0">
             <ProgressTemplate>
<img alt="" src="images/progress_bar.gif" height="25" />             </ProgressTemplate>
         </asp:UpdateProgress>   
            </div>
          </div>
    


    </section>

    
</asp:Content>
