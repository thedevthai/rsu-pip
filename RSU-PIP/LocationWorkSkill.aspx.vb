﻿
Public Class LocationWorkSkill
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlSL As New SkillLocationController
    Dim acc As New UserController
    Dim ctlSk As New SkillController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("UserLogin").Value) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If
        If Not IsPostBack Then

            LoadYearToDDL()
            LoadSkillToDDL()

            LoadGroupToDDL()

            LoadLocationInWorkSkillToGrid()
            LoadLocationNotWorkSkillToGrid()

        End If
    End Sub
    Private Sub LoadYearToDDL()
        Dim ctlCs As New CourseController
        Dim y As Integer = StrNull2Zero(DisplayYear(ctlCs.GET_DATE_SERVER))
        Dim LastRow As Integer
        dt = ctlCs.Courses_GetYear
        LastRow = dt.Rows.Count - 1

        If dt.Rows.Count > 0 Then
            With ddlYear
                .Enabled = True
                .DataSource = dt
                .DataTextField = "CYear"
                .DataValueField = "CYear"
                .DataBind()

                If dt.Rows(LastRow)(0) = y Then
                    ddlYear.Items.Add(y + 1)
                    ddlYear.Items(LastRow + 1).Value = y + 1
                ElseIf dt.Rows(LastRow)(0) > y Then
                    'ddlYear.Items.Add(y + 2)
                    'ddlYear.Items(LastRow + 1).Value = y + 2
                ElseIf dt.Rows(LastRow)(0) < y Then
                    ddlYear.Items.Add(y)
                    ddlYear.Items(LastRow + 1).Value = y
                    ddlYear.Items.Add(y + 1)
                    ddlYear.Items(LastRow + 2).Value = y + 1
                End If
                .SelectedIndex = 0
            End With
        Else
            ddlYear.Items.Add(y)
            ddlYear.Items(0).Value = y
            ddlYear.Items.Add(y + 1)
            ddlYear.Items(1).Value = y + 1
            ddlYear.SelectedIndex = 0
        End If
        ddlYear.SelectedValue = Request.Cookies("EDUYEAR").Value
        dt = Nothing
    End Sub
    Private Sub LoadGroupToDDL()
        Dim ctlFct As New LocationGroupController
        dt = ctlFct.LocationGroup_Get
        If dt.Rows.Count > 0 Then
            With ddlLocationTypeAdd
                .Enabled = True
                .DataSource = dt
                .DataTextField = "Name"
                .DataValueField = "Code"
                .DataBind()
                .SelectedIndex = 0
            End With

            With ddlLocationTypeDel
                .Enabled = True
                .DataSource = dt
                .DataTextField = "Name"
                .DataValueField = "Code"
                .DataBind()
                .SelectedIndex = 0
            End With

        End If
        dt = Nothing
    End Sub

    Private Sub LoadLocationNotWorkSkillToGrid()

        'dt = ctlCs.LocationSkill_GetLocationNoCourse(StrNull2Zero(ddlYear.SelectedValue), StrNull2Zero(ddlCourse.SelectedValue), Trim(txtSearch.Text))
        dt = ctlSL.SkillLocation_GetLocationNoWorkSkill(StrNull2Zero(ddlYear.SelectedValue), StrNull2Zero(ddlSkill.SelectedValue), Trim(txtSearch.Text))
        If dt.Rows.Count > 0 Then

            With grdLocation
                .Visible = True
                .DataSource = dt
                .DataBind()
            End With
        Else
            grdLocation.Visible = False
        End If
    End Sub

    Private Sub LoadLocationInWorkSkillToGrid()
        'dt = ctlCs.CourseLocation_GetLocationInCourse(StrNull2Zero(ddlYear.SelectedValue), StrNull2Zero(ddlCourse.SelectedValue), Trim(txtSearchLocation.Text))

        dt = ctlSL.SkillLocation_GetLocationInWorkSkill(StrNull2Zero(ddlYear.SelectedValue), StrNull2Zero(ddlSkill.SelectedValue), Trim(txtSearchLocation.Text))

        If dt.Rows.Count > 0 Then
            lblCount.Text = dt.Rows.Count
            lblNo.Visible = False
            With grdLocationSkill
                .Visible = True
                .DataSource = dt
                .DataBind()
            End With
        Else
            lblCount.Text = 0
            lblNo.Visible = True
            grdLocationSkill.Visible = False
        End If
    End Sub

    Private Sub LoadSkillToDDL()
        dt = ctlSk.Skill_Get4Selection
        If dt.Rows.Count > 0 Then
            With ddlSkill
                .Enabled = True
                .DataSource = dt
                .DataTextField = "Name"
                .DataValueField = "UID"
                .DataBind()
                .SelectedIndex = 0
            End With
        End If
        dt = Nothing
    End Sub

    Private Sub grdLocation_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdLocation.PageIndexChanging
        grdLocation.PageIndex = e.NewPageIndex
        LoadLocationNotWorkSkillToGrid()
    End Sub
    Private Sub grdLocation_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdLocation.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#ffdfef';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")
        End If

    End Sub
    Private Sub AddLocationToWorkSkill()

        Dim item As Integer
        Dim ctlLW As New LocationWorkController

        For i = 0 To grdLocation.Rows.Count - 1
            With grdLocation
                Dim chkS As CheckBox = .Rows(i).Cells(0).FindControl("chkSelect")
                If chkS.Checked Then
                    ctlLW.LocationWorkDetail_Save(.DataKeys(i).Value, StrNull2Zero(ddlSkill.SelectedValue))

                    item = ctlSL.SkillLocation_Add(ddlYear.SelectedValue, ddlSkill.SelectedValue, .DataKeys(i).Value, "Y", "Y", Request.Cookies("UserLogin").Value)

                    acc.User_GenLogfile(Request.Cookies("UserLogin").Value, ACTTYPE_ADD, "LocationSkill", "เพิ่ม แหล่งฝึกในรายวิชาฝึก:" & ddlSkill.SelectedItem.Text & ">>" & .Rows(i).Cells(1).Text, "")

                End If


            End With
        Next
        LoadLocationInWorkSkillToGrid()
        LoadLocationNotWorkSkillToGrid()
         ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)
    End Sub


    Private Sub grdLocationSkill_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdLocationSkill.PageIndexChanging
        grdLocationSkill.PageIndex = e.NewPageIndex
        LoadLocationInWorkSkillToGrid()
    End Sub

    Private Sub grdLocationSkill_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdLocationSkill.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgDel"
                    If ctlSL.SkillLocation_Delete(e.CommandArgument) Then
                        acc.User_GenLogfile(Request.Cookies("UserLogin").Value, ACTTYPE_DEL, "LocationSkill", "ลบ แหล่งฝึกในรายวิชาฝึก:" & ddlSkill.SelectedItem.Text & ">>itemID:" & e.CommandArgument, "")
                        DisplayMessage(Me, "ลบข้อมูลเรียบร้อย")
                        LoadLocationInWorkSkillToGrid()
                        LoadLocationNotWorkSkillToGrid()
                    Else
                        DisplayMessage(Me, "ไม่สามารถลบข้อมูลได้ กรุณาตรวจสอบและลองใหม่อีกครั้ง")
                    End If


            End Select


        End If
    End Sub

    Private Sub grdLocationSkill_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdLocationSkill.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then

            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(3).FindControl("imgDel")
            imgD.Attributes.Add("onClick", scriptString)

        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#ffdfef';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If

    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click
        AddLocationToWorkSkill()
    End Sub

    Protected Sub cmdClear_Click(sender As Object, e As EventArgs) Handles cmdClear.Click
        For i = 0 To grdLocation.Rows.Count - 1
            With grdLocation
                Dim chkS As CheckBox = .Rows(i).Cells(0).FindControl("chkSelect")
                chkS.Checked = False
            End With
        Next
    End Sub

    Protected Sub ddlYear_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlYear.SelectedIndexChanged

        grdLocationSkill.PageIndex = 0
        grdLocation.PageIndex = 0
        LoadSkillToDDL()
        LoadLocationInWorkSkillToGrid()
        LoadLocationNotWorkSkillToGrid()
    End Sub

    Protected Sub ddlSkill_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSkill.SelectedIndexChanged

        grdLocationSkill.PageIndex = 0
        grdLocation.PageIndex = 0
        LoadLocationInWorkSkillToGrid()
        LoadLocationNotWorkSkillToGrid()
    End Sub

    Protected Sub cmdFindStd_Click(sender As Object, e As EventArgs) Handles cmdFindLocationInCourse.Click
        grdLocationSkill.PageIndex = 0
        LoadLocationInWorkSkillToGrid()
    End Sub

    Protected Sub cmdFind_Click(sender As Object, e As EventArgs) Handles cmdFind.Click
        grdLocation.PageIndex = 0
        LoadLocationNotWorkSkillToGrid()
    End Sub

    Protected Sub lnkSubmitDel_Click(sender As Object, e As EventArgs) Handles lnkSubmitDel.Click
        ctlSL.SkillLocation_DeleteByGroup(ddlLocationTypeDel.SelectedValue, ddlSkill.SelectedValue)

        grdLocationSkill.PageIndex = 0
        grdLocation.PageIndex = 0
        LoadLocationInWorkSkillToGrid()
        LoadLocationNotWorkSkillToGrid()

        DisplayMessage(Me.Page, "ลบเรียบร้อย")
    End Sub

    Protected Sub lnkSubmitAdd_Click(sender As Object, e As EventArgs) Handles lnkSubmitAdd.Click
        Dim ctlL As New LocationController

        dt = ctlL.Location_GetByGroupID(ddlLocationTypeAdd.SelectedValue)

        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                ctlSL.SkillLocation_Add(ddlYear.SelectedValue, ddlSkill.SelectedValue, dt.Rows(i)("LocationID"), "Y", "Y", Request.Cookies("UserLogin").Value)

            Next
        End If

        acc.User_GenLogfile(Request.Cookies("UserLogin").Value, ACTTYPE_ADD, "LocationSkill", "เพิ่ม แหล่งฝึกในรายวิชาฝึกทั้งประเภท:" & ddlLocationTypeAdd.SelectedValue & ">>" & ddlSkill.SelectedItem.Text, "")

        grdLocationSkill.PageIndex = 0
        grdLocation.PageIndex = 0
        LoadLocationInWorkSkillToGrid()
        LoadLocationNotWorkSkillToGrid()

        DisplayMessage(Me.Page, "บันทึกเรียบร้อย")

    End Sub
End Class

