﻿
Public Class GradePreceptorCopy
    Inherits System.Web.UI.Page

    Dim ctlG As New AssessmentController
    Dim ctlCs As New CourseController
    Dim dt As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("UserLogin").Value) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If
        If Not IsPostBack Then
            LoadYearToDDL()
            LoadSubjectToDDL()
            LoadSubjectNoGradeToGrid()
        End If
    End Sub
    Private Sub LoadYearToDDL()
        Dim y As Integer = StrNull2Zero(DisplayYear(ctlCs.GET_DATE_SERVER))
        Dim LastRow As Integer
        dt = ctlCs.Courses_GetYear
        LastRow = dt.Rows.Count - 1

        If dt.Rows.Count > 0 Then
            With ddlYear
                .Enabled = True
                .DataSource = dt
                .DataTextField = "CYear"
                .DataValueField = "CYear"
                .DataBind()

                If dt.Rows(LastRow)(0) = y Then
                    ddlYear.Items.Add(y + 1)
                    ddlYear.Items(LastRow + 1).Value = y + 1
                ElseIf dt.Rows(LastRow)(0) > y Then
                    'ddlYear.Items.Add(y + 2)
                    'ddlYear.Items(LastRow + 1).Value = y + 2
                ElseIf dt.Rows(LastRow)(0) < y Then
                    ddlYear.Items.Add(y)
                    ddlYear.Items(LastRow + 1).Value = y
                    ddlYear.Items.Add(y + 1)
                    ddlYear.Items(LastRow + 2).Value = y + 1
                End If
                .SelectedIndex = 0
            End With

        Else
            ddlYear.Items.Add(y)
            ddlYear.Items(0).Value = y
            ddlYear.Items.Add(y + 1)
            ddlYear.Items(1).Value = y + 1
            ddlYear.SelectedIndex = 0
        End If
        ddlYear.SelectedValue = Request.Cookies("EDUYEAR").Value
        dt = Nothing
    End Sub
    Private Sub LoadSubjectToDDL()
        Dim ctlSubj As New CourseController

        dt = ctlSubj.Courses_GetByYear(ddlYear.SelectedValue)

        ddlSubject.Items.Clear()
        If dt.Rows.Count > 0 Then
            ddlSubject.Items.Clear()

            With ddlSubject
                .Items.Add("-")
                .Items(0).Value = "-"
            End With

            For i = 0 To dt.Rows.Count - 1
                With ddlSubject
                    .Items.Add("" & dt.Rows(i)("SubjectCode") & " : " & dt.Rows(i)("NameTH"))
                    .Items(i + 1).Value = dt.Rows(i)("SubjectCode")
                End With
            Next
        Else
            ddlSubject.Items.Clear()
            dt = Nothing
        End If
    End Sub

    Private Sub LoadSubjectNoGradeToGrid()

        dt = ctlG.GradePreceptor_GetSubjectNoGrade(StrNull2Zero(ddlYear.SelectedValue))
        If dt.Rows.Count > 0 Then
            With grdSubject
                .Visible = True
                .DataSource = dt
                .DataBind()
            End With
            lblNo.Visible = False
            chkAll.Visible = True
            cmdSave.Visible = True
        Else
            lblNo.Visible = True
            chkAll.Visible = False
            cmdSave.Visible = False
        End If

        dt = Nothing
    End Sub

    Private Sub grdData_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdSubject.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#ffdfef';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If

    End Sub
    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click
        Dim dtCs As New DataTable

        If chkAll.Checked Then
            dtCs = ctlCs.Courses_GetByYear(ddlYear.SelectedValue)
            For i = 0 To dtCs.Rows.Count - 1
                ctlG.GradePreceptor_Copy(StrNull2Zero(ddlYear.SelectedValue), ddlSubject.SelectedValue, dtCs.Rows(i)("SubjectCode"))
            Next

        Else
            For i = 0 To grdSubject.Rows.Count - 1
                With grdSubject
                    Dim chkS As CheckBox = .Rows(i).Cells(0).FindControl("chkSelect")
                    If chkS.Checked Then
                        ctlG.GradePreceptor_Copy(StrNull2Zero(ddlYear.SelectedValue), ddlSubject.SelectedValue, .DataKeys(i).Value)
                    End If
                End With
            Next
        End If
        LoadSubjectNoGradeToGrid()
         ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)
    End Sub

    Protected Sub ddlYear_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlYear.SelectedIndexChanged
        LoadSubjectToDDL()
        LoadSubjectNoGradeToGrid()
    End Sub

End Class

