﻿Imports System.IO
Imports System.Data.OleDb
Imports System.Data
Imports Subgurim.Controles
Public Class CourseStudent_Modify
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlCs As New CourseController
    Dim acc As New UserController
    Dim ctlSk As New SkillController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("UserLogin").Value) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If
        If Not IsPostBack Then
            LoadYearToDDL()
            LoadCourseToDDL()
            LoadSkillToDDL()
            LoadStudentCourseToGrid()
        End If
    End Sub
    Private Sub LoadYearToDDL()
        Dim y As Integer = StrNull2Zero(DisplayYear(ctlCs.GET_DATE_SERVER))
        Dim LastRow As Integer
        dt = ctlCs.Courses_GetYear
        LastRow = dt.Rows.Count - 1

        If dt.Rows.Count > 0 Then
            With ddlYear
                .Enabled = True
                .DataSource = dt
                .DataTextField = "CYear"
                .DataValueField = "CYear"
                .DataBind()

                If dt.Rows(LastRow)(0) = y Then
                    ddlYear.Items.Add(y + 1)
                    ddlYear.Items(LastRow + 1).Value = y + 1
                ElseIf dt.Rows(LastRow)(0) > y Then
                    'ddlYear.Items.Add(y + 2)
                    'ddlYear.Items(LastRow + 1).Value = y + 2
                ElseIf dt.Rows(LastRow)(0) < y Then
                    ddlYear.Items.Add(y)
                    ddlYear.Items(LastRow + 1).Value = y
                    ddlYear.Items.Add(y + 1)
                    ddlYear.Items(LastRow + 2).Value = y + 1
                End If
                .SelectedIndex = 0
            End With
        Else
            ddlYear.Items.Add(y)
            ddlYear.Items(0).Value = y
            ddlYear.Items.Add(y + 1)
            ddlYear.Items(1).Value = y + 1
            ddlYear.SelectedIndex = 0
        End If
        ddlYear.SelectedValue = Request.Cookies("EDUYEAR").Value
        dt = Nothing
    End Sub

    Private Sub LoadSkillToDDL()
        dt = ctlSk.Skill_Get4Selection
        If dt.Rows.Count > 0 Then
            With ddlSkill
                .Enabled = True
                .DataSource = dt
                .DataTextField = "Name"
                .DataValueField = "UID"
                .DataBind()
                .SelectedIndex = 0
            End With
        End If
        dt = Nothing
    End Sub
    Private Sub LoadStudentCourseToGrid()

        If StrNull2Zero(ddlCourse.SelectedValue) <> 0 Then
            dt = ctlCs.CourseStudent_Get(StrNull2Zero(ddlYear.SelectedValue), StrNull2Zero(ddlCourse.SelectedValue), Trim(txtSearchStd.Text))

            If dt.Rows.Count > 0 Then
                lblCount.Text = dt.Rows.Count
                lblNo.Visible = False
                With grdStudent
                    .Visible = True
                    .DataSource = dt
                    .DataBind()
                End With
            Else
                lblCount.Text = 0
                lblNo.Visible = True
                grdStudent.Visible = False
            End If
        Else
            lblCount.Text = 0
            lblNo.Visible = True
            grdStudent.Visible = False
        End If
    End Sub

    Private Sub LoadCourseToDDL()
        ddlCourse.Items.Clear()

        If Request.Cookies("ROLE_ADM").Value = True Then
            dt = ctlCs.Courses_GetByYear(ddlYear.SelectedValue)
        Else
            If Request.Cookies("ROLE_ADV").Value = True Then
                dt = ctlCs.Courses_GetByCoordinator(ddlYear.SelectedValue, DBNull2Zero(Request.Cookies("ProfileID").Value))
            Else
                Exit Sub
            End If
        End If

        If dt.Rows.Count > 0 Then
            ddlCourse.Items.Clear()
            For i = 0 To dt.Rows.Count - 1
                With ddlCourse
                    .Items.Add("" & dt.Rows(i)("SubjectCode") & " : " & dt.Rows(i)("NameTH"))
                    .Items(i).Value = dt.Rows(i)("CourseID")
                End With
            Next
            Panel1.Visible = True

        Else
            ddlCourse.Items.Clear()
            Panel1.Visible = False

        End If

    End Sub
    Private Sub grdStudent_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdStudent.PageIndexChanging
        grdStudent.PageIndex = e.NewPageIndex
        LoadStudentCourseToGrid()
    End Sub

    Private Sub grdStudent_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdStudent.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit"
                    EditData(e.CommandArgument())
            End Select
        End If
    End Sub

    Private Sub EditData(UID As Integer)
        dt = ctlCs.CourseStudent_GetByUID(UID)
        If dt.Rows.Count > 0 Then
            ddlYear.SelectedValue = dt.Rows(0)("CYear")
            ddlCourse.SelectedValue = dt.Rows(0)("CourseID")
            ddlSkill.SelectedValue = dt.Rows(0)("SkillUID")
            txtStudentCode.Text = dt.Rows(0)("Student_Code")
            lblStudentName.Text = dt.Rows(0)("FirstName") & " " & dt.Rows(0)("LastName")

            ddlYear.Enabled = False
            ddlCourse.Enabled = False
            txtStudentCode.ReadOnly = True
        Else
            DisplayMessage(Me.Page, "ไม่พบข้อมูล")
        End If

    End Sub

    Private Sub grdStudent_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdStudent.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then

            'Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            'Dim imgD As Image = e.Row.Cells(6).FindControl("imgDel")
            'imgD.Attributes.Add("onClick", scriptString)

        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#ffdfef';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If

    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click
        ctlCs.CourseStudent_Save(ddlCourse.SelectedValue, txtStudentCode.Text, Request.Cookies("UserLogin").Value, StrNull2Zero(ddlSkill.SelectedValue))

        acc.User_GenLogfile(Request.Cookies("UserLogin").Value, ACTTYPE_ADD, "CourseStudent", "เพิ่ม นักศึกษาในรายวิชาฝึก:" & ddlCourse.SelectedValue & ">>" & txtStudentCode.Text, "")
        LoadStudentCourseToGrid()
         ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)
    End Sub

    Protected Sub ddlYear_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlYear.SelectedIndexChanged
        grdStudent.PageIndex = 0
        LoadCourseToDDL()
        LoadStudentCourseToGrid()
    End Sub

    Protected Sub ddlCourse_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCourse.SelectedIndexChanged
        grdStudent.PageIndex = 0
        LoadStudentCourseToGrid()
    End Sub

    Protected Sub cmdFindStd_Click(sender As Object, e As EventArgs) Handles cmdFindStd.Click
        grdStudent.PageIndex = 0
        LoadStudentCourseToGrid()
    End Sub

    Protected Sub cmdImport_Click(sender As Object, e As EventArgs) Handles cmdImport.Click
        System.Threading.Thread.Sleep(3000)
        UpdateProgress1.Visible = True

        Dim connectionString As String = ""
        Try

            lblResult.Visible = False

            Dim fileName As String = Path.GetFileName("~/" & tmpUpload & "/" & Session("fname"))
            Dim fileExtension As String = Path.GetExtension("~/" & tmpUpload & "/" & Session("fname"))

            Dim fileLocation As String = Server.MapPath("~/" & tmpUpload & "/" & fileName)

            'Check whether file extension is xls or xslx

            If fileExtension = ".xls" Then
                connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & fileLocation & ";Extended Properties=""Excel 8.0;HDR=Yes;IMEX=2"""
            ElseIf fileExtension = ".xlsx" Then
                connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & fileLocation & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""
            End If

            'Create OleDB Connection and OleDb Command

            Dim con As New OleDbConnection(connectionString)
            Dim cmd As New OleDbCommand()
            cmd.CommandType = System.Data.CommandType.Text
            cmd.Connection = con
            Dim dAdapter As New OleDbDataAdapter(cmd)
            Dim dtExcelRecords As New DataTable()
            con.Open()
            Dim dtExcelSheetName As DataTable = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
            Dim getExcelSheetName As String = dtExcelSheetName.Rows(0)("Table_Name").ToString()
            cmd.CommandText = "SELECT * FROM [" & getExcelSheetName & "]"
            dAdapter.SelectCommand = cmd
            dAdapter.Fill(dtExcelRecords)
            con.Close()

            Dim k As Integer = 0
            For i = 0 To dtExcelRecords.Rows.Count - 1
                With dtExcelRecords.Rows(i)
                    If .Item(0).ToString <> "" Then
                        ctlCs.CourseStudent_Save(.Item(0).ToString, .Item(1), Request.Cookies("UserLogin").Value, .Item(2))
                        k = k + 1
                    End If
                End With
            Next
            'grdData.DataSource = dtExcelRecords
            'grdData.DataBind()


            acc.User_GenLogfile(Request.Cookies("UserLogin").Value, ACTTYPE_ADD, "StudentCourse", "Update รายวิชา/งาน นักศึกษา : " & k & " คน", "จากไฟล์ excel")

            lblResult.Text = "ข้อมูลทั้งหมด " & k & "เรคอร์ด ถูก update เรียบร้อย"
            lblResult.Visible = True
            dtExcelRecords = Nothing

            LoadStudentCourseToGrid()
            UpdateProgress1.Visible = False
        Catch ex As Exception
            DisplayMessage(Me.Page, "Error : " & ex.Message)
        End Try
    End Sub
End Class

