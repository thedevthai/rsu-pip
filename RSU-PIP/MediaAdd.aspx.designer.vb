﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class MediaAdd
    
    '''<summary>
    '''lblID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblID As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''txtOrder control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtOrder As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtTitle control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtTitle As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''ddlLevel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlLevel As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''ddlMajor control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlMajor As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''ddlGroup control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlGroup As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''optType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optType As Global.System.Web.UI.WebControls.RadioButtonList
    
    '''<summary>
    '''pnURL control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnURL As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''txtURL control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtURL As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''pnFile control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnFile As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''hlnkNews control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hlnkNews As Global.System.Web.UI.WebControls.HyperLink
    
    '''<summary>
    '''FileUpload control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents FileUpload As Global.System.Web.UI.WebControls.FileUpload
    
    '''<summary>
    '''chkFistPage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkFistPage As Global.System.Web.UI.WebControls.CheckBox
    
    '''<summary>
    '''chkStatus control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkStatus As Global.System.Web.UI.WebControls.CheckBox
    
    '''<summary>
    '''cmdSave control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cmdSave As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''cmdCancel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cmdCancel As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''LinkButton2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LinkButton2 As Global.System.Web.UI.WebControls.LinkButton
End Class
