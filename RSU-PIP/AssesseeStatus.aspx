﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="AssesseeStatus.aspx.vb" Inherits=".AssesseeStatus" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    
  
</asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <section class="content-header">
      <h1>ตรวจสอบสถานะการประเมิน</h1>   
    </section>

<section class="content">  

     <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-Search"></i>

              <h3 class="box-title">ค้นหา</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                                 
            </div>
            <div class="box-body"> 
              
<table border="0" align="left" cellPadding="1" cellSpacing="1">
<tr>
                                                    <td align="left" class="texttopic">
                                                        ปีการศึกษา</td>
                                                    <td align="left">
                                                        <asp:Label ID="lblYear" runat="server"></asp:Label>
                                                    </td>
</tr>
<tr>
  <td align="left" class="texttopic">รายวิชา</td>
  <td align="left">
                                                      <asp:DropDownList ID="ddlCourse" runat="server" CssClass="Objcontrol" AutoPostBack="True">                                                      </asp:DropDownList>                                                    </td>
</tr>
<tr>
  <td align="left" class="texttopic">แหล่งฝึก</td>
  <td align="left">
                                                      <asp:DropDownList ID="ddlLocation" runat="server" CssClass="Objcontrol" AutoPostBack="True">                                                      </asp:DropDownList>                                                    </td>
</tr>
<tr>
  <td align="left" class="texttopic">ค้นหานักศึกษา</td>
  <td align="left">
                  <asp:TextBox ID="txtSearch" runat="server" CssClass="Objcontrol"></asp:TextBox>                                                    &nbsp;<span class="text10_blue">(ค้นหาจากรหัสนักศึกษา ,ชื่อ หรือ นามสกุล)</span></td>
</tr>
<tr>
  <td align="left" class="texttopic">สถานะการประเมิน</td>
  <td align="left">
                  <asp:RadioButtonList ID="optStatus" runat="server" RepeatDirection="Horizontal">
                      <asp:ListItem Selected="True" Value="">ทั้งหมด</asp:ListItem>
                      <asp:ListItem Value="Y">ประเมินแล้ว</asp:ListItem>
                      <asp:ListItem Value="N">ยังไม่ประเมิน</asp:ListItem>
                  </asp:RadioButtonList>
    </td>
</tr>
<tr>
  <td align="left" class="texttopic">&nbsp;</td>
  <td align="left">
                  <asp:Button ID="cmdFind" runat="server" CssClass="btn btn-save" Text="ค้นหา" Width="100px" />
    </td>
</tr>
  </table>  
   
                                         
</div>
            <div class="box-footer clearfix">
           
            </div>
          </div>

   
     <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-user-circle-o"></i>

              <h3 class="box-title">รายการผลการประเมิน</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                                 
            </div>
            <div class="box-body"> 
 <asp:GridView ID="grdData" 
                             runat="server" CellPadding="0" ForeColor="#333333" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" Width="100%" AllowPaging="True" PageSize="20">
            <RowStyle BackColor="#F7F7F7" HorizontalAlign="Center" />
            <columns>
            <asp:BoundField HeaderText="No." DataField="nRow">
              <itemstyle HorizontalAlign="Center" VerticalAlign="Top" Width="30px" />                      </asp:BoundField>
            <asp:BoundField DataField="Student_Code" HeaderText="รหัสนักศึกษา">
                <ItemStyle Width="90px" />
                </asp:BoundField>
                <asp:BoundField DataField="StudentName" HeaderText="ชื่อ - สกุล">
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="MajorName" HeaderText="สาขาวิชา" />
                <asp:BoundField DataField="SubjectName" HeaderText="รายวิชาที่ฝึก" />
                <asp:BoundField DataField="LocationName" HeaderText="แหล่งฝึก" >
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="สถานะ">
              <itemtemplate>
                <asp:ImageButton ID="imgStatus" runat="server" ImageUrl="images/icon-ok.png" Visible='<%# ConvertYN2Boolean(DataBinder.Eval(Container.DataItem, "AssessmentStatus")) %>' />                        

              </itemtemplate>
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />          
            </asp:TemplateField>
            </columns>
            <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />          
            <pagerstyle CssClass="dc_pagination dc_paginationC dc_paginationC11" HorizontalAlign ="Center" />          
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <headerstyle CssClass="th"  Font-Bold="false" HorizontalAlign="Center" 
                      VerticalAlign="Middle" />          
            <EditRowStyle BackColor="#2461BF" />
            <AlternatingRowStyle BackColor="White" />
          </asp:GridView>
                                       
</div>
            <div class="box-footer clearfix">
           
            </div>
          </div>
    



</section>    

</asp:Content>
