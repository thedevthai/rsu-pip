﻿Public Class PracticeHistoryModify
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlAss As New AssessmentController
    Dim ctlSk As New SkillController
    Dim ctlU As New UserController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("UserLogin").Value) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If
        If Not IsPostBack Then
            LoadYearToDDL()
            LoadLocationToDDL()
            LoadSkillToDDL()
            LoadSubjectToDDL()
            If Not Request("id") = Nothing Then
                LoadAssessment(Request("id"))
            Else
                If Not Request("stdcode") = Nothing Then
                    txtCode.Text = Request("stdcode")
                    LoadStudentData()

                    txtUID.Enabled = False

                End If
            End If

        End If
        txtAccumulatedHours.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")

    End Sub
    Private Sub LoadYearToDDL()
        Dim ctlM As New MasterController
        dt = ctlM.EduYear_Gen

        Dim StartYear, EndYear, iYear, n As Integer
        n = 0
        If dt.Rows.Count > 0 Then
            StartYear = dt.Rows(0)("StartYear")
            EndYear = dt.Rows(0)("EndYear")
            iYear = StartYear
            Do While iYear <= EndYear
                ddlYear.Items.Add(iYear)
                ddlYear.Items(n).Value = iYear

                iYear = iYear + 1
                n = n + 1
            Loop
        End If
        ddlYear.SelectedValue = Request.Cookies("EDUYEAR").Value
        dt = Nothing
    End Sub
    Private Sub LoadSubjectToDDL()
        Dim ctlSubj As New SubjectController
        dt = ctlSubj.Subject_Get
        ddlSubject.Items.Clear()
        If dt.Rows.Count > 0 Then
            ddlSubject.Items.Clear()
            ddlSubject.Items.Add("")
            ddlSubject.Items(0).Value = ""

            For i = 0 To dt.Rows.Count - 1
                With ddlSubject
                    .Items.Add("" & dt.Rows(i)("SubjectCode") & " : " & dt.Rows(i)("NameTH"))
                    .Items(i + 1).Value = dt.Rows(i)("SubjectCode")
                End With
            Next
        Else
            ddlSubject.Items.Clear()
            dt = Nothing
        End If
    End Sub

    Private Sub LoadLocationToDDL()
        ddlLocation.Items.Clear()
        Dim ctlL As New LocationController
        Dim dtL As New DataTable
        dtL = ctlL.Location_GetActive

        If dtL.Rows.Count > 0 Then
            ddlLocation.Items.Clear()
            With ddlLocation
                .Items.Add("")
                .Items(0).Value = 0

                For i = 0 To dtL.Rows.Count - 1
                    .Items.Add("" & dtL.Rows(i)("LocationName"))
                    .Items(i + 1).Value = dtL.Rows(i)("LocationID")
                Next
            End With
        End If

    End Sub
    Private Sub LoadSkillToDDL()
        dt = ctlSk.Skill_GetActive
        If dt.Rows.Count > 0 Then
            With ddlSkill
                .Enabled = True
                .DataSource = dt
                .DataTextField = "Name"
                .DataValueField = "UID"
                .DataBind()
                .SelectedIndex = 0
            End With
        End If
        dt = Nothing
    End Sub
    Private Sub LoadTurnPhase()
        ddlPhase.Items.Clear()
        Dim ctlTP As New TimePhaseController
        Dim dtP As New DataTable
        dtP = ctlTP.TurnPhase_GetByYear(StrNull2Zero(ddlYear.SelectedValue))
        ddlPhase.DataSource = dtP
        ddlPhase.DataTextField = "PhaseDesc"
        ddlPhase.DataValueField = "PhaseID"
        ddlPhase.DataBind()
        dtP = Nothing
    End Sub
    'Private Sub LoadSkillPhase()
    '    ddlPhase.Items.Clear()
    '    Dim ctlTP As New SkillPhaseController
    '    Dim dtP As New DataTable

    '    dtP = ctlTP.SkillPhase_GetBySkill(StrNull2Zero(ddlYear.SelectedValue), 0) 'StrNull2Zero(ddlSkill.SelectedValue)

    '    If dtP.Rows.Count > 0 Then
    '        ddlPhase.Items.Add("")
    '        ddlPhase.Items(0).Value = "0"
    '        For i = 0 To dtP.Rows.Count - 1
    '            ddlPhase.Items.Add(dtP.Rows(i)("PhaseNo") & " : " & dtP.Rows(i)("PhaseName"))
    '            ddlPhase.Items(i + 1).Value = dtP.Rows(i)("SkillPhaseID")
    '        Next
    '    Else
    '        ddlPhase.DataSource = dtP
    '        ddlPhase.DataBind()
    '    End If
    '    dtP = Nothing
    'End Sub
    Private Sub LoadStudentData()
        Dim dtStd As New DataTable
        Dim ctlS As New StudentController
        dtStd = ctlS.Student_GetByCode(txtCode.Text)
        If dtStd.Rows.Count > 0 Then
            With dtStd.Rows(0)
                txtCode.Text = String.Concat(.Item("Student_Code"))
                txtName.Text = String.Concat(.Item("StudentName"))
            End With
        End If
        dtStd = Nothing
    End Sub

    Private Sub LoadAssessment(AssessmentID As Integer)
        Dim dtAsm As New DataTable
        dtAsm = ctlAss.Assessment_GetByUID(AssessmentID)
        If dtAsm.Rows.Count > 0 Then
            With dtAsm.Rows(0)
                ddlYear.SelectedValue = String.Concat(.Item("PYear"))
                ddlLocation.SelectedValue = String.Concat(.Item("LocationID"))
                ddlSkill.SelectedValue = String.Concat(.Item("SkillUID"))
                LoadTurnPhase()
                'ddlPhase.SelectedValue = String.Concat(.Item("SkillPhaseID"))
                ddlPhase.SelectedValue = String.Concat(.Item("PhaseID"))

                If String.Concat(.Item("SubjectCode")) <> "" Then
                    ddlSubject.SelectedValue = String.Concat(.Item("SubjectCode"))
                End If

                txtAccumulatedHours.Text = String.Concat(.Item("AccumulatedHours"))

                txtUID.Text = String.Concat(.Item("AssessmentID"))
                txtCode.Text = String.Concat(.Item("Student_Code"))
                txtName.Text = String.Concat(.Item("StudentName"))

            End With
        End If
        dtAsm = Nothing
    End Sub
    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click

        ctlAss.Assessment_Save(StrNull2Zero(txtUID.Text), StrNull2Zero(ddlYear.SelectedValue), txtCode.Text, StrNull2Zero(ddlLocation.SelectedValue), StrNull2Zero(ddlPhase.SelectedValue), StrNull2Zero(ddlSkill.SelectedValue), ddlSubject.SelectedValue, StrNull2Zero(txtAccumulatedHours.Text), Request.Cookies("UserLoginID").Value)

        ctlU.User_GenLogfile(Request.Cookies("UserLogin").Value, "ADD/UPDATE", "เพิ่ม/แก้ไข Assessment from PracticeHistory", "แก้ไขประวัติการฝึก ID:" & txtUID.Text & "stdcode=" & txtCode.Text, "")

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)
    End Sub

    Protected Sub cmdBack_Click(sender As Object, e As EventArgs) Handles cmdBack.Click
        Response.Redirect("PracticeHistory?ActionType=std&ItemType=pthistory&std=" & txtCode.Text)
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocation.SelectedIndexChanged
        'LoadSkillPhase()
    End Sub

    Private Sub ddlYear_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlYear.SelectedIndexChanged
        LoadTurnPhase()
    End Sub
End Class

