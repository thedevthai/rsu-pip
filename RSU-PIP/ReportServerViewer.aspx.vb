﻿Imports System.Net
Imports System.Data.SqlClient
Imports Microsoft.Reporting.WebForms
Imports System.IO
Imports System.Text

Public Class ReportServerViewer
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("UserLogin").Value) Then
            Response.Redirect("Default.aspx")
        End If
        If Not IsPostBack Then
            LoadReport()
        End If
        'Response.AppendHeader("content-disposition", "attachment;filename=xxx.pdf")
        'Response.Charset = ""
        'Response.ContentType = "application/vnd.ms-pdf"
        'Response.Write("<script type='text/javascript'>window.close();</script>")
    End Sub
    Private Sub LoadReport()

        'System.Threading.Thread.Sleep(1000)
        'UpdateProgress1.Visible = True


        Dim credential As New ReportServerCredentials
        ReportViewer1.Reset()
        ReportViewer1.ServerReport.ReportServerCredentials = credential
        ReportViewer1.ServerReport.ReportServerUrl = credential.ReportServerUrl
        ReportViewer1.ShowPrintButton = True

        'Me.ReportViewer1.SetDisplayMode(DisplayMode.PrintLayout)

        'System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("th-TH")
        Dim xParam As New List(Of ReportParameter)

        Select Case FagRPT

            Case "REQACCOUNT", "REQESTIMATE"
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath(ReportName)
                xParam.Add(New ReportParameter("Year", Request("y")))
                xParam.Add(New ReportParameter("LocationGroupID", Request("lgrp")))
            Case "DOCSTDLIST"
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath(ReportName)
                xParam.Add(New ReportParameter("Year", Request("y")))
                xParam.Add(New ReportParameter("LocationGroupID", Request("lg")))
                xParam.Add(New ReportParameter("LevelClass", Request("lv")))
                xParam.Add(New ReportParameter("MajorID", Request("mj")))
                xParam.Add(New ReportParameter("LocationID", Request("l")))
                xParam.Add(New ReportParameter("UserID", Request.Cookies("UserLoginID").Value))
            Case "STDLOCATION", "SCORE", "GRADE", "ASSESSMENTRESULT"
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath(ReportName)
                xParam.Add(New ReportParameter("Year", Request("y")))
                xParam.Add(New ReportParameter("SubjectCode", Request("c")))

            Case "STDASSESSMENT2", "PAYMENTBYCOURSE"
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath(ReportName)
                xParam.Add(New ReportParameter("Year", Request("y")))
                xParam.Add(New ReportParameter("CourseID", Request("c")))

            Case "STDASSBYCOURSE", "PHASECOUNT"
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath(ReportName)
                xParam.Add(New ReportParameter("Year", Request("y")))
                xParam.Add(New ReportParameter("LocationGroupID", "0"))
            Case "TIMEPHASE", "STDASSESSMENT", "STDASSBYSTD"
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath(ReportName)
                xParam.Add(New ReportParameter("Year", Request("y")))
            Case "DOC"
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath(ReportName)
                xParam.Add(New ReportParameter("Year", Request("y")))
                xParam.Add(New ReportParameter("DocumentUID", Request("d")))

            Case "HOUR"
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath(ReportName)
                xParam.Add(New ReportParameter("LevelClass", Request("lv")))
            Case "PAYAMOUNT"
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath(ReportName)
                xParam.Add(New ReportParameter("Year", Request("y")))
                xParam.Add(New ReportParameter("CourseID", "0"))
            Case "PRECEPTOR_SCORE", "PRECEPTOR_SCORE_ALL"
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath(ReportName)
                xParam.Add(New ReportParameter("Year", Request("y")))
                xParam.Add(New ReportParameter("SubjectCode", Request("c")))

                If Request.Cookies("ROLE_ADM").Value = True Or Request.Cookies("ROLE_SPA").Value = True Or Request.Cookies("ROLE_TCH").Value = True Or Request.Cookies("ROLE_ADV").Value = True Then
                    xParam.Add(New ReportParameter("LocationID", "0"))
                Else
                    If Session("ROLE_CO-1") = True Then
                        xParam.Add(New ReportParameter("LocationID", "0"))
                    Else
                        xParam.Add(New ReportParameter("LocationID", Request.Cookies("LocationID").Value.ToString()))
                    End If
                End If

        End Select

        ReportViewer1.ServerReport.SetParameters(xParam)

        If Reportskey <> "XLS" Then
            ' Variables
            Dim warnings As Warning()
            Dim streamIds As String()
            Dim mimeType As String = String.Empty
            Dim encoding As String = String.Empty
            Dim extension As String = String.Empty


            ' Setup the report viewer object and get the array of bytes

            Dim bytes As Byte() = ReportViewer1.ServerReport.Render("PDF", Nothing, mimeType, encoding, extension, streamIds, Nothing)

            ' Now that you have all the bytes representing the PDF report, buffer it and send it to the client.
            Response.Buffer = True
            Response.Clear()
            Response.ContentType = mimeType
            'Response.AddHeader("content-disposition", Convert.ToString("attachment; filename=C:\ttt.pdf"))
            Response.BinaryWrite(bytes)
            ' create the file
            'Response.Flush()
            ' send it to the client to download
        Else
            ' Variables
            Dim warnings As Warning()
            Dim streamIds As String()
            Dim mimeType As String = String.Empty
            Dim encoding As String = String.Empty
            Dim extension As String = String.Empty


            ' Setup the report viewer object and get the array of bytes

            Dim bytes As Byte() = ReportViewer1.ServerReport.Render("EXCEL", Nothing, mimeType, encoding, extension, streamIds, Nothing)

            If FagRPT = "DOC" Then

                Dim oFileStream As System.IO.FileStream
                Dim strFilePath As String = String.Empty
                Dim ctlDoc As New DocumentController
                Dim dtDoc As New DataTable
                Dim DocPath, FileDataName As String

                dtDoc = ctlDoc.Document_GetByUID(StrNull2Zero(Request("d")))

                DocPath = dtDoc.Rows(0)("DocumentPath")
                FileDataName = dtDoc.Rows(0)("FileDataName")

                strFilePath = DocPath & "" & FileDataName & ".xls"

                'If File.Exists(strFilePath) Then
                '    File.Delete(strFilePath)
                'End If

                'oFileStream = New System.IO.FileStream(strFilePath, System.IO.FileMode.Create)
                'oFileStream.Write(bytes, 0, bytes.Length)
                'oFileStream.Close()


                Response.AddHeader("content-disposition", Convert.ToString("attachment; filename=" & FileDataName & ".xls"))

                Response.BinaryWrite(bytes)
                Response.Flush()


                'Response.Redirect("DocumentList.aspx?action=Y")
            Else
                ' Now that you have all the bytes representing the PDF report, buffer it and send it to the client.
                Response.Buffer = True
                Response.Clear()
                Response.ContentType = mimeType

                Response.AddHeader("content-disposition", Convert.ToString("attachment; filename=RSUPIP_" & FagRPT & "_" & Request("y") & "." & extension))

                Response.BinaryWrite(bytes)
                ' create the file
                Response.Flush()
            End If

        End If

    End Sub


End Class