﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="PsychologyAssessmentModify.aspx.vb" Inherits=".PsychologyAssessmentModify" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    
    
</asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <section class="content-header">
      <h1>ข้อมูลการประเมินด้านจิตเวช</h1>   
    </section>

<section class="content">  
<div class="row">  
    <section class="col-lg-12 connectedSortable">
     <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-user-circle"></i>

              <h3 class="box-title">ข้อมูลนักศึกษา</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                                 
                <asp:HiddenField ID="hdUID" runat="server" />
            </div>
            <div class="box-body"> 
                <table width="100%" border="0" class="table table-hover" cellPadding="1" cellSpacing="1" >
<tr>
                                                    <td width="80" class="texttopic">
                                                        รหัสนักศึกษา</td>
                                                    <td>
                                                        <asp:Label ID="lblStudentCode" runat="server"></asp:Label>
                                                    </td>                                                  
                                                    <td width="100" class="texttopic">ชื่อ - นามสกุล</td>
                                                    <td>
                                                        <asp:Label ID="lblStudentName" runat="server"></asp:Label>
                                                    </td>
</tr>
<tr>
  <td class="texttopic">ชื่อเล่น</td>
  <td><asp:Label ID="lblNickName" runat="server"></asp:Label></td>
  <td class="texttopic">สาขา</td>
  <td><asp:Label ID="lblMajorName" runat="server"></asp:Label></td>
</tr>

<tr>
  <td class="texttopic">ผู้ประเมิน</td>
  <td>
                                                        
                                                        <asp:Label ID="lblAssessorName" runat="server"></asp:Label>
                                                    </td>
      <td class="texttopic">&nbsp;</td>
                                                    <td>
                                                        
                                                        &nbsp;</td>
</tr>
</table>  
                         
</div>
            <div class="box-footer clearfix">
           
            </div>
          </div>
 </section>
        </div>
<div class="row">
 <section class="col-lg-12 connectedSortable">
    <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-thumbs-up"></i>

              <h3 class="box-title">ภาวะทางด้านจิตเวช</h3>
            </div>
            <div class="box-body mailbox-messages">       
                       <asp:CheckBoxList ID="chkPros" runat="server" CssClass="mailbox-messages">
                          </asp:CheckBoxList>
</div>
            <div class="box-footer text-center">
            </div>
          </div>
</section> 

    </div>
<div class="row">
    <section class="col-lg-12 connectedSortable">
    
  <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-commenting-o"></i>
              <h3 class="box-title">รายละเอียด</h3>
            </div>
            <div class="box-body">       
                       
                                    <asp:TextBox ID="txtComment" runat="server" Height="100px" TextMode="MultiLine" Width="100%" BackColor="#FFFFC1" CssClass="Objcontrol"></asp:TextBox>
                       
</div>
           
          </div>
    <div align="center"> <asp:Button ID="cmdSave" runat="server" Text="บันทึก" CssClass="btn btn-save" Width="100px" /> 
        <asp:Button ID="cmdDelete" runat="server" Text="ลบ" CssClass="btn btn-danger" Width="100px" /> 
                <asp:Button ID="cmdBack" runat="server" Text="<< กลับหน้ารายชื่อนักศึกษา" CssClass="btn btn-find" /> 
     </div>
        <br />
  <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-info"></i>

              <h3 class="box-title">หมายเหตุ</h3>
            </div>
            <div class="box-body">    
                1.  
                <br />
             

    </div>
          
          </div>
</section>
</div>
</section>    
</asp:Content>
