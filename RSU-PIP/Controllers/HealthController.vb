﻿Imports Microsoft.ApplicationBlocks.Data
Public Class HealthController
    Inherits ApplicationBaseClass
    Dim ds As New DataSet
    Public Function StudentVaccine_GetByGroup(StdCode As String, VaccineGroupID As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "StudentVaccine_GetByGroup", StdCode, VaccineGroupID)
        Return ds.Tables(0)
    End Function
    Public Function StudentVaccine_Save(ByVal UID As Integer, StudentCode As String, VaccineGroup As String _
           , ByVal vdate As String _
            , ByVal OrderNo As String _
           , ByVal vaccineName As String _
           , ByVal LotNo As String, HospitalName As String, MUser As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("StudentVaccine_Save"), UID, StudentCode, VaccineGroup, vdate, OrderNo, vaccineName, LotNo, HospitalName, MUser)
    End Function

    Public Function StudentVaccine_GetByUID(pUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "StudentVaccine_GetByUID", pUID)
        Return ds.Tables(0)
    End Function

    Public Function StudentVaccine_Delete(pUID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "StudentVaccine_Delete", pUID)
    End Function

    Public Function Vaccine_UpdateCertificate(ByVal StdCode As String, ByVal FilePath As String, ByVal CUser As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, ("Vaccine_UpdateCertificate"), StdCode, FilePath, CUser)
    End Function



    '-----------------------------------------------------

    Public Function StudentHealth_Get(StdCode As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "StudentHealth_Get", StdCode)
        Return ds.Tables(0)
    End Function
    Public Function StudentHealth_Save(ByVal UID As Integer, StudentCode As String, ByVal AdmitDate As String, Descriptions As String, ByVal Result As String, HospitalName As String, MUser As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("StudentHealth_Save"), UID, StudentCode, AdmitDate, Descriptions, Result, HospitalName, MUser)
    End Function

    Public Function StudentHealth_GetByUID(pUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "StudentHealth_GetByUID", pUID)
        Return ds.Tables(0)
    End Function

    Public Function StudentHealth_Delete(pUID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "StudentHealth_Delete", pUID)
    End Function

    Public Function StudentHealth_UpdateCertificate(ByVal UID As Integer, ByVal StdCode As String, ByVal FilePath As String, ByVal CUser As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, ("StudentHealth_UpdateCertificate"), UID, StdCode, FilePath, CUser)
    End Function

    Public Function Gen_StudentVaccineCovid(ByVal EduYear As String, ByVal Level As String, ByVal ZoneID As String, ProvinceID As String, LocationID As String, ByVal CUser As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, ("Gen_StudentVaccineCovid"), EduYear, Level, ZoneID, ProvinceID, LocationID, UserID)
    End Function

End Class
