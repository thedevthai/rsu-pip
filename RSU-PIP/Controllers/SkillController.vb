﻿Imports Microsoft.ApplicationBlocks.Data
Public Class SkillController
    Inherits ApplicationBaseClass
    Dim ds As New DataSet

    Public Function Skill_GetYear() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Skill_GetYear")
        Return ds.Tables(0)
    End Function


    Public Function Skill_Get() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Skill_GetAll")
        Return ds.Tables(0)
    End Function
    Public Function Skill_Get4Selection() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Skill_Get4Selection")
        Return ds.Tables(0)
    End Function

    Public Function Skill_GetByYear(EduYear As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Skill_GetByYear", EduYear)
        Return ds.Tables(0)
    End Function
    Public Function Skill_GetByLocation(EduYear As Integer, LocationID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Skill_GetByLocation", EduYear, LocationID)
        Return ds.Tables(0)
    End Function
    Public Function Skill_GetByLevelClass(EduYear As Integer, LevelClass As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Skill_GetByLevelClass", EduYear, LevelClass)
        Return ds.Tables(0)
    End Function


    Public Function Skill_GetBySubject(CourseID As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Skill_GetBySubject", CourseID)
        Return ds.Tables(0)
    End Function
    Public Function Skill_GetByUID(UID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Skill_GetUID", UID)
        Return ds.Tables(0)
    End Function
    Public Function Skill_GetByStudent(pYear As Integer, StudentCode As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Skill_GetByStudent", pYear, StudentCode)
        Return ds.Tables(0)
    End Function
    Public Function Skill_Save(ByVal UID As Integer, ByVal Code As Integer, ByVal Name As String, Name2 As String, AliasName As String, ByVal StatusFlag As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Skill_Save"), UID, Code, Name, Name2, AliasName, StatusFlag)
    End Function

    Public Function Skill_Delete(pYear As Integer, pCode As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "Skill_Delete", pYear, pCode)
    End Function

    Public Function Skill_GetActive() As DataTable

        ds = SqlHelper.ExecuteDataset(ConnectionString, "Skill_GetActive")
        Return ds.Tables(0)
    End Function

    Public Function Skill_GetByCode(Code As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Skill_GetByUID", Code)
        Return ds.Tables(0)
    End Function

    Public Function Skill_CheckDup(Code As Integer) As Boolean
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Skill_CheckDup", Code)
        If ds.Tables(0).Rows.Count > 0 Then
            If ds.Tables(0).Rows(0)(0) > 0 Then
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If
    End Function


    Public Function Skill_GetBySearch(Search As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Skill_GetBySearch", Search)
        Return ds.Tables(0)
    End Function

    Public Function Skill_GetNoCourse(pYear As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Skill_GetNoCourse", pYear)
        Return ds.Tables(0)
    End Function

    Public Function Skill_Add(ByVal UID As Integer, ByVal NameTH As String, ByVal NameEN As String, ByVal AliasName As String, ByVal StatusFlag As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Skill_Add"), UID, NameTH, NameEN, AliasName, StatusFlag)
    End Function

    Public Function Skill_Update(ByVal SkillUID_Old As Integer, ByVal SkillUID_New As Integer, ByVal NameTH As String, ByVal NameEN As String, ByVal AliasName As String, ByVal StatusFlag As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "Skill_Update", SkillUID_Old, SkillUID_New, NameTH, NameEN, AliasName, StatusFlag)

    End Function


    Public Function Skill_Delete(ByVal pID As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "Skill_Delete", pID)
    End Function

#Region "SubjectSkill"
    Public Function SubjectSkill_GetBySubject(SubjectID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "SubjectSkill_GetBySubject", SubjectID)
        Return ds.Tables(0)
    End Function
    Public Function SubjectSkill_GetByCourseID(CourseID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "SubjectSkill_GetByCourseID", CourseID)
        Return ds.Tables(0)
    End Function
    Public Function SubjectSkill_DeleteBySubject(SubjectID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "SubjectSkill_DeleteBySubject", SubjectID)
    End Function

    Public Function SubjectSkill_Save(ByVal SubjectID As Integer, ByVal SkillUID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("SubjectSkill_Save"), SubjectID, SkillUID)
    End Function
#End Region
End Class

Public Class SkillLocationController
    Inherits ApplicationBaseClass
    Dim ds As New DataSet
    Public Function SkillLocation_Get() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "SkillLocation_Get")
        Return ds.Tables(0)
    End Function
    Public Function SkillLocation_GetWithBlank() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "SkillLocation_GetWithBlank")
        Return ds.Tables(0)
    End Function

    Public Function SkillLocation_GetByID(id As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "SkillLocation_GetByID", id)
        Return ds.Tables(0)
    End Function
    Public Function SkillLocation_GetByGroup(group As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "SkillLocation_GetByGroup", group)
        Return ds.Tables(0)
    End Function

    Public Function SkillLocation_DeleteByLocation(LocationID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "SkillLocation_DeleteByLocation", LocationID)
    End Function

    Public Function SkillLocation_Get(LocationID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "SkillLocation_Get", LocationID)
        Return ds.Tables(0)
    End Function
    Public Function SkillLocation_Save(EduYear As Integer, LocationID As Integer, SkillUID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "SkillLocation_Save", EduYear, LocationID, SkillUID)
    End Function
    Public Function SkillLocation_Save(id As Integer, group As String, name As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "SkillLocation_Save", id, group, name)
    End Function

    Public Function SkillLocation_GetLocationInWorkSkill(pYear As Integer, SkillUID As Integer, Optional pKey As String = "") As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "SkillLocation_GetLocationInWorkSkill", pYear, SkillUID, pKey)
        Return ds.Tables(0)
    End Function

    Public Function SkillLocation_GetLocationNoWorkSkill(pYear As Integer, pCode As Integer, Optional pKey As String = "") As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("SkillLocation_GetLocationNoWorkSkill"), pYear, pCode, pKey)
        Return ds.Tables(0)
    End Function

    Public Function LocationSkill_Get(LocationID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "SkillLocationSkill_Get", LocationID)
        Return ds.Tables(0)
    End Function
    Public Function SkillLocation_Add(EduYear As Integer, ByVal SkillUID As Integer _
        , ByVal LocationID As Integer _
        , ByVal isTrain As String _
        , ByVal isSPV As String _
        , ByVal MUser As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("SkillLocation_Add"), EduYear, SkillUID, LocationID, isTrain, isSPV, MUser)
    End Function
    Public Function SkillLocation_Delete(pID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "SkillLocation_Delete", pID)
    End Function
    Public Function SkillLocation_DeleteByGroup(LGID As Integer, SkillUID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "SkillLocation_DeleteByGroup", LGID, SkillUID)
    End Function
    Public Function SkillLocation_GetByLocation(pYear As Integer, LocationID As Integer, LevelClass As Integer) As DataTable

        ds = SqlHelper.ExecuteDataset(ConnectionString, "SkillLocation_GetByLocation", pYear, LocationID, LevelClass)
        Return ds.Tables(0)
    End Function
End Class
