﻿Imports Microsoft.ApplicationBlocks.Data

Public Class EventController
    Inherits ApplicationBaseClass
    Dim ds As New DataSet

    Public Function Events_GetAll() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Events_GetAll"))
        Return ds.Tables(0)
    End Function

    Public Function Events_GetList() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Events_GetList"))
        Return ds.Tables(0)
    End Function
    Public Function Calendar_Get() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Event_Get"))
        Return ds.Tables(0)
    End Function


    Public Function Events_Add(ByVal Title As String, FirstPage As Integer, ByVal EventType As String, ByVal LinkPath As String, ByVal isPublic As Integer, ByVal ContentEvent As String, orderNo As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Events_Add"), Title, FirstPage, EventType, LinkPath, isPublic, ContentEvent, orderNo)
    End Function

    Public Function Events_Update(ByVal EventID As Integer, ByVal Title As String, FirstPage As Integer, ByVal EventType As String, ByVal LinkPath As String, ByVal isPublic As Integer, ByVal ContentEvent As String, orderNo As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "Events_Update", EventID, Title, FirstPage, EventType, LinkPath, isPublic, ContentEvent, orderNo)
    End Function

    Public Function Course_Get() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Course_Get"))
        Return ds.Tables(0)
    End Function
    Public Function Course_GetActive() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Course_GetActive"))
        Return ds.Tables(0)
    End Function

    Public Function Course_GetByUID(pUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Course_GetByUID"), pUID)
        Return ds.Tables(0)
    End Function
    Public Function Course_GetBySearch(pKey As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Course_GetBySearch"), pKey)
        Return ds.Tables(0)
    End Function

    Public Function Course_Save(ByVal UID As Integer, ByVal CourseID As String, ByVal Name As String, ByVal Branch As String, ByVal Address As String, Tel As String, Email As String, Status As String, ByVal UpdBy As Integer) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Course_Save"), UID, CourseID, Name, Branch, Address, Tel, Email, Status, UpdBy)

    End Function

    Public Function Course_Delete(ByVal CourseUID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Course_Delete"), CourseUID)
    End Function

    Public Function Events_GetByPersonUID(pUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Events_GetByPersonUID"), pUID)
        Return ds.Tables(0)
    End Function
    Public Function Events_GetByUID(pUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Events_GetByUID"), pUID)
        Return ds.Tables(0)
    End Function
    Public Function Events_DeleteByPerson(PersonUID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "Events_DeleteByPerson", PersonUID)
    End Function

    Public Function Events_Save(UID As Integer, Name As String, Bdate As String, Edate As String, Detail As String, statusflag As String, bgcolor As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "Events_Save", UID, Name, Bdate, Edate, Detail, statusflag, bgcolor)
    End Function

    Public Function Events_Delete(ByVal UID As Long) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Events_Delete"), UID)
    End Function

End Class
