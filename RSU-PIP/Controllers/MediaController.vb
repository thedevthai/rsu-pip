﻿Imports Microsoft.ApplicationBlocks.Data

Public Class MediaController
    Inherits ApplicationBaseClass
    Dim ds As New DataSet

    Public Function Media_Get() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Media_Get"))
        Return ds.Tables(0)
    End Function

    Public Function Media_GetByID(id As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Media_GetByID"), id)
        Return ds.Tables(0)
    End Function

    Public Function Media_Add(ByVal Title As String, FirstPage As String, ByVal MediaType As String, ByVal LinkPath As String, ByVal StatusFlag As String, orderNo As Integer, StdLevel As Integer, MajorUID As Integer, GroupUID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Media_Add"), Title, FirstPage, MediaType, LinkPath, StatusFlag, orderNo, StdLevel, MajorUID, GroupUID)
    End Function

    Public Function Media_Update(ByVal MediaID As Integer, ByVal Title As String, FirstPage As String, ByVal MediaType As String, ByVal LinkPath As String, ByVal StatusFlag As String, orderNo As Integer, StdLevel As Integer, MajorUID As Integer, GroupUID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "Media_Update", MediaID, Title, FirstPage, MediaType, LinkPath, StatusFlag, orderNo, StdLevel, MajorUID, GroupUID)
    End Function
    Public Function Media_SetFirstPage(ByVal pID As Integer, ByVal isFirst As String) As Integer
        SQL = "update Media  set FirstPage='" & isFirst & "' where Mediaid =" & pID
        Return SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.Text, SQL)
    End Function

    Public Function Media_Delete(ByVal pID As Integer) As Integer
        SQL = "delete from Media where Mediaid =" & pID
        Return SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.Text, SQL)
    End Function

    Public Function Media_Get(sLevel As Integer, MajorID As Integer, GroupID As Integer, isFirstPage As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Media_GetSearch"), sLevel, MajorID, GroupID, isFirstPage)
        Return ds.Tables(0)
    End Function
    Public Function Media_GetByStatus() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Media_GetByStatus"))
        Return ds.Tables(0)
    End Function
End Class
