﻿Imports Microsoft.ApplicationBlocks.Data

Public Class ElearningController

    Inherits BaseClass
    Public ds As New DataSet
    Public Function Course_Get() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Course_Get"))
        Return ds.Tables(0)
    End Function
    Public Function Course_GetActive() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Course_GetActive"))
        Return ds.Tables(0)
    End Function

    Public Function Course_GetByUID(pUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Course_GetByUID"), pUID)
        Return ds.Tables(0)
    End Function
    Public Function Course_GetBySearch(pKey As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Course_GetBySearch"), pKey)
        Return ds.Tables(0)
    End Function

    Public Function Course_Save(ByVal UID As Integer, ByVal CourseID As String, ByVal Name As String, ByVal Branch As String, ByVal Address As String, Tel As String, Email As String, Status As String, ByVal UpdBy As Integer) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Course_Save"), UID, CourseID, Name, Branch, Address, Tel, Email, Status, UpdBy)

    End Function

    Public Function Course_Delete(ByVal CourseUID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Course_Delete"), CourseUID)
    End Function

    Public Function CourseAssign_GetByPersonUID(pUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("CourseAssignment_GetByPersonUID"), pUID)
        Return ds.Tables(0)
    End Function 
    Public Function CourseAssign_GetByUID(pUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("CourseAssignment_GetByUID"), pUID)
        Return ds.Tables(0)
    End Function
    Public Function CourseAssign_DeleteByPerson(PersonUID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "CourseAssignment_DeleteByPerson", PersonUID)
    End Function

    Public Function CourseAssign_Save(PersonUID As Integer, CourseUID As Integer, Duedate As String, Recure As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "CourseAssignment_Save", PersonUID, CourseUID, Duedate, Recure)
    End Function

    Public Function CourseAssign_Delete(ByVal UID As Long) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("CourseAssign_Delete"), UID)
    End Function
End Class

