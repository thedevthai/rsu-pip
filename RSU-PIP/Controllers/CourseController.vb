﻿Imports Microsoft.ApplicationBlocks.Data
Public Class CourseController

    Inherits ApplicationBaseClass
    Dim ds As New DataSet
    Public Function Year_GetByStudent(StdCode As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Year_GetByStudent", StdCode)
        Return ds.Tables(0)
    End Function

    Public Function Courses_GetYear() As DataTable
        SQL = "SELECT  CYEAR FROM Courses GROUP BY CYEAR ORDER BY CYEAR"
        ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, SQL)
        Return ds.Tables(0)
    End Function

    Public Function Courses_GetByCoordinator(pYear As Integer, pID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Courses_GetByCoordinator", pYear, pID)
        Return ds.Tables(0)
    End Function
    Public Function Courses_GetByLocation(pYear As Integer, pLocationID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Courses_GetByLocation", pYear, pLocationID)
        Return ds.Tables(0)
    End Function

    Public Function Courses_GetFromAssessment(pYear As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Courses_GetFromAssessment", pYear)
        Return ds.Tables(0)
    End Function
    Public Function Courses_GetByYear(pYear As Integer) As DataTable

        ds = SqlHelper.ExecuteDataset(ConnectionString, "Courses_GetByYear", pYear)
        Return ds.Tables(0)
    End Function

    Public Function Courses_GetByLevelClass(pYear As Integer, LevelClass As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Courses_GetByLevelClass", pYear, LevelClass)
        Return ds.Tables(0)
    End Function

    Public Function Courses_Get4Selection(pYear As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Courses_Get4Selection", pYear)
        Return ds.Tables(0)
    End Function



    'Public Function Courses_GetByAssessment(pYear As Integer) As DataTable
    '    ds = SqlHelper.ExecuteDataset(ConnectionString, "Courses_GetByAssessment", pYear)
    '    Return ds.Tables(0)
    'End Function

    Public Function Courses_GetSubjectByID(PCourseID As Integer) As DataTable

        ds = SqlHelper.ExecuteDataset(ConnectionString, "Courses_GetSubjectByID", PCourseID)
        Return ds.Tables(0)
    End Function

    Public Function Courses_GetSubjectBySubjectCode(Code As String) As DataTable

        ds = SqlHelper.ExecuteDataset(ConnectionString, "Courses_GetSubjectBySubjectCode", Code)
        Return ds.Tables(0)
    End Function
    Public Function Courses_GetCourseIDBySubjectCode(Code As String) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Courses_GetSubjectBySubjectCode", Code)
        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
        Else
            Return 0
        End If
    End Function

    Public Function Courses_GetByStudent(pYear As Integer, StudentCode As String) As DataTable

        ds = SqlHelper.ExecuteDataset(ConnectionString, "Courses_GetByStudent", pYear, StudentCode)
        Return ds.Tables(0)
    End Function


    Public Function Courses_GetBySearch(pYear As Integer, pKey As String) As DataTable
        SQL = "select * from  View_Courses  Where CYEAR=" & pYear & " And (SubjectName like '%" & pKey & "%' OR SubjectCode like '%" & pKey & "%'"
        ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, SQL)
        Return ds.Tables(0)
    End Function
    Public Function Courses_Add(ByVal CYEAR As Integer _
           , ByVal SubjectCode As String _
           , ByVal UpdBy As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Courses_Add"), CYEAR, SubjectCode, UpdBy)
    End Function

    Public Function Courses_Delete(pYear As Integer, pCode As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "Courses_Delete", pYear, pCode)
    End Function

    Public Function CoursesCoordinator_Get(pYear As Integer, pCode As Integer) As DataTable
        SQL = "select * from  View_CourseCoordinator   Where CYEAR=" & pYear & " And CourseID=" & pCode
        ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, SQL)
        Return ds.Tables(0)
    End Function

    Public Function CoursesCoordinator_CheckStatus(PersonID As Integer) As Boolean
        ds = SqlHelper.ExecuteDataset(ConnectionString, "CoursesCoordinator_CheckStatus", PersonID)
        If ds.Tables(0).Rows.Count > 0 Then
            If ds.Tables(0).Rows(0)(0) > 0 Then
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If

    End Function

    Public Function CoursesCoordinator_Add(ByVal CYEAR As Integer _
           , ByVal CourseID As Integer _
            , ByVal SubjectCode As String _
           , ByVal PersonID As Integer _
           , ByVal UpdBy As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("CourseCoordinator_Add"), CYEAR, CourseID, SubjectCode, PersonID, UpdBy)
    End Function

    Public Function CoursesCoordinator_Delete(pID As Integer) As Integer
        SQL = "delete from   CourseCoordinator  Where itemID=" & pID
        Return SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.Text, SQL)
    End Function

    Public Function Course4Student_GetByStdICode(pYear As Integer, pCode As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Course4Student_GetByStudentCode", pYear, pCode)
        Return ds.Tables(0)
    End Function

    Public Function CourseCoordinator_Get4Assessment(pYear As Integer, pUserID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "CourseCoordinator_Get4Assessment", pYear, pUserID)
        Return ds.Tables(0)
    End Function

    Public Function CourseStudent_GetStudentInCourse(pYear As Integer, CID As Integer, Optional pKey As String = "") As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "CourseStudent_GetStudentInCourse", pYear, CID, pKey)
        Return ds.Tables(0)
    End Function
    Public Function CourseStudent_GetStudentNoRegister(pYear As Integer, CID As Integer, LocationID As Integer, Optional pKey As String = "") As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "CourseStudent_GetStudentNoRegister", pYear, CID, LocationID, pKey)
        Return ds.Tables(0)
    End Function

    Public Function Student_GetStudentNoRegister(pYear As Integer, SkillUID As Integer, LocationID As Integer, Optional pKey As String = "") As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Student_GetStudentNoRegister", pYear, SkillUID, LocationID, pKey)
        Return ds.Tables(0)
    End Function

    Public Function Student_GetStudentNoAssessment(pYear As Integer, SkillPhaseID As Integer, Optional pKey As String = "") As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Student_GetStudentNotAssessment"), pYear, SkillPhaseID, pKey)
        Return ds.Tables(0)

    End Function

    Public Function CourseStudent_Get(pYear As Integer, CID As Integer, Optional pKey As String = "") As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "CourseStudent_Get", pYear, CID, pKey)
        Return ds.Tables(0)
    End Function
    Public Function CourseStudent_GetByUID(UID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "CourseStudent_GetByUID", UID)
        Return ds.Tables(0)
    End Function

    Public Function CourseStudent_GetStudentNoCourse(pYear As Integer, pCode As Integer, pMajor As Integer, pLevel As Integer, Optional pKey As String = "") As DataTable

        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("CourseStudent_GetStudentNoCourse"), pYear, pCode, pMajor, pLevel, pKey)

        Return ds.Tables(0)
    End Function

    Public Function CourseStudent_Add(ByVal CID As Integer _
           , ByVal StudentCode As String _
           , ByVal UpdBy As String, SkillUID As Integer) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("CourseStudent_Add"), CID, StudentCode, UpdBy, SkillUID)
    End Function

    Public Function CourseStudent_Save(ByVal CID As Integer _
           , ByVal StudentCode As String _
           , ByVal UpdBy As String, SkillUID As Integer) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("CourseStudent_Save"), CID, StudentCode, UpdBy, SkillUID)
    End Function


    Public Function CourseStudent_Delete(pID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "CourseStudent_DeleteByUID", pID)
    End Function

    Public Function CourseStudent_DeleteByMajorAndLevel(pMajorID As Integer, cID As Integer, cLevel As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("CourseStudent_DeleteByMajor"), pMajorID, cID, cLevel)
    End Function




End Class
