﻿Imports Microsoft.ApplicationBlocks.Data
Public Class StudentController
    Inherits ApplicationBaseClass
    Dim ds As New DataSet

    Public Function Student_Get() As DataTable

        ds = SqlHelper.ExecuteDataset(ConnectionString, "Student_Get")
        Return ds.Tables(0)
    End Function
    Public Function Student_GetSearch(MajorID As String, LevelClass As String, Search As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Student_GetSearch", MajorID, LevelClass, Search)
        Return ds.Tables(0)
    End Function

    Public Function GetStudent_notUser() As DataTable
        SQL = "select * from  View_Student_notUser   Order by FirstName "
        ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, SQL)
        Return ds.Tables(0)
    End Function


    Public Function GetStudent_notUserBySearch(id As String) As DataTable
        SQL = "select * from View_Student_notUser  where Student_Code like '%" & id & "%' OR FirstName like '%" & id & "%'  order by firstname "
        ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, SQL)
        Return ds.Tables(0)
    End Function

    Public Function GetStudentName(pCode As String) As String
        Dim strName As String
        SQL = "select  FirstName,LastName  from  Students Where Student_Code='" & pCode & "'"
        ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, SQL)
        strName = String.Concat(ds.Tables(0).Rows(0)(0)) & " " & String.Concat(ds.Tables(0).Rows(0)(1))
        Return strName
    End Function

    Public Function GetStudentSex(pCode As String) As String
        Dim strName As String
        SQL = "select  Gender  from  Students Where Student_Code='" & pCode & "'"
        ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, SQL)
        strName = String.Concat(ds.Tables(0).Rows(0)(0))
        Return strName
    End Function
    Public Function GetStudent_ByID(id As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Student_GetByCode", id)
        Return ds.Tables(0)

    End Function

    Public Function Student_GetCodeByID(StudentID As Integer) As String
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Student_GetCodeByID", StudentID)
        If ds.Tables(0).Rows.Count > 0 Then
            If String.Concat(ds.Tables(0).Rows(0)(0)) <> "" Then
                Return String.Concat(ds.Tables(0).Rows(0)(0))
            Else
                Return "0"
            End If
        Else
            Return "0"
        End If
    End Function

    Public Function GetPhaseNoOfAssessment(BYear As Integer, StdCode As String, subjCode As String) As String
        ds = SqlHelper.ExecuteDataset(ConnectionString, "GetPhaseNoOfAssessment", BYear,StdCode,subjCode)
        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Str(ds.Tables(0).Rows(0)(0))
        Else
            Return ""
        End If

    End Function

    Public Function GetAssessmentInfo(BYear As Integer, StdCode As String, subjCode As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "GetAssessmentInfo", BYear, StdCode, subjCode)
        Return ds.Tables(0)
    End Function


    Public Function GetStudent_ByMajorAndLevel(pMajorID As Integer, pLevel As Integer) As DataTable
        SQL = "select * from  Students  where  MajorID=" & pMajorID & " And LevelClass=" & pLevel
        ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, SQL)
        Return ds.Tables(0)

    End Function
    Public Function GetStudent_ByLevelClass(id As Integer) As DataTable
        SQL = "select * from  Students  where  LevelClass=" & id
        ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, SQL)
        Return ds.Tables(0)

    End Function

    Public Function Student_GetLevelClass() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Student_GetLevelClass"))
        Return ds.Tables(0)
    End Function

    Public Function Student_GetCountByLevelClass(Level As Integer) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Student_GetCountByLevelClass"), Level)
        If ds.Tables(0).Rows.Count > 0 Then
            If ds.Tables(0).Rows(0)(0) > 0 Then
                Return ds.Tables(0).Rows(0)(0)
            Else
                Return 0
            End If
        Else
            Return 0
        End If

    End Function
    Public Function Student_GetByCode(StudentCode As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Student_GetByCode", StudentCode)
        Return ds.Tables(0)
    End Function

    Public Function GetStudentBio_ByMajorID(id As Integer) As DataTable
        SQL = "select * from  View_Student_Bio  where  MajorID=" & id
        ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, SQL)
        Return ds.Tables(0)

    End Function

    Public Function GetStudentBio_ByID(id As String) As DataTable
        SQL = "select * from  View_Student_Bio  where Student_Code='" & id & "'"
        ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, SQL)
        Return ds.Tables(0)
    End Function
    Public Function Student_GetByStatus(Level As Integer, pStatus As String, search As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Student_GetByStatus", Level, pStatus, search)
        Return ds.Tables(0)
    End Function
    Public Function Student_GetSex(StudentCode As String) As String
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Student_GetSex", StudentCode)
        Return String.Concat(ds.Tables(0).Rows(0)(0))
    End Function

    Public Function Student_GetNoUser() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Student_GetNoUser")
        Return ds.Tables(0)
    End Function

    Public Function Student_NoUserBySearch(Search As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Student_GetNoUserBySearch", Search)
        Return ds.Tables(0)
    End Function


    Public Function GetStudent_ByQuery(id As String) As DataTable
        SQL = "select * from  View_Student_Bio  where " & id & " order by firstname "
        ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, SQL)
        Return ds.Tables(0)
    End Function


    Public Function GetStudent_BySearch4Print(MajorID As String, MinorID As String, SubMinor As String, Adv As Integer, lvclass As String, pKey As String) As DataTable

        'SQL = "select IIF(CONVERT(varchar,StudentStatus)=40,'สำเร็จการศึกษา',CONVERT(varchar,LevelClass)) ClassName  , * from  View_Student_Bio  where (Student_Code like '%" & pKey & "%' OR FirstName like '%" & pKey & "%' OR  LastName like '%" & pKey & "%') "

        'If MajorID <> 0 Then
        '    SQL &= " And MajorID=" & MajorID
        'End If

        'If MinorID <> 0 Then
        '    SQL &= " And MinorID=" & MinorID
        'End If

        'If SubMinor <> 0 Then
        '    SQL &= " And SubMinorID=" & SubMinor
        'End If

        'If Adv <> 0 Then
        '    SQL &= " And AdvisorID=" & Adv
        'End If

        'If lvclass <> 0 Then
        '    SQL &= " And LevelClass=" & lvclass
        'End If


        'SQL &= " order by LevelClass ,firstname "
        ds = SqlHelper.ExecuteDataset(ConnectionString, "StudentBio_GetSearch", MajorID, MinorID, SubMinor, Adv, lvclass, pKey)
        Return ds.Tables(0)
    End Function

    Public Function Student_Add(ByVal pCode As String, ByVal pPrefix As String, ByVal pFName As String, pLName As String, ByVal pSex As String, MajorID As Integer, MinorID As Integer, SubMinorID As Integer, AdvisorID As Integer, gpax As Double, lvClass As Integer, ProvinceID As Integer, StdStatus As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Student_Add"), pCode, pPrefix, pFName, pLName, pSex, MajorID, MinorID, SubMinorID, AdvisorID, gpax, lvClass, ProvinceID, StdStatus)
    End Function
    Public Function Student_UpdateSmall(ByVal StdCode_ID As Integer, ByVal StdCode As String, ByVal pPrefix As String, ByVal FirstName As String, ByVal LastName As String, ByVal pSex As String, MajorID As Integer, MinorID As Integer, SubMinorID As Integer, AdvisorID As Integer, gpax As Double, lvClass As Integer, UpdateBy As String, StdStatus As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "Student_UpdateSmall", StdCode_ID, StdCode, pPrefix, FirstName, LastName, pSex, MajorID, MinorID, SubMinorID, AdvisorID, gpax, lvClass, UpdateBy, StdStatus)

    End Function
    Public Function Student_AddByImport(ByVal pCode As String, ByVal pPrefix As String, ByVal pFName As String, pLName As String, ByVal pSex As String, MajorID As Integer, AdvisorID As Integer, gpax As Double, lvClass As Integer, ProvinceID As Integer, StdStatus As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Student_AddByImport"), pCode, pPrefix, pFName, pLName, pSex, MajorID, AdvisorID, gpax, lvClass, ProvinceID, StdStatus)
    End Function

    Public Function TMP_Student_SET2BIO(ByVal pCode As String, user As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("TMP_Student_SET2BIO"), pCode, user)
        'SQL = "Insert Into tmpStudentBio  Select Student_ID,'" & user & "' From Students  where " & pCode
        'Return SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.Text, SQL)
    End Function
    Public Function TMP_Student_SET2BIOBySTDID(ByVal pCode As String, user As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("TMP_Student_SET2BIOBySTDID"), pCode, user)
    End Function

    Public Function TMP_Student_SET2CERT(ByVal pCode As String, user As String, DocumentDate As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("TMP_Student_SET2CERT"), pCode, user, DocumentDate)
    End Function
    Public Function TMP_Student_SET2CERTBySTDID(ByVal pCode As String, user As String, DocumentDate As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("TMP_Student_SET2CERTBySTDID"), pCode, user, DocumentDate)
    End Function


    Public Function Student_Update(ByVal StdCode As String, ByVal Prefix As String, ByVal FirstName As String, ByVal LastName As String, ByVal NickName As String, ByVal Gender As String, ByVal Email As String, ByVal BirthDay As String, ByVal BloodGroup As String, ByVal Telephone As String, ByVal Mobile As String, ByVal CongenitalDisease As String, ByVal MedicineUsually As String, ByVal MedicalHistory As String, ByVal Hobby As String, ByVal Talent As String, ByVal Expectations As String, ByVal Father_FirstName As String, ByVal Father_LastName As String, ByVal Father_Career As String, ByVal Father_Tel As String, ByVal Mother_FirstName As String, ByVal Mother_LastName As String, ByVal Mother_Career As String, ByVal Mother_Tel As String, ByVal Sibling As Integer, ByVal ChildNo As Integer, ByVal Address As String, ByVal District As String, ByVal City As String, ByVal ProvinceID As Integer, ByVal ProvinceName As String, ByVal ZipCode As String, ByVal PrimarySchool As String, ByVal PrimaryProvince As String, ByVal PrimaryYear As Integer, ByVal SecondarySchool As String, ByVal SecondaryProvince As String, ByVal SecondaryYear As Integer, ByVal HighSchool As String, ByVal HighProvince As String, ByVal HighYear As Integer, ByVal ContactName As String, ByVal ContactTel As String, ByVal ContactAddress As String, ByVal ContactRelation As String, picpath As String, UpdateBy As String, gpax As Double, AddressLive As String, TelLive As String, FirstNameEN As String, LastNameEN As String, Religion As String, MedicalRecommend As String, HospitalName As String, DoctorName As String, Characteristic As String, FavoriteSubject As String, EduExperience As String, PayorID As Integer, PayorDetail As String, CVLink As String, Motto As String, Facebook As String, LineID As String, MajorID As Integer, AdvisorID As Integer, LevelClass As Integer, WL As Double, HL As Double, CardID As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "Student_Update", StdCode, Prefix, FirstName, LastName, NickName, Gender, Email, Telephone, Mobile, BirthDay, BloodGroup, Father_FirstName, Father_LastName, Father_Career, Father_Tel, Mother_FirstName, Mother_LastName, Mother_Career, Mother_Tel, Sibling, ChildNo, Address, District, City, ProvinceID, ProvinceName, ZipCode, CongenitalDisease, MedicineUsually, MedicalHistory, Hobby, Talent, Expectations, PrimarySchool, PrimaryProvince, PrimaryYear, SecondarySchool, SecondaryProvince, SecondaryYear, HighSchool, HighProvince, HighYear, ContactName, ContactAddress, ContactTel, ContactRelation, picpath, UpdateBy, gpax, AddressLive, TelLive, FirstNameEN, LastNameEN, Religion, MedicalRecommend, HospitalName, DoctorName, Characteristic, FavoriteSubject, EduExperience, PayorID, PayorDetail, CVLink, Motto, Facebook, LineID, MajorID, AdvisorID, LevelClass, WL, HL, CardID)

    End Function



    Public Function Student_Delete(ByVal pID As String) As Integer
       
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "Student_Delete", pID)

    End Function

    Public Function StudentLevel_UpdateOneLevel() As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "StudentLevel_UpdateOneLevel")
    End Function

    Public Function StudentLevel_UpdateByCode(MajorID As Integer, ByVal pID As String, pLevel As Integer, pPass As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "StudentLevel_UpdateByCode", MajorID, pID, pLevel, pPass)
    End Function

    Public Function Student_UpdateLevelClass(MajorID As Integer, pLevelA As Integer, pLevelB As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "Student_UpdateLevelClass", MajorID, pLevelA, pLevelB)
    End Function

    Public Function StudentStatus_GetAll() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "StudentStatus_GetAll")
        Return ds.Tables(0)
    End Function


#Region "Recommend"

#End Region

End Class
