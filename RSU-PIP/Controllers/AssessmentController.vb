﻿Imports Microsoft.ApplicationBlocks.Data

Public Class AssessmentController
    Inherits ApplicationBaseClass
    Dim ds As New DataSet
    Public Function Assessment_GetByUID(AssessmentID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Assessment_GetByUID", AssessmentID)
        Return ds.Tables(0)
    End Function

    Public Function Assessment_GetBySearch(EduYear As String, SkillUID As String, LocationID As String, PhaseNo As String, stdkey As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Assessment_GetBySearch", EduYear, SkillUID, LocationID, PhaseNo, stdkey)
        Return ds.Tables(0)
    End Function


    Public Function Assessment_GetByStudentSearch(EduYear As String, StdLevel As String, LocationID As String, PhaseNo As String, SkillUID As String, SubjCode As String, stdkey As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Student_Assessment", EduYear, StdLevel, LocationID, PhaseNo, SkillUID, SubjCode, stdkey)
        Return ds.Tables(0)
    End Function


    Public Function Assessment_GetStudentSex(RegYear As String, LocationID As Integer, Phase As Integer, SkillUID As Integer) As String

        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Assessment_GetStudentSex"), RegYear, LocationID, Phase, SkillUID)

        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Str(ds.Tables(0).Rows(0)(0))
        Else
            Return ""
        End If
    End Function

    Public Function Assessment_GetStudentCountBySex(RegYear As String, LocationID As Integer, Phase As Integer, sSex As String, SkillUID As Integer) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Assessment_GetStudentCountBySex"), RegYear, LocationID, Phase, sSex, SkillUID)

        Return DBNull2Zero(ds.Tables(0).Rows(0)(0))

    End Function
    Public Function Assessment_GetCount(RegYear As String, LocationID As Integer, SkillPhaseID As Integer, SkillUID As Integer) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Assessment_GetStudentCount"), RegYear, LocationID, SkillPhaseID, SkillUID)

        Return DBNull2Zero(ds.Tables(0).Rows(0)(0))

    End Function

    Public Function Assessment_GetCountBySex(RegYear As String, LocationID As Integer, SkillPhaseID As Integer, sSex As String, SkillUID As Integer) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Assessment_GetStudentCountBySex"), RegYear, LocationID, SkillPhaseID, sSex, SkillUID)

        Return DBNull2Zero(ds.Tables(0).Rows(0)(0))

    End Function

    Public Function Assessment_GetCheckDup(RegYear As String, Phase As Integer, stdcode As String) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Assessment_CheckDup"), RegYear, stdcode, Phase)
        If ds.Tables(0).Rows.Count > 0 Then
            Return 1
        Else
            Return 0
        End If
    End Function
    Public Function Assessment_GetResultByStudent(RegYear As String, StdCode As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Assessment_GetResultByStudent"), RegYear, StdCode)

        Return ds.Tables(0)

    End Function

    Public Function Assessment_GetResultByLocation(RegYear As String, SubjCode As String, SkillPhaseID As Integer, LID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Assessment_GetResultByLocation"), RegYear, SubjCode, SkillPhaseID, LID)
        Return ds.Tables(0)
    End Function
    Public Function Assessment_GetResultByLocation(RegYear As String, StdLevel As String, SkillPhaseID As String, SkillUID As String, LocationID As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Assessment_GetStudentByLocation"), RegYear, StdLevel, SkillPhaseID, SkillUID, LocationID)
        Return ds.Tables(0)
    End Function

    Public Function Assessment_Add(RegYear As String, SubjectCode As String, Student_Code As String, LocationID As Integer, SkillPhaseID As Integer, SKillUID As Integer, UpdBy As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Assessment_Add"), RegYear, SubjectCode, Student_Code, LocationID, SkillPhaseID, SKillUID, UpdBy)
    End Function

    Public Function Assessment_Update(UID As Integer, EduYear As String, Student_Code As String, LocationID As Integer, Phase As Integer, SkillUID As Integer, UpdBy As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Assessment_Update"), UID, EduYear, Student_Code, LocationID, Phase, SkillUID, UpdBy)
    End Function

    Public Function Assessment_Save(UID As Integer, EduYear As String, Student_Code As String, LocationID As Integer, PhaseID As Integer, SkillUID As Integer, SubjectCode As String, AccumulatedHours As Integer, UpdBy As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Assessment_Save"), UID, EduYear, Student_Code, LocationID, PhaseID, SkillUID, SubjectCode, AccumulatedHours, UpdBy)
    End Function


    Public Function Assessment_DeleteByID(ByVal pID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "Assessment_DeleteByID", pID)
    End Function
    Public Function EvaluationGroup_Get(ByVal pSearch As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("EvaluationGroup_Get"), pSearch)
        Return ds.Tables(0)
    End Function
    Public Function EvaluationGroup_GetActive() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("EvaluationGroup_GetActive"))
        Return ds.Tables(0)
    End Function
    Public Function EvaluationGroup_GetByUID(ByVal PUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("EvaluationGroup_GetByUID"), PUID)
        Return ds.Tables(0)
    End Function

    Public Function EvaluationTopic_GetWeightRate(ByVal PUID As Integer) As Double
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("EvaluationTopic_GetWeightRate"), PUID)
        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Dbl(ds.Tables(0).Rows(0)(0))
        Else
            Return 1
        End If

    End Function
    Public Function EvaluationGroup_GetBaseScore(ByVal PUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("EvaluationGroup_GetBaseScore"), PUID)
        Return ds.Tables(0)
    End Function
    Public Function EvaluationGroup_GetName(ByVal PUID As Integer) As String
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("EvaluationGroup_GetName"), PUID)
        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Str(ds.Tables(0).Rows(0)(0))
        Else
            Return ""
        End If
    End Function

    Public Function EvaluationGroup_GetCompare(ByVal PUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("EvaluationGroup_GetCompare"), PUID)

        Return ds.Tables(0)

    End Function

    Public Function EvaluationGroup_Delete(ByVal pID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "EvaluationGroup_Delete", pID)
    End Function
    Public Function EvaluationGroup_Save(ByVal pID As Integer, Name As String, AliasName As String, FullScore As Integer, statusflag As String, isAdvisor As String, isPreceptor As String, isCompare As String, isNoScore As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "EvaluationGroup_Save", pID, Name, AliasName, FullScore, statusflag, isAdvisor, isPreceptor, isCompare, isNoScore)
    End Function
    Public Function EvaluationGroup_GetSearch(ByVal pSearch As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("EvaluationGroup_GetSearch"), pSearch)
        Return ds.Tables(0)
    End Function
    Public Function EvaluationGroup_GetAssessment(ByVal pYear As Integer, SubjectCode As String, UserGroup As String, StudentCode As String, AssessorUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("EvaluationGroup_GetAssessment"), pYear, SubjectCode, UserGroup, StudentCode, AssessorUID)
        Return ds.Tables(0)
    End Function



    Public Function EvaluationTopic_Get(ByVal pSearch As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("EvaluationTopic_Get"), pSearch)
        Return ds.Tables(0)
    End Function

    Public Function EvaluationTopicNotSetForm_Get(ByVal EvaGroup As Integer, ByVal pSearch As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("EvaluationTopicNotSetForm_Get"), EvaGroup, pSearch)
        Return ds.Tables(0)
    End Function

    Public Function EvaluationTopic_GetByGroup(ByVal GroupUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("EvaluationTopic_GetByGroup"), GroupUID)
        Return ds.Tables(0)
    End Function

    Public Function EvaluationTopic_GetByUID(ByVal PUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("EvaluationTopic_GetByUID"), PUID)
        Return ds.Tables(0)
    End Function
    Public Function EvaluationTopic_Delete(ByVal pID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "EvaluationTopic_Delete", pID)
    End Function
    Public Function EvaluationTopic_Save(ByVal pID As Integer, Name As String, Weight As Integer, statusflag As String, Remark As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "EvaluationTopic_Save", pID, Name, Weight, statusflag, Remark)
    End Function


#Region "Eva Form"
    Public Function EvaluationForm_Add(ByVal SEQNO As Integer, ByVal EvaluationTopicUID As Integer, EvaluationGroupUID As Integer, StatusFlag As String, Remark As String, Cuser As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "EvaluationForm_Add", SEQNO, EvaluationTopicUID, EvaluationGroupUID, StatusFlag, Remark, Cuser)
    End Function
    Public Function EvaluationForm_UpdateSEQ(UID As Integer, ByVal SEQNO As Integer, MUser As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "EvaluationForm_UpdateSEQ", UID, SEQNO, MUser)
    End Function
    Public Function EvaluationForm_Delete(UID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "EvaluationForm_Delete", UID)
    End Function
#End Region

    Public Function Subject_GetNoEvaluationSearch(ByVal pYear As Integer, ByVal pSearch As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Subject_GetNoEvaluationSearch"), pYear, pSearch)
        Return ds.Tables(0)
    End Function
    Public Function Subject_GetNoEvaluationSearch2(ByVal pYear As Integer, ByVal pSearch As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Subject_GetNoEvaluationSearch2"), pYear, pSearch)
        Return ds.Tables(0)
    End Function

    Public Function AssessmentSubject_Add(ByVal EduYear As Integer, ByVal SubjectID As Integer, SubjectCode As String, EvaGroup As Integer, Cuser As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "AssessmentSubject_Add", EduYear, SubjectID, SubjectCode, EvaGroup, Cuser)
    End Function
    Public Function AssessmentSubject_Copy(ByVal EduYear1 As Integer, EduYear2 As Integer, Cuser As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "AssessmentSubject_Copy", EduYear1, EduYear2, Cuser)
    End Function
    Public Function AssessmentSubject_GetSearch(ByVal pYear As Integer, ByVal pSearch As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("AssessmentSubject_GetSearch"), pYear, pSearch)
        Return ds.Tables(0)
    End Function
    Public Function AssessmentSubject_DeleteByUID(UID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "AssessmentSubject_DeleteByUID", UID)
    End Function
    Public Function AssessmentSubject_Delete(ByVal EduYear As Integer, ByVal SubjectID As Integer, EvaGroup As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "AssessmentSubject_Delete", EduYear, SubjectID, EvaGroup)
    End Function

    Public Function AssessmentSubject_DeleteBySubject(ByVal EduYear As Integer, ByVal SubjectID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "AssessmentSubject_DeleteBySubjectID", EduYear, SubjectID)
    End Function
    Public Function AssessmentSubject_GetYear() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("AssessmentSubject_GetYear"))
        Return ds.Tables(0)
    End Function

    Public Function AssessmentSubject_GetByUID(ByVal UID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("AssessmentSubject_GetByUID"), UID)
        Return ds.Tables(0)
    End Function

    Public Function Assessee_GetByCoordinator(ByVal PYear As Integer, ByVal PersonID As Integer, ByVal CourseID As Integer, ByVal pSearch As String, Status As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Assessee_GetByCoordinator"), PYear, PersonID, CourseID, pSearch, Status)
        Return ds.Tables(0)
    End Function
    Public Function Assessee_GetByPrecepter(ByVal PYear As Integer, ByVal PersonID As Integer, ByVal CourseID As Integer, ByVal pSearch As String, Status As String, ByVal LocationID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Assessee_GetByPrecepter"), PYear, PersonID, CourseID, pSearch, Status, LocationID)
        Return ds.Tables(0)
    End Function

    Public Function Assessee_GetBySearch(ByVal PYear As Integer, ByVal CourseID As Integer, ByVal LocationID As Integer, ByVal pSearch As String, Status As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Assessee_GetBySearch"), PYear, CourseID, LocationID, pSearch, Status)
        Return ds.Tables(0)
    End Function

    Public Function Assessee_Get4Prominent(ByVal PYear As Integer, ByVal LocationID As Integer, ByVal PhaseID As Integer, ByVal pSearch As String, Status As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Assessee_Get4Prominent"), PYear, LocationID, PhaseID, pSearch, Status)
        Return ds.Tables(0)
    End Function

    Public Function StudentAssessment_GetBySearch(ByVal PYear As Integer, ByVal CourseID As Integer, ByVal LocationID As Integer, ByVal pSearch As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("StudentAssessment_GetBySearch"), PYear, CourseID, LocationID, pSearch)
        Return ds.Tables(0)
    End Function

    Public Function StudentAssessment_GetSearch4Admin(ByVal PYear As Integer, ByVal CourseID As Integer, ByVal LocationID As Integer, ByVal pSearch As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("StudentAssessment_GetSearch4Admin"), PYear, CourseID, LocationID, pSearch)
        Return ds.Tables(0)
    End Function
    Public Function StudentAssessmentGrade_GetByUID(ByVal UID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("StudentAssessmentGrade_GetByUID"), UID)
        Return ds.Tables(0)
    End Function
    Public Function StudentAssessmentGrade_Get(ByVal PYear As Integer, ByVal SubjectCode As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("StudentAssessmentGrade_Get"), PYear, SubjectCode)
        Return ds.Tables(0)
    End Function
    Public Function StudentAssessmentGrade_Get(ByVal pYear As Integer, SubjectCode As String, Search As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("StudentAssessmentGrade_GetSearch"), pYear, SubjectCode, Search)
        Return ds.Tables(0)
    End Function


    Public Function AssessmentStudent_Get(ByVal pYear As Integer, SubjectCode As String, StdCode As String, guid As Integer, AssessorUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("AssessmentStudent_Get"), pYear, SubjectCode, StdCode, guid, AssessorUID)
        Return ds.Tables(0)
    End Function

    Public Function AssessmentStudent_Save(ByVal pYear As Integer, SubjectCode As String, StdCode As String, AssessorUID As Integer, GroupUID As Integer, TopicUID As Integer, Score1 As Double, Score2 As Double, CUser As String, AssessorRoleID As String, AssessorLocationID As Integer, isExclude As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("AssessmentStudent_Save"), pYear, SubjectCode, StdCode, AssessorUID, GroupUID, TopicUID, Score1, Score2, CUser, AssessorRoleID, AssessorLocationID, isExclude)
    End Function

    Public Function AssessmentStudent_SaveScore(ByVal pYear As Integer, SubjectCode As String, StdCode As String, AssessorUID As Integer, GroupUID As Integer, Score As Double, NetScore As Double, CUser As String, AssessorRoleID As String, AssessorLocationID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("AssessmentStudent_SaveScore"), pYear, SubjectCode, StdCode, AssessorUID, GroupUID, Score, NetScore, CUser, AssessorRoleID, AssessorLocationID)
    End Function
    Public Function StudentAssessmentGrade_Save(ByVal pYear As Integer, SubjectCode As String, StdCode As String, CUser As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("StudentAssessmentGrade_Save"), pYear, SubjectCode, StdCode, CUser)
    End Function

    Public Function StudentAssessmentGrade_SaveByUID(ByVal UID As Integer, ByVal Score As Double, Grade As String, CUser As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("StudentAssessmentGrade_SaveByUID"), UID, Score, Grade, CUser)
    End Function
    Public Function StudentAssessmentGrade_SaveReason(ByVal UID As Integer, Score_Old As Double, Score_New As Double, Grade_Old As String, Grade_New As String, Comment As String, EditorUID As Integer, CUser As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("StudentAssessmentGrade_SaveReason"), UID, Score_Old, Score_New, Grade_Old, Grade_New, Comment, EditorUID, CUser)
    End Function


    Public Function AssessmentResult_GetByAdvisor(ByVal pYear As Integer, SubjectCode As String, AssessorUID As Integer, Search As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("AssessmentResult_GetByAdvisor"), pYear, SubjectCode, AssessorUID, Search)
        Return ds.Tables(0)
    End Function
    Public Function AssessmentResult_Get(ByVal pYear As Integer, SubjectCode As String, Search As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("AssessmentResult_Get"), pYear, SubjectCode, Search)
        Return ds.Tables(0)
    End Function
    Public Function AssessmentResult_Preceptor_Get(ByVal pYear As Integer, SubjectCode As String, LocationID As Integer, Search As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("AssessmentResult_Preceptor_Get"), pYear, SubjectCode, LocationID, Search)
        Return ds.Tables(0)
    End Function
    Public Function EvaluationFullScore_Get4Preceptor(ByVal pYear As Integer, SubjectCode As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("EvaluationFullScore_Get4Preceptor"), pYear, SubjectCode)
        Return ds.Tables(0)
    End Function
    Public Function AssessmentResult_GetByPersonID(ByVal pYear As Integer, SubjectCode As String, PersonID As Integer, Search As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("AssessmentResult_GetByPersonID"), pYear, SubjectCode, PersonID, Search)
        Return ds.Tables(0)
    End Function


#Region "Grade"
    Public Function Grade_Get(ByVal pYear As Integer, ByVal pSearch As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Grade_Get"), pYear, pSearch)
        Return ds.Tables(0)
    End Function
    Public Function Grade_GetByID(ByVal PID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Grade_GetByID"), PID)
        Return ds.Tables(0)
    End Function
    Public Function Grade_Save(ByVal EduYear As Integer, SubjectCode As String, Grade As String, MinScore As Double, MaxScore As Double) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "Grade_Save", EduYear, SubjectCode, Grade, MinScore, MaxScore)
    End Function

    Public Function Grade_Delete(ByVal UID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "Grade_Delete", UID)
    End Function

    Public Function Grade_GetSubjectNoGrade(ByVal pYear As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Grade_GetSubjectNoGrade"), pYear)
        Return ds.Tables(0)
    End Function
    Public Function Grade_Copy(ByVal EduYear As Integer, SubjectCodeSource As String, SubjectCodeDestination As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "Grade_Copy", EduYear, SubjectCodeSource, SubjectCodeDestination)
    End Function

#End Region

#Region "Grade Preceptor"
    Public Function GradePreceptor_Get(pYear As Integer, ByVal pSearch As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Grade_Preceptor_Get"), pYear, pSearch)
        Return ds.Tables(0)
    End Function
    Public Function GradePreceptor_GetByID(ByVal PID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Grade_Preceptor_GetByID"), PID)
        Return ds.Tables(0)
    End Function
    Public Function GradePreceptor_Save(ByVal EduYear As Integer, SubjectCode As String, Grade As String, MinScore As Double, MaxScore As Double) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "Grade_Preceptor_Save", EduYear, SubjectCode, Grade, MinScore, MaxScore)
    End Function

    Public Function GradePreceptor_Delete(ByVal UID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "Grade_Preceptor_Delete", UID)
    End Function

    Public Function GradePreceptor_GetSubjectNoGrade(ByVal pYear As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Grade_Preceptor_GetSubjectNoGrade"), pYear)
        Return ds.Tables(0)
    End Function
    Public Function GradePreceptor_Copy(ByVal EduYear As Integer, SubjectCodeSource As String, SubjectCodeDestination As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "Grade_Preceptor_Copy", EduYear, SubjectCodeSource, SubjectCodeDestination)
    End Function


#End Region

    Public Function PracticeHistory_GetFoundation(ByVal stdcode As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("PracticeHistory_GetFoundation"), stdcode)
        Return ds.Tables(0)
    End Function
    Public Function PracticeHistory_GetByStudent(ByVal stdcode As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("PracticeHistory_GetByStudent"), stdcode)
        Return ds.Tables(0)
    End Function
    Public Function StudentAssessmentScore_GetByUID(ByVal UID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("StudentAssessmentScore_GetByUID"), UID)
        Return ds.Tables(0)
    End Function
    Public Function AssessmentStudentCommentText_Get(ByVal EduYear As Integer, StudentCode As String, LocationID As Integer, UserID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("AssessmentStudentCommentText_Get"), EduYear, StudentCode, LocationID, UserID)
        Return ds.Tables(0)
    End Function


    Public Function AssessmentStudentReason_Save(ByVal pYear As Integer, EvaGroupUID As Integer, StdCode As String, SubjectCode As String, Score As Double, CUser As String, Comment As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("AssessmentStudentReason_Save"), pYear, EvaGroupUID, StdCode, SubjectCode, Score, CUser, Comment)
    End Function

    Public Function AssessmentStudentComment_Save(EduYear As Integer, StudentCode As String, LocationID As Integer, UserID As Integer, ProsID As Integer, CUser As String, Comment As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("AssessmentStudentComment_Save"), EduYear, StudentCode, LocationID, UserID, ProsID, CUser, Comment)
    End Function

    Public Function AssessmentStudentComment_Delete(EduYear As Integer, StudentCode As String, LocationID As Integer, UserID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("AssessmentStudentComment_Delete"), EduYear, StudentCode, LocationID, UserID)
    End Function


    Public Function AssessmentStudentCommentText_Save(EduYear As Integer, StudentCode As String, LocationID As Integer, UserID As Integer, Sure As Integer, Reason As String, Comment As String, CUser As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("AssessmentStudentCommentText_Save"), EduYear, StudentCode, LocationID, UserID, Sure, Reason, Comment, CUser)
    End Function


#Region "Psychology"

    Public Function StudentPsychologyAssessment_Get(LocationID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("StudentPsychologyAssessment_Get"), LocationID)
        Return ds.Tables(0)
    End Function

    Public Function StudentPsychologyAssessment_GetByUID(ByVal UID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("StudentPsychologyAssessment_GetByUID"), UID)
        Return ds.Tables(0)
    End Function

    Public Function StudentPsychologyAssessment_Save(UID As Integer, StudentCode As String, Comment As String, CUser As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("StudentPsychologyAssessment_Save"), UID, StudentCode, Comment, CUser)
    End Function

    Public Function StudentPsychologyProblem_Save(StudentCode As String, ProsID As Integer, CUser As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("StudentPsychologyProblem_Save"), StudentCode, ProsID, CUser)
    End Function
    Public Function StudentPsychologyProblem_Delete(StudentCode As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("StudentPsychologyProblem_Delete"), StudentCode)
    End Function
    Public Function StudentPsychologyAssessment_Delete(UID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("StudentPsychologyAssessment_Delete"), UID)
    End Function
#End Region

End Class
