﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="AssesseeList.aspx.vb" Inherits=".AssesseeList" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    
  
</asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
      <section class="content-header">
      <h1>ทำแบบประเมิน<asp:Label ID="lblFor" runat="server"></asp:Label></h1>   
    </section>

<section class="content">  

     <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-filter"></i>

              <h3 class="box-title">เลือกรายวิชา</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                                 
            </div>
            <div class="box-body"> 
                <table border="0" align="left" cellPadding="1" cellSpacing="1">
<tr>
                                                    <td align="left" class="texttopic">
                                                        ปีการศึกษา :                                                        </td>
                                                    <td align="left">
                                                        <asp:Label ID="lblYear" runat="server"></asp:Label>
                                                    </td>
                                                    <td align="left" class="texttopic">&nbsp;</td>
</tr>
<tr>
  <td align="left" class="texttopic">รายวิชา :</td>
  <td align="left">
                                                      <asp:DropDownList ID="ddlCourse" runat="server" 
                                                            CssClass="Objcontrol" AutoPostBack="True">                                                      </asp:DropDownList>                                                    </td>
  <td align="left">(จะแสดงเฉพาะรายวิชาที่รับฝึก/รับผิดชอบ)</td>
</tr>
  </table>  
                            
</div>
            <div class="box-footer clearfix">
           
            </div>
          </div>

   
     <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-user-circle-o"></i>

              <h3 class="box-title">รายชื่อนักศึกษาที่ต้องประเมิน</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                                 
            </div>
            <div class="box-body"> 
 <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
         

        <tr>
          <td  align="left" valign="top" ><table border="0" cellspacing="2" cellpadding="2">
            <tr>
              <td width="50" >ค้นหา</td>
              <td >
                  <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>                </td>
              <td >
                  <asp:Button ID="cmdFind" runat="server" CssClass="btn btn-save" Text="ค้นหา" />
                  </td>
                 <td   >
                     <asp:Button ID="cmdNoScore" runat="server" CssClass="btn btn-save" Text="เฉพาะที่ยังไม่ประเมิน" />
                </td>
              <td >
                  <asp:Button ID="cmdAll" runat="server" CssClass="btn btn-save" Text="ดูทั้งหมด" />
                </td>
            </tr>
            </table></td>
      </tr>

        <tr>
          <td align="center" valign="top">
              <asp:GridView ID="grdData" 
                             runat="server" CellPadding="0" ForeColor="#333333" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" Width="100%">
            <RowStyle BackColor="#F7F7F7" HorizontalAlign="Center" />
            <columns>
            <asp:BoundField HeaderText="No." DataField="nRow">
              <itemstyle HorizontalAlign="Center" VerticalAlign="Top" Width="30px" />                      </asp:BoundField>
            <asp:BoundField DataField="Student_Code" HeaderText="รหัสนักศึกษา">
                <ItemStyle Width="90px" />
                </asp:BoundField>
                <asp:BoundField DataField="StudentName" HeaderText="ชื่อ - สกุล">
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>                
                <asp:BoundField DataField="LocationName" HeaderText="แหล่งฝึก" />
                <asp:BoundField DataField="PhaseNo" HeaderText="ผลัดฝึก" />
                 <asp:BoundField DataField="PhaseName" HeaderText="วันที่ฝึก" />
            <asp:TemplateField HeaderText="สถานะ">
              <itemtemplate>
                <asp:ImageButton ID="imgStatus" runat="server" ImageUrl="images/icon-ok.png" Visible='<%# ConvertYN2Boolean(DataBinder.Eval(Container.DataItem, "AssessmentStatus")) %>' />                        

              </itemtemplate>
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />          
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ประเมิน">
              <itemtemplate>
                <asp:ImageButton ID="imgEdit" runat="server" ImageUrl="images/icon-edit.png" 
                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Student_Code") %>' />                        </itemtemplate>
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />          
            </asp:TemplateField>
            </columns>
            <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />          
            <pagerstyle CssClass="dc_pagination dc_paginationC dc_paginationC11"  HorizontalAlign="Center" />          
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <headerstyle CssClass="th"  Font-Bold="false" HorizontalAlign="Center" 
                      VerticalAlign="Middle" />          
            <EditRowStyle BackColor="#2461BF" />
            <AlternatingRowStyle BackColor="White" />
          </asp:GridView></td>
      </tr>
       
    </table>
                            
</div>
            <div class="box-footer clearfix">
           
            </div>
          </div>
    



</section>    
</asp:Content>
