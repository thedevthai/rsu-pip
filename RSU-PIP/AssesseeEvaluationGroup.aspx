﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="AssesseeEvaluationGroup.aspx.vb" Inherits=".AssesseeEvaluationGroup" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    
    
</asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<section class="content-header">
      <h1>เลือกแบบประเมิน</h1>   
    </section>

<section class="content">  

     <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-user-circle"></i>

              <h3 class="box-title">ข้อมูลนักศึกษา</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                                 
            </div>
            <div class="box-body"> 
                <table width="100%" border="0"  cellPadding="1" cellSpacing="1" >
<tr>
                                                    <td width="80"  class="texttopic">
                                                        รหัสนักศึกษา</td>
                                                    <td >
                                                        <asp:Label ID="lblCode" runat="server"></asp:Label>
                                                    </td>
                                                    <td width="100"  class="texttopic">ชื่อ - นามสกุล</td>
                                                    <td >
                                                        <asp:Label ID="lblStudentName" runat="server"></asp:Label>
                                                    </td>
                                                    <td width="50"  class="texttopic">ชื่อเล่น </td>
                                                    <td >
                                                        <asp:Label ID="lblNickName" runat="server"></asp:Label>
                                                    </td>
</tr>
<tr>
  <td  class="texttopic">สาขา</td>
  <td  colspan="3"><asp:Label ID="lblMajorName" runat="server"></asp:Label>
                                                      </td>
      <td width="60"  class="texttopic">แหล่งฝึก </td>
                                                    <td >
                                                        
                                                        <asp:Label ID="lblLocationName" runat="server"></asp:Label>
                                                        
                                                    &nbsp;(<asp:Label ID="lblLocationID" runat="server"></asp:Label>
                                                        
                                                        )</td>
</tr>
<tr>
  <td  class="texttopic">ปีการศึกษา </td>
  <td >
                                                        <asp:Label ID="lblYear" runat="server"></asp:Label>
                                                    </td>
  <td  class="texttopic">รายวิชา</td>
  <td ><asp:Label ID="lblSubjectCode" runat="server"></asp:Label>
                                                    &nbsp;
                                                        <asp:Label ID="lblSubjectName" runat="server"></asp:Label>
                                                    </td>

     <td width="50"  class="texttopic">ผลัดที่ </td>
                                                    <td >
                                                        <asp:Label ID="lblPhaseNo" runat="server"></asp:Label>
                                                    </td>

</tr>

<tr>
  <td  class="texttopic">ผู้ประเมิน</td>
  <td  colspan="3">
                                                        <asp:Label ID="lblAssessorName" runat="server"></asp:Label>
                                                    </td>
      <td width="60"  class="texttopic">ตำแหน่ง</td>
                                                    <td >
                                                        
                                                        <asp:Label ID="lblAssessorRole" runat="server"></asp:Label>
                                                        
                                                    </td>
</tr>
</table>  
                         
</div>
            <div class="box-footer clearfix">
           
            </div>
          </div>
  <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-filter"></i>

              <h3 class="box-title">เลือกแบบประเมิน</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                                 
            </div>
            <div class="box-body">       
              <asp:GridView ID="grdData"  runat="server" CellPadding="0" ForeColor="#333333"   GridLines="None"  AutoGenerateColumns="False" Width="100%">
            <RowStyle BackColor="#F7F7F7" HorizontalAlign="Center" />
            <columns>
            <asp:BoundField HeaderText="No." DataField="nRow">
              <itemstyle HorizontalAlign="Center" VerticalAlign="Top" Width="30px" />                      </asp:BoundField>
            <asp:BoundField DataField="Name" HeaderText="แบบประเมิน">
                <HeaderStyle Horizontalalign="left" />
                <ItemStyle Horizontalalign="left" />
                </asp:BoundField>
                <asp:BoundField DataField="FullScore" HeaderText="เต็ม">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Center" Width="90px" />
                </asp:BoundField>
                <asp:BoundField HeaderText="ได้" DataField="Score">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Center" Width="90px" />
                </asp:BoundField>
            <asp:TemplateField HeaderText="ประเมิน">
              <itemtemplate>
                <asp:ImageButton ID="imgEdit" runat="server" ImageUrl="images/icon-edit.png" 
                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "EvaluationGroupUID") %>' />                        </itemtemplate>
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />          
            </asp:TemplateField>
            </columns>
            <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />          
            <pagerstyle CssClass="dc_pagination dc_paginationC dc_paginationC11"  HorizontalAlign="Center" />          
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <headerstyle CssClass="th"  Font-Bold="false" HorizontalAlign="Center" 
                      VerticalAlign="Middle" />          
            <EditRowStyle BackColor="#2461BF" />
            <AlternatingRowStyle BackColor="White" />
          </asp:GridView> 
                       
</div>
            <div class="box-footer text-center">
                <asp:Button ID="cmdBack" runat="server" Text="หน้ารายชื่อนักศึกษา" CssClass="btn btn-find" />
            </div>
          </div>
</section>    
</asp:Content>
