﻿<%@ Page Title="Student" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="StudentSelect.aspx.vb" Inherits=".StudentSelect" %>
<%@ Import Namespace="System.Data" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">    
        function openModals(sender, title, message) {
            $("#spnTitle").text(title);
            $("#spnMsg").text(message);
            $('#btnConfirm').attr('onclick', "$('#mdEditStudent').modal('hide');setTimeout(function(){" + $(sender).prop('href') + "}, 50);");
            $('.editemp').click(function () {
             var fname = $(this).attr('data-fname');
             $('#fname').val(fname);
             $('#mdEditStudent').modal('show');

            });
        }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">   
     <section class="content-header">
      <h1>Student Selection</h1>     
    </section>
    <section class="content">  
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">เลือกนักศึกษา</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body"> 
              <table id="tbdata" class="table table-bordered table-striped">
                <thead>
                <tr>
                 <th class="sorting_asc_disabled">เลือก</th>   
                    <th>รหัส</th>          
                  <th>ชื่อ - นามสกุล</th>              
                    <th>ชั้นปี</th>                   
                     <th>สาขา</th>                    
                </tr>
                </thead>
                <tbody>
            <% For Each row As DataRow In dtStd.Rows %>
                <tr>
                     <td class="text-center" width="35">                        
                               <a href="PsychologyAssessmentModify.aspx?t=new&std=<% =String.Concat(row("Student_Code")) %>" ><img src="images/select.png"/></a>
                      
                     </td>

                  <td class="text-center"><% =String.Concat(row("Student_Code")) %></td>                  
                  <td><% = String.Concat(row("StudentName"))  %></td>              
                    <td class="text-center"><% =String.Concat(row("LevelClass")) %></td>
                    <td  class="text-center"><% =String.Concat(row("MajorName")) %></td>                  
                </tr>
            <%  Next %>
                </tbody>               
              </table>                                    
            </div>
            <!-- /.box-body -->
          </div>
 
    <!-- Modal HTML -->
    <div id="mdEditStudent" class="modal fade" role="dialog" data-backdrop="static">
      <div class="modal-dialog modal-lg">
          <form action="Save.vb" method="post">
        <div class="modal-content">
          <div class="modal-header bg-primary">
            <div class="icon-box">
              <i class="fa fa-user-circle"></i> Student infomation.
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            
          </div>
          <div class="modal-body">
            <h4><span id="spnTitle"></span></h4>    
              
              <input type="hidden" name="id" id="id" value="" />
                 <div class="row">
            <div class="col-md-3">
              <div class="form-group">
                <label>Business Unit</label>
                  <asp:TextBox ID="txtBU" runat="server" cssclass="form-control" placeholder="BU Code"></asp:TextBox>   
                 </div>
              <!-- /.form-group -->
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Name (ไทย)</label> <asp:TextBox ID="txtNameTH" runat="server" cssclass="form-control" placeholder="ชื่อภาษาไทย"></asp:TextBox>
                  <input type="text" name="fname" id="fname">
                 </div>
              <!-- /.form-group -->
            </div>
                     <div class="col-md-6">
              <div class="form-group">
                <label>Name (English)</label> <asp:TextBox ID="txtNameEN" runat="server" cssclass="form-control" placeholder="English Name"></asp:TextBox>
                 </div>
              <!-- /.form-group -->
            </div>
</div>
                <div class="row">

 <div class="col-md-3">
              <div class="form-group">
                <label>Address no.</label> <asp:TextBox ID="txtAddressNo" runat="server" cssclass="form-control" placeholder="บ้านเลขที่"></asp:TextBox>
                 </div>
              <!-- /.form-group -->
            </div>

                     <div class="col-md-3">
              <div class="form-group">
                <label>Lane</label> <asp:TextBox ID="txtLane" runat="server" cssclass="form-control" placeholder="ซอย"></asp:TextBox>
                 </div>
            </div>
                     <div class="col-md-3">
              <div class="form-group">
                <label>Road</label> <asp:TextBox ID="txtRoad" runat="server" cssclass="form-control" placeholder="ถนน"></asp:TextBox>
                 </div>
            </div>
                     <div class="col-md-3">
              <div class="form-group">
                <label>Sub District</label> <asp:TextBox ID="txtSubDistrict" runat="server" cssclass="form-control" placeholder="ตำบล/แขวง"></asp:TextBox>
                 </div>
            </div>
            </div>
                   <div class="row">

 <div class="col-md-3">
              <div class="form-group">
                <label>district</label> <asp:TextBox ID="txtDistrict" runat="server" cssclass="form-control" placeholder="อำเภอ"></asp:TextBox>
                 </div>
              <!-- /.form-group -->
            </div>

                     <div class="col-md-3">
              <div class="form-group">
                <label>Province</label> 
                  <asp:DropDownList ID="ddlProvince" runat="server" cssclass="form-control select2" Width="100%" ></asp:DropDownList>
                   
                 </div>
            </div>
                     <div class="col-md-3">
              <div class="form-group">
                <label>Zip Code</label> <asp:TextBox ID="txtZipcode" runat="server" cssclass="form-control" placeholder="รหัสไปรษณีย์"></asp:TextBox>
                 </div>
            </div>
                 <div class="col-md-3">
              <div class="form-group">
                <label>Country</label> 
                  <asp:DropDownList ID="ddlCountry" runat="server" cssclass="form-control select2" Width="100%" >
                      <asp:ListItem Selected="True" Value="TH">ประเทศไทย</asp:ListItem>
                  </asp:DropDownList>
                   
                 </div>
            </div>     
            </div>
            <div class="row">             
          
               <div class="col-md-3">
              <div class="form-group">
                <label>Telephone</label> <asp:TextBox ID="txtTel" runat="server" cssclass="form-control" placeholder="เบอร์โทร"></asp:TextBox>
                 </div>
            </div>
                 <div class="col-md-3">
              <div class="form-group">
                <label>Fax</label> <asp:TextBox ID="txtFax" runat="server" cssclass="form-control" placeholder="แฟกซ์"></asp:TextBox>
                 </div>
            </div>

                     <div class="col-md-3">
              <div class="form-group">
                <label>E-mail</label> <asp:TextBox ID="txtEmail" runat="server" cssclass="form-control" placeholder="อีเมล์"></asp:TextBox>
                 </div>
            </div>
 
                     <div class="col-md-3">
              <div class="form-group">
                <label>Website</label> <asp:TextBox ID="txtWebsite" runat="server" cssclass="form-control" placeholder="เว็บไซต์"></asp:TextBox>
                 </div>
            </div>
 
            </div>
          <h5 class="text-info">Address display (for report)</h5>
    <div class="row">             
          
               <div class="col-md-6">
              <div class="form-group">
                <label>ภาษาไทย</label> <asp:TextBox ID="txtAddressTha" runat="server" cssclass="form-control" placeholder="ที่อยู่ภาษาไทย"></asp:TextBox>
                 </div>
            </div>
                 <div class="col-md-6">
              <div class="form-group">
                <label>English</label> <asp:TextBox ID="txtAddressEng" runat="server" cssclass="form-control" placeholder="ที่อยู่ภาษาอังกฤษ"></asp:TextBox>
                 </div>
            </div>               
 
            </div> 
              
             
          </div>
        </div>
     
        </form> 

      </div>
    </div>
    <!--- End Modal ---> 
    </section>
</asp:Content>
