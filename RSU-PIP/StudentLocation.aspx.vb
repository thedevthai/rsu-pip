﻿
Public Class StudentLocation
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlCs As New courseController
    Dim acc As New UserController
    Dim ctlCfg As New SystemConfigController
    Dim ctlReg As New RegisterController

    Public dtLoc As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("UserLogin").Value) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If

        If Request.Cookies("ROLE_STD").Value = True Then
            If Not SystemOnlineTime() Then
                Response.Redirect("ResultPage.aspx?t=closed")
            End If
        End If

        If Not IsPostBack Then

            LoadYearToDDL()
            LoadCourseToDDL()

            If Not Request("y") Is Nothing Then
                ddlYear.SelectedValue = Request("y")
                LoadCourseToDDL()
                ddlCourse.SelectedValue = Request("cid")
            End If
            LoadSkillSubject()
            LoadLocationInCourseToGrid(StrNull2Zero(ddlYear.SelectedValue), StrNull2Zero(ddlSkill.SelectedValue))

        End If
    End Sub
    Private Function SystemOnlineTime() As Boolean
        Dim Bdate As Integer = StrNull2Zero(ConvertStrDate2DBString(ctlCfg.SystemConfig_GetByCode(CFG_STARTDATE)))
        Dim Edate As Integer = StrNull2Zero(ConvertStrDate2DBString(ctlCfg.SystemConfig_GetByCode(CFG_ENDDATE)))
        Dim sToday As Integer = StrNull2Zero(ConvertDate2DBString(ctlCfg.GET_DATE_SERVER))
        Dim bAvailable As Boolean
        If sToday < Bdate Then
            bAvailable = False
        ElseIf sToday > Edate Then
            bAvailable = False
        Else
            bAvailable = True
        End If
        Return bAvailable
    End Function

    Private Sub LoadYearToDDL()
        Dim y As Integer = StrNull2Zero(DisplayYear(ctlCs.GET_DATE_SERVER))
        Dim LastRow As Integer
        dt = ctlCs.Courses_GetYear
        LastRow = dt.Rows.Count - 1

        If dt.Rows.Count > 0 Then
            With ddlYear
                .Enabled = True
                .DataSource = dt
                .DataTextField = "CYear"
                .DataValueField = "CYear"
                .DataBind()

                If dt.Rows(LastRow)(0) = y Then
                    ddlYear.Items.Add(y + 1)
                    ddlYear.Items(LastRow + 1).Value = y + 1
                ElseIf dt.Rows(LastRow)(0) > y Then
                    'ddlYear.Items.Add(y + 2)
                    'ddlYear.Items(LastRow + 1).Value = y + 2
                ElseIf dt.Rows(LastRow)(0) < y Then
                    ddlYear.Items.Add(y)
                    ddlYear.Items(LastRow + 1).Value = y
                    ddlYear.Items.Add(y + 1)
                    ddlYear.Items(LastRow + 2).Value = y + 1
                End If
                .SelectedIndex = 0
            End With
        Else
            ddlYear.Items.Add(y)
            ddlYear.Items(0).Value = y
            ddlYear.Items.Add(y + 1)
            ddlYear.Items(1).Value = y + 1
            ddlYear.SelectedIndex = 0
        End If
        ddlYear.SelectedValue = Request.Cookies("EDUYEAR").Value
        dt = Nothing
    End Sub
    Private Sub LoadLocationInCourseToGrid(y As Integer, cid As Integer)
        Dim ctlSL As New SkillLocationController

        dtLoc = ctlSL.SkillLocation_GetLocationInWorkSkill(y, cid, "")

        If dtLoc.Rows.Count > 0 Then
            lblCount.Text = dtLoc.Rows.Count
            lblNo.Visible = False
        Else
            lblCount.Text = 0
            lblNo.Visible = True
        End If

    End Sub

    Private Sub LoadCourseToDDL()
        ddlCourse.Items.Clear()
        dt = ctlCs.Course4Student_GetByStdICode(StrNull2Zero(ddlYear.SelectedValue), Request.Cookies("ProfileID").Value)
        If dt.Rows.Count > 0 Then
            ddlCourse.Items.Clear()
            For i = 0 To dt.Rows.Count - 1
                With ddlCourse
                    .Items.Add("" & dt.Rows(i)("SubjectCode") & " : " & dt.Rows(i)("NameTH"))
                    .Items(i).Value = dt.Rows(i)("CourseID")
                End With
            Next


        End If
        dt = Nothing
    End Sub

    Function HasSelected(year As Integer, subj As String, LID As Integer) As Boolean
        Dim iCount As Integer = 0
        iCount = ctlReg.GetStudentRegister_CountSelected(year, Request.Cookies("ProfileID").Value, subj, LID)

        If iCount > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    'Private Sub SelectLocationToBusket(RegYear, Student_Code, LocationID, SubjectCode)
    '    Dim iCount As Integer = 0
    '    iCount = ctlReg.GetStudentRegister_Count(RegYear, Student_Code, SubjectCode)

    '    If iCount < 2 Then

    '        ctlReg.StudentRegister_Add(RegYear, Student_Code, iCount + 1, LocationID, SubjectCode, 1, Request.Cookies("UserLogin").Value)

    '        acc.User_GenLogfile(Request.Cookies("UserLogin").Value, ACTTYPE_ADD, "StudentRegister", "นักศึกษาเลือกแหล่งฝึก LID:" & LocationID & ">>" & Student_Code, "วิชา :" & SubjectCode)
    '        DisplayMessage(Me, "บันทึกเรียบร้อยแล้ว")
    '        LoadLocationInCourseToGrid(StrNull2Zero(ddlYear.SelectedValue), StrNull2Zero(ddlCourse.SelectedValue))

    '    Else
    '        DisplayMessage(Me, "ท่านได้เลือกแหล่งฝึกสำหรับวิชานี้ ครบตามจำนวนแล้ว ท่านไม่สามารถเลือกเพิ่มได้อีก")
    '    End If

    'End Sub


    Protected Sub ddlYear_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlYear.SelectedIndexChanged
        LoadLocationInCourseToGrid(StrNull2Zero(ddlYear.SelectedValue), StrNull2Zero(ddlSkill.SelectedValue))
    End Sub

    Protected Sub ddlCourse_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCourse.SelectedIndexChanged
        LoadSkillSubject()
        LoadLocationInCourseToGrid(StrNull2Zero(ddlYear.SelectedValue), StrNull2Zero(ddlSkill.SelectedValue))
    End Sub

    Protected Sub cmdFindStd_Click(sender As Object, e As EventArgs) Handles cmdFindStd.Click
        LoadLocationInCourseToGrid(StrNull2Zero(ddlYear.SelectedValue), StrNull2Zero(ddlSkill.SelectedValue))
    End Sub

    Private Sub LoadSkillSubject()
        Dim ctlK As New SkillController
        dt = ctlK.SubjectSkill_GetByCourseID(ddlCourse.SelectedValue)
        If dt.Rows.Count > 0 Then
            ddlSkill.DataSource = dt
            ddlSkill.DataValueField = "SkillUID"
            ddlSkill.DataTextField = "SkillName"
            ddlSkill.DataBind()
            ddlSkill.SelectedIndex = 0
        End If
    End Sub

End Class

