﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="PreceptorData.aspx.vb" Inherits=".PreceptorData" %>
<%@ Register assembly="FUA" namespace="Subgurim.Controles" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   <section class="content-header">
      <h1>จัดการข้อมูลอาจารย์แหล่งฝึก (Preceptor)</h1>   
    </section>

<section class="content">  

       <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-search-plus"></i>

              <h3 class="box-title">เพิ่ม/แก้ไข ข้อมูลอาจารย์แหล่งฝึก</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                                 
            </div>
            <div class="box-body"> 
   <table border="0" align="left" cellpadding="0" cellspacing="2">
      <tr>
         <td width="100">ID</td>
         <td>
                     <asp:Label ID="lblID" runat="server"></asp:Label>&nbsp;&nbsp;<span class="text12_nblue">(กำหนดให้อัตโนติโดยโปรแกรม)</span>                     </td>
         <td align="right">
              <asp:Label ID="lblMode" runat="server" Text="Mode : Add New" 
                  CssClass="block_Mode"></asp:Label>
            </td>
      </tr>
         <tr>
         <td>แหล่งฝึก</td>
         <td align="left"><table border="0" cellspacing="1" cellpadding="0">
           <tr>
             <td>
                                                        <asp:DropDownList ID="ddlLocation" runat="server"  
                                                            AutoPostBack="True" CssClass="Objcontrol">                                                        </asp:DropDownList>                                                    </td>
             <td><asp:Image ID="imgArrowLocation" runat="server" ImageUrl="images/arrow-orange-icons.png" />
                                            <asp:TextBox ID="txtFindLocation" runat="server" 
                     Width="80px"></asp:TextBox>
                                              &nbsp;<asp:LinkButton ID="lnkFindLocation" 
                     runat="server" CssClass="buttonPink">ค้นหา</asp:LinkButton>                                              </td>
           </tr>
         </table></td>
         <td align="left">&nbsp;</td>
         </tr>
        <tr>
         <td>คำนำหน้า</td>
         <td><asp:DropDownList ID="ddlPrefix" runat="server" CssClass="Objcontrol"> </asp:DropDownList></td>
         <td>&nbsp;</td>
        </tr>
       <tr>
         <td>ชื่อ</td>
         <td>
             <asp:TextBox ID="txtFirstName" runat="server" Width="200px"></asp:TextBox>           </td>
         <td>&nbsp;</td>
       </tr>
      
       <tr>
         <td>นามสกุล</td>
         <td><asp:TextBox ID="txtLastName" runat="server" Width="200px"></asp:TextBox></td>
         <td>&nbsp;</td>
       </tr>
       <tr>
         <td  >ตำแหน่ง</td>
             <td  ><asp:TextBox ID="txtPositionName" runat="server" Width="200px"></asp:TextBox></td>
             <td  >&nbsp;</td>
       </tr>
           <tr>
         <td  >E-mail</td>
             <td  ><asp:TextBox ID="txtMail" runat="server" Width="200px"></asp:TextBox>&nbsp;<asp:RegularExpressionValidator 
                     ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtMail" 
                     CssClass="alert alert-error show"  ErrorMessage="&lt;&lt; รูปแบบอีเมลไม่ถูกต้อง" 
                     ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>               </td>
             <td  >&nbsp;</td>
          </tr>
          
       <tr>
         <td colspan="3" align="center"> <asp:Button ID="cmdSave" runat="server" CssClass="btn btn-save" Width="100" Text="บันทึก"></asp:Button>
         </td>
       </tr>
       
     </table>
                                                              
</div>
            <div class="box-footer clearfix">
           
            </div>
          </div>


      <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="0">
             <ProgressTemplate>
<img alt="" src="images/progress_bar.gif" height="25" />             </ProgressTemplate>
         </asp:UpdateProgress> 

    <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-user-md"></i>

              <h3 class="box-title">รายชื่ออาจารย์</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                                 
            </div>
            <div class="box-body"> 
<table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">     
     <tr>
          <td  align="left" valign="top">
              
              <table border="0" cellspacing="1" cellpadding="0">
            <tr>
              <td width="50" >ค้นหา</td>
              <td >
                  <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>                </td>
              <td >
                   <asp:Button ID="cmdFind" runat="server" CssClass="btn btn-find" Width="70" Text="ค้นหา"></asp:Button>
    <asp:Button ID="cmdAll" runat="server" CssClass="btn btn-find" Width="70" Text="ดูทั้งหมด"></asp:Button>              </td>
            
                  <td colspan="3" class="text12_nblue"><strong>&nbsp;*</strong>คำค้นหาสามารถค้นหาได้จาก 
                      ชื่อ,  นามสกุล หรือ ชื่อแหล่งฝึก</td>
              </tr>
          </table>

          </td>
      </tr>
       <tr>
          <td align="left" valign="top"  >
              <asp:Label ID="lblStudentCount" runat="server"></asp:Label>           </td>
    </tr>

   <tr>
     <td>
         <asp:GridView ID="grdData" 
                             runat="server" CellPadding="0" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" AllowPaging="True" CssClass="txtcontent" 
                             Font-Bold="False" Width="100%">
                        <RowStyle BackColor="#F7F7F7" />
                        <columns>
                        <asp:BoundField DataField="PrefixName" HeaderText="คำนำหน้า" >                            </asp:BoundField>
                            <asp:BoundField DataField="FirstName" HeaderText="ชื่อ" >
                            <HeaderStyle HorizontalAlign="Left" />                            </asp:BoundField>
                            <asp:BoundField DataField="LastName" HeaderText="นามสกุล" >
                            <HeaderStyle HorizontalAlign="Left" />                            </asp:BoundField>
                            <asp:BoundField HeaderText="ตำแหน่ง" DataField="PositionName">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />                            </asp:BoundField>
                            <asp:BoundField DataField="LocationName" HeaderText="สังกัดแหล่งฝึก" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgEdit" runat="server" 
                                        CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PersonID") %>' 
                                        ImageUrl="images/icon-edit.png" />
                                    &nbsp;
                                    <asp:ImageButton ID="imgDel" runat="server" 
                                        CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PersonID") %>' 
                                        ImageUrl="images/delete.png" />                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                        </columns>
                        <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />                     
                        <pagerstyle HorizontalAlign="Center" 
                             CssClass="dc_pagination dc_paginationC dc_paginationC11" />                     
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <headerstyle CssClass="th" Font-Bold="True" 
                                                            />                     
                        <EditRowStyle BackColor="#2461BF" />
                        <AlternatingRowStyle BackColor="White" />
                     </asp:GridView>  </td>
    </tr>
 </table>
</div>
            <div class="box-footer clearfix">
           
            </div>
          </div>  
    
    


    </section>

 
</asp:Content>
