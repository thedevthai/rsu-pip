﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Health.aspx.vb" Inherits=".Health" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server"> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <section class="content-header">
      <h1>ข้อมูลประวัติการเจ็บป่วย</h1>   
    </section>

<section class="content">  
<div class="row">  
    <section class="col-lg-12 connectedSortable">
     <div class="box box-default">
            <div class="box-header">
              <i class="fa fa-user-circle"></i>

              <h3 class="box-title">ข้อมูลนักศึกษา</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                                 
            </div>
            <div class="box-body"> 
                <table width="100%" border="0" class="table table-hover" cellPadding="1" cellSpacing="1" >
<tr>
                                                    <td width="80" class="texttopic">
                                                        รหัสนักศึกษา</td>
                                                    <td>
                                                        <asp:Label ID="lblStudentCode" runat="server"></asp:Label>
                                                    </td>                                                  
                                                    <td width="100" class="texttopic">ชื่อ - นามสกุล</td>
                                                    <td>
                                                        <asp:Label ID="lblStudentName" runat="server"></asp:Label>
                                                    </td>
</tr>
<tr>
  <td class="texttopic">ชื่อเล่น</td>
  <td><asp:Label ID="lblNickName" runat="server"></asp:Label></td>
  <td class="texttopic">สาขา</td>
  <td><asp:Label ID="lblMajorName" runat="server"></asp:Label></td>
</tr>
</table>  
                         
</div>
            <div class="box-footer clearfix">
           
            </div>
          </div>
 </section>
        </div>
<div class="row">
 <section class="col-lg-12 connectedSortable">                         
    <div class="box box-success">
            <div class="box-header">
              <i class="fa fa-thumbs-up"></i>

              <h3 class="box-title">บันทึกข้อมูลการเจ็บป่วย</h3>
                <asp:HiddenField ID="hdGUID" runat="server" />
            </div>
            <div class="box-body mailbox-messages">      
                         
                   <div class="row">
                <div class="col-md-2">
          <div class="form-group">
            <label>วันที่</label>
               <asp:TextBox ID="txtGenDate" runat="server"  cssclass="form-control text-center"  autocomplete="off" data-date-format="dd/mm/yyyy"
                    data-date-language="th-th" data-provide="datepicker"  onkeyup="chkstr(this,this.value)"></asp:TextBox> 
          </div>
        </div>  
                  <div class="col-md-3">
          <div class="form-group">
            <label>โรค/อาการ/ผ่าตัด</label>
               <asp:TextBox ID="txtDescription" runat="server"  cssclass="form-control text-center" ></asp:TextBox> 
          </div>
        </div>   
         <div class="col-md-3">
          <div class="form-group">
            <label>ชื่อสถานพยาบาล</label>
               <asp:TextBox ID="txtHospitalGeneral" runat="server"  cssclass="form-control text-center" ></asp:TextBox> 
          </div>

        </div>   
  <div class="col-md-4">
          <div class="form-group">
            <label>ผลการรักษา</label>
              <asp:RadioButtonList ID="optResult" runat="server" RepeatDirection="Horizontal" CssClass="mailbox-messages"> 
                  <asp:ListItem Value="1">หายแล้ว/สิ้นสุดการรักษาแล้ว</asp:ListItem>
                  <asp:ListItem Value="2">โรคประจำตัว/โรคเรื้อรัง</asp:ListItem>
                  <asp:ListItem Value="3">กำลังรักษาอยู่</asp:ListItem>
                  </asp:RadioButtonList>    
          </div>

        </div>      
                       </div>
                 <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>ไฟล์เอกสาร Certificate (PDF,.jpg)</label>
                                            <asp:FileUpload ID="FileUpload1" runat="server" Width="80%" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label></label>
                                            <asp:HyperLink ID="hlnkDoc" runat="server" CssClass="btn btn-success" Target="_blank"></asp:HyperLink>
                                        </div>
                                    </div>
                      <div class="col-md-12 text-center">
          <div class="form-group">
            <label> </label>
              <p>
              <asp:Button ID="cmdSaveGeneral" runat="server" Text="บันทึก" CssClass="btn btn-save" Width="100px" />  
                   <asp:Button ID="cmdCancel" runat="server" Text="ยกเลิก" CssClass="btn btn-default" Width="100px" />  
          </p></div>

        </div>  
                                </div>

         <div class="row text-center">  
              <div class="col-md-12">
  <asp:GridView ID="grdGeneral" 
                             runat="server" CellPadding="0" 
                                                        GridLines="None" CssClass="table table-hover" 
                             Font-Bold="False" Width="100%" AutoGenerateColumns="False">
                        <RowStyle BackColor="#F7F7F7" />
                        <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />                     
                        <pagerstyle HorizontalAlign="Center" 
                             CssClass="dc_pagination dc_paginationC dc_paginationC11" />                     
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <headerstyle CssClass="table table-hover" Font-Bold="True" HorizontalAlign="Center" 
                                                            />                     
                        <Columns>
                            <asp:BoundField DataField="AdmitDTT" HeaderText="วันที่" />
                            <asp:BoundField HeaderText="โรค/อาการ" DataField="Descriptions" />
                            <asp:BoundField HeaderText="ผลการรักษา" DataField="Result" />
                            <asp:BoundField DataField="HospitalName" HeaderText="สถานบริการ" />                             
                            <asp:HyperLinkField HeaderText="ใบรับรองแพทย์"   Target="_blank" DataNavigateUrlFields="CertificatePath"  DataTextField="CertPath" />                      
                            <asp:TemplateField>
                                <ItemTemplate>
                                   <asp:ImageButton ID="imgEdit" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID") %>' ImageUrl="images/icon-edit.png" /> 
                                    <asp:ImageButton ID="imgDel" runat="server"  CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID") %>' ImageUrl="images/delete.png" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="65px" />
                            </asp:TemplateField>
                        </Columns>
                        <EditRowStyle BackColor="#2461BF" />
                        <AlternatingRowStyle BackColor="White" />
                     </asp:GridView> 
</div>

</div>



</div>
            <div class="box-footer">
                
            </div>
          </div>    
 
</section>
</div>
</section>    
</asp:Content>
