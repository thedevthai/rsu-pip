﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="UserPreceptor.aspx.vb" Inherits=".UserPreceptor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">      
</asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  
<section class="content-header">
      <h1>กำหนดอาจารย์ (RSU) ให้ทำหน้าที่ อาจารย์แหล่งฝึก</h1>   
</section>

<section class="content">  
    <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-search-plus"></i>

              <h3 class="box-title">กำหนดอาจารย์ (RSU) ให้ทำหน้าที่ อาจารย์แหล่งฝึก</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                                 
            </div>
            <div class="box-body">

    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td   valign="top" class="Topic_header">Step 1. ค้นหาและเลือกแหล่งฝึก</td>
      </tr>
        <tr>
            <td align="center" valign="top">
             
            
<table border="0" align="left" cellPadding="1" cellSpacing="1">
                                                <tr>
                                                    <td align="left" >
                                                        ปีการศึกษา :                                                       </td>
                                                    <td align="left" >
                                                        <asp:DropDownList ID="ddlYear" runat="server" AutoPostBack="True" 
                                                            CssClass="Objcontrol">                                                        </asp:DropDownList>                                                    </td>
                                                    <td align="left" >&nbsp;</td>
                                                </tr>
                                                <tr>
                                                  <td align="left" >แหล่งฝึก :</td>
                                                  <td align="left" >
                                        <asp:TextBox ID="txtFindLocation" runat="server" 
                      Width="150px"></asp:TextBox>
                                                    </td>
                                                  <td align="left" >  
                                                      <asp:Button ID="lnkFind" runat="server" CssClass="btn btn-find" text="ค้นหา" Width="70px" /> </td>
                                                </tr>
                                                </table>      </td>
      </tr>
       <tr>
          <td  align="left" valign="top">
              <asp:GridView ID="grdLocation" 
                             runat="server" CellPadding="0" ForeColor="#333333" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" Width="100%" DataKeyNames="LocationID">
            <RowStyle BackColor="#F7F7F7" HorizontalAlign="Center" />
            <columns>
            <asp:BoundField HeaderText="แหล่งฝึก" DataField="LocationName">
                <HeaderStyle HorizontalAlign="Left" />
              <itemstyle HorizontalAlign="Left" />                      </asp:BoundField>
                <asp:BoundField DataField="ProvinceName" HeaderText="จังหวัด">
                </asp:BoundField>
                <asp:BoundField DataField="LocationGroupName" HeaderText="ประเภท">
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
            <asp:TemplateField HeaderText="จัดการ">
              <itemtemplate>
                <asp:ImageButton ID="imgEdit" runat="server" ImageUrl="images/icon-edit.png" 
                                    CommandArgument='<%# Container.DataItemIndex  %>' />                        </itemtemplate>
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />          
            </asp:TemplateField>
            </columns>
            <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />          
            <pagerstyle CssClass="dc_pagination dc_paginationC dc_paginationC11"  HorizontalAlign="Center" />          
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <headerstyle CssClass="th" Font-Bold="True" HorizontalAlign="Center" 
                      VerticalAlign="Middle" />          
            <EditRowStyle BackColor="#2461BF" />
            <AlternatingRowStyle BackColor="White" />
          </asp:GridView></td>
      </tr>
       <tr>
          <td  align="left" valign="top">&nbsp;</td>
      </tr>
       <tr>
          <td  align="left" valign="top" class="Topic_header">Step 2.กำหนดอาจารย์ให้ทำหน้าที่อาจารย์แหล่งฝึก</td>
      </tr>
       <tr>
          <td  align="left" valign="top">
              <table >
                  <tr>
                      <td >แหล่งฝึก</td>
                      <td align="right" width="50">
                          <asp:Label ID="lblLocationID" runat="server"></asp:Label>
&nbsp;:</td>
                      <td>
                          <asp:Label ID="lblLocationName" runat="server"></asp:Label>
                      </td>
                  </tr>
              </table>
           </td>
      </tr>
       <tr>
          <td valign="top"><table width="100%" border="0" cellspacing="2" cellpadding="0">
                <tr>
                  <td width="50%" class="MenuSt">รายชื่อ อ. ที่กำหนดให้</td>
                  <td rowspan="2"><img src="images/arrow-32.png" width="32" height="32" /></td>
                  <td width="50%"  class="MenuSt">เลือกเพิ่ม อ.แหล่งฝึก</td>
            </tr>
                <tr>
                  <td valign="top" align="center">
                      <asp:GridView ID="grdCoor" 
                             runat="server" CellPadding="2" ForeColor="#333333" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" Width="100%">
                    <RowStyle BackColor="#F7F7F7" HorizontalAlign="Center" />
                    <columns>
                    <asp:BoundField HeaderText="No.">
                      <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />                                      </asp:BoundField>
                    <asp:BoundField HeaderText="ชื่อ อ.แหล่งฝึก" DataField="PreceptorName">
                        <HeaderStyle HorizontalAlign="Left" />
                      <itemstyle HorizontalAlign="Left" VerticalAlign="Middle" />                                      </asp:BoundField>
                    <asp:TemplateField HeaderText="ลบ">
                      <itemtemplate>
                        <asp:ImageButton ID="imgDel" runat="server" 
                                    ImageUrl="images/delete.png"  CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID") %>' />                                        </itemtemplate>
                      <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />                  
                    </asp:TemplateField>
                    </columns>
                    <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />                  
                    <pagerstyle CssClass="dc_pagination dc_paginationC dc_paginationC11"  HorizontalAlign="Center" />                  
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <headerstyle CssClass="th" Font-Bold="True" HorizontalAlign="Center" 
                      VerticalAlign="Middle" />                  
                    <EditRowStyle BackColor="#2461BF" />
                    <AlternatingRowStyle BackColor="White" />
                  </asp:GridView>
                  <asp:Label ID="lblNo" runat="server" CssClass="alert alert-error show"  Width="100%"  Text="No Data"></asp:Label></td>
                  <td><table width="100%" border="0" cellspacing="1" cellpadding="0">
                    <tr>
                      <td><table border="0" cellspacing="2" cellpadding="0">
                        <tr>
                          <td>ค้นหา</td>
                          <td>
                              <asp:TextBox ID="txtSearch" runat="server" Width="200px"></asp:TextBox>                            </td>
                          <td>
                  <asp:Button ID="cmdFind" runat="server" CssClass="btn btn-find" Width="70px" Text="ค้นหา" />                </td>
                        </tr>
                      </table></td>
                    </tr>
                    <tr>
                      <td>
         <asp:GridView ID="grdData" 
                             runat="server" CellPadding="0" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" AllowPaging="True" CssClass="txtcontent" 
                             Font-Bold="False" Width="100%">
                        <RowStyle BackColor="#F7F7F7" />
                        <columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgSelect" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PersonID") %>' ImageUrl="images/icon_arrow.png" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                        <asp:BoundField DataField="PrefixName" HeaderText="คำนำหน้า" >                            </asp:BoundField>
                            <asp:BoundField DataField="FirstName" HeaderText="ชื่อ" >
                            <HeaderStyle HorizontalAlign="Left" />                            </asp:BoundField>
                            <asp:BoundField DataField="LastName" HeaderText="นามสกุล" >
                            <HeaderStyle HorizontalAlign="Left" />                            </asp:BoundField>
                        </columns>
                        <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />                     
                        <pagerstyle HorizontalAlign="Center" 
                             CssClass="dc_pagination dc_paginationC dc_paginationC11" />                     
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <headerstyle CssClass="th" Font-Bold="True" 
                                                            />                     
                        <EditRowStyle BackColor="#2461BF" />
                        <AlternatingRowStyle BackColor="White" />
                     </asp:GridView>  </td>
                    </tr>
                  </table></td>
                </tr>
                
              </table></td>
      </tr>
       <tr>
          <td align="center" valign="top">&nbsp;</td>
      </tr>
        </table>
 </div> 
        <div class="box-footer clearfix">           
                
            </div>
          </div>
                           
</section>   
</asp:Content>
