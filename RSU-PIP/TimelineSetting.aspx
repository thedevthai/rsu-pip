﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="TimelineSetting.aspx.vb" Inherits=".TimelineSetting" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <section class="content-header">
      <h1>
        Timeline Setting
     <small>กำหนดเวลาการทำงานระบบ</small> </h1>
       
    </section>

    <!-- Main content -->
<section class="content">   
       
    <ul class="timeline">

    <!-- timeline time label -->
    <li class="time-label">
        <span class="bg-red">
            ปีการศึกษาหลัก
        </span>
    </li>
    <!-- /.timeline-label -->

    <!-- timeline item -->
    <li>
        <!-- timeline icon -->
        <i class="fa fa-calendar bg-blue"></i>
        <div class="timeline-item">
            <span class="time"><i class="fa fa-clock-o"></i>
                <asp:Label ID="lblYearUpdate" runat="server"></asp:Label></span>
            <h3 class="timeline-header">กำหนดปีการศึกษาหลัก<asp:TextBox ID="txtEduYear" runat="server"></asp:TextBox>
                <asp:Button ID="cmdSaveYear" CssClass="btn btn-primary btn-xs" runat="server" Text="Save"/>                
            </h3>
            
        </div>
    </li>
    <!-- END timeline item -->
  <!-- timeline time label -->
    <li class="time-label">
        <span class="bg-navy">
            ปีการศึกษาสำหรับการประเมิน
        </span>
    </li>
    <!-- /.timeline-label -->

    <!-- timeline item -->
    <li>
        <!-- timeline icon -->
        <i class="fa fa-calendar bg-lime"></i>
        <div class="timeline-item">
            <span class="time"><i class="fa fa-clock-o"></i>
                <asp:Label ID="lblYearAssm" runat="server"></asp:Label></span>
            <h3 class="timeline-header">กำหนดปีการศึกษาสำหรับการประเมิน<asp:TextBox ID="txtYearAssm" runat="server"></asp:TextBox>
                <asp:Button ID="cmdSaveYearAssm" CssClass="btn btn-primary btn-xs" runat="server" Text="Save"/>                
            </h3>
            
        </div>
    </li>
    <!-- END timeline item -->
   <!-- timeline time label -->
    <li class="time-label">
        <span class="bg-green">
           นักศึกษาเลือกแหล่งฝึก
        </span>
    </li>
    <!-- /.timeline-label -->

    <!-- timeline item -->
    <li>
        <!-- timeline icon -->
        <i class="fa fa-registered bg-purple"></i>
        <div class="timeline-item">
            <span class="time"><i class="fa fa-clock-o"></i>
                <asp:Label ID="lblRegUpdate" runat="server"></asp:Label></span>

            <h3 class="timeline-header">กำหนดวันที่เปิด-ปิด ให้นักศึกษาเลือกแหล่งฝึก</h3>
            <div class="timeline-body">
                ตั้งแต่วันที่ <asp:TextBox ID="txtReg_Start" runat="server"></asp:TextBox>
            
                 ถึง <asp:TextBox ID="txtReg_End" runat="server"></asp:TextBox>
 <asp:Button ID="cmdSaveSelect" CssClass="btn bg-purple btn-xs" runat="server" Text="Save" />
            </div>           
        </div>
    </li>
    <!-- END timeline item -->   
        
         <!-- timeline time label -->
    <li class="time-label">
        <span class="bg-purple">
            ประกาศผลการคัดเลือก
        </span>
    </li>
    <!-- /.timeline-label -->

    <!-- timeline item -->
    <li>
        <!-- timeline icon -->
        <i class="fa fa-bullhorn bg-yellow"></i>
        <div class="timeline-item">
            <span class="time"><i class="fa fa-clock-o"></i>
                <asp:Label ID="lblAnnouceUpdate" runat="server"></asp:Label></span>

            <h3 class="timeline-header">วันที่ประกาศผลการคัดเลือกแหล่งฝึก <asp:TextBox ID="txtAnnouncedDate" runat="server"></asp:TextBox>
                <asp:Button ID="cmdSaveAnnounced" CssClass="btn btn-warning btn-xs" runat="server" Text="Save" />
                
            </h3>
        </div>
    </li>
    <!-- END timeline item -->

 <!-- timeline time label -->
    <li class="time-label">
        <span class="bg-maroon">
            อาจารย์ประเมินนักศึกษา
        </span>
    </li>
    <!-- /.timeline-label -->

    <!-- timeline item -->
    <li>
        <!-- timeline icon -->
        <i class="fa fa-sort-numeric-desc bg-green"></i>
        <div class="timeline-item">
            <span class="time"><i class="fa fa-clock-o"></i>
                <asp:Label ID="lblAssessmentUpdate" runat="server"></asp:Label></span>

            <h3 class="timeline-header">กำหนดวันที่เปิด-ปิด ให้อาจารย์ประจำรายวิชาประเมินนักศึกษา</h3>

            <div class="timeline-body">
               ตั้งแต่วันที่ <asp:TextBox ID="txtAssess_Start" runat="server"></asp:TextBox>
            
                 ถึง <asp:TextBox ID="txtAssess_End" runat="server"></asp:TextBox>

                <asp:Button ID="cmdSaveAssessment" CssClass="btn bg-green btn-xs" runat="server" Text="Save" />
                
            </div>          
        </div>
    </li>
    <!-- END timeline item -->       

        <!-- timeline time label -->
    <li class="time-label">
        <span class="bg-yellow">
            แหล่งฝึกประเมินนักศึกษา
        </span>
    </li>
    <!-- /.timeline-label -->

    <!-- timeline item -->
    <li>
        <!-- timeline icon -->
        <i class="fa fa-sort-numeric-desc bg-purple"></i>
        <div class="timeline-item">
            <span class="time"><i class="fa fa-clock-o"></i>
                <asp:Label ID="Label1" runat="server"></asp:Label></span>

            <h3 class="timeline-header">กำหนดวันที่เปิด-ปิด ให้แหล่งฝึกประเมินนักศึกษา</h3>

            <div class="timeline-body">
               ตั้งแต่วันที่ <asp:TextBox ID="txtAssessLoc_Start" runat="server"></asp:TextBox>
            
                 ถึง <asp:TextBox ID="txtAssessLoc_End" runat="server"></asp:TextBox>

                <asp:Button ID="cmdSaveLocAssessment" CssClass="btn bg-green btn-xs" runat="server" Text="Save" />
                
            </div>          
        </div>
    </li>
    <!-- END timeline item -->   


</ul>
       <!--
      <div class="box box-success">
            <div class="box-header">
              <i class="fa fa-users"></i>

              <h3 class="box-title"></h3>             
                 <div class="box-tools pull-right">
               <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                 
            </div>
            <div class="box-body"> 
            </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>
-->  
  
</section>
</asp:Content>
