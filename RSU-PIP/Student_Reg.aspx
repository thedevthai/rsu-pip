﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Student_Reg.aspx.vb" Inherits=".Student_Reg" %>
<%@ Register assembly="FUA" namespace="Subgurim.Controles" tagprefix="cc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <link rel="stylesheet" type="text/css" href="css/pagestyles.css">
  <link rel="stylesheet" type="text/css" href="css/rsustyles.css">
  <link rel="stylesheet" type="text/css" href="css/uidialog.css">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

  <section class="content-header">
    <h1>แก้ไขข้อมูลนักศึกษา
      <small></small>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <section class="col-lg-12 connectedSortable">
        <div class="box box-primary">
          <div class="box-header">
            <i class="fa fa-info-circle"></i>
            <h3 class="box-title">ข้อมูลทั่วไป
              <asp:HiddenField ID="hdStudentID" runat="server" />
            </h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label>รหัสนักศึกษา</label>
                  <asp:Label CssClass="form-control text-center text10b_pink" ID="lblStdCode" runat="server"></asp:Label>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>คำนำหน้าชื่อ</label>
                  <asp:DropDownList ID="ddlPrefix" runat="server" CssClass="form-control">
                    <asp:ListItem>นาย</asp:ListItem>
                    <asp:ListItem>นางสาว</asp:ListItem>
                    <asp:ListItem>นาง</asp:ListItem>
                    <asp:ListItem>อื่นๆ</asp:ListItem>
                  </asp:DropDownList>&nbsp;<small>*อื่นๆให้กรอกรวมในช่องเดียวกันกับชื่อ</small>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>เพศ</label>
                  <asp:RadioButtonList ID="optGender" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Selected="True" Value="M">ชาย</asp:ListItem>
                    <asp:ListItem Value="F">หญิง</asp:ListItem>
                  </asp:RadioButtonList>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>ชื่อเล่น <span class="star-red">*</span></label>
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server"
                    ControlToValidate="txtNickName" CssClass="star-red" ErrorMessage="โปรดกรอก"></asp:RequiredFieldValidator>
                  <asp:TextBox ID="txtNickName" runat="server" CssClass="form-control text-center" MaxLength="20"></asp:TextBox>

                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label>ชื่อ (ภาษาไทย) <span class="star-red">*</span></label>
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                    ControlToValidate="txtFirstNameTH" CssClass="star-red" ErrorMessage="โปรดกรอก">
                  </asp:RequiredFieldValidator>
                  <asp:TextBox ID="txtFirstNameTH" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox> <br />

                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>นามสกุล (ภาษาไทย) <span class="star-red">*</span></label>
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server"
                    ControlToValidate="txtLastNameTH" CssClass="star-red" ErrorMessage="โปรดกรอก">
                  </asp:RequiredFieldValidator>
                  <asp:TextBox ID="txtLastNameTH" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox> <br />

                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>ชื่อ (ภาษาอังกฤษ) <span class="star-red">*</span></label>
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                    ControlToValidate="txtFirstNameEN" CssClass="star-red" ErrorMessage="โปรดกรอก">
                  </asp:RequiredFieldValidator>
                  <asp:TextBox ID="txtFirstNameEN" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox> <br />

                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>นามสกุล (ภาษาอังกฤษ) <span class="star-red">*</span></label>
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server"
                    ControlToValidate="txtLastNameEN" CssClass="star-red" ErrorMessage="โปรดกรอก">
                  </asp:RequiredFieldValidator>
                  <asp:TextBox ID="txtLastNameEN" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox> <br />

                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label>สาขาวิชา</label>
                  <asp:DropDownList ID="ddlMajor" runat="server" CssClass="form-control select2"> </asp:DropDownList>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>อาจารย์ที่ปรึกษา</label>

                  <asp:DropDownList CssClass="form-control select2" ID="ddlAdvisor" runat="server"> </asp:DropDownList>
                </div>

              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>GPAX</label>
                  <asp:TextBox ID="txtGPAX" runat="server" CssClass="form-control text-center" MaxLength="4"></asp:TextBox>
                </div>

              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>กำลังศึกษาชั้นปีที่</label>
                  <asp:DropDownList ID="ddlLevelClass" runat="server" CssClass="form-control select2">
                    <asp:ListItem>1</asp:ListItem>
                    <asp:ListItem Value="2">2</asp:ListItem>
                    <asp:ListItem>3</asp:ListItem>
                    <asp:ListItem>4</asp:ListItem>
                    <asp:ListItem>5</asp:ListItem>
                    <asp:ListItem>6</asp:ListItem>
                    <asp:ListItem Value="0">สำเร็จการศึกษา</asp:ListItem>
                  </asp:DropDownList>


                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="box box-primary">
          <div class="box-header">
            <i class="fa fa-asterisk"></i>

            <h3 class="box-title">ข้อมูลส่วนตัว</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label>วันเกิด <span class="star-red">*</span></label><br />
                  <asp:DropDownList ID="ddlDay" runat="server" CssClass="Objcontrol"> </asp:DropDownList>
                  <asp:DropDownList ID="ddlMonth" runat="server" CssClass="Objcontrol"> </asp:DropDownList>
                  <asp:DropDownList ID="ddlYear" runat="server" CssClass="Objcontrol"> </asp:DropDownList>
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <label>กรุ๊ปเลือด</label>
                  <asp:DropDownList CssClass="form-control select2" ID="ddlBloodGroup" runat="server">
                    <asp:ListItem Selected="True">A</asp:ListItem>
                    <asp:ListItem>B</asp:ListItem>
                    <asp:ListItem>AB</asp:ListItem>
                    <asp:ListItem>O</asp:ListItem>
                  </asp:DropDownList>
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <label>น้ำหนัก (Kg.) <span class="star-red">*</span></label>
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="txtWL"
                    CssClass="star-red" ErrorMessage="โปรดกรอก"></asp:RequiredFieldValidator>
                  <asp:TextBox ID="txtWL" runat="server" CssClass="form-control text-center" MaxLength="6"></asp:TextBox>
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <label>ส่วนสูง (cm.) <span class="star-red">*</span></label>
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ControlToValidate="txtHL"
                    CssClass="star-red" ErrorMessage="โปรดกรอก"></asp:RequiredFieldValidator>
                  <asp:TextBox ID="txtHL" runat="server" CssClass="form-control text-center" MaxLength="6"></asp:TextBox>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>เลขที่บัตรประชาชน<span class="star-red">*</span></label>
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ControlToValidate="txtCardID"
                    CssClass="star-red" ErrorMessage="โปรดกรอก"></asp:RequiredFieldValidator>
                  <asp:TextBox ID="txtCardID" runat="server" MaxLength="13" CssClass="form-control text-center">
                  </asp:TextBox>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label>ศาสนา <span class="star-red">*</span></label>
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ControlToValidate="txtRegion"
                    CssClass="star-red" ErrorMessage="โปรดกรอก"></asp:RequiredFieldValidator>
                  <asp:TextBox ID="txtRegion" runat="server" CssClass="form-control text-center" MaxLength="20"></asp:TextBox>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>อีเมล <span class="star-red">*</span></label>
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtEmail"
                    CssClass="star-red" ErrorMessage="โปรดกรอกอีเมล"></asp:RequiredFieldValidator>
                  <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                    ControlToValidate="txtEmail" CssClass="star-red" ErrorMessage=" รูปแบบอีเมลไม่ถูกต้อง"
                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                  </asp:RegularExpressionValidator>
                  <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>

                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>โทรศัพท์บ้าน</label>
                  <asp:TextBox ID="txtTelephone" runat="server" CssClass="form-control" MaxLength="25"></asp:TextBox>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>มือถือ <span class="star-red">*</span></label>
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtMobile"
                    CssClass="text-sm star-red" ErrorMessage="โปรดกรอก"></asp:RequiredFieldValidator>
                  <asp:TextBox ID="txtMobile" runat="server" CssClass="form-control" MaxLength="25"></asp:TextBox> <br />

                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>โรคประจำตัว <span class="star-red">*</span></label>
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server"
                    ControlToValidate="txtCongenitalDisease" CssClass="star-red"
                    ErrorMessage="โปรดกรอกหากไม่มีให้ใส่ -"></asp:RequiredFieldValidator>
                  <asp:TextBox ID="txtCongenitalDisease" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox> <br />

                </div> <!-- row -->
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>ยาที่ใช้เป็นประจำ <span class="star-red">*</span></label>
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server"
                    ControlToValidate="txtMedicineUsually" CssClass="star-red" ErrorMessage="โปรดกรอกหากไม่มีให้ใส่ -">
                  </asp:RequiredFieldValidator>
                  <asp:TextBox ID="txtMedicineUsually" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox> <br />

                </div> <!-- row -->
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>ประวัติการแพ้ยา <span class="star-red">*<asp:RequiredFieldValidator
                        ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtMedicalHistory"
                        CssClass="star-red" ErrorMessage="โปรดกรอก หากไม่มีให้ใส่ -">
                      </asp:RequiredFieldValidator>
                    </span></label>
                  <asp:TextBox ID="txtMedicalHistory" runat="server"
                    CssClass="form-control" MaxLength="50"></asp:TextBox>
                </div> <!-- row -->
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>คําแนะนําในการปฏิบัติตัวเมื่อมีอาการ</label>
                  <asp:TextBox ID="txtMedicalRecommend" runat="server"  CssClass="form-control" MaxLength="200"></asp:TextBox>


                </div> <!-- row -->
              </div>
            </div>
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label>สิทธิการรักษา</label>
                  <asp:DropDownList CssClass="form-control select2" ID="ddlPayor" runat="server">
                  </asp:DropDownList>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>ระบุ</label>
                  <asp:TextBox ID="txtPayorDesc" runat="server"  CssClass="form-control"></asp:TextBox>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>ชื่อสถานพยาบาลที่รักษา</label>
                  <asp:TextBox ID="txtHospital" runat="server" MaxLength="50"  CssClass="form-control"></asp:TextBox>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>ชื่อแพทย์ที่รักษา</label>
                  <asp:TextBox ID="txtDoctor" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label>ชื่อบิดา <span class="star-red">*</span></label>
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server"
                    ControlToValidate="txtFather_FirstName" CssClass="star-red" ErrorMessage="โปรดกรอก">
                  </asp:RequiredFieldValidator>
                  <asp:TextBox ID="txtFather_FirstName" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>นามสกุลบิดา <span class="star-red">*</span></label>
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server"
                    ControlToValidate="txtFather_LastName" CssClass="star-red" ErrorMessage="โปรดกรอก">
                  </asp:RequiredFieldValidator>
                  <asp:TextBox ID="txtFather_LastName" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>อาชีพบิดา <span class="star-red">*</span></label>
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator21" runat="server"
                    ControlToValidate="txtFather_Career" CssClass="star-red" ErrorMessage="โปรดกรอก">
                  </asp:RequiredFieldValidator>
                  <asp:TextBox ID="txtFather_Career" runat="server" CssClass="form-control" MaxLength="20"></asp:TextBox>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>เบอร์โทรศัพท์บิดา <span class="star-red">*</span></label>
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator22" runat="server"
                    ControlToValidate="txtFather_Tel" CssClass="star-red" ErrorMessage="โปรดกรอก">
                  </asp:RequiredFieldValidator>
                  <asp:TextBox ID="txtFather_Tel" runat="server" CssClass="form-control" MaxLength="25"></asp:TextBox>
                </div>
              </div>

            </div>
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label>ชื่อมารดา <span class="star-red">*</span></label>
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator23" runat="server"
                    ControlToValidate="txtMother_FirstName" CssClass="star-red" ErrorMessage="โปรดกรอก">
                  </asp:RequiredFieldValidator>
                  <asp:TextBox ID="txtMother_FirstName" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>นามสกุลมารดา <span class="star-red">*</span></label>
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator24" runat="server"
                    ControlToValidate="txtMother_LastName" CssClass="star-red" ErrorMessage="โปรดกรอก">
                  </asp:RequiredFieldValidator>
                  <asp:TextBox ID="txtMother_LastName" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>อาชีพมารดา <span class="star-red">*</span></label>
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator25" runat="server"
                    ControlToValidate="txtMother_Career" CssClass="star-red" ErrorMessage="โปรดกรอก">
                  </asp:RequiredFieldValidator>
                  <asp:TextBox ID="txtMother_Career" runat="server" CssClass="form-control" MaxLength="20"></asp:TextBox>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>เบอร์โทรศัพท์มารดา <span class="star-red">*</span></label>
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator26" runat="server"
                    ControlToValidate="txtMother_Tel" CssClass="star-red" ErrorMessage="โปรดกรอก">
                  </asp:RequiredFieldValidator>
                  <asp:TextBox ID="txtMother_Tel" runat="server" CssClass="form-control" MaxLength="25"></asp:TextBox>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label>จำนวนพี่น้อง <span class="star-red">*</span></label>
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator27" runat="server"
                    ControlToValidate="txtSibling" CssClass="star-red" ErrorMessage="โปรดกรอก">
                  </asp:RequiredFieldValidator>
                  <asp:TextBox ID="txtSibling" runat="server" CssClass="form-control" MaxLength="2"></asp:TextBox>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>เป็นลูกคนที่ <span class="star-red">*</span></label>
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator28" runat="server"
                    ControlToValidate="txtChildNo" CssClass="star-red" ErrorMessage="โปรดกรอก">
                  </asp:RequiredFieldValidator>
                  <asp:TextBox ID="txtChildNo" runat="server" CssClass="form-control" MaxLength="2"></asp:TextBox>
                </div>
              </div>

            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>อุปนิสัย</label>
                  <asp:TextBox ID="txtCharacter" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>งานอดิเรก <span class="star-red">*</span></label>
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator29" runat="server" ControlToValidate="txtHobby"
                    CssClass="star-red" ErrorMessage="โปรดกรอก"></asp:RequiredFieldValidator>
                  <asp:TextBox ID="txtHobby" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-9">
                <div class="form-group">
                  <label>ที่อยู่ระหว่างการศึกษา<span class="star-red">*</span></label> <asp:RequiredFieldValidator ID="RequiredFieldValidator33" runat="server"
                    ControlToValidate="txtAddressLive" CssClass="star-red" ErrorMessage="โปรดกรอก">
                  </asp:RequiredFieldValidator>
                  <asp:TextBox ID="txtAddressLive" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>เบอร์โทรศัพท์</label>
                  <asp:TextBox ID="txtTelLive" runat="server" CssClass="form-control" MaxLength="25"></asp:TextBox>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-9">
                <div class="form-group">
                  <label>ที่อยู่ตามบัตรประชาชน <span class="star-red">*</span> </label>
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator32" runat="server"
                    ControlToValidate="txtAddressCard" CssClass="star-red" ErrorMessage="โปรดกรอก">
                  </asp:RequiredFieldValidator>
                  <asp:TextBox ID="txtAddressCard" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox>
                </div>
              </div>                         
              <div class="col-md-2">
                <div class="form-group">
                  <label>จังหวัด <span class="star-red">*</span></label>
                  <asp:DropDownList CssClass="form-control select2" ID="ddlProvince" runat="server"> </asp:DropDownList>
                </div>
              </div>
              <div class="col-md-1">
                <div class="form-group">
                  <label>รหัสไปรษณีย์</label>
                  <asp:TextBox ID="txtZipCode" runat="server"  CssClass="form-control"  MaxLength="5"></asp:TextBox>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="box box-primary">
          <div class="box-header">
            <i class="fa fa-graduation-cap"></i>

            <h3 class="box-title">ประวัติการศึกษา</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
            <table border="0" align="center" cellpadding="0" cellspacing="2" width="100%">
              <tr>
                <td width="200" align="center">ระดับ</td>
                <td width="300" align="center">สถานศึกษา</td>
                <td width="200" align="center">จังหวัด</td>
                <td width="200" align="center">ปีที่จบ</td>
              </tr>
              <tr>
                <td>ประถมศึกษา</td>
                <td align="center">
                  <asp:TextBox ID="txtPrimarySchool" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                </td>
                <td align="center">
                  <asp:TextBox ID="txtPrimaryProvince" runat="server" CssClass="form-control" MaxLength="20"></asp:TextBox>
                </td>
                <td align="center">
                  <asp:TextBox ID="txtPrimaryYear" runat="server" MaxLength="4" Width="77px"></asp:TextBox>
                </td>
              </tr>
              <tr>
                <td>มัธยมศึกษาตอนต้น</td>
                <td align="center">
                  <asp:TextBox ID="txtSecondarySchool" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                </td>
                <td align="center">
                  <asp:TextBox ID="txtSecondaryProvince" runat="server" CssClass="form-control" MaxLength="20"></asp:TextBox>
                </td>
                <td align="center">
                  <asp:TextBox ID="txtSecondaryYear" runat="server" MaxLength="4" Width="77px"></asp:TextBox>
                </td>
              </tr>
              <tr>
                <td>มัธยมศึกษาตอนปลาย</td>
                <td align="center">
                  <asp:TextBox ID="txtHighSchool" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                </td>
                <td align="center">
                  <asp:TextBox ID="txtHighProvince" runat="server" CssClass="form-control" MaxLength="20"></asp:TextBox>
                </td>
                <td align="center">
                  <asp:TextBox ID="txtHighYear" runat="server" MaxLength="4" Width="77px"></asp:TextBox>
                </td>
              </tr>

              <tr>
                <td class="text-blue text-bold">วุฒิการศึกษาอื่นๆ</td>
                <td align="center">
                  &nbsp;</td>
                <td align="center">
                        <asp:HiddenField ID="hdEduUID" runat="server" />
                      </td>
                <td align="center">
                  &nbsp;</td>
              </tr>

              <tr>
                <td colspan="4" align="left">

                     <div class="col-md-3">
                <div class="form-group">
                  <label>หลักสูตร</label>
                  <asp:TextBox ID="txtCourse" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                </div>
              </div>
                     <div class="col-md-3">
                <div class="form-group">
                  <label>สถานศึกษา</label>
                  <asp:TextBox ID="txtSchool" runat="server" MaxLength="50"  CssClass="form-control"></asp:TextBox>
                </div>
              </div>
                     <div class="col-md-3">
                <div class="form-group">
                  <label>จังหวัด</label>
                  <asp:TextBox ID="txtProvinceEdu" runat="server" MaxLength="20"  CssClass="form-control"></asp:TextBox>
                </div>
              </div>
                     <div class="col-md-1">
                <div class="form-group">
                  <label>ปีที่จบ</label>
                   <asp:TextBox ID="txtYearEdu" runat="server"  CssClass="form-control" MaxLength="4"></asp:TextBox>
                </div>
              </div>
                     <div class="col-md-2">
                <div class="form-group">
                  <br />
                  <asp:Button ID="cmdAddEdu" runat="server" Text="+ บันทึก" CssClass="btn btn-success" />
                </div>
              </div>
                </td>
              </tr>
              <tr>
                <td colspan="4">
                  <asp:GridView ID="grdEdu" runat="server" CellPadding="0" ForeColor="#333333" GridLines="None"
                    AutoGenerateColumns="False" Width="100%" CssClass="table table-hover">
                    <RowStyle BackColor="#F7F7F7" HorizontalAlign="Center" />
                    <columns>
                      <asp:BoundField DataField="CourseName" HeaderText="หลักสูตร">
                      </asp:BoundField>
                      <asp:BoundField DataField="Institute" HeaderText="สถานศึกษา" />
                      <asp:BoundField DataField="ProvinceName" HeaderText="จังหวัด">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                      </asp:BoundField>
                      <asp:BoundField HeaderText="ปีที่จบ" DataField="EduYear">
                        <itemstyle HorizontalAlign="Center" />
                      </asp:BoundField>
                      <asp:TemplateField HeaderText="Edit">
                        <itemtemplate>
                          <asp:ImageButton ID="imgEdit" runat="server" ImageUrl="images/icon-edit.png"
                            CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID") %>' />
                        </itemtemplate>
                        <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />
                      </asp:TemplateField>
                      <asp:TemplateField HeaderText="Delete">
                        <itemtemplate>
                          <asp:ImageButton ID="imgDel" runat="server" ImageUrl="images/delete.png"
                            CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID") %>' />
                        </itemtemplate>
                        <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />
                      </asp:TemplateField>
                    </columns>
                    <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <pagerstyle CssClass="dc_pagination dc_paginationC dc_paginationC11" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <headerstyle CssClass="th" Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                    <EditRowStyle BackColor="#2461BF" />
                    <AlternatingRowStyle BackColor="White" />
                  </asp:GridView>
                </td>
              </tr>
            </table>
          </div>
        </div>
      </section>
    </div>
    <div class="row">
      <section class="col-lg-6 connectedSortable">
        <div class="box box-primary">
          <div class="box-header">
            <i class="fa fa-soccer-ball-o"></i>
            <h3 class="box-title">ประสบการณ์/กิจกรรม/ความสามารถพิเศษ</h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>วิชาที่ชอบเรียน <span class="star-red">*</span></label>
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator30" runat="server"
                    ControlToValidate="txtFavoriteSubject" CssClass="star-red" ErrorMessage="โปรดกรอก">
                  </asp:RequiredFieldValidator>
                  <asp:TextBox ID="txtFavoriteSubject" runat="server" CssClass="form-control" MaxLength="100">
                  </asp:TextBox>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>ความสามารถพิเศษ <span class="star-red">*</span></label>
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator31" runat="server" ControlToValidate="txtTalent"
                    CssClass="star-red" ErrorMessage="โปรดกรอก">
                  </asp:RequiredFieldValidator>
                  <asp:TextBox ID="txtTalent" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox>

                </div>

              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>ความคาดหวังในอาชีพ</label>
                  <asp:TextBox ID="txtExpectations" runat="server" CssClass="form-control" MaxLength="200"></asp:TextBox>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>คติพจน์</label>
                  <asp:TextBox ID="txtMotto" runat="server" CssClass="form-control" MaxLength="200"></asp:TextBox>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label> ประสบการณ์การทํางาน/กิจกรรมระหว่างศึกษา</label>
                  <asp:TextBox ID="txtExperience" runat="server" CssClass="form-control" Height="80px"
                    TextMode="MultiLine" MaxLength="1000"></asp:TextBox>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="box box-primary">
          <div class="box-header">
            <i class="fa fa-registered"></i>
            <h3 class="box-title">Resume/CV and Social ID</h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label>Personal CV Link</label>
                  <asp:TextBox ID="txtLinkCV" runat="server" CssClass="form-control" MaxLength="1000"></asp:TextBox>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label>Facebook (Copy URL มาวางในช่องด้านล่าง)</label>
                  <asp:TextBox ID="txtFacebook" runat="server" CssClass="form-control" MaxLength="200"></asp:TextBox>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label>Line Id</label>
                  <asp:TextBox ID="txtLineID" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                </div>
              </div>
            </div>
          </div>
        </div>

      </section>
      <section class="col-lg-6 connectedSortable">
        <div class="box box-primary">
          <div class="box-header">
            <i class="fa fa-ambulance"></i>
            <h3 class="box-title">ผู้ติดต่อฉุกเฉิน</h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label>ชื่อ-นามสกุล <span class="star-red">*</span></label>
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server"
                    ControlToValidate="txtContactName" CssClass="star-red" ErrorMessage="โปรดกรอก">
                  </asp:RequiredFieldValidator>
                  <asp:TextBox ID="txtContactName" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>เบอร์โทรศัพท์ <span class="star-red">*</span></label>
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server"
                    ControlToValidate="txtContactTel" CssClass="star-red" ErrorMessage="โปรดกรอก">
                  </asp:RequiredFieldValidator>
                  <asp:TextBox ID="txtContactTel" runat="server" CssClass="form-control" MaxLength="25"></asp:TextBox>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>เกี่ยวข้องเป็น <span class="star-red">*</span></label>
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server"
                    ControlToValidate="txtContactRelation" CssClass="star-red" ErrorMessage="โปรดกรอก">
                  </asp:RequiredFieldValidator>
                  <asp:TextBox ID="txtContactRelation" runat="server" CssClass="form-control" MaxLength="20"></asp:TextBox>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label>ที่อยู่ <span class="star-red">*</span></label>
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server"
                    ControlToValidate="txtContactAddress" CssClass="star-red" ErrorMessage="โปรดกรอก">
                  </asp:RequiredFieldValidator>
                  <asp:TextBox ID="txtContactAddress" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="box box-primary">
          <div class="box-header">
            <i class="fa fa-picture-o"></i>
            <h3 class="box-title">รูปประจำตัว</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
            <table width="100%" border="0" cellspacing="2" cellpadding="0">
              <tr>
                <td width="100">รูปภาพ</td>
                <td>
                  <cc1:FileUploaderAJAX ID="FileUploaderAJAX1" runat="server" />
                </td>
                <td rowspan="3" valign="top"><b>คำแนะนำ</b><br />
                  1. ชนิดไฟล์รูปภาพเป็น JPG เท่านั้น และ ขนาดไม่เกิน 500 KB <br />    
                  ควรมีความกว้างประมาณ 150 pixel และความสูงประมาณ 180 pixel<br />
                  2. ชื่อไฟล์ต้องเป็นภาษาอังกฤษ<br />
                  3. ควรเป็นภาพติดบัตร เป็นภาพทางการ และสุภาพ สวมชุดนักศึกษา หรือ ชุดปฏิบัติวิชาชีพเท่านั้น <br />    
                  (งดเว้นภาพโพสต์ท่าทาง เช่น ใส่หมวก, แว่นตาแฟชั่น, ชูสองนิ้ว ฯลฯ)</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>
                  <asp:Image ID="picStudent" runat="server" ImageUrl="images/unpic.jpg" Width="150px" />
                </td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
            </table>
          </div>
        </div>
      </section>
    </div>
    <div class="row">
      <section class="col-lg-12 connectedSortable">
        <div class="box box-danger">
          <div class="box-header">
            <i class="fa fa-registered"></i>
            <h3 class="box-title">บันทึกข้อตกลง</h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
            <div>1.</div>
            <div>2.</div>
            <div>3.</div>
            <div>4.</div>
            <div>5.</div>
            <div>
              <asp:CheckBox ID="chkAccepted" runat="server" CssClass="mailbox-messages"
                Text="ข้าพเจ้าอ่านและยอมรับในเงื่อนไขทุกประการ" />
            </div>
          </div>
        </div>
      </section>
    </div>
    <asp:Panel ID="Panel1" runat="server" Style="display: none;" DefaultButton="btnOk">
      <table width="300" border="0" align="center" cellpadding="2" cellspacing="0" class="dialog_box">
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td class="dialog_box title" id="headbox">ผลการทำงาน</td>
              </tr>
              <tr>
                <td align="center" class="dialog_content">
                  <asp:Label ID="lblAlert" runat="server">บันทึกข้อมูลเรียบร้อยแล้ว</asp:Label>
                </td>
              </tr>
              <tr>
                <td align="center">
                  <asp:Button ID="btnOK" runat="server" Text="Close" CssClass="buttonModal" />
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </asp:Panel>
    <div align="center">
      <asp:Button ID="cmdSave" runat="server" CssClass="btn btn-save" Text="บันทึก" Width="100px" />
      &nbsp;
      <asp:Button ID="cmdPrint" runat="server" CssClass="btn btn-success" Text="พิมพ์" Width="100px" />
         &nbsp;
      <asp:Button ID="cmdDelete" runat="server" CssClass="btn btn-danger" Text="ลบ" Width="100px" />
    </div>
  </section>
</asp:Content>