﻿'Imports System.Drawing
'Imports System.IO
Public Class Vaccine
    Inherits System.Web.UI.Page

    Dim ctlH As New HealthController
    Dim ctlStd As New StudentController
    Dim dt As New DataTable
    Private Const UploadDirectory As String = "~/certificate/"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("UserLogin").Value) Then
            Response.Redirect("Default.aspx")
        End If
        If Not IsPostBack Then
            LoadStudentInfo()
            hdGUID.Value = ""
            hdCUID.Value = ""
            LoadGeneral()
            LoadCovid()
        End If
    End Sub

    Private Sub LoadStudentInfo()
        dt = ctlStd.GetStudent_ByID(Request.Cookies("ProfileID").Value)
        If dt.Rows.Count > 0 Then
            lblStudentCode.Text = String.Concat(dt.Rows(0)("Student_Code"))
            lblStudentName.Text = String.Concat(dt.Rows(0)("StudentName"))
            lblMajorName.Text = String.Concat(dt.Rows(0)("MajorName"))
            lblNickName.Text = String.Concat(dt.Rows(0)("NickName"))
        End If
        dt = Nothing
    End Sub

    Private Sub LoadGeneral()
        dt = ctlH.StudentVaccine_GetByGroup(lblStudentCode.Text, "G")
        If dt.Rows.Count > 0 Then
            grdGeneral.Visible = True
            grdGeneral.DataSource = dt
            grdGeneral.DataBind()
        Else
            grdGeneral.DataSource = Nothing
            grdGeneral.Visible = False
        End If
        hdGUID.Value = ""
        dt = Nothing
    End Sub

    Private Sub LoadCovid()
        dt = ctlH.StudentVaccine_GetByGroup(lblStudentCode.Text, "C")
        If dt.Rows.Count > 0 Then
            grdCovid.Visible = True
            grdCovid.DataSource = dt
            grdCovid.DataBind()

            If String.Concat(dt.Rows(0)("CertPath")) <> "" Then
                hlnkDoc.Text = "คลิกเพื่อดู Certificate" ' DBNull2Str(dt.Rows(0)("CertPath"))
                hlnkDoc.NavigateUrl = "../Certificate/" & DBNull2Str(dt.Rows(0)("CertPath"))
                hlnkDoc.Visible = True
            Else
                hlnkDoc.Visible = False
            End If

            cmdUpload.Enabled = True
            FileUpload1.Enabled=True
        Else
            grdCovid.DataSource = Nothing
            grdCovid.Visible = False
            cmdUpload.Enabled = False
            hlnkDoc.Visible = False
            FileUpload1.Enabled = False
        End If
        hdCUID.Value = ""
        dt = Nothing
    End Sub
    Private Sub EditCovid(UID As Integer)
        dt = ctlH.StudentVaccine_GetByUID(UID)
        If dt.Rows.Count > 0 Then

            With dt.Rows(0)
                hdCUID.Value = .Item("UID")
                If String.Concat(.Item("VaccineDate")) <> "" Then
                    txtCovidDate.Text = DisplayStr2ShortDateTH(String.Concat(.Item("VaccineDate")))
                Else
                    txtCovidDate.Text = ""
                End If
                ddlOrderNo.SelectedValue = .Item("OrderNo")
                ddlVaccineCovid.SelectedValue = .Item("VaccineName")
                txtLotNo.Text = .Item("LotNo")
                txtHospitalName1.Text = .Item("HospitalName")
            End With
        End If
    End Sub

    Private Sub EditGeneral(UID As Integer)
        dt = ctlH.StudentVaccine_GetByUID(UID)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                hdGUID.Value = .Item("UID")
                If String.Concat(.Item("VaccineDate")) <> "" Then
                    txtGenDate.Text = DisplayStr2ShortDateTH(String.Concat(.Item("VaccineDate")))
                Else
                    txtGenDate.Text = ""
                End If
                txtOrderNoGen.Text = .Item("OrderNo")
                ddlVaccineGeneral.SelectedValue = .Item("VaccineName")
                txtOrderNoGen.Text = .Item("LotNo")
                txtHospitalGeneral.Text = .Item("HospitalName")
            End With
        End If
    End Sub


    Protected Sub cmdSaveCovid_Click(sender As Object, e As EventArgs) Handles cmdSaveCovid.Click
        If txtCovidDate.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาป้อนวันที่รับวัคซีนก่อน');", True)
            Exit Sub
        End If

        Dim VaccineDate As Integer
        If txtCovidDate.Text <> "" Then
            VaccineDate = CInt(ConvertStrDate2DBString(txtCovidDate.Text))
        End If

        ctlH.StudentVaccine_Save(StrNull2Zero(hdCUID.Value), lblStudentCode.Text, "C", VaccineDate, ddlOrderNo.SelectedValue, ddlVaccineCovid.SelectedValue, txtLotNo.Text, txtHospitalName1.Text, Request.Cookies("UserLoginID").Value)

        txtCovidDate.Text = ""
        ddlOrderNo.SelectedIndex = 0
        ddlVaccineCovid.SelectedIndex = 0
        txtLotNo.Text = ""
        txtHospitalName1.Text = ""

        LoadCovid()
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)
    End Sub

    Protected Sub cmdSaveGeneral_Click(sender As Object, e As EventArgs) Handles cmdSaveGeneral.Click

        If txtGenDate.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาป้อนวันที่รับวัคซีนก่อน');", True)
            Exit Sub
        End If

        Dim VaccineDate As Integer
        If txtGenDate.Text <> "" Then
            VaccineDate = CInt(ConvertStrDate2DBString(txtGenDate.Text))
        End If

        ctlH.StudentVaccine_Save(StrNull2Zero(hdGUID.Value), lblStudentCode.Text, "G", VaccineDate, txtOrderNoGen.Text, ddlVaccineGeneral.SelectedValue, txtLotNo.Text, txtHospitalGeneral.Text, Request.Cookies("UserLoginID").Value)


        txtGenDate.Text = ""
        txtOrderNoGen.Text = ""
        ddlVaccineGeneral.SelectedIndex = 0
        txtOrderNoGen.Text = ""
        txtHospitalGeneral.Text = ""

        LoadGeneral()
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)
    End Sub

    Private Sub grdCovid_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdCovid.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then
            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(5).FindControl("imgDelC")
            imgD.Attributes.Add("onClick", scriptString)
        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#ded5f2';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")
        End If
    End Sub

    Private Sub grdGeneral_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdGeneral.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then
            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(5).FindControl("imgDelG")
            imgD.Attributes.Add("onClick", scriptString)
        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#ded5f2';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If
    End Sub

    Private Sub grdCovid_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdCovid.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEditC"
                    EditCovid(e.CommandArgument())
                Case "imgDelC"
                    ctlH.StudentVaccine_Delete(e.CommandArgument())
                    LoadCovid()
            End Select
        End If
    End Sub

    Private Sub grdGeneral_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdGeneral.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEditG"
                    EditGeneral(e.CommandArgument())
                Case "imgDelG"
                    ctlH.StudentVaccine_Delete(e.CommandArgument())
                    LoadGeneral()
            End Select
        End If
    End Sub

    Protected Sub cmdUpload_Click(sender As Object, e As EventArgs) Handles cmdUpload.Click
        If hlnkDoc.Text = "" Then
            If FileUpload1.PostedFile.FileName = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ท่านยังไม่ได้เลือกไฟล์สำหรับอัพโหลด');", True)
                Exit Sub
            End If
        End If

        Dim f_Path As String
        f_Path = ""
        If FileUpload1.HasFile Then
            f_Path = lblStudentCode.Text & System.IO.Path.GetExtension(FileUpload1.PostedFile.FileName)
            ctlH.Vaccine_UpdateCertificate(lblStudentCode.Text, f_Path, Request.Cookies("UserLoginID").Value)
            UploadFile(FileUpload1, f_Path)
        End If

    End Sub
    Sub UploadFile(ByVal Fileupload As Object, sName As String)
        'Dim FileFullName As String = Fileupload.PostedFile.FileName
        'Dim FileNameInfo As String = Path.GetFileName(FileFullName)
        Dim filename As String = System.IO.Path.GetFileName(Fileupload.PostedFile.FileName)
        Dim objfile As System.IO.FileInfo = New System.IO.FileInfo(Server.MapPath("Certificate/" & sName))
        If objfile.Exists Then
            objfile.Delete()
        End If


        'If FileNameInfo <> "" Then
        Fileupload.PostedFile.SaveAs(Server.MapPath("Certificate/" & sName))
        'End If


        hlnkDoc.Text = "คลิกเพื่อดู Certificate" ' DBNull2Str(dt.Rows(0)("CertPath"))
        hlnkDoc.NavigateUrl = "../Certificate/" & sName
        hlnkDoc.Visible = True
        cmdUpload.Enabled = True
        FileUpload1.Enabled = True

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','Upload done.');", True)

        'objfile = Nothing
    End Sub

    Protected Sub cmdCancelGeneral_Click(sender As Object, e As EventArgs) Handles cmdCancelGeneral.Click
        hdGUID.Value = ""
        txtGenDate.Text = ""
        txtOrderNoGen.Text = ""
        ddlVaccineGeneral.SelectedIndex = 0
        txtOrderNoGen.Text = ""
        txtHospitalGeneral.Text = ""
    End Sub

    Protected Sub cmdCancelCovid_Click(sender As Object, e As EventArgs) Handles cmdCancelCovid.Click
        hdCUID.Value = ""
        txtCovidDate.Text = ""
        ddlOrderNo.SelectedIndex = 0
        ddlVaccineCovid.SelectedIndex = 0
        txtLotNo.Text = ""
        txtHospitalName1.Text = ""
    End Sub
End Class

