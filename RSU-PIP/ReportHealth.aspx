﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="ReportHealth.aspx.vb" Inherits=".ReportHealth" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  
</asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  
    <div class="Page_Header">
        รายงานการรับวัคซีนและประวัติการเจ็บป่วยของนักศึกษา</div>    
<section class="content">   
    <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-thumbs-up"></i>

              <h3 class="box-title">บันทึกข้อมูลการรับวัคซีน Covid-19</h3>
                  <asp:HiddenField ID="hdCUID" runat="server" />
            </div>
            <div class="box-body">       
                   <div class="row">
                        <div class="col-md-2">
          <div class="form-group">
            <label>ปีการศึกษา</label>
                   <asp:DropDownList ID="ddlYear" runat="server" CssClass="form-control select2"></asp:DropDownList>     
          </div>
        </div>  

                <div class="col-md-2">
          <div class="form-group">
            <label>นศ.ชั้นปี</label>
                 <asp:DropDownList ID="ddlLevel" runat="server" cssclass="form-control select2">
                     <asp:ListItem Value="0">---ทั้งหมด---</asp:ListItem>
                  <asp:ListItem Value="4">4</asp:ListItem>
                  <asp:ListItem Value="6">6</asp:ListItem>
              </asp:DropDownList>   
          </div>
        </div>  
                       <div class="col-md-2">
          <div class="form-group">
            <label>ฝึกงานภาค</label>
               <asp:DropDownList ID="ddlZone" runat="server" cssclass="form-control select2" AutoPostBack="True">
                   <asp:ListItem Value="0">---ทุกภาค---</asp:ListItem>
                  <asp:ListItem Value="N">เหนือ</asp:ListItem>
                  <asp:ListItem Value="S">ใต้</asp:ListItem>
                  <asp:ListItem Value="E">ตะวันออก</asp:ListItem>
                       <asp:ListItem Value="W">ตะวันตก</asp:ListItem>
                  <asp:ListItem Value="C">กลาง</asp:ListItem>
                  <asp:ListItem Value="NE">อีสาน</asp:ListItem>
              </asp:DropDownList>   
          </div>
        </div>
  <div class="col-md-2">
          <div class="form-group">
            <label>จังหวัด</label>
              <asp:DropDownList ID="ddlProvince" runat="server" cssclass="form-control select2" AutoPostBack="True">                 
              </asp:DropDownList>                
          </div>

        </div>         
         
              <div class="col-md-4">
          <div class="form-group">
            <label>แหล่งฝึก</label>
                <asp:DropDownList ID="ddlLocation" runat="server" cssclass="form-control select2">                 
              </asp:DropDownList>  
          </div>

        </div>  
   </div>  
       <div class="row text-center"> 
        <div class="col-md-12">
          <div class="form-group">
            <label> </label>
              <br />
 <asp:Button ID="cmdView" runat="server" Text="ดูรายงาน" CssClass="btn btn-save" Width="100px" /> 
          </div>

        </div>                  
           
       </div>      

                    <div class="row text-center">  
                          <div class="col-md-12">


                  </div>     
</div>

</div>
            <div class="box-footer">
                
            </div>
          </div>
     

    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">          
       <tr>
          <td  align="center" valign="top"  >
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="0">
             <ProgressTemplate>
<img alt="" src="images/progress_bar.gif" height="25" />             </ProgressTemplate>
         </asp:UpdateProgress>      </td>
      </tr>
       <tr>
         <td  align="center" valign="top" >
             <asp:UpdatePanel ID="UpdatePanel1" runat="server"> 
            <ContentTemplate> 
                 <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="100%" Height="100%" ProcessingMode="Remote" ShowParameterPrompts="False" ShowPrintButton="true" DocumentMapWidth="100%" ZoomMode="PageWidth">
    </rsweb:ReportViewer>      
                
            </ContentTemplate> 
        </asp:UpdatePanel>
           </td>
       </tr>
       <tr>
         <td  align="center" valign="top"  >
             <asp:Label ID="lblResult" runat="server" CssClass="OptionPanels" 
                 Text="" Width="95%"></asp:Label>
           </td>
       </tr>
        <tr>
         <td    align="left" valign="top" >&nbsp;</td>
       </tr>

       <tr>
         <td   align="center" valign="top" >&nbsp;</td>
      </tr>
       <tr>
         <td height="200"  align="left" valign="top" >&nbsp;</td>
       </tr>
    
    </table>
 
    </section>
</asp:Content>
