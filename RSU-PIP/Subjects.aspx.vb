﻿
Public Class Subjects
    Inherits System.Web.UI.Page

    Dim ctlLG As New SubjectController
    Dim dt As New DataTable
    Dim acc As New UserController
    Dim ctlK As New SkillController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("UserLogin").Value) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If
        If Not IsPostBack Then
            ClearData()
            lblID.Text = ""
            grdData.PageIndex = 0
            LoadWorkSkill()
            LoadSubjectToGrid()
        End If

        'txtCode.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")
        txtLevelClass.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")
    End Sub
    Private Sub LoadSubjectToGrid()

        If Trim(txtSearch.Text) <> "" Then
            dt = ctlLG.Subject_GetBySearch(txtSearch.Text)
        Else
            dt = ctlLG.Subject_Get
        End If

        With grdData
            .Visible = True
            .DataSource = dt
            .DataBind()
        End With

    End Sub

    Private Sub LoadWorkSkill()
        chkSkill.Items.Clear()

        dt = ctlK.Skill_GetActive()
        With chkSkill
            .Enabled = True
            .DataSource = dt
            .DataTextField = "Name"
            .DataValueField = "UID"
            .DataBind()
            .Visible = True
        End With
        dt = Nothing
    End Sub


    Private Sub LoadWorkSkillSubject(SubjectID As Integer)
        chkSkill.ClearSelection()
        dt = ctlK.SubjectSkill_GetBySubject(SubjectID)
        Dim itemCount, dataRow As Integer
        itemCount = chkSkill.Items.Count
        dataRow = dt.Rows.Count

        If dt.Rows.Count > 0 Then
            For i = 0 To dataRow - 1
                For n = 0 To itemCount - 1
                    If dt.Rows(i)("SkillUID") = chkSkill.Items(n).Value Then
                        chkSkill.Items(n).Selected = True
                    End If
                Next
            Next
        End If
    End Sub


    Private Sub grdData_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdData.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit"
                    EditData(e.CommandArgument())
                Case "imgDel"
                    If ctlLG.Subject_Delete(e.CommandArgument) Then

                        acc.User_GenLogfile(Request.Cookies("UserLogin").Value, ACTTYPE_DEL, "Subject", "ลบรายวิชา:" & txtCode.Text & ">>" & txtNameTH.Text, "")
                        DisplayMessage(Me, "ลบข้อมูลเรียบร้อย")
                        grdData.PageIndex = 0
                        LoadSubjectToGrid()
                    Else
                        DisplayMessage(Me, "ไม่สามารถลบข้อมูลได้ กรุณาตรวจสอบและลองใหม่อีกครั้ง")
                    End If


            End Select


        End If
    End Sub
    Private Sub EditData(ByVal pID As String)
        dt = ctlLG.Subject_GetByCode(pID)

        Dim objList As New SubjectInfo
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                isAdd = False
                hdSubjectID.Value = .Item("SubjectID")
                Me.lblID.Text = DBNull2Str(dt.Rows(0)(objList.tblField(objList.fldPos.f01_SubjectCode).fldName))
                Me.txtCode.Text = DBNull2Str(dt.Rows(0)(objList.tblField(objList.fldPos.f01_SubjectCode).fldName))
                txtNameTH.Text = DBNull2Str(dt.Rows(0)(objList.tblField(objList.fldPos.f02_NameTH).fldName))
                txtNameEN.Text = DBNull2Str(dt.Rows(0)(objList.tblField(objList.fldPos.f03_NameEN).fldName))
                txtUnit.Text = DBNull2Str(dt.Rows(0)(objList.tblField(objList.fldPos.f04_SubjectUnit).fldName))
                'optMajor.SelectedValue = DBNull2Str(dt.Rows(0)(objList.tblField(objList.fldPos.f05_MajorID).fldName))
                chkStatus.Checked = CBool(dt.Rows(0)(objList.tblField(objList.fldPos.f08_isPublic).fldName))
                txtAliasName.Text = DBNull2Str(dt.Rows(0)("AliasName"))
                txtLevelClass.Text = DBNull2Str(dt.Rows(0)("LevelClass"))

                txtNameCert1EN.Text = DBNull2Str(dt.Rows(0)("NameCert1EN"))
                txtNameCert1TH.Text = DBNull2Str(dt.Rows(0)("NameCert1TH"))
                txtNameCert2EN.Text = DBNull2Str(dt.Rows(0)("NameCert2EN"))
                txtNameCert2TH.Text = DBNull2Str(dt.Rows(0)("NameCert2TH"))

                LoadWorkSkillSubject(StrNull2Zero(hdSubjectID.Value))

                isAdd = False
            End With
        End If
        dt = Nothing
        objList = Nothing
    End Sub
    Private Sub ClearData()
        hdSubjectID.Value = 0
        Me.lblID.Text = ""
        Me.txtCode.Text = ""
        txtNameTH.Text = ""
        txtNameEN.Text = ""
        txtAliasName.Text = ""
        chkStatus.Checked = True
        txtUnit.Text = ""
        txtLevelClass.Text = ""
        isAdd = True
        txtNameCert1EN.Text = ""
        txtNameCert1TH.Text = ""
        txtNameCert2EN.Text = ""
        txtNameCert2TH.Text = ""
        chkSkill.ClearSelection()
    End Sub

    Private Sub grdData_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdData.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then

            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(8).FindControl("imgDel")
            imgD.Attributes.Add("onClick", scriptString)

        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#ffdfef';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If


    End Sub
    Private Sub SaveSkillList(SubjectID As Integer)
        ctlK.SubjectSkill_DeleteBySubject(SubjectID)
        For i = 0 To chkSkill.Items.Count - 1
            If chkSkill.Items(i).Selected Then
                ctlK.SubjectSkill_Save(SubjectID, StrNull2Zero(chkSkill.Items(i).Value))
            End If
        Next
    End Sub


    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click
        If txtNameTH.Text = "" Or txtCode.Text = "" Then
            DisplayMessage(Me, "กรุณากรอกข้อมูลให้ครบถ้วน")
            Exit Sub
        End If
        'If optMajor.SelectedIndex = -1 Then
        '    DisplayMessage(Me, "กรุณาเลือกสาขาวิชาก่อน")
        '    Exit Sub
        'End If


        Dim SkillList As String
        SkillList = ""
        For i = 0 To chkSkill.Items.Count - 1
            If chkSkill.Items(i).Selected Then
                If i <> chkSkill.Items.Count - 1 Then
                    SkillList &= chkSkill.Items(i).Value & "|"
                Else
                    SkillList &= chkSkill.Items(i).Value
                End If

            End If
        Next



        Dim item As Integer

        If lblID.Text = "" Then

            item = ctlLG.Subject_Add(txtCode.Text, txtNameTH.Text, txtNameEN.Text, StrNull2Double(txtUnit.Text), "", Request.Cookies("UserLogin").Value, Boolean2Decimal(chkStatus.Checked), txtAliasName.Text, StrNull2Zero(txtLevelClass.Text), txtNameCert1TH.Text, txtNameCert2TH.Text, txtNameCert1EN.Text, txtNameCert2EN.Text)

            acc.User_GenLogfile(Request.Cookies("UserLogin").Value, ACTTYPE_ADD, "Subject", "เพิ่มใหม่ รายวิชา:" & txtCode.Text, "Result:" & item)

        Else
            item = ctlLG.Subject_Update(lblID.Text, txtCode.Text, txtNameTH.Text, txtNameEN.Text, StrNull2Double(txtUnit.Text), "", Request.Cookies("UserLogin").Value, Boolean2Decimal(chkStatus.Checked), txtAliasName.Text, StrNull2Zero(txtLevelClass.Text), txtNameCert1TH.Text, txtNameCert2TH.Text, txtNameCert1EN.Text, txtNameCert2EN.Text)

            acc.User_GenLogfile(Request.Cookies("UserLogin").Value, ACTTYPE_UPD, "Subject", "แก้ไข รายวิชา:" & txtCode.Text, "Result:" & item)

        End If

        SaveSkillList(hdSubjectID.Value)

        grdData.PageIndex = 0
        LoadSubjectToGrid()
        ClearData()
         ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)


    End Sub

    Protected Sub cmdClear_Click(sender As Object, e As EventArgs) Handles cmdClear.Click
        ClearData()
    End Sub

    Protected Sub cmdFind_Click(sender As Object, e As EventArgs) Handles cmdFind.Click
        grdData.PageIndex = 0
        LoadSubjectToGrid()
    End Sub
    Protected Sub cmdAll_Click(sender As Object, e As EventArgs) Handles cmdAll.Click
        txtSearch.Text = ""
        grdData.PageIndex = 0
        LoadSubjectToGrid()
    End Sub

    Protected Sub grdData_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdData.PageIndexChanging
        grdData.PageIndex = e.NewPageIndex
        LoadSubjectToGrid()
    End Sub
End Class

