﻿Imports System.IO
Imports Newtonsoft.Json
Imports System.Web
Public Class Site
    Inherits System.Web.UI.MasterPage

    Dim dt As New DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If Session("RoleID") = isShopAccess Then
        '    hlnkUserName.NavigateUrl = "LocationsEdit.aspx?id=" & Request.Cookies("LocationID").Value
        'Else
        '    hlnkUserName.NavigateUrl = "#"
        'End If
        If IsNothing(Request.Cookies("UserLogin").Value) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If


        If Not IsPostBack Then

            Dim ctlStd As New StudentController
            Dim ctlPsn As New PersonController
            Dim ctlU As New UserController

            Dim picPaths As String

            'dt = ctlU.User_GetByID(DBNull2Zero(Request.Cookies("UserLoginID").Value))

            lblLocationName.Visible = True
            If Request.Cookies("UserProfileID").Value = 1 Then
                dt = ctlStd.GetStudent_ByID(Request.Cookies("ProfileID").Value)
                picPaths = stdPic
                lblLocationName.Text = "วิทยาลัยเภสัชศาสตร์"
            Else
                dt = ctlPsn.Person_GetByID(StrNull2Zero(Request.Cookies("ProfileID").Value))
                picPaths = personPic
            End If

            If dt.Rows.Count > 0 Then

                If Request.Cookies("UserProfileID").Value = 1 Then
                    lblLocationName.Text = "วิทยาลัยเภสัชศาสตร์"
                Else
                    lblLocationName.Text = String.Concat(dt.Rows(0).Item("LocationName"))
                End If

                If DBNull2Str(dt.Rows(0).Item("PicturePath")) <> "" Then

                    Dim objfile As FileInfo = New FileInfo(Server.MapPath("~/" & picPaths & "/" & dt.Rows(0).Item("PicturePath")))

                    If objfile.Exists Then
                        imgUser.ImageUrl = "~/" & picPaths & "/" & dt.Rows(0).Item("PicturePath")
                    Else
                        imgUser.ImageUrl = "~/" & picPaths & "/nopic" & dt.Rows(0).Item("Gender") & ".jpg"
                    End If
                End If
                hlnkUserName.Text = String.Concat(dt.Rows(0).Item("FirstName")) & " " & dt.Rows(0).Item("LastName") & " (" & dt.Rows(0).Item("NickName") & ")"
            End If
            dt = Nothing
        End If
        'LoadCalendarData()
    End Sub
    Dim ctlE As New EventController
    Dim dtCalendar As New DataTable
    Public Shared json As String
    Private Sub LoadCalendarData()
        dtCalendar = ctlE.Calendar_Get()
        json = JsonConvert.SerializeObject(dtCalendar, Formatting.Indented)
    End Sub

    Protected Sub lnkAccount_Click(sender As Object, e As EventArgs) Handles lnkAccount.Click

        Select Case Request.Cookies("UserProfileID").Value
            Case 1
                Response.Redirect("Student_Reg.aspx?id=" & Request.Cookies("ProfileID").Value)
            Case 2
                Response.Redirect("Person_Reg.aspx?id=" & Request.Cookies("ProfileID").Value)
            Case 3
                Response.Redirect("Person_Reg.aspx?id=" & Request.Cookies("ProfileID").Value)
            Case 4
                Response.Redirect("Person_Reg.aspx?id=" & Request.Cookies("ProfileID").Value)
        End Select

    End Sub

    Protected Sub lnkChangePass_Click(sender As Object, e As EventArgs) Handles lnkChangePass.Click
        Response.Redirect("ChangePassword.aspx")
    End Sub
End Class