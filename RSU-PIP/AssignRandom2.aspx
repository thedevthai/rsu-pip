﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="AssignRandom2.aspx.vb" Inherits=".AssignRandom2" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server"> 
</asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <section class="content-header">
      <h1>คัดเลือกแหล่งฝึก สำหรับ นศ.ปี 6 (Random)</h1>   
    </section>
<section class="content">  

     <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-random"></i>

              <h3 class="box-title">Randomize</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                                 
            </div>
            <div class="box-body"> 
    <div class="col-lg-3"></div>
<div class="col-lg-6 row">           
      <div class="row">
   <div class="col-md-12">
          <div class="form-group">
            <label>ปีการศึกษา</label>
              <asp:DropDownList ID="ddlYear" runat="server" CssClass="form-control select2" AutoPostBack="True"></asp:DropDownList> 
          </div>
        </div>
             </div>
    <div class="row">
           <div class="col-md-12">
          <div class="form-group">
            <label>รายวิชา</label>
               <asp:DropDownList ID="ddlCourse" runat="server" CssClass="form-control select2" AutoPostBack="True"></asp:DropDownList>
          </div>

        </div>
         </div>
               
                <div class="row"> 
                <div class="col-md-12">
          <div class="form-group">
            <label>งานที่ฝึก</label>
                <asp:DropDownList ID="ddlSkill" runat="server" CssClass="form-control select2"></asp:DropDownList>  
          </div>

        </div>
     
      
      </div>

  <div class="row">
   <div class="col-md-12 text-center">  
   <asp:Button ID="cmdRandom" runat="server" CssClass="btn btn-primary"  Text="Randomize" />       
       </div>
      </div>

       <br /> 

 <div class="row">
   <div class="col-md-12"> 
  <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center"> 
       <tr>
          <td  align="center" valign="top"  >
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="0">
             <ProgressTemplate>
<img alt="" src="images/progress_bar.gif" height="25" />             </ProgressTemplate>
         </asp:UpdateProgress>      </td>
      </tr>
       <tr>
         <td >
             <asp:Panel ID="pnResult" runat="server">
                 <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Randomize Complete</h4>
                สุ่มคัดเลือกแหล่งฝึกเรียบร้อย.
              </div>

             </asp:Panel>
           </td>
       </tr>
       
    </table>
     </div>
      </div>

    </div>
 <div class="col-lg-3"></div>             
  
    
                </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>

   
    </section>
</asp:Content>
