﻿Public Class EventDetail
    Inherits System.Web.UI.Page

    Dim dt As New DataTable
    Dim ctlNews As New EventController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            LoadNews(Request("id"))
        End If
    End Sub
    Private Sub LoadNews(id As Integer)
        Dim dtN As New DataTable

        dtN = ctlNews.Events_GetByUID(id)
        If dtN.Rows.Count > 0 Then
            With dtN.Rows(0)
                lblTitle.Text = .Item("Title")
                lblContent.Text = .Item("Details")
                lblDate.Text = .Item("StartDate") & "-" & .Item("EndDate")
            End With

        End If
        dtN = Nothing
    End Sub
End Class
