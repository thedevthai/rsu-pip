﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucMenu.ascx.vb" Inherits=".ucMenu" %>
<%@ Register assembly="DevExpress.Web.v17.2, Version=17.2.13.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
    <link rel="stylesheet" type="text/css" href="css/rsustyles.css"> 
<ul class="sidebar-menu" data-widget="tree">
       <li class="header">MAIN NAVIGATION</li>

        <% If Request("actionType") = "h"  %>
            <li class="active"><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> <span>หน้าแรก</span></a></li>
        <% Else %>
            <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> <span>หน้าแรก</span></a></li> 
        <% End If %> 
        
      <% If Request("ItemType") = "calendar" Then%>
        <li class="active"><a href="Events.aspx?ActionType=calendar"> <i class="fa fa-calendar"></i>ปฏิทินกิจกรรม</a></li>
      <% Else %>
        <li><a href="Events.aspx?ActionType=calendar"><i class="fa fa-calendar"></i>ปฏิทินกิจกรรม</a></li>
      <% End If %>

<% If StrNull2Boolean(Request.Cookies("ROLE_STD").Value) = True Then %>
      
                <% If Request.QueryString("ActionType") = "mnuStudentBio" Then%>
                    <li class="active"><a href="Student_Bio.aspx?ActionType=mnuStudentBio"><i class="fa fa-user"></i> <span>ข้อมูลประวัตินักศึกษา</span></a></li>
                <% Else%>
                    <li><a href="Student_Bio.aspx?ActionType=mnuStudentBio"><i class="fa fa-user"></i> <span>ข้อมูลประวัตินักศึกษา</span></a></li>
                <% End If%>         
         
                <% If Request.QueryString("ActionType") = "pthistory" Then%>
                    <li class="active"><a  href="PracticeHistory.aspx?ActionType=pthistory"><i class="fa fa-history"></i> <span>ประวัติการฝึกปฏิบัติงานวิชาชีพ</span></a></li>
                <% Else%>
                    <li><a href="PracticeHistory.aspx?ActionType=pthistory"><i class="fa fa-history"></i> <span>ประวัติการฝึกปฏิบัติงานวิชาชีพ</span></a></li>  
                <% End If%>       

                <% If Request.QueryString("ActionType") = "vac" Then%>
                    <li class="active"><a  href="vaccine.aspx?ActionType=vac"><i class="fa fa-tint"></i> <span>ประวัติวัคซีน</span></a></li>
                <% Else%>
                    <li><a href="vaccine.aspx?ActionType=vac"><i class="fa fa-tint"></i> <span>ประวัติวัคซีน</span></a></li>  
                <% End If%>  
                <% If Request.QueryString("ActionType") = "health" Then%>
                    <li class="active"><a  href="health.aspx?ActionType=health"><i class="fa fa-stethoscope"></i> <span>ประวัติการเจ็บป่วย</span></a></li>
                <% Else%>
                    <li><a href="health.aspx?ActionType=health"><i class="fa fa-stethoscope"></i> <span>ประวัติการเจ็บป่วย</span></a></li>  
                <% End If%>   
     
                <% If Request.QueryString("ActionType") = "mnuPrintBio" Then%>
                    <li class="active"><a  href="ReportViewerStudentBio.aspx?ActionType=mnuPrintBio" ><i class="fa fa-print"></i> <span>พิมพ์ประวัติ</span></a> </li>
                <% Else%>
                    <li><a href="ReportViewerStudentBio.aspx?ActionType=mnuPrintBio"><i class="fa fa-print"></i> <span>พิมพ์ประวัติ</span></a> </li>
                <% End If%>               

                <% If Request("ItemType") = "viewloc" Then%>
                    <li class="active"><a href="LocationList.aspx?ItemType=viewloc"><i class="fa fa-hospital-o"></i>แหล่งฝึก</a></li>
                <% Else %>
                    <li><a href="LocationList.aspx?ItemType=viewloc"><i class="fa fa-hospital-o"></i>แหล่งฝึก</a></li>
                <% End If %>  

                <% If Request("ItemType") = "creg" Then%>
                    <li class="active"><a href="Courses_Reg.aspx?ItemType=creg"><i class="fa fa-book"></i>รายวิชาที่ลงทะเบียนฝึก</a></li>
                <% Else %>
                    <li><a href="Courses_Reg.aspx?ItemType=creg"><i class="fa fa-book"></i>รายวิชาที่ลงทะเบียนฝึก</a></li>
                <% End If %>  
       
                <% If Request.QueryString("ActionType") = "slt" Then  %>
                    <li class="active treeview">
                <% Else %>
                    <li class="treeview">
                <% End If %>        
                    <a href="#">
                        <i class="fa fa-check-square-o"></i> <span>การเลือกแหล่งฝึก</span>
                        <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <% If Request("ItemType") = "1" Then%>
                            <li class="active"><a href="StudentLocation_Manage.aspx?ActionType=slt&ItemType=1"><i class="fa fa-check text-maroon"></i> เลือกแหล่งฝึกงาน </a></li>
                        <% Else %>
                            <li><a href="StudentLocation_Manage.aspx?ActionType=slt&ItemType=1"><i class="fa fa-check"></i> เลือกแหล่งฝึกงาน</a></li>
                        <% End If %>
                 
                      <% If Request("ItemType") = "2" Then%>
                        <li class="active"><a href="StudentLocation_Manage.aspx?ActionType=slt&ItemType=2"><i class="fa fa-th-list text-maroon"></i>จัดการแหล่งฝึกที่เลือก</a></li>
                      <% Else %>
                        <li><a href="StudentLocation_Manage.aspx?ActionType=slt&ItemType=2"><i class="fa fa-th-list"></i>จัดการแหล่งฝึกที่เลือก</a></li>
                      <% End If %> 

                       <% If Request("ItemType") = "3" Then%>
                        <li class="active"><a href="ResultStudent.aspx?ActionType=slt&ItemType=3"><i class="fa fa-star text-maroon"></i>ผลการคัดเลือก/จัดสรร</a></li>
                      <% Else %>
                        <li><a href="ResultStudent.aspx?ActionType=slt&ItemType=3"><i class="fa fa-star"></i>ผลการคัดเลือก/จัดสรร</a></li>
                      <% End If %> 

                </ul>     
            </li>

         <!-- รายงานสำหรับนักศึกษา -->
    
       <% If Request("ActionType") = "rpt" Then  %>
        <li class="active treeview">
     <% Else %>
         <li class="treeview">
     <% End If %>        
          <a href="#">
            <i class="fa fa-pie-chart"></i> <span>รายงาน</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
                      
      <% If Request("ItemType") = "rpt1" Then%>
          <li class="active"><a href="ReportREQByLocation.aspx?ActionType=rpt&ItemType=rpt1"><i class="fa fa-bar-chart"></i>สรุปข้อมูลความต้องการของแหล่งฝึก</a></li>
      <% Else%>
          <li><a href="ReportREQByLocation.aspx?ActionType=rpt&ItemType=rpt1"><i class="fa fa-bar-chart"></i>สรุปข้อมูลความต้องการของแหล่งฝึก</a></li>
      <% End If%>            
     
      <% If Request("ItemType") = "rpt2" Then%>
          <li class="active"><a href="ReportStudentLocation.aspx?ActionType=rpt&ItemType=rpt2"><i class="fa fa-area-chart"></i>การเลือกแหล่งฝึกของนักศึกษา</a></li>
      <% Else%>
          <li><a href="ReportStudentLocation.aspx?ActionType=rpt&ItemType=rpt2"><i class="fa fa-area-chart"></i>การเลือกแหล่งฝึกของนักศึกษา</a></li>
      <% End If%> 

              </ul>
             </li>

<% End If %> <!-- End Student -->

<% If StrNull2Boolean(Request.Cookies("ROLE_ADM").Value) = False Then %>

        <% If StrNull2Boolean(Request.Cookies("ROLE_TCH").Value) = True Or (StrNull2Boolean(Request.Cookies("ROLE_ADV").Value) = True) Then %>
                <% If Request("ItemType") = "bio" Then%>
                    <li class="active"><a href="StudentPrintBio.aspx?ActionType=std&ItemType=bio"><i class="fa fa-print text-maroon"></i>พิมพ์ประวัตินักศึกษา</a></li>
                <% Else %>
                    <li><a href="StudentPrintBio.aspx?ActionType=std&ItemType=bio"><i class="fa fa-print"></i>พิมพ์ประวัตินักศึกษา</a></li>
                <% End If %> 
        <% End If %>

        <% If StrNull2Boolean(Request.Cookies("ROLE_PCT").Value) = True Then %>

                  <% If Request("ItemType") = "1"  %>
                    <li class="active"><a href="Location.aspx?ActionType=pct&ItemType=1"><i class="fa fa-home text-maroon"></i>จัดการข้อมูลแหล่งฝึก</a></li>
                  <% Else %>
                  <li><a href="Location.aspx?ActionType=pct&ItemType=1"><i class="fa fa-home"></i>จัดการข้อมูลแหล่งฝึก</a></li>
                  <% End If %>        


                   <% If Request("ItemType") = "2"  %>
                    <li class="active"><a href="ResultProceptor.aspx?ActionType=pct&ItemType=2"><i class="fa fa-user-md text-maroon"></i>รายชื่อนักศึกษาที่เข้ามาฝึก</a></li>
                  <% Else %>
                  <li><a href="ResultProceptor.aspx?ActionType=pct&ItemType=2"><i class="fa fa-user-md"></i>รายชื่อนักศึกษาที่เข้ามาฝึก</a></li>
                  <% End If %>
          
    
           <% End If %>
                <!-- end of Preceptor -->     
<% End If %>
    
     <!-- Start of Admin and Super Admin -->        
<% If StrNull2Boolean(Request.Cookies("ROLE_ADM").Value) = True Or StrNull2Boolean(Request.Cookies("ROLE_SPA").Value) = True Then%>           
    
      <% If Request.QueryString("ActionType") = "std" Then  %>
        <li class="active treeview">
     <% Else %>
         <li class="treeview">
     <% End If %>        
          <a href="#">
            <i class="fa fa-user-md"></i> <span>นักศึกษา</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">                	        
            
      <% If Request("ItemType") = "std" Then%>
        <li class="active"><a href="Student.aspx?ActionType=std&ItemType=std"> <i class="fa fa-user  text-maroon"></i>ข้อมูลนักศึกษา </a></li>
      <% Else %>
        <li><a href="Student.aspx?ActionType=std&ItemType=std"><i class="fa fa-user"></i> ข้อมูลนักศึกษา</a></li>
      <% End If %>
                 
      <% If Request("ItemType") = "bio" Then%>
        <li class="active"><a href="StudentPrintBio.aspx?ActionType=std&ItemType=bio"><i class="fa fa-print text-maroon"></i>พิมพ์ประวัตินักศึกษา</a></li>
      <% Else %>
        <li><a href="StudentPrintBio.aspx?ActionType=std&ItemType=bio"><i class="fa fa-print"></i>พิมพ์ประวัตินักศึกษา</a></li>
      <% End If %>  

      <% If Request.QueryString("ItemType") = "pthistory" Then%>
          <li class="active"><a href="Student.aspx?ActionType=std&ItemType=pthistory"><i class="fa fa-history"></i> ประวัติการฝึกปฏิบัติงานวิชาชีพ</a></li>
      <% Else%>
          <li><a href="Student.aspx?ActionType=std&ItemType=pthistory"><i class="fa fa-history"></i> ประวัติการฝึกปฏิบัติงานวิชาชีพ</a></li>  
      <% End If%> 



          </ul>     
            </li>


     <% If Request.QueryString("ActionType") = "tch" Then  %>
        <li class="active treeview">
     <% Else %>
         <li class="treeview">
     <% End If %>        
          <a href="#">
            <i class="fa fa-user-secret"></i> <span>อาจารย์</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">                	        
            
      <% If Request("ItemType") = "tch" Then%>
        <li class="active"><a href="TeacherData.aspx?ActionType=tch&ItemType=tch"> <i class="fa fa-user-md text-maroon"></i>ข้อมูลอาจารย์ </a></li>
      <% Else %>
        <li><a href="TeacherData.aspx?ActionType=tch&ItemType=tch"><i class="fa fa-user-md"></i> ข้อมูลอาจารย์</a></li>
      <% End If %>
                 
      <% If Request("ItemType") = "pct" Then%>
        <li class="active"><a href="PreceptorData.aspx?ActionType=tch&ItemType=pct"><i class="fa fa-stethoscope text-maroon"></i>ข้อมูลอาจารย์แหล่่งฝึก</a></li>
      <% Else %>
        <li><a href="PreceptorData.aspx?ActionType=tch&ItemType=pct"><i class="fa fa-stethoscope"></i>ข้อมูลอาจารย์แหล่่งฝึก</a></li>
      <% End If %>  
          </ul>     
            </li>

                <% If Request.QueryString("ActionType") = "sk" Then%>
                    <li class="active"><a href="Skill.aspx?ActionType=sk"><i class="fa fa-briefcase"></i> <span>ข้อมูลงานที่ฝึก</span></a></li>
                <% Else%>
                    <li><a href="Skill.aspx?ActionType=sk"><i class="fa fa-briefcase"></i> <span>ข้อมูลงานที่ฝึก</span></a></li>
                <% End If%>

                <% If Request.QueryString("ActionType") = "sj" Then%>
                    <li class="active"><a href="Subjects.aspx?ActionType=sj"><i class="fa fa-book"></i> <span>ข้อมูลรายวิชา</span></a></li>
                <% Else%>
                    <li><a href="Subjects.aspx?ActionType=sj"><i class="fa fa-book"></i> <span>ข้อมูลรายวิชา</span></a></li>
                <% End If%>
                      
     <% If Request.QueryString("ActionType") = "loc" Then  %>
        <li class="active treeview">
     <% Else %>
         <li class="treeview">
     <% End If %>        
          <a href="#">
            <i class="fa fa-hospital-o"></i> <span>แหล่งฝึก</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">                	        
            
      <% If Request("ItemType") = "grp" Then%>
        <li class="active"><a href="LocationGroup.aspx?ActionType=loc&ItemType=grp"> <i class="fa fa-bank text-maroon"></i>ประเภทแหล่งฝึก</a></li>
      <% Else %>
        <li><a href="LocationGroup.aspx?ActionType=loc&ItemType=grp"><i class="fa fa-bank"></i>ประเภทแหล่งฝึก</a></li>
      <% End If %>

               <% If Request("ItemType") = "wrk" Then%>
        <li class="active"><a href="LocationWork.aspx?ActionType=loc&ItemType=wrk"> <i class="fa fa-anchor text-maroon"></i>ส่วนงานของแหล่งฝึก</a></li>
      <% Else %>
        <li><a href="LocationWork.aspx?ActionType=loc&ItemType=wrk"><i class="fa fa-anchor"></i>ส่วนงานของแหล่งฝึก</a></li>
      <% End If %>

                 
      <% If Request("ItemType") = "zone" Then%>
        <li class="active"><a href="LocationZone.aspx?ActionType=loc&ItemType=zone"><i class="fa fa-map-marker text-maroon"></i>โซน</a></li>
      <% Else %>
        <li><a href="LocationZone.aspx?ActionType=loc&ItemType=zone"><i class="fa fa-map-marker"></i>โซน</a></li>
      <% End If %>  

        <% If Request("ItemType") = "bra" Then%>
        <li class="active"><a href="LocationOffice.aspx?ActionType=loc&ItemType=bra"><i class="fa fa-flag text-maroon"></i>กลุ่มสาขา</a></li>
      <% Else %>
        <li><a href="LocationOffice.aspx?ActionType=loc&ItemType=bra"><i class="fa fa-flag"></i>กลุ่มสาขา</a></li>
      <% End If %>  
                <% If Request("ItemType") = "l" Then%>
        <li class="active"><a href="LocationList.aspx?ActionType=loc&ItemType=l"><i class="fa fa-hospital-o text-maroon"></i>แหล่งฝึก</a></li>
      <% Else %>
        <li><a href="LocationList.aspx?ActionType=loc&ItemType=l"><i class="fa fa-hospital-o"></i>แหล่งฝึก</a></li>
      <% End If %>  

          </ul>     
            </li>

    

     <% If Request.QueryString("ActionType") = "phase" Then  %>
        <li class="active treeview">
     <% Else %>
         <li class="treeview">
     <% End If %>        
          <a href="#">
            <i class="fa fa-clock-o"></i> <span>ผลัดฝึก</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">                	        
            
      <% If Request("ItemType") = "tn" Then%>
        <li class="active"><a href="TurnPhase.aspx?ActionType=phase&ItemType=tn"> <i class="fa fa-hourglass-3 text-maroon"></i>กำหนดผลัดฝึก</a></li>
      <% Else %>
        <li><a href="TurnPhase.aspx?ActionType=phase&ItemType=tn"><i class="fa fa-hourglass-3"></i>กำหนดผลัดฝึก</a></li>
      <% End If %>
                 
      <!-- % If Request("ItemType") = "phase" Then%>
        <li class="active"><a href="TimePhase.aspx?ActionType=phase&ItemType=phase"><i class="fa fa-mouse-pointer text-maroon"></i>กำหนดเปิดผลัดฝึกให้รายวิชา</a></li>
      < % Else % >
        <li><a href="TimePhase.aspx?ActionType=phase&ItemType=phase"><i class="fa fa-mouse-pointer"></i>กำหนดเปิดผลัดฝึกให้รายวิชา</a></li>
      < % End If % -->     
              
      <% If Request("ItemType") = "phase2" Then%>
        <li class="active"><a href="SkillPhase.aspx?ActionType=phase&ItemType=phase2"><i class="fa fa-mouse-pointer text-maroon"></i>กำหนดผลัดฝึกให้งานที่ฝึก</a></li>
      <% Else %>
        <li><a href="SkillPhase.aspx?ActionType=phase&ItemType=phase2"><i class="fa fa-mouse-pointer"></i>กำหนดผลัดฝึกให้งานที่ฝึก</a></li>
      <% End If %>   


          </ul>     
            </li>


     <% If Request.QueryString("ActionType") = "setup" Then  %>
        <li class="active treeview">
     <% Else %>
         <li class="treeview">
     <% End If %>        
          <a href="#">
            <i class="fa fa-database"></i> <span>กำหนดข้อมูลประจำปี</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">      
             
              
      <% If Request("ItemType") = "tn" Then%>
        <li class="active"><a href="TurnPhase.aspx?ActionType=setup&ItemType=tn"> <i class="fa fa-hourglass-3 text-maroon"></i>กำหนดผลัดฝึก</a></li>
      <% Else %>
        <li><a href="TurnPhase.aspx?ActionType=setup&ItemType=tn"><i class="fa fa-hourglass-3"></i>กำหนดผลัดฝึก</a></li>
      <% End If %>

        <% If Request("ItemType") = "y1" Then%>
        <li class="active"><a href="Courses.aspx?ActionType=setup&ItemType=y1"> <i class="fa fa-book"></i>กำหนดรายวิชาที่เปิดฝึกงาน</a></li>
      <% Else %>
        <li><a href="Courses.aspx?ActionType=setup&ItemType=y1"><i class="fa fa-book"></i>กำหนดรายวิชาที่เปิดฝึกงาน</a></li>
      <% End If %>           

      <!-- % If Request("ItemType") = "phase" Then%>
        <li class="active"><a href="TimePhase.aspx?ActionType=setup&ItemType=phase"><i class="fa fa-mouse-pointer text-maroon"></i>กำหนดเปิดผลัดฝึกให้รายวิชา</a></li>
      < % Else %>
        <li><a href="TimePhase.aspx?ActionType=setup&ItemType=phase"><i class="fa fa-mouse-pointer"></i>กำหนดเปิดผลัดฝึกให้รายวิชา</a></li>
      < % End If %-->    
              
      <% If Request("ItemType") = "phase2" Then%>
        <li class="active"><a href="SkillPhase.aspx?ActionType=setup&ItemType=phase2"><i class="fa fa-mouse-pointer text-maroon"></i>กำหนดผลัดฝึกให้งานที่ฝึก</a></li>
      <% Else %>
        <li><a href="SkillPhase.aspx?ActionType=setup&ItemType=phase2"><i class="fa fa-mouse-pointer"></i>กำหนดผลัดฝึกให้งานที่ฝึก</a></li>
      <% End If %>   
    
      
                
      <% If Request("ItemType") = "y4" Then%>
        <li class="active"><a href="LocationWorkSkill.aspx?ActionType=setup&ItemType=y4"><i class="fa fa-hospital-o"></i>กำหนดงานที่เปิดฝึกให้แหล่งฝึก</a></li>
      <% Else %>
        <li><a href="LocationWorkSkill.aspx?ActionType=setup&ItemType=y4"><i class="fa fa-hospital-o"></i>กำหนดงานที่เปิดฝึกให้แหล่งฝึก</a></li>
      <% End If %>            

                
      <% If Request("ItemType") = "y5" Then%>
        <li class="active"><a href="RequirementsReg.aspx?ActionType=setup&ItemType=y5"><i class="fa fa-male"></i>บันทึกจำนวนนักศึกษาที่แหล่งฝึกรับ</a></li>
      <% Else %>
        <li><a href="RequirementsReg.aspx?ActionType=setup&ItemType=y5"><i class="fa fa-male"></i>บันทึกจำนวนนักศึกษาที่แหล่งฝึกรับ</a></li>
      <% End If %>            

                          
      <% If Request("ItemType") = "y6" Then%>
        <li class="active"><a href="RequirementSearch.aspx?ActionType=setup&ItemType=y6"><i class="fa fa-search"></i>รายการจำนวนนักศึกษาที่แหล่งฝึกรับ</a></li>
      <% Else %>
        <li><a href="RequirementSearch.aspx?ActionType=setup&ItemType=y6"><i class="fa fa-search"></i>รายการจำนวนนักศึกษาที่แหล่งฝึกรับ</a></li>
      <% End If %>  


                 
      <% If Request("ItemType") = "y2" Then%>
        <li class="active"><a href="CourseCoordinator.aspx?ActionType=setup&ItemType=y2"><i class="fa fa-user-md"></i>กำหนดอาจารย์ประจำวิชา</a></li>
      <% Else %>
        <li><a href="CourseCoordinator.aspx?ActionType=setup&ItemType=y2"><i class="fa fa-user-md"></i>กำหนดอาจารย์ประจำวิชา</a></li>
      <% End If %>            

  
      <% If Request("ItemType") = "y3" Then%>
        <li class="active"><a href="CourseStudent.aspx?ActionType=setup&ItemType=y3"><i class="fa fa-users"></i>กำหนดนักศึกษาในรายวิชา/งาน</a></li>
      <% Else %>
        <li><a href="CourseStudent.aspx?ActionType=setup&ItemType=y3"><i class="fa fa-users"></i>กำหนดนักศึกษาในรายวิชา/งาน</a></li>
      <% End If %>            

 <!--% If Request("ItemType") = "y7" Then%>
        <li class="active"><a href="CourseStudent_Modify.aspx?ActionType=setup&ItemType=y7"><i class="fa fa-users"></i>จัดการรายวิชาและงานที่ฝึกรายนักศึกษา</a></li>
      < % Else %>
        <li><a href="CourseStudent_Modify.aspx?ActionType=setup&ItemType=y7"><i class="fa fa-users"></i>จัดการรายวิชาและงานที่ฝึกรายนักศึกษา</a></li>
      < % End If % -->  


          </ul>     
            </li>

    
     <% If Request("ActionType") = "sl" Then  %>
        <li class="active treeview">
     <% Else %>
         <li class="treeview">
     <% End If %>        
          <a href="#">
            <i class="fa fa-check-square"></i> <span>ข้อมูลการเลือกแหล่งฝึก</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">

               <% If Request("ItemType") = "sl1" Then%>
        <li class="active"><a href="StudentRegister.aspx?ActionType=sl&ItemType=sl1"> <i class="fa fa-list-ol"></i>ข้อมูลการเลือกแหล่งฝึก</a></li>
      <% Else %>
        <li><a href="StudentRegister.aspx?ActionType=sl&ItemType=sl1"> <i class="fa fa-list-ol"></i>ข้อมูลการเลือกแหล่งฝึก</a></li>
      <% End If %>
                 
      <% If Request("ItemType") = "sl2" Then%>
        <li class="active"><a href="StudentNoRegister.aspx?ActionType=sl&ItemType=sl2"> <i class="fa fa-circle-o"></i>นักศึกษาที่ยังไม่ได้เลือกแหล่งฝึก</a></li>
      <% Else %>
        <li><a href="StudentNoRegister.aspx?ActionType=sl&ItemType=sl2"> <i class="fa fa-circle-o"></i>นักศึกษาที่ยังไม่ได้เลือกแหล่งฝึก</a></li>
      <% End If %>  

 <% If Request("ItemType") = "sl3" Then%>
        <li class="active"><a href="StudentRegisByAdmin.aspx?ActionType=sl&ItemType=sl3"> <i class="fa fa-circle-o"></i>เลือกแหล่งฝึกให้ นศ. (by Admin)</a></li>
      <% Else %>
        <li><a href="StudentRegisByAdmin.aspx?ActionType=sl&ItemType=sl3"> <i class="fa fa-circle-o"></i>เลือกแหล่งฝึกให้ นศ. (by Admin)</a></li>
      <% End If %> 


              </ul>
             </li>

    
     <% If Request("ActionType") = "asg" Then  %>
        <li class="active treeview">
     <% Else %>
         <li class="treeview">
     <% End If %>        
          <a href="#">
            <i class="fa fa-filter"></i> <span>การคัดเลือก/จัดสรร</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">

               <% If Request("ItemType") = "rnd" Then%>
        <li class="active"><a href="AssignMenu.aspx?ActionType=asg&ItemType=rnd"> <i class="fa fa-spinner"></i>คัดเลือก/จัดสรรด้วยการสุ่ม (Random)</a></li>
      <% Else %>
        <li><a href="AssignMenu.aspx?ActionType=asg&ItemType=rnd"> <i class="fa fa-spinner"></i>คัดเลือก/จัดสรรด้วยการสุ่ม (Random)</a></li>
      <% End If %>
                 
      <% If Request("ItemType") = "man" Then%>
        <li class="active"><a href="AssignManual.aspx?ActionType=asg&ItemType=man"> <i class="fa fa-external-link-square"></i>คัดเลือก/จัดสรรด้วยวิธี Manual</a></li>
      <% Else %>
        <li><a href="AssignManual.aspx?ActionType=asg&ItemType=man"> <i class="fa fa-external-link-square"></i>คัดเลือก/จัดสรรด้วยวิธี Manual</a></li>
      <% End If %>  

       <% If Request("ItemType") = "spec" Then%>
        <li class="active"><a href="AssignSpecify.aspx?ActionType=asg&ItemType=spec"> <i class="fa fa-mouse-pointer"></i>คัดเลือก/จัดสรรด้วยวิธี Specify</a></li>
      <% Else %>
        <li><a href="AssignSpecify.aspx?ActionType=asg&ItemType=spec"> <i class="fa fa-mouse-pointer"></i>คัดเลือก/จัดสรรด้วยวิธี Specify</a></li>
      <% End If %>  

  <% If Request("ItemType") = "find" Then%>
        <li class="active"><a href="ReportAssesmentByStudent.aspx?ActionType=asg&ItemType=find"> <i class="fa fa-search"></i>ค้นหาผลการคัดเลือก/จัดสรร</a></li>
      <% Else %>
        <li><a href="ReportAssesmentByStudent.aspx?ActionType=asg&ItemType=find"> <i class="fa fa-search"></i>ค้นหาผลการคัดเลือก/จัดสรร</a></li>
      <% End If %>  

      <% If Request("ItemType") = "null" Then%>
        <li class="active"><a href="StudentNoAssessment.aspx?ActionType=asg&ItemType=null"> <i class="fa fa-star-o"></i>รายชื่อนักศึกษาที่ยังไม่ได้แหล่งฝึก</a></li>
      <% Else %>
        <li><a href="StudentNoAssessment.aspx?ActionType=asg&ItemType=null"> <i class="fa fa-star-o"></i>รายชื่อนักศึกษาที่ยังไม่ได้แหล่งฝึก</a></li>
      <% End If %>  


    


              </ul>
             </li>
 <% End If %>        
     

<% If (StrNull2Boolean(Request.Cookies("ROLE_TCH").Value) = True) Or (StrNull2Boolean(Request.Cookies("ROLE_ADV").Value) = True) Or (StrNull2Boolean(Request.Cookies("ROLE_ADM").Value) = True) Or (StrNull2Boolean(Request.Cookies("ROLE_SPA").Value) = True) Then %>

     <% If Request("ActionType") = "psy"  %>
                    <li class="active"><a href="PsychologyAssessment.aspx?ActionType=psy"><i class="fa fa-dot-circle-o text-maroon"></i>การประเมินด้านจิตเวช</a></li>
                  <% Else %>
                      <li><a href="PsychologyAssessment.aspx?ActionType=psy"><i class="fa fa-dot-circle-o"></i>การประเมินด้านจิตเวช</a></li>
                  <% End If %>
       
            <% If Request("ActionType") = "spv" Then  %>
                <li class="active treeview">
            <% Else %>
                <li class="treeview">
            <% End If %>        
          <a href="#">
            <i class="fa fa-skyatlas"></i> <span>การนิเทศ</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>

          <ul class="treeview-menu">
              <% If Request("ItemType") = "spv3" Then%>
                <li class="active"><a href="SupervisorSelect.aspx?ActionType=spv&ItemType=spv3"><i class="fa fa-location-arrow"></i>เลือกสถานที่นิเทศ</a></li>
                <% Else %>
                    <li><a href="SupervisorSelect.aspx?ActionType=spv&ItemType=spv3"><i class="fa fa-location-arrow"></i>เลือกสถานที่นิเทศ</a></li>
                <% End If %>   
              
                <% If Request("ItemType") = "spv4" Then%>
                <li class="active"><a href="SupervisionPlan.aspx?ActionType=spv&ItemType=spv4"><i class="fa fa-paper-plane-o"></i>แผนการนิเทศของท่าน</a></li>
                <% Else %>
                    <li><a href="SupervisionPlan.aspx?ActionType=spv&ItemType=spv4"><i class="fa fa-paper-plane-o"></i>แผนการนิเทศของท่าน</a></li>
                <% End If %>  
                         
        <% If (StrNull2Boolean(Request.Cookies("ROLE_ADM").Value) = True) Or (StrNull2Boolean(Request.Cookies("ROLE_SPA").Value) = True) Then %>    

                <% If Request("ItemType") = "spv1" Then%>
                    <li class="active"><a href="SupervisionLocation.aspx?ActionType=spv&ItemType=spv1"><i class="fa fa-map-marker"></i> จัดการแหล่งฝึกที่ต้องนิเทศ </a></li>
                <% Else %>
                    <li><a href="SupervisionLocation.aspx?ActionType=spv&ItemType=spv1"><i class="fa fa-map-marker"></i>จัดการแหล่งฝึกที่ต้องนิเทศ</a></li>
                <% End If %>
                                       
                   <% If Request("ItemType") = "spv2" Then%>
                    <li class="active"><a href="SupervisorManage.aspx?ActionType=spv&ItemType=spv2"><i class="fa fa-car"></i>จัดสรรการออกนิเทศ (Manual)</a></li>
                  <% Else %>
                    <li><a href="SupervisorManage.aspx?ActionType=spv&ItemType=spv2"><i class="fa fa-car"></i>จัดสรรการออกนิเทศ (Manual)</a></li>
                  <% End If %>
                

                  <% If Request("ItemType") = "spv5" Then%>
                <li class="active"><a href="SupervisorSend.aspx?ActionType=spv&ItemType=spv5"><i class="fa fa-send"></i>ส่งงานนิเทศ</a></li>
              <% Else %>
                <li><a href="SupervisorSend.aspx?ActionType=spv&ItemType=spv5"><i class="fa fa-send"></i>ส่งงานนิเทศ</a></li>
              <% End If %>
              
        <% End If %> 
        
          </ul></li>

    

        <% If Request("ActionType") = "grdm" Then  %>
        <li class="active treeview">
     <% Else %>
         <li class="treeview">
     <% End If %>        
          <a href="#">
            <i class="fa fa-sort-alpha-asc"></i> <span>จัดการเกณฑ์ตัดเกรด</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
 <% If Request("ItemType") = "gd" Then%>
                    <li class="active"><a href="Grade.aspx?ActionType=grdm&ItemType=gd"><i class="fa fa-sort-alpha-asc"></i>Define Grade</a></li>
                <% Else%>
                    <li><a href="Grade.aspx?ActionType=grdm&ItemType=gd"><i class="fa fa-sort-alpha-asc"></i>Define Grade</a></li>
                <% End If%>    
              
 <% If Request("ItemType") = "gdc" Then%>
                <li class="active"><a href="GradeCopy.aspx?ActionType=grdm&ItemType=gdc"><i class="fa fa-sort-alpha-asc"></i>Grade Copy</a></li>
                <% Else%>
                    <li><a href="GradeCopy.aspx?ActionType=grdm&ItemType=gdc"><i class="fa fa-sort-alpha-asc"></i>Grade Copy</a></li>
                <% End If%>   


               <% If Request("ItemType") = "gdp" Then%>
                    <li class="active"><a href="GradePreceptor.aspx?ActionType=grdm&ItemType=gdp"><i class="fa fa-sort-alpha-asc"></i>Define Grade (แหล่งฝึก)</a></li>
                <% Else%>
                    <li><a href="GradePreceptor.aspx?ActionType=grdm&ItemType=gdp"><i class="fa fa-sort-alpha-asc"></i>Define Grade (แหล่งฝึก)</a></li>
                <% End If%>   

                <% If Request("ItemType") = "gdpc" Then%>
               <li class="active"><a href="GradePreceptorCopy.aspx?ActionType=grdm&ItemType=gdpc"><i class="fa fa-sort-alpha-asc"></i>Grade Copy(แหล่งฝึก)</a></li>
                <% Else%>
                    <li><a href="GradePreceptorCopy.aspx?ActionType=grdm&ItemType=gdpc"><i class="fa fa-sort-alpha-asc"></i>Grade Copy (แหล่งฝึก)</a></li>
                <% End If%> 

</ul>
             </li>


        <% If Request("ActionType") = "eva" Then  %>
        <li class="active treeview">
     <% Else %>
         <li class="treeview">
     <% End If %>        
          <a href="#">
            <i class="fa fa-line-chart"></i> <span>จัดการแบบประเมิน</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">

        <% If (StrNull2Boolean(Request.Cookies("ROLE_ADM").Value) = True) Or (StrNull2Boolean(Request.Cookies("ROLE_SPA").Value) = True) Then %>   
             
              <% If Request("ItemType") = "ev" Then%>
                <li class="active"><a href="EvaluationTopic.aspx?ActionType=eva&ItemType=ev"><i class="fa fa-trophy"></i>หัวข้อการประเมิน</a></li>
              <% Else %>
                <li><a href="EvaluationTopic.aspx?ActionType=eva&ItemType=ev"><i class="fa fa-trophy"></i>หัวข้อการประเมิน</a></li>
              <% End If %>


              <% If Request("ItemType") = "evg" Then%>
                <li class="active"><a href="EvaluationGroup.aspx?ActionType=eva&ItemType=evg"><i class="fa fa-th-large"></i>กลุ่มแบบประเมิน</a></li>
              <% Else %>
                <li><a href="EvaluationGroup.aspx?ActionType=eva&ItemType=evg"><i class="fa fa-th-large"></i>กลุ่มแบบประเมิน</a></li>
              <% End If %>
        
              <% If Request("ItemType") = "frm" Then%>
                <li class="active"><a href="EvaluationForm.aspx?ActionType=eva&ItemType=frm"><i class="fa fa-file-text"></i>แบบประเมิน</a></li>
              <% Else %>
                <li><a href="EvaluationForm.aspx?ActionType=eva&ItemType=frm"><i class="fa fa-file-text"></i>แบบประเมิน</a></li>
              <% End If %>
       

              <% If Request("ItemType") = "evas" Then%>
                <li class="active"><a href="EvaluationSubject.aspx?ActionType=eva&ItemType=evas"><i class="fa fa-paperclip"></i>ผูกแบบประเมินกับรายวิชา</a></li>
              <% Else %>
                <li><a href="EvaluationSubject.aspx?ActionType=eva&ItemType=evas"><i class="fa fa-paperclip"></i>ผูกแบบประเมินกับรายวิชา</a></li>
              <% End If %>

              
              <% If Request("ItemType") = "evl" Then%>
                <li class="active"><a href="AssessmentSubject.aspx?ActionType=eva&ItemType=evl"><i class="fa fa-tasks"></i>รายการแบบประเมิน</a></li>
              <% Else %>
                <li><a href="AssessmentSubject.aspx?ActionType=eva&ItemType=evl"><i class="fa fa-tasks"></i>รายการแบบประเมิน</a></li>
              <% End If %>
        <% End If %>  

        <% If (StrNull2Boolean(Request.Cookies("ROLE_ADV").Value) = True And (StrNull2Boolean(Request.Cookies("ROLE_ADM").Value) = False And StrNull2Boolean(Request.Cookies("ROLE_SPA").Value) = False)) Then %> 
              <% If Request("ItemType") = "frm" Then%>
                <li class="active"><a href="EvaluationFormList.aspx?ActionType=eva&ItemType=frm"><i class="fa fa-file-text"></i>แบบประเมิน</a></li>
              <% Else %>
                <li><a href="EvaluationFormList.aspx?ActionType=eva&ItemType=frm"><i class="fa fa-file-text"></i>แบบประเมิน</a></li>
              <% End If %>
        <% End If %> 


              </ul>
             </li>
<% End If %>  
     
<% If (StrNull2Boolean(Request.Cookies("ROLE_ADV").Value) = True) Then %>  
    
     <% If Request("ActionType") = "assm" Then  %>
        <li class="active treeview">
     <% Else %>
         <li class="treeview">
     <% End If %>        
          <a href="#">
            <i class="fa fa-thumbs-up"></i> <span>การประเมินผล</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">

               <% If Request("ItemType") = "asg" Then%>
        <li class="active"><a href="AssessmentGate.aspx?ActionType=assm&ItemType=assm"> <i class="fa fa-check-square-o text-maroon"></i>ทำการประเมิน</a></li>
      <% Else %>
        <li><a href="AssessmentGate.aspx?ActionType=assm&ItemType=assm"> <i class="fa fa-check-square-o"></i>ทำการประเมิน</a></li>
      <% End If %>
                  
              </ul>
             </li>
   <% End If %> 

  <% If (StrNull2Boolean(Request.Cookies("ROLE_ADV").Value) = True) Or (StrNull2Boolean(Request.Cookies("ROLE_ADM").Value) = True) Then %> 
    
         <% If Request("ActionType") = "ass" Then  %>
            <li class="active treeview">
         <% Else %>
             <li class="treeview">
         <% End If %>        
              <a href="#">
                <i class="fa fa-trophy"></i> <span>ผลการประเมิน(admin)</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
        <% If (StrNull2Boolean(Request.Cookies("ROLE_ADM").Value) = True) Then %> 

              <% If Request("ItemType") = "score" Then%>
                <li class="active"><a href="AssesseeListAdmin.aspx?ActionType=ass&ItemType=score"> <i class="fa fa-pencil"></i>ดู/แก้ไข คะแนนประเมิน</a></li>
              <% Else %>
                <li><a href="AssesseeListAdmin.aspx?ActionType=ass&ItemType=score"> <i class="fa fa-pencil"></i>ดู/แก้ไข คะแนนประเมิน</a></li>
              <% End If %>
                 
              <% If Request("ItemType") = "st" Then%>
                <li class="active"><a href="AssesseeStatus.aspx?ActionType=ass&ItemType=st"> <i class="fa fa-asterisk"></i>สถานะผู้ถูกประเมิน</a></li>
              <% Else %>
                <li><a href="AssesseeStatus.aspx?ActionType=ass&ItemType=st"> <i class="fa fa-asterisk"></i>สถานะผู้ถูกประเมิน</a></li>
              <% End If %>  
         <% End If %>  

        
          <% If Request("ItemType") = "sum" Then%>
            <li class="active"><a href="AssessmentResult.aspx?ActionType=ass&ItemType=sum"> <i class="fa fa-sitemap"></i>สรุปคะแนนประเมิน</a></li>
          <% Else %>
            <li><a href="AssessmentResult.aspx?ActionType=ass&ItemType=sum"> <i class="fa fa-sitemap"></i>สรุปคะแนนประเมิน</a></li>
          <% End If %>  
      

              </ul>
             </li>

   <% End If %> 

  <% If (StrNull2Boolean(Request.Cookies("ROLE_ADM").Value) = True) Or (StrNull2Boolean(Request.Cookies("ROLE_SPA").Value) = True) Or (StrNull2Boolean(Request.Cookies("ROLE_TCH").Value) = True) Or (StrNull2Boolean(Request.Cookies("ROLE_ADV").Value) = True) Then %> 
        
     <% If Request("ActionType") = "grd" Then  %>
            <li class="active treeview">
         <% Else %>
             <li class="treeview">
         <% End If %>        
              <a href="#">
                <i class="fa fa-list-ol"></i><span>คะแนนประเมิน/เกรด</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">

              <% If Request("ItemType") = "mod" Then%>
                <li class="active"><a href="GradeModify.aspx?ActionType=grd&ItemType=mod"> <i class="fa fa-pencil"></i>ดู/แก้ไข</a></li>
              <% Else %>
                <li><a href="GradeModify.aspx?ActionType=grd&ItemType=mod"> <i class="fa fa-pencil"></i>ดู/แก้ไข</a></li>
              <% End If %>

                   <% If Request("ItemType") = "rpct1" Then%>
                             <li class="active"><a href="ReportConditionBySubject.aspx?ActionType=grd&ItemType=rpct1"><i class="fa fa-bar-chart"></i>คะแนนประเมินตามหัวข้อประเมิน</a></li>
                         <% Else%>
                             <li><a href="ReportConditionBySubject.aspx?ActionType=grd&ItemType=rpct1"><i class="fa fa-bar-chart"></i>คะแนนประเมินตามหัวข้อประเมิน</a></li>
                         <% End If%>
                     
                         <% If Request("ItemType") = "rpct2" Then%>
                             <li class="active"><a href="ReportConditionBySubject.aspx?ActionType=grd&ItemType=rpct2"><i class="fa fa-bar-chart"></i>สรุปคะแนนประเมินรายแบบประเมิน</a></li>
                         <% Else%>
                             <li><a href="ReportConditionBySubject.aspx?ActionType=grd&ItemType=rpct2"><i class="fa fa-bar-chart"></i>สรุปคะแนนประเมินรายแบบประเมิน</a></li>
                         <% End If%>

             
        
        <%--  <% If Request("ItemType") = "sum" Then%>
            <li class="active"><a href="GradeFinal.aspx?ActionType=grd&ItemType=sum"> <i class="fa fa-sort-alpha-asc"></i>สรุปเกรด(Finalize)</a></li>
          <% Else %>
            <li><a href="GradeFinal.aspx?ActionType=grd&ItemType=sum"> <i class="fa fa-sort-alpha-asc"></i>สรุปเกรด(Finalize)</a></li>
          <% End If %>  --%>
      

              </ul>
             </li> 


         <% If Request("ActionType") = "rpt" Then  %>
            <li class="active treeview">
         <% Else %>
             <li class="treeview">
         <% End If %>        
              <a href="#">
                <i class="fa fa-pie-chart"></i> <span>รายงาน</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">

        <% If Request("ActionType") = "rpt0" Then  %>
            <li class="active treeview">
         <% Else %>
             <li class="treeview">
         <% End If %>        
              <a href="#">
                <i class="fa fa-stethoscope"></i> <span>รายงานสุขภาพนักศึกษา</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                    <% If Request("ItemType") = "h1" Then%>
                        <li class="active"><a href="ReportHealth.aspx?ActionType=rpt0&ItemType=h1"><i class="fa fa-circle-o text-red"></i>การรับวัคซีน Covid-19</a></li>
                    <% Else%>
                        <li><a href="ReportHealth.aspx?ActionType=rpt0&ItemType=h1"><i class="fa fa-circle-o text-red"></i>การรับวัคซีน Covid-19</a></li>
                    <% End If%>  
                    <% If Request("ItemType") = "h2" Then%>
                        <li class="active"><a href="#?ActionType=rpt0&ItemType=h2"><i class="fa fa-circle-o text-warning"></i>การรับวัคซีนทั่วไป</a></li>
                    <% Else%>
                        <li><a href="#?ActionType=rpt0&ItemType=h2"><i class="fa fa-circle-o text-warning"></i>การรับวัคซีนทั่วไป</a></li>
                    <% End If%> 

                   <% If Request("ItemType") = "h3" Then%>
                        <li class="active"><a href="#?ActionType=rpt0&ItemType=h3"><i class="fa fa-circle-o text-info"></i>การป่วยด้วย Covid-19</a></li>
                    <% Else%>
                        <li><a href="#?ActionType=rpt0&ItemType=h3"><i class="fa fa-circle-o text-info"></i>การป่วยด้วย Covid-19</a></li>
                    <% End If%>  
                    <% If Request("ItemType") = "h4" Then%>
                        <li class="active"><a href="#?ActionType=rpt0&ItemType=h4"><i class="fa fa-circle-o text-success"></i>การเจ็บป่วยทั่วไป</a></li>
                    <% Else%>
                        <li><a href="#?ActionType=rpt0&ItemType=h4"><i class="fa fa-circle-o text-success"></i>การเจ็บป่วยทั่วไป</a></li>
                    <% End If%> 
             
              </ul>
        </li>

                      
      <% If Request("ItemType") = "rpt1" Then%>
          <li class="active"><a href="ReportREQByLocation.aspx?ActionType=rpt&ItemType=rpt1"><i class="fa fa-bar-chart"></i>สรุปข้อมูลความต้องการของแหล่งฝึก</a></li>
      <% Else%>
          <li><a href="ReportREQByLocation.aspx?ActionType=rpt&ItemType=rpt1"><i class="fa fa-bar-chart"></i>สรุปข้อมูลความต้องการของแหล่งฝึก</a></li>
      <% End If%>            
     
      <% If Request("ItemType") = "rpt2" Then%>
          <li class="active"><a href="ReportStudentLocation.aspx?ActionType=rpt&ItemType=rpt2"><i class="fa fa-area-chart"></i>การเลือกแหล่งฝึกของนักศึกษา</a></li>
      <% Else%>
          <li><a href="ReportStudentLocation.aspx?ActionType=rpt&ItemType=rpt2"><i class="fa fa-area-chart"></i>การเลือกแหล่งฝึกของนักศึกษา</a></li>
      <% End If%> 

              
      <% If Request("ItemType") = "rpt3" Then%>
          <li class="active"><a href="ReportConditionByYear.aspx?ActionType=rpt&ItemType=rpt3&m=manage"><i class="fa fa-street-view"></i>การจัดสรรนักศึกษาประจำแหล่งฝึก</a></li>
      <% Else%>
          <li><a href="ReportConditionByYear.aspx?ActionType=rpt&ItemType=rpt3&m=manage"><i class="fa fa-street-view"></i>การจัดสรรนักศึกษาประจำแหล่งฝึก</a></li>
      <% End If%> 
              
      <% If Request("ItemType") = "rpt4" Then%>
          <li class="active"><a href="ReportConditionByYear.aspx?ActionType=rpt&ItemType=rpt4&m=stdassessment"><i class="fa fa-venus-mars"></i>การจัดสรรแหล่งฝึกแยกตามรายชื่อนักศึกษา</a></li>
      <% Else%>
          <li><a href="ReportConditionByYear.aspx?ActionType=rpt&ItemType=rpt4&m=stdassessment"><i class="fa fa-venus-mars"></i>การจัดสรรแหล่งฝึกแยกตามรายชื่อนักศึกษา</a></li>
      <% End If%> 
              
      <% If Request("ItemType") = "rpt5" Then%>
          <li class="active"><a href="ReportCondition.aspx?ActionType=rpt&ItemType=rpt5&m=docstdlist"><i class="fa fa-file-pdf-o"></i>ใบแจ้งผลัดฝึก</a></li>
      <% Else%>
          <li><a href="ReportCondition.aspx?ActionType=rpt&ItemType=rpt5&m=docstdlist"><i class="fa fa-file-pdf-o"></i>ใบแจ้งผลัดฝึก</a></li>
      <% End If%> 
              
     
              
      <% If Request("ItemType") = "rpt7" Then%>
          <li class="active"><a href="ReportConditionByYear.aspx?ActionType=rpt&ItemType=rpt7&m=timephase"><i class="fa fa-umbrella"></i>รายงานช่วงเวลาที่มีนักศึกษาฝึก</a></li>
      <% Else%>
          <li><a href="ReportConditionByYear.aspx?ActionType=rpt&ItemType=rpt7&m=timephase"><i class="fa fa-umbrella"></i>รายงานช่วงเวลาที่มีนักศึกษาฝึก</a></li>
      <% End If%> 
              
      <% If Request("ItemType") = "rpt8" Then%>
          <li class="active"><a href="ReportConditionByYear.aspx?ActionType=rpt&ItemType=rpt8&m=phasecount"><i class="fa fa-tree"></i>สรุปจำนวนนักศึกษาแต่ละผลัดฝึก</a></li>
      <% Else%>
          <li><a href="ReportConditionByYear.aspx?ActionType=rpt&ItemType=rpt8&m=phasecount"><i class="fa fa-tree"></i>สรุปจำนวนนักศึกษาแต่ละผลัดฝึก</a></li>
      <% End If%>

              </ul>
             </li>

  <% End If %> 


 <% If StrNull2Boolean(Request.Cookies("ROLE_SPA").Value) = True Or StrNull2Boolean(Request.Cookies("ROLE_ADM").Value) = True Then%>
    
     
     <% If Request("ActionType") = "setting" Then  %>
        <li class="active treeview">
     <% Else %>
         <li class="treeview">
     <% End If %>        
          <a href="#">
            <i class="fa fa-gears"></i> <span>ตั้งค่าข้อมูลพื้นฐาน</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">     


      <% If Request("ItemType") = "prov" Then%>
        <li class="active"><a href="Provinces.aspx?ActionType=setting&ItemType=prov"><i class="fa fa-map-marker"></i>จังหวัด</a></li>
      <% Else %>
        <li><a href="Provinces.aspx?ActionType=setting&ItemType=prov"><i class="fa fa-map-marker"></i>จังหวัด</a></li>
      <% End If %>  
       <% If Request("ItemType") = "pre" Then%>
        <li class="active"><a href="Prefix.aspx?ActionType=setting&ItemType=pre"><i class="fa fa-get-pocket"></i>คำนำหน้าชื่อ</a></li>
      <% Else %>
        <li><a href="Prefix.aspx?ActionType=setting&ItemType=pre"><i class="fa fa-get-pocket"></i>คำนำหน้าชื่อ</a></li>
      <% End If %> 
       
      <% If Request("ItemType") = "pos" Then%>
        <li class="active"><a href="Positions.aspx?ActionType=setting&ItemType=pos"> <i class="fa fa-odnoklassniki"></i>ชื่อตำแหน่ง</a></li>
      <% Else %>
        <li><a href="Positions.aspx?ActionType=setting&ItemType=pos"> <i class="fa fa-odnoklassniki"></i>ชื่อตำแหน่ง</a></li>
      <% End If %>
                 
      <% If Request("ItemType") = "dept" Then%>
        <li class="active"><a href="Department.aspx?ActionType=setting&ItemType=dept"><i class="fa fa-briefcase"></i>ชื่อภาควิชา</a></li>
      <% Else %>
        <li><a href="Department.aspx?ActionType=setting&ItemType=dept"><i class="fa fa-briefcase"></i>ชื่อภาควิชา</a></li>
      <% End If %>  



      <% If Request("ItemType") = "lev" Then%>
        <li class="active"><a href="DataConfigLevel.aspx?ActionType=setting&ItemType=lev"><i class="fa fa-stumbleupon"></i>กำหนดระดับชั้นนักศึกษา</a></li>
      <% Else %>
        <li><a href="DataConfigLevel.aspx?ActionType=setting&ItemType=lev"><i class="fa fa-stumbleupon"></i>กำหนดระดับชั้นนักศึกษา</a></li>
      <% End If %> 
            <% If Request("ItemType") = "pay" Then%>
        <li class="active"><a href="PaymentConfig.aspx?ActionType=setting&ItemType=pay"><i class="fa fa-dollar"></i>กำหนดค่าตอบแทน</a></li>
      <% Else %>
        <li><a href="PaymentConfig.aspx?ActionType=setting&ItemType=pay"><i class="fa fa-dollar"></i>กำหนดค่าตอบแทน</a></li>
      <% End If %>             
        <% If Request("ItemType") = "sys" Then%>
        <li class="active"><a href="DataConfig.aspx?ActionType=setting&ItemType=sys"><i class="fa fa-gears"></i>กำหนดค่าเริ่มต้นระบบ</a></li>
      <% Else %>
        <li><a href="DataConfig.aspx?ActionType=setting&ItemType=sys"><i class="fa fa-gears"></i>กำหนดค่าเริ่มต้นระบบ</a></li>
      <% End If %>   

          </ul></li>

    
     <% If Request("ActionType") = "user" Then  %>
        <li class="active treeview">
     <% Else %>
         <li class="treeview">
     <% End If %>        
          <a href="#">
            <i class="fa fa-users"></i> <span>User Account</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">

              
               <% If Request("ItemType") = "role" Then%>
        <li class="active"><a href="Roles.aspx?ActionType=user&ItemType=role"> <i class="fa fa-key"></i>กำหนดสิทธิ์ (Role)</a></li>
      <% Else %>
        <li><a href="Roles.aspx?ActionType=user&ItemType=role"> <i class="fa fa-key"></i>กำหนดสิทธิ์ (Role)</a></li>
      <% End If %>


               <% If Request("ItemType") = "add" Then%>
        <li class="active"><a href="Users.aspx?ActionType=user&ItemType=add"> <i class="fa fa-users"></i>จัดการผู้ใช้งาน</a></li>
      <% Else %>
        <li><a href="Users.aspx?ActionType=user&ItemType=add"> <i class="fa fa-users"></i>จัดการผู้ใช้งาน</a></li>
      <% End If %>
                 
     <% If Request("ItemType") = "user1" Then%>
        <li class="active"><a href="UserStudent.aspx?ActionType=user&ItemType=user1"> <i class="fa fa-users"></i>สำหรับนักศึกษา</a></li>
      <% Else %>
        <li><a href="UserStudent.aspx?ActionType=user&ItemType=user1"> <i class="fa fa-users"></i>สำหรับนักศึกษา</a></li>
      <% End If %>

                   <% If Request("ItemType") = "user2" Then%>
        <li class="active"><a href="UserAdvisor.aspx?ActionType=user&ItemType=user2"> <i class="fa fa-user-md"></i>สำหรับอาจารย์และบุคลากร</a></li>
      <% Else %>
        <li><a href="UserAdvisor.aspx?ActionType=user&ItemType=user2"> <i class="fa fa-user-md"></i>สำหรับอาจารย์และบุคลากร</a></li>
      <% End If %>

                   <% If Request("ItemType") = "user3" Then%>
        <li class="active"><a href="UserPreceptor.aspx?ActionType=user&ItemType=user3"> <i class="fa fa-stethoscope"></i>Advisor to Preceptor</a></li>
      <% Else %>
        <li><a href="UserPreceptor.aspx?ActionType=user&ItemType=user3"> <i class="fa fa-stethoscope"></i>Advisor to Preceptor</a></li>
      <% End If %>



      <% If Request("ItemType") = "history" Then%>
        <li class="active"><a href="LogfilesByUser.aspx?ActionType=user&ItemType=history"> <i class="fa fa-font"></i>ตรวจสอบประวัติการใช้งาน</a></li>
      <% Else %>
        <li><a href="LogfilesByUser.aspx?ActionType=user&ItemType=history"> <i class="fa fa-font"></i>ตรวจสอบประวัติการใช้งาน</a></li>
      <% End If %>  

        <% If Request("ItemType") = "pass" Then%>
        <li class="active"><a href="ChangeUsername.aspx?ActionType=user&ItemType=pass"> <i class="fa fa-coffee"></i>เปลี่ยน Username</a></li>
      <% Else %>
        <li><a href="ChangeUsername.aspx?ActionType=user&ItemType=pass"> <i class="fa fa-coffee"></i>เปลี่ยน Username</a></li>
      <% End If %>  

              </ul>
             </li> 

     <% If Request("ActionType") = "media" Then  %>
        <li class="active treeview">
     <% Else %>
         <li class="treeview">
     <% End If %>        
          <a href="#">
            <i class="fa fa-cloud-download"></i> <span>เอกสารดาวน์โหลด</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
             <% If Request("ItemType") = "media" Then%>
        <li class="active"><a href="MediaList.aspx?ActionType=media&ItemType=media"> <i class="fa fa-download"></i>รายการเอกสาร</a></li>
      <% Else %>
        <li><a href="MediaList.aspx?ActionType=media&ItemType=media"> <i class="fa fa-download"></i>รายการเอกสาร</a></li>
      <% End If %>

       <% If Request("ItemType") = "addmed" Then%>
        <li class="active"><a href="MediaAdd.aspx?ActionType=media&ItemType=addmed"> <i class="fa fa-upload"></i>Upload เอกสาร </a></li>
      <% Else %>
        <li><a href="MediaAdd.aspx?ActionType=media&ItemType=addmed"><i class="fa fa-upload"></i>Upload เอกสาร</a></li>
      <% End If %>
        </ul></li>

     
     <% If Request("ActionType") = "news" Then  %>
        <li class="active treeview">
     <% Else %>
         <li class="treeview">
     <% End If %>        
          <a href="#">
            <i class="fa fa-bullhorn"></i> <span>ข่าวประกาศ</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
             <% If Request("ItemType") = "news" Then%>
        <li class="active"><a href="News_List.aspx?ActionType=news&ItemType=news"> <i class="fa fa-rss"></i>รายการข่าวประกาศ</a></li>
      <% Else %>
        <li><a href="News_List.aspx?ActionType=news&ItemType=news"> <i class="fa fa-rss"></i>รายการข่าวประกาศ</a></li>
      <% End If %>

       <% If Request("ItemType") = "addnews" Then%>
        <li class="active"><a href="News_Manage.aspx?ActionType=news&ItemType=addnews"> <i class="fa fa-share-alt"></i>เพิ่มข่าวประกาศ </a></li>
      <% Else %>
        <li><a href="News_Manage.aspx?ActionType=news&ItemType=addnews"><i class="fa fa-share-alt"></i> เพิ่มข่าวประกาศ</a></li>
      <% End If %>

      <% If Request("ItemType") = "evmod" Then%>
        <li class="active"><a href="EventModify.aspx?ActionType=news&ItemType=evmod"> <i class="fa fa-calendar"></i>จัดการปฏิทินกิจกรรม</a></li>
      <% Else %>
        <li><a href="EventModify.aspx?ActionType=news&ItemType=evmod"><i class="fa fa-calendar"></i>จัดการปฏิทินกิจกรรม</a></li>
      <% End If %>

        </ul></li>

    
    <% End If %>

<% If StrNull2Boolean(Request.Cookies("ROLE_SPA").Value) = True Or StrNull2Boolean(Request.Cookies("ROLE_ADM").Value) = True Then%>     
        <% If Request.QueryString("ActionType") = "timeline" Then%>
            <li class="active"><a href="TimelineSetting.aspx?ActionType=timeline"><i class="fa fa-clock-o"></i> <span>กำหนด Timeline</span></a></li>
        <% Else%>
            <li><a href="TimelineSetting.aspx?ActionType=timeline"><i class="fa fa-clock-o"></i> <span>กำหนด Timeline</span></a></li>
        <% End If%> 

     
<% End If%>  
    

        <% If Request.QueryString("ActionType") = "chg" Then%>
            <li class="active"><a href="ChangePassword.aspx?ActionType=chg"><i class="fa fa-key"></i> <span>เปลี่ยนรหัสผ่าน</span></a></li>
        <% Else%>
            <li><a href="ChangePassword.aspx?ActionType=chg"><i class="fa fa-key"></i> <span>เปลี่ยนรหัสผ่าน</span></a></li>
        <% End If%> 
                 
        <li><a href="Default.aspx?logout=y"><i class="fa fa-power-off"></i> <span>ออกจากระบบ</span></a></li>
    <!-- 
 <li><a href="Default.aspx?logout=y"><span><< < % =Request.Cookies("ROLE_STD").Value %> >></span></a></li>
     <li><a href="Default.aspx?logout=y"><span><< < % =Request.Cookies("UserLogin").Value %> >></span></a></li>
        -->
</ul>
