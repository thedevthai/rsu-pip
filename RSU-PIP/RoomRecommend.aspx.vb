﻿Public Class RoomRecommend
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlR As New RecommendController


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("UserLogin").Value) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If

        If Not IsPostBack Then
            grdData.PageIndex = 0
            LoadRecommendToGrid()
        End If
    End Sub

    Private Sub LoadRecommendToGrid()

        If Request.Cookies("ROLE_STD").Value = True Then
            dt = ctlR.RoomRecommend_GetByStudent(Request.Cookies("UserLogin").Value, Trim(txtSearchStd.Text))
        Else
            dt = ctlR.RoomRecommend_GetSearch(Trim(txtSearchStd.Text))
        End If

        If dt.Rows.Count > 0 Then
            lblCount.Text = dt.Rows.Count
            lblNo.Visible = False
            With grdData
                .Visible = True
                .DataSource = dt
                .DataBind()
            End With
        Else
            lblCount.Text = 0
            lblNo.Visible = True
            grdData.Visible = False
            grdData.DataSource = Nothing
        End If

        dt = Nothing
    End Sub

    Private Sub grdData_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdData.PageIndexChanging
        grdData.PageIndex = e.NewPageIndex
        LoadRecommendToGrid()
    End Sub

    Private Sub grdData_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdData.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgView"
                    'DisplayPopUpWindows(Me.Page, "window.open('" + ResolveUrl("RecommendModify.aspx?id=" + e.CommandArgument() + "','pop','width=1000,height=800,left=270,top=180,titlebar=no,menubar=no,resizable=yes,toolbar=no,scrollbars=no');"))

                    Response.Redirect("RoomRecommendModify.aspx?id=" & e.CommandArgument)
                Case "imgDel"
                    If ctlR.RoomRecommend_Delete(e.CommandArgument) Then
                        Dim acc As New UserController
                        acc.User_GenLogfile(Request.Cookies("UserLogin").Value, ACTTYPE_DEL, "RoomRecommend", "ลบคำแนะนำ:" & e.CommandArgument, "")
                        DisplayMessage(Me, "ลบข้อมูลเรียบร้อย")
                        LoadRecommendToGrid()
                    Else
                        DisplayMessage(Me, "ไม่สามารถลบข้อมูลได้ กรุณาตรวจสอบและลองใหม่อีกครั้ง")
                    End If
            End Select


        End If
    End Sub
    Private Sub grdData_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdData.RowDataBound

        'If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then
        '    Dim imgE As ImageButton = e.Row.Cells(4).FindControl(0)
        '    If e.Row.Cells(3).Text = "Approve" Then
        '        imgE.Visible = False
        '    End If

        'End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#ffdfef';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If

    End Sub


    Protected Sub cmdFind_Click(sender As Object, e As EventArgs) Handles cmdFind.Click
        grdData.PageIndex = 0
        LoadRecommendToGrid()
    End Sub

    Protected Sub cmdAdd_Click(sender As Object, e As EventArgs) Handles cmdAdd.Click
        Response.Redirect("RoomRecommendModify.aspx?new=y")
    End Sub
End Class

