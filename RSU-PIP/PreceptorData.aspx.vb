﻿Imports System.IO
Imports System.Data.OleDb
Imports System.Data
Imports Subgurim.Controles

Public Class PreceptorData
    Inherits System.Web.UI.Page

    Dim dt As New DataTable
    Dim ctlStd As New PersonController
    Dim objUser As New UserController
    Dim ctlbase As New ApplicationBaseClass

    Dim objLct As New LocationInfo
    Dim ctlLct As New LocationController



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            grdData.PageIndex = 0
            isAdd = True
            ClearData()
            UpdateProgress1.Visible = False
           
            LoadPrefixToDDL()
            LoadLocationToDDL()
            LoadPerson()
        End If

      
    End Sub

    Private Sub LoadPrefixToDDL()
        dt = ctlbase.Prefix_GetForPreceptor
        If dt.Rows.Count > 0 Then
            With ddlPrefix
                .Enabled = True
                .DataSource = dt
                .DataTextField = "PrefixName"
                .DataValueField = "PrefixID"
                .DataBind()
                '.SelectedIndex = 0
            End With
        End If
        dt = Nothing
    End Sub
    Private Sub LoadLocationToDDL()
        If txtFindLocation.Text = "" Then
            dt = ctlLct.Location_Get
        Else
            dt = ctlLct.Location_GetBySearch(txtFindLocation.Text)
        End If

        ddlLocation.Items.Clear()

        If dt.Rows.Count > 0 Then
            With ddlLocation
                .Visible = True
                For i = 0 To dt.Rows.Count - 1
                    .Items.Add(dt.Rows(i)(objLct.tblField(objLct.fldPos.f01_LocationName).fldName))
                    .Items(i).Value = dt.Rows(i)(objLct.tblField(objLct.fldPos.f00_LocationID).fldName)
                Next
                .SelectedIndex = 0
            End With
        End If

    End Sub
    Private Sub LoadPersonByLocation(pLoc As Integer)

        dt = ctlStd.Preceptor_GetByLocation(pLoc)

        If dt.Rows.Count > 0 Then
            With grdData
                .Visible = True
                .DataSource = dt
                .DataBind()

                lblStudentCount.Text = "พบรายชื่อทั้งหมด " & dt.Rows.Count & " คน"
            End With
        Else
            lblStudentCount.Text = "ไม่พบรายชื่อในแหล่งฝึกนี้"
            grdData.Visible = False
        End If
    End Sub

    Private Sub LoadPerson()

        If Trim(txtSearch.Text) <> "" Then
            dt = ctlStd.GetPerson_SearchByType("P", txtSearch.Text)
        Else
            dt = ctlStd.Person_GetActiveByType("P")
        End If


        If dt.Rows.Count > 0 Then
            With grdData
                .Visible = True
                .DataSource = dt
                .DataBind()

                'For i = 0 To dt.Rows.Count - 1
                '    .Rows(i).Cells(0).Text = i + 1
                'Next
                lblStudentCount.Text = "พบรายชื่อทั้งหมด " & dt.Rows.Count & " คน"
            End With
        Else
            grdData.Visible = False
        End If
    End Sub

    Dim acc As New UserController

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click

        If txtFirstName.Text = "" Or txtLastName.Text = "" Then
            DisplayMessage(Me.Page, "กรุณป้อนข้อมูลให้ครบถ้วน")
            Exit Sub
        End If

        If lblID.Text = "" Then

            ctlStd.Person_Add(ddlPrefix.SelectedValue, txtFirstName.Text, txtLastName.Text, txtPositionName.Text, txtMail.Text, "P", ddlLocation.SelectedValue)
            acc.User_GenLogfile(Request.Cookies("UserLogin").Value, ACTTYPE_ADD, "Persons", "เพิ่มรายชื่ออาจารย์แหล่งฝึกใหม่ :" & txtFirstName.Text & " " & txtLastName.Text, "เพิ่มแบบทีละคน")
        Else
            ctlStd.Person_UpdateSmall(StrNull2Zero(lblID.Text), ddlPrefix.SelectedValue, txtFirstName.Text, txtLastName.Text, txtPositionName.Text, txtMail.Text, "P", ddlLocation.SelectedValue)
            acc.User_GenLogfile(Request.Cookies("UserLogin").Value, ACTTYPE_UPD, "Persons", "แก้ไขชื่ออาจารย์แหล่งฝึก :" & txtFirstName.Text & " " & txtLastName.Text, "เพิ่มแบบทีละคน")

        End If


        DisplayMessage(Me.Page, "บันทึกข้อมูลเรียบร้อย")
        isAdd = True
        LoadPerson()
        ClearData()
    End Sub
    Private Sub ClearData()
        lblID.Text = ""
        txtFirstName.Text = ""
        txtLastName.Text = ""
        ddlPrefix.SelectedIndex = 0
        txtPositionName.Text = ""
        txtMail.Text = ""
        lblMode.Text = "Mode : Add New"
    End Sub
    Private Sub grdData_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdData.PageIndexChanging
        grdData.PageIndex = e.NewPageIndex
        LoadPerson()
    End Sub

    Private Sub grdData_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdData.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit"
                    EditData(e.CommandArgument())
                Case "imgDel"
                    If ctlStd.Person_Delete(e.CommandArgument) Then

                        objUser.User_GenLogfile(Request.Cookies("UserLogin").Value, "DEL", "Persons", "ลบอาจารย์ :" & e.CommandArgument, "")

                        DisplayMessage(Me, "ลบข้อมูลเรียบร้อย")
                    Else
                        DisplayMessage(Me, "ไม่สามารถลบข้อมูลได้ กรุณาตรวจสอบและลองใหม่อีกครั้ง")
                    End If

                    LoadPerson()

            End Select

        End If
    End Sub

    Private Sub grdData_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdData.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then

            'e.Row.Cells(0).Text = e.Row.RowIndex + 1
            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(5).FindControl("imgDel")
            imgD.Attributes.Add("onClick", scriptString)

        End If
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#ffdfef';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If

    End Sub

    Private Sub EditData(ByVal pID As String)
        ClearData()

        dt = ctlStd.Person_GetByID(pID)

        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                isAdd = False
                lblID.Text = .Item("PersonID")

                If String.Concat(.Item("PrefixID")) <> "" Then
                    Me.ddlPrefix.SelectedValue = String.Concat(.Item("PrefixID"))
                End If

                txtMail.Text = String.Concat(.Item("Email"))
                Me.txtFirstName.Text = String.Concat(.Item("FirstName"))
                txtLastName.Text = String.Concat(.Item("LastName"))
                txtPositionName.Text = String.Concat(.Item("PositionName"))
                ddlLocation.SelectedValue = .Item("LocationID")
                lblMode.Text = "Mode : Edit"

            End With

        End If

        dt = Nothing
    End Sub

    Protected Sub cmdFind_Click(sender As Object, e As EventArgs) Handles cmdFind.Click
        grdData.PageIndex = 0
        LoadPerson()
    End Sub

    Protected Sub lnkFindLocation_Click(sender As Object, e As EventArgs) Handles lnkFindLocation.Click
        LoadLocationToDDL()
        grdData.PageIndex = 0
        LoadPersonByLocation(StrNull2Zero(ddlLocation.SelectedValue))
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocation.SelectedIndexChanged
        grdData.PageIndex = 0
        LoadPersonByLocation(StrNull2Zero(ddlLocation.SelectedValue))
    End Sub

    Protected Sub cmdAll_Click(sender As Object, e As EventArgs) Handles cmdAll.Click
        txtSearch.Text = ""
        grdData.PageIndex = 0
        LoadPersonByLocation(0)
    End Sub
End Class