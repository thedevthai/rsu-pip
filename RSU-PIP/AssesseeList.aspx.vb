﻿Public Class AssesseeList
    Inherits System.Web.UI.Page

    Dim ctlLG As New AssessmentController
    Dim ctlCs As New Coursecontroller
    Dim dt As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("UserLogin").Value) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If
        If Not SystemOnlineTime() Then
            Response.Redirect("ResultPage.aspx?t=closed&p=assm")
        End If

        If Not IsPostBack Then
            lblYear.Text = Request.Cookies("ASSMYEAR").Value
            LoadCourseToDDL()

            If Not Session("courseassessment") = Nothing Then
                ddlCourse.SelectedValue = Session("courseassessment")
            End If

            If Session("assessorroleid") = "P" Then
                lblFor.Text = "สำหรับอาจารย์แหล่งฝึก"
            Else
                lblFor.Text = "สำหรับอาจารย์ประจำรายวิชา"
            End If

            LoadAssesseeListToGrid()
        End If

    End Sub
    Private Function SystemOnlineTime() As Boolean
        Dim ctlCfg As New SystemConfigController

        Dim Bdate As Integer = StrNull2Zero(ConvertStrDate2DBString(ctlCfg.SystemConfig_GetByCode(CFG_STARTASSM)))
        Dim Edate As Integer = StrNull2Zero(ConvertStrDate2DBString(ctlCfg.SystemConfig_GetByCode(CFG_ENDASSM)))
        Dim sToday As Integer = StrNull2Zero(ConvertDate2DBString(ctlCfg.GET_DATE_SERVER))

        Dim Edate2 As Integer = sToday

        If Request.Cookies("ROLE_PCT").Value = True Then
            Edate2 = StrNull2Zero(ConvertStrDate2DBString(ctlCfg.SystemConfig_GetByCode(CFG_ENDASSM_LOC)))
            If Edate2 > Edate Then
                Edate = Edate2
            End If
        End If


        Dim bAvailable As Boolean
        If sToday < Bdate Then
            bAvailable = False
        ElseIf sToday > Edate Then
            bAvailable = False
        Else
            bAvailable = True
        End If
        Return bAvailable
    End Function


    Private Sub LoadCourseToDDL()
        ddlCourse.Items.Clear()
        Dim LocationID As Integer

        If Session("assessorroleid") = "P" Then
            LocationID = Session("locationassessment")
            dt = ctlCs.Courses_GetByLocation(StrNull2Zero(lblYear.Text), LocationID)
        Else
            If Session("assessorroleid") = "A" Then
                dt = ctlCs.Courses_GetByCoordinator(lblYear.Text, DBNull2Zero(Request.Cookies("ProfileID").Value))
            Else
                Exit Sub
            End If
        End If

        If dt.Rows.Count > 0 Then
            ddlCourse.Items.Clear()
            For i = 0 To dt.Rows.Count - 1
                With ddlCourse
                    .Items.Add("" & dt.Rows(i)("SubjectCode") & " : " & dt.Rows(i)("SubjectName"))
                    .Items(i).Value = dt.Rows(i)("CourseID")
                End With
            Next

            If Not Request("sj") Is Nothing Then
                ddlCourse.SelectedValue = ctlCs.Courses_GetCourseIDBySubjectCode(Request("sj"))
            End If
        End If

    End Sub

    Private Sub LoadAssesseeListToGrid(Optional pStatus As String = "")
        If Session("assessorroleid") = "A" Then
            dt = ctlLG.Assessee_GetByCoordinator(StrNull2Zero(lblYear.Text), DBNull2Zero(Request.Cookies("ProfileID").Value), StrNull2Zero(ddlCourse.SelectedValue), txtSearch.Text, pStatus)
        ElseIf Session("assessorroleid") = "P" Then
            dt = ctlLG.Assessee_GetByPrecepter(StrNull2Zero(lblYear.Text), DBNull2Zero(Request.Cookies("ProfileID").Value), StrNull2Zero(ddlCourse.SelectedValue), txtSearch.Text, pStatus, StrNull2Zero(Session("locationassessment")))
        End If

        With grdData
            .Visible = True
            .DataSource = dt
            .DataBind()
        End With
        dt = Nothing
    End Sub

    Private Sub grdData_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdData.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit"
                    Dim strS() As String
                    strS = Split(ddlCourse.SelectedItem.Text, ":")

                    Response.Redirect("AssesseeEvaluationGroup.aspx?p=a&std=" & e.CommandArgument() & "&sj=" & RTrim(strS(0)))
            End Select
        End If
    End Sub
    Private Sub grdData_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdData.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#ffdfef';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")
        End If
    End Sub

    Protected Sub cmdFind_Click(sender As Object, e As EventArgs) Handles cmdFind.Click
        LoadAssesseeListToGrid()
    End Sub
    Protected Sub cmdAll_Click(sender As Object, e As EventArgs) Handles cmdAll.Click
        txtSearch.Text = ""
        LoadAssesseeListToGrid()
    End Sub

    Protected Sub cmdNoScore_Click(sender As Object, e As EventArgs) Handles cmdNoScore.Click
        LoadAssesseeListToGrid("N")
    End Sub

    Protected Sub ddlCourse_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCourse.SelectedIndexChanged
        LoadAssesseeListToGrid()
    End Sub
End Class

