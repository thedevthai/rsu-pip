﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Homes
    
    '''<summary>
    '''lblMember1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMember1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblMember2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMember2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblMember3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMember3 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblMember4 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMember4 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblMember5 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMember5 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblMember6 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMember6 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblMember7 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMember7 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblMember8 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMember8 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''grdData control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents grdData As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''HyperLink2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents HyperLink2 As Global.System.Web.UI.WebControls.HyperLink
    
    '''<summary>
    '''grdMedia4a control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents grdMedia4a As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''grdMedia4b control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents grdMedia4b As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''grdMedia4c control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents grdMedia4c As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''grdMedia6a control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents grdMedia6a As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''grdMedia6b control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents grdMedia6b As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''grdMedia6g control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents grdMedia6g As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''grdMedia6c control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents grdMedia6c As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''grdMedia6d control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents grdMedia6d As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''grdMedia6e control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents grdMedia6e As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''grdMedia6f control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents grdMedia6f As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''dtlMember control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dtlMember As Global.System.Web.UI.WebControls.DataList
    
    '''<summary>
    '''lblOnline control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblOnline As Global.System.Web.UI.WebControls.Label
End Class
