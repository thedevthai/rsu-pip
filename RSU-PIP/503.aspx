﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="503.aspx.vb" Inherits="._503" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<section class="content">  

     <div class="error-page">
        <h2 class="headline text-red"><i class="fa fa-warning text-red"></i></h2>

        <div class="error-content">
          <h3 class="text-red"> แจ้งปิดปรับปรุงระบบชั่วคราว</h3>

          <p>
            เรียน ผู้ใช้งานทุกท่าน 
          </p>
            <p>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ขณะนี้ทีมพัฒนาระบบ <span Class="text-red text-bold">กำลังทำการปรับปรุง/แก้ไขปัญหา</span> ในหน้านี้
                <br />จึงมีความจำเป็นต้องปิดบริการชั่วคราว  

 

            <div class="text-center"><asp:Label ID="lblRemark" runat="server" CssClass="text-red text-bold"></asp:Label></div>
                
</p>
            
              

               <span class="pull-right">ขออภัยในความไม่สะดวก<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ทีมพัฒนาระบบ</span>
           
               </p>

          
        </div>
        <!-- /.error-content -->
      </div>
</section>
</asp:Content>
