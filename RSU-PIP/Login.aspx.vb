﻿Imports System.Net.Mail
Imports Newtonsoft.Json
Imports System.Web
Public Class Login
    Inherits System.Web.UI.Page

    Dim dt As New DataTable
    Dim acc As New UserController
    Dim enc As New CryptographyEngine
    Dim ctlNews As New NewsController
    Dim ctlM As New MediaController
    'Public dtS As New DataTable
    Dim ctlE As New EventController
    Dim dtCalendar As New DataTable
    Public Shared json As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim jsString As String = "if ((event.which && event.which == 13) || (event.keyCode && event.keyCode == 13)) {document.forms[0].all['" + cmdLogin.ClientID + "'].click();return false;} else return true; "
        txtPassword.Attributes.Add("onkeydown", jsString)
        If Not Request("logout") Is Nothing Then
            If Request("logout").ToLower() = "yes" Or Request("logout").ToLower() = "y" Then  '
                'Session.Contents.RemoveAll()
                'Session.Contents.RemoveAll()
                'Session.Abandon()
                'Session.RemoveAll()
                Response.Cookies.Clear()
                Request.Cookies.Clear()

                'Dim delCookie1 As HttpCookie
                'delCookie1 = New HttpCookie("rsu")
                'delCookie1.Expires = DateTime.Now.AddDays(-1D)
                'Response.Cookies.Add(delCookie1)
            End If
        End If

        If Not IsPostBack Then
            lblVersion.Text = "1.0.0"
            'If Request("logout") = "YES" And (Not Request("logout") Is Nothing) Then
            '    Session.Abandon()

            '    'Request.Cookies("UserLogin").Value = Nothing
            'End If
            LoadNewsToGrid()

            LoadMedia_4a()
            LoadMedia_4b()
            LoadMedia_4c()

            LoadMedia_6a()
            LoadMedia_6b()
            LoadMedia_6c()
            LoadMedia_6d()
            LoadMedia_6e()
            LoadMedia_6f()
            LoadMedia_6g()

            LoadEventList()
            'SendAlertPreceptor()
            LoadUserOnline()

            txtUserName.Focus()
        End If
        LoadCalendarData()

        btnOK.OnClientClick = String.Format("fnClickOK('{0}','{1}')", btnOK.UniqueID, "")
    End Sub
    Private Sub LoadCalendarData()
        dtCalendar = ctlE.Calendar_Get()
        json = JsonConvert.SerializeObject(dtCalendar, Formatting.Indented)
    End Sub
    Private Sub SendAlertPreceptor()
        dt = acc.SendAlert_Get
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                acc.SendAlert_Save(dt.Rows(i)("LocationID"), dt.Rows(i)("TimePhaseID"), dt.Rows(i)("Email"), "N")
            Next
        End If

        dt = acc.SendAlert_GetMail
        For i = 0 To dt.Rows.Count - 1
            With dt.Rows(i)
                SendMailAlert(.Item("TimePhaseID"), .Item("LocationID"), .Item("LocationName"), .Item("StartDate"), .Item("Email"))
            End With

        Next

    End Sub

    Private Sub SendMailAlert(TimePhaseID As Integer, LocationID As Integer, LocationName As String, StartDate As String, ByVal sTo As String)
        Try

            Dim MySubject As String = "ขอส่งตัวนักศึกษาเข้าฝึกปฏิบัติวิชาชีพ"
            Dim MyMessageBody As String = ""

            MyMessageBody = "<p>เรียน อาจารย์แหล่งฝึก " & LocationName & "<br />    ศูนย์ฝึกปฏิบัติงานวิชาชีพ วิทยาลัยเภสัชศาสตร์ มหาวิทยาลัยรังสิต ขอส่งนักศึกษาวิทยาลัยเภสัชศาสตร์ เข้าฝึกปฏิบัติวิชาชีพทางเภสัชศาสตร์ ในหน่วยงานของท่าน ในวันที่ " & StartDate & " <br />"

            MyMessageBody &= "  รายละเอียดเพิ่มเติมสามารถดูได้ที่เว็บไซต์ระบบฝึกปฏิบัติวิชาชีพ www.rsupip.com <br/> "
            MyMessageBody &= "  หากมีปัญหาการใช้งานหรือข้อสงสัยใดๆ ติดต่อแจ้งเรื่องได้ที่ rsupip@gmail.com หรือ 02-218-8450-1 </p> <br />"
            MyMessageBody &= "<p><b>วิทยาลัยเภสัชศาสตร์ มหาวิทยาลัยรังสิต</b> <br />52/347 หมู่ 7 ต.หลักหก อ.เมืองปทุมธานี จ.ปทุมธานี 12000  <br /> โทร/โทรสาร 0-2997 2200-30 ต่อ 1423 เบอร์มือถือ 092-2548960 </p>"

            Dim RecipientEmail As String = sTo
            Dim SenderEmail As String = "rsupip@gmail.com"
            Dim SenderDisplayName As String = "หน่วยฝึกปฏิบัติงานวิชาชีพ วิทยาลัยเภสัชศาสตร์ มหาวิทยาลัยรังสิต"
            Dim SenderEmailPassword As String = "r$up1p"

            Dim HOST = "smtp.gmail.com"
            Dim PORT = "587" 'TLS Port

            Dim mail As New MailMessage
            mail.Subject = MySubject
            mail.Body = MyMessageBody

            mail.IsBodyHtml = True
            mail.To.Add(RecipientEmail)
            mail.From = New MailAddress(SenderEmail, SenderDisplayName)

            Dim SMTP As New SmtpClient(HOST)
            SMTP.EnableSsl = True
            SMTP.Credentials = New System.Net.NetworkCredential(SenderEmail.Trim(), SenderEmailPassword.Trim())
            SMTP.DeliveryMethod = SmtpDeliveryMethod.Network
            SMTP.Port = PORT
            SMTP.Send(mail)
            'MsgBox("Sent Message To : " & RecipientEmail, MsgBoxStyle.Information, "Sent!")

            acc.SendAlert_UpdateStatus(LocationID, TimePhaseID, "Y")
        Catch ex As Exception
            DisplayMessage(Me.Page, ex.Message)

        End Try
        '(4) Send the MailMessage (will use the Web.config settings)

    End Sub


    Private Sub LoadNewsToGrid()

        dt = ctlNews.News_GetFirstPage

        With grdData
            .Visible = True
            .DataSource = dt
            .DataBind()

            For i = 0 To .Rows.Count - 1
                Dim img1 As Image = .Rows(i).Cells(0).FindControl("imgNews")

                Dim hlnkN As HyperLink = .Rows(i).Cells(0).FindControl("hlnkNews")
                hlnkN.Text = dt.Rows(i)("Title")

                Select Case dt.Rows(i)("NewsType")
                    Case "URL"
                        img1.ImageUrl = "images/www.png"
                        hlnkN.NavigateUrl = String.Concat(dt.Rows(i)("LinkPath"))
                    Case "UPL"
                        Dim str As String()
                        str = Split(Right(String.Concat(dt.Rows(i)("LinkPath")), 6), ".")

                        Select Case str(1)
                            Case "pdf"
                                img1.ImageUrl = "images/pdf.png"
                            Case "doc", "docx"
                                img1.ImageUrl = "images/word.png"
                            Case "ppt", "pptx"
                                img1.ImageUrl = "images/ppt.png"
                            Case "xls", "xlsx"
                                img1.ImageUrl = "images/xls.png"
                            Case Else
                                img1.ImageUrl = "images/pdf.png"
                        End Select

                        hlnkN.NavigateUrl = tmpUpload & "/" & String.Concat(dt.Rows(i)("LinkPath"))

                    Case "CON"
                        img1.ImageUrl = "images/newspaper.jpg"
                        hlnkN.NavigateUrl = "News.aspx?id=" & .DataKeys(i).Value
                    Case Else
                        img1.ImageUrl = "images/newspaper.jpg"
                        hlnkN.NavigateUrl = "News.aspx?id=" & .DataKeys(i).Value
                End Select

                'Dim img2 As Image = .Rows(i).Cells(0).FindControl("imgNew")
                'img2.Visible = False
                'If DateDiff(DateInterval.Day, Today.Date, dt.Rows(i)("NewsDate")) <= 5 Then
                '    img2.Visible = True
                'End If
                '.Rows(i).Cells(2).Text = "Update เมื่อ" & DisplayDateTH(dt.Rows(i)("NewsDate"))
            Next

        End With

    End Sub
    Private Sub LoadMedia_4a()

        dt = ctlM.Media_Get(4, 0, 1, "Y")

        With grdMedia4a
            .Visible = True
            .DataSource = dt
            .DataBind()

            For i = 0 To .Rows.Count - 1
                Dim img1 As Image = .Rows(i).Cells(0).FindControl("imgNews")

                Dim hlnkN As HyperLink = .Rows(i).Cells(0).FindControl("hlnkMedia4a")
                hlnkN.Text = dt.Rows(i)("Title")

                Select Case dt.Rows(i)("MediaType")
                    Case "URL"
                        img1.ImageUrl = "images/www.png"
                        hlnkN.NavigateUrl = String.Concat(dt.Rows(i)("LinkPath"))
                    Case "UPL"
                        Dim str As String()
                        str = Split(Right(String.Concat(dt.Rows(i)("LinkPath")), 6), ".")

                        Select Case str(1)
                            Case "pdf"
                                img1.ImageUrl = "images/pdf_download.png"
                            Case "doc", "docx"
                                img1.ImageUrl = "images/word.png"
                            Case "ppt", "pptx"
                                img1.ImageUrl = "images/ppt.png"
                            Case "xls", "xlsx"
                                img1.ImageUrl = "images/xls.png"
                            Case Else
                                img1.ImageUrl = "images/pdf.png"
                        End Select

                        hlnkN.NavigateUrl = DocumentUpload & "/4/" & String.Concat(dt.Rows(i)("LinkPath"))

                    Case "CON"
                        img1.ImageUrl = "images/newspaper.jpg"
                        hlnkN.NavigateUrl = "News.aspx?id=" & .DataKeys(i).Value
                    Case Else
                        img1.ImageUrl = "images/newspaper.jpg"
                        hlnkN.NavigateUrl = "News.aspx?id=" & .DataKeys(i).Value
                End Select

                'Dim img2 As Image = .Rows(i).Cells(0).FindControl("imgNew")
                'img2.Visible = False
                'If DateDiff(DateInterval.Day, Today.Date, dt.Rows(i)("NewsDate")) <= 5 Then
                '    img2.Visible = True
                'End If
                '.Rows(i).Cells(2).Text = "Update เมื่อ" & DisplayDateTH(dt.Rows(i)("NewsDate"))
            Next

        End With

    End Sub
    Private Sub LoadMedia_4b()

        dt = ctlM.Media_Get(4, 0, 2, "Y")

        With grdMedia4b
            .Visible = True
            .DataSource = dt
            .DataBind()

            For i = 0 To .Rows.Count - 1
                Dim img1 As Image = .Rows(i).Cells(0).FindControl("imgNews")

                Dim hlnkN As HyperLink = .Rows(i).Cells(0).FindControl("hlnkMedia4b")
                hlnkN.Text = dt.Rows(i)("Title")

                Select Case dt.Rows(i)("MediaType")
                    Case "URL"
                        img1.ImageUrl = "images/www.png"
                        hlnkN.NavigateUrl = String.Concat(dt.Rows(i)("LinkPath"))
                    Case "UPL"
                        Dim str As String()
                        str = Split(Right(String.Concat(dt.Rows(i)("LinkPath")), 6), ".")

                        Select Case str(1)
                            Case "pdf"
                                img1.ImageUrl = "images/pdf_download.png"
                            Case "doc", "docx"
                                img1.ImageUrl = "images/word.png"
                            Case "ppt", "pptx"
                                img1.ImageUrl = "images/ppt.png"
                            Case "xls", "xlsx"
                                img1.ImageUrl = "images/xls.png"
                            Case Else
                                img1.ImageUrl = "images/pdf.png"
                        End Select

                        hlnkN.NavigateUrl = DocumentUpload & "/4/" & String.Concat(dt.Rows(i)("LinkPath"))

                    Case "CON"
                        img1.ImageUrl = "images/newspaper.jpg"
                        hlnkN.NavigateUrl = "News.aspx?id=" & .DataKeys(i).Value
                    Case Else
                        img1.ImageUrl = "images/newspaper.jpg"
                        hlnkN.NavigateUrl = "News.aspx?id=" & .DataKeys(i).Value
                End Select

                'Dim img2 As Image = .Rows(i).Cells(0).FindControl("imgNew")
                'img2.Visible = False
                'If DateDiff(DateInterval.Day, Today.Date, dt.Rows(i)("NewsDate")) <= 5 Then
                '    img2.Visible = True
                'End If
                '.Rows(i).Cells(2).Text = "Update เมื่อ" & DisplayDateTH(dt.Rows(i)("NewsDate"))
            Next

        End With

    End Sub
    Private Sub LoadMedia_4c()

        dt = ctlM.Media_Get(4, 0, 3, "Y")

        With grdMedia4c
            .Visible = True
            .DataSource = dt
            .DataBind()

            For i = 0 To .Rows.Count - 1
                Dim img1 As Image = .Rows(i).Cells(0).FindControl("imgNews")

                Dim hlnkN As HyperLink = .Rows(i).Cells(0).FindControl("hlnkMedia4c")
                hlnkN.Text = dt.Rows(i)("Title")

                Select Case dt.Rows(i)("MediaType")
                    Case "URL"
                        img1.ImageUrl = "images/www.png"
                        hlnkN.NavigateUrl = String.Concat(dt.Rows(i)("LinkPath"))
                    Case "UPL"
                        Dim str As String()
                        str = Split(Right(String.Concat(dt.Rows(i)("LinkPath")), 6), ".")

                        Select Case str(1)
                            Case "pdf"
                                img1.ImageUrl = "images/pdf_download.png"
                            Case "doc", "docx"
                                img1.ImageUrl = "images/word.png"
                            Case "ppt", "pptx"
                                img1.ImageUrl = "images/ppt.png"
                            Case "xls", "xlsx"
                                img1.ImageUrl = "images/xls.png"
                            Case Else
                                img1.ImageUrl = "images/pdf.png"
                        End Select

                        hlnkN.NavigateUrl = DocumentUpload & "/4/" & String.Concat(dt.Rows(i)("LinkPath"))

                    Case "CON"
                        img1.ImageUrl = "images/newspaper.jpg"
                        hlnkN.NavigateUrl = "News.aspx?id=" & .DataKeys(i).Value
                    Case Else
                        img1.ImageUrl = "images/newspaper.jpg"
                        hlnkN.NavigateUrl = "News.aspx?id=" & .DataKeys(i).Value
                End Select

                'Dim img2 As Image = .Rows(i).Cells(0).FindControl("imgNew")
                'img2.Visible = False
                'If DateDiff(DateInterval.Day, Today.Date, dt.Rows(i)("NewsDate")) <= 5 Then
                '    img2.Visible = True
                'End If
                '.Rows(i).Cells(2).Text = "Update เมื่อ" & DisplayDateTH(dt.Rows(i)("NewsDate"))
            Next

        End With

    End Sub

    Private Sub LoadMedia_6a()

        dt = ctlM.Media_Get(6, 2, 1, "Y")

        With grdMedia6a
            .Visible = True
            .DataSource = dt
            .DataBind()

            For i = 0 To .Rows.Count - 1
                Dim img1 As Image = .Rows(i).Cells(0).FindControl("imgNews")

                Dim hlnkN As HyperLink = .Rows(i).Cells(0).FindControl("hlnkMedia6a")
                hlnkN.Text = dt.Rows(i)("Title")

                Select Case dt.Rows(i)("MediaType")
                    Case "URL"
                        img1.ImageUrl = "images/www.png"
                        hlnkN.NavigateUrl = String.Concat(dt.Rows(i)("LinkPath"))
                    Case "UPL"
                        Dim str As String()
                        str = Split(Right(String.Concat(dt.Rows(i)("LinkPath")), 6), ".")

                        Select Case str(1)
                            Case "pdf"
                                img1.ImageUrl = "images/pdf_download.png"
                            Case "doc", "docx"
                                img1.ImageUrl = "images/word.png"
                            Case "ppt", "pptx"
                                img1.ImageUrl = "images/ppt.png"
                            Case "xls", "xlsx"
                                img1.ImageUrl = "images/xls.png"
                            Case Else
                                img1.ImageUrl = "images/pdf.png"
                        End Select

                        hlnkN.NavigateUrl = DocumentUpload & "/6/" & String.Concat(dt.Rows(i)("LinkPath"))

                    Case "CON"
                        img1.ImageUrl = "images/newspaper.jpg"
                        hlnkN.NavigateUrl = "News.aspx?id=" & .DataKeys(i).Value
                    Case Else
                        img1.ImageUrl = "images/newspaper.jpg"
                        hlnkN.NavigateUrl = "News.aspx?id=" & .DataKeys(i).Value
                End Select

                'Dim img2 As Image = .Rows(i).Cells(0).FindControl("imgNew")
                'img2.Visible = False
                'If DateDiff(DateInterval.Day, Today.Date, dt.Rows(i)("NewsDate")) <= 5 Then
                '    img2.Visible = True
                'End If
                '.Rows(i).Cells(2).Text = "Update เมื่อ" & DisplayDateTH(dt.Rows(i)("NewsDate"))
            Next

        End With

    End Sub
    Private Sub LoadMedia_6b()

        dt = ctlM.Media_Get(6, 2, 2, "Y")

        With grdMedia6b
            .Visible = True
            .DataSource = dt
            .DataBind()

            For i = 0 To .Rows.Count - 1
                Dim img1 As Image = .Rows(i).Cells(0).FindControl("imgNews")

                Dim hlnkN As HyperLink = .Rows(i).Cells(0).FindControl("hlnkMedia6b")
                hlnkN.Text = dt.Rows(i)("Title")

                Select Case dt.Rows(i)("MediaType")
                    Case "URL"
                        img1.ImageUrl = "images/www.png"
                        hlnkN.NavigateUrl = String.Concat(dt.Rows(i)("LinkPath"))
                    Case "UPL"
                        Dim str As String()
                        str = Split(Right(String.Concat(dt.Rows(i)("LinkPath")), 6), ".")

                        Select Case str(1)
                            Case "pdf"
                                img1.ImageUrl = "images/pdf_download.png"
                            Case "doc", "docx"
                                img1.ImageUrl = "images/word.png"
                            Case "ppt", "pptx"
                                img1.ImageUrl = "images/ppt.png"
                            Case "xls", "xlsx"
                                img1.ImageUrl = "images/xls.png"
                            Case Else
                                img1.ImageUrl = "images/pdf.png"
                        End Select

                        hlnkN.NavigateUrl = DocumentUpload & "/6/" & String.Concat(dt.Rows(i)("LinkPath"))

                    Case "CON"
                        img1.ImageUrl = "images/newspaper.jpg"
                        hlnkN.NavigateUrl = "News.aspx?id=" & .DataKeys(i).Value
                    Case Else
                        img1.ImageUrl = "images/newspaper.jpg"
                        hlnkN.NavigateUrl = "News.aspx?id=" & .DataKeys(i).Value
                End Select

                'Dim img2 As Image = .Rows(i).Cells(0).FindControl("imgNew")
                'img2.Visible = False
                'If DateDiff(DateInterval.Day, Today.Date, dt.Rows(i)("NewsDate")) <= 5 Then
                '    img2.Visible = True
                'End If
                '.Rows(i).Cells(2).Text = "Update เมื่อ" & DisplayDateTH(dt.Rows(i)("NewsDate"))
            Next

        End With

    End Sub

    Private Sub LoadMedia_6c()

        dt = ctlM.Media_Get(6, 2, 3, "Y")

        With grdMedia6c
            .Visible = True
            .DataSource = dt
            .DataBind()

            For i = 0 To .Rows.Count - 1
                Dim img1 As Image = .Rows(i).Cells(0).FindControl("imgNews")

                Dim hlnkN As HyperLink = .Rows(i).Cells(0).FindControl("hlnkMedia6c")
                hlnkN.Text = dt.Rows(i)("Title")

                Select Case dt.Rows(i)("MediaType")
                    Case "URL"
                        img1.ImageUrl = "images/www.png"
                        hlnkN.NavigateUrl = String.Concat(dt.Rows(i)("LinkPath"))
                    Case "UPL"
                        Dim str As String()
                        str = Split(Right(String.Concat(dt.Rows(i)("LinkPath")), 6), ".")

                        Select Case str(1)
                            Case "pdf"
                                img1.ImageUrl = "images/pdf_download.png"
                            Case "doc", "docx"
                                img1.ImageUrl = "images/word.png"
                            Case "ppt", "pptx"
                                img1.ImageUrl = "images/ppt.png"
                            Case "xls", "xlsx"
                                img1.ImageUrl = "images/xls.png"
                            Case Else
                                img1.ImageUrl = "images/pdf.png"
                        End Select

                        hlnkN.NavigateUrl = DocumentUpload & "/6/" & String.Concat(dt.Rows(i)("LinkPath"))

                    Case "CON"
                        img1.ImageUrl = "images/newspaper.jpg"
                        hlnkN.NavigateUrl = "News.aspx?id=" & .DataKeys(i).Value
                    Case Else
                        img1.ImageUrl = "images/newspaper.jpg"
                        hlnkN.NavigateUrl = "News.aspx?id=" & .DataKeys(i).Value
                End Select

                'Dim img2 As Image = .Rows(i).Cells(0).FindControl("imgNew")
                'img2.Visible = False
                'If DateDiff(DateInterval.Day, Today.Date, dt.Rows(i)("NewsDate")) <= 5 Then
                '    img2.Visible = True
                'End If
                '.Rows(i).Cells(2).Text = "Update เมื่อ" & DisplayDateTH(dt.Rows(i)("NewsDate"))
            Next

        End With

    End Sub
    Private Sub LoadMedia_6d()

        dt = ctlM.Media_Get(6, 1, 1, "Y")

        With grdMedia6d
            .Visible = True
            .DataSource = dt
            .DataBind()

            For i = 0 To .Rows.Count - 1
                Dim img1 As Image = .Rows(i).Cells(0).FindControl("imgNews")

                Dim hlnkN As HyperLink = .Rows(i).Cells(0).FindControl("hlnkMedia6d")
                hlnkN.Text = dt.Rows(i)("Title")

                Select Case dt.Rows(i)("MediaType")
                    Case "URL"
                        img1.ImageUrl = "images/www.png"
                        hlnkN.NavigateUrl = String.Concat(dt.Rows(i)("LinkPath"))
                    Case "UPL"
                        Dim str As String()
                        str = Split(Right(String.Concat(dt.Rows(i)("LinkPath")), 6), ".")

                        Select Case str(1)
                            Case "pdf"
                                img1.ImageUrl = "images/pdf_download.png"
                            Case "doc", "docx"
                                img1.ImageUrl = "images/word.png"
                            Case "ppt", "pptx"
                                img1.ImageUrl = "images/ppt.png"
                            Case "xls", "xlsx"
                                img1.ImageUrl = "images/xls.png"
                            Case Else
                                img1.ImageUrl = "images/pdf.png"
                        End Select

                        hlnkN.NavigateUrl = DocumentUpload & "/6/" & String.Concat(dt.Rows(i)("LinkPath"))

                    Case "CON"
                        img1.ImageUrl = "images/newspaper.jpg"
                        hlnkN.NavigateUrl = "News.aspx?id=" & .DataKeys(i).Value
                    Case Else
                        img1.ImageUrl = "images/newspaper.jpg"
                        hlnkN.NavigateUrl = "News.aspx?id=" & .DataKeys(i).Value
                End Select

                'Dim img2 As Image = .Rows(i).Cells(0).FindControl("imgNew")
                'img2.Visible = False
                'If DateDiff(DateInterval.Day, Today.Date, dt.Rows(i)("NewsDate")) <= 5 Then
                '    img2.Visible = True
                'End If
                '.Rows(i).Cells(2).Text = "Update เมื่อ" & DisplayDateTH(dt.Rows(i)("NewsDate"))
            Next

        End With

    End Sub
    Private Sub LoadMedia_6e()

        dt = ctlM.Media_Get(6, 1, 2, "Y")

        With grdMedia6e
            .Visible = True
            .DataSource = dt
            .DataBind()

            For i = 0 To .Rows.Count - 1
                Dim img1 As Image = .Rows(i).Cells(0).FindControl("imgNews")

                Dim hlnkN As HyperLink = .Rows(i).Cells(0).FindControl("hlnkMedia6e")
                hlnkN.Text = dt.Rows(i)("Title")

                Select Case dt.Rows(i)("MediaType")
                    Case "URL"
                        img1.ImageUrl = "images/www.png"
                        hlnkN.NavigateUrl = String.Concat(dt.Rows(i)("LinkPath"))
                    Case "UPL"
                        Dim str As String()
                        str = Split(Right(String.Concat(dt.Rows(i)("LinkPath")), 6), ".")

                        Select Case str(1)
                            Case "pdf"
                                img1.ImageUrl = "images/pdf_download.png"
                            Case "doc", "docx"
                                img1.ImageUrl = "images/word.png"
                            Case "ppt", "pptx"
                                img1.ImageUrl = "images/ppt.png"
                            Case "xls", "xlsx"
                                img1.ImageUrl = "images/xls.png"
                            Case Else
                                img1.ImageUrl = "images/pdf.png"
                        End Select

                        hlnkN.NavigateUrl = DocumentUpload & "/6/" & String.Concat(dt.Rows(i)("LinkPath"))

                    Case "CON"
                        img1.ImageUrl = "images/newspaper.jpg"
                        hlnkN.NavigateUrl = "News.aspx?id=" & .DataKeys(i).Value
                    Case Else
                        img1.ImageUrl = "images/newspaper.jpg"
                        hlnkN.NavigateUrl = "News.aspx?id=" & .DataKeys(i).Value
                End Select

                'Dim img2 As Image = .Rows(i).Cells(0).FindControl("imgNew")
                'img2.Visible = False
                'If DateDiff(DateInterval.Day, Today.Date, dt.Rows(i)("NewsDate")) <= 5 Then
                '    img2.Visible = True
                'End If
                '.Rows(i).Cells(2).Text = "Update เมื่อ" & DisplayDateTH(dt.Rows(i)("NewsDate"))
            Next

        End With

    End Sub
    Private Sub LoadMedia_6f()

        dt = ctlM.Media_Get(6, 1, 3, "Y")

        With grdMedia6f
            .Visible = True
            .DataSource = dt
            .DataBind()

            For i = 0 To .Rows.Count - 1
                Dim img1 As Image = .Rows(i).Cells(0).FindControl("imgNews")

                Dim hlnkN As HyperLink = .Rows(i).Cells(0).FindControl("hlnkMedia6f")
                hlnkN.Text = dt.Rows(i)("Title")

                Select Case dt.Rows(i)("MediaType")
                    Case "URL"
                        img1.ImageUrl = "images/www.png"
                        hlnkN.NavigateUrl = String.Concat(dt.Rows(i)("LinkPath"))
                    Case "UPL"
                        Dim str As String()
                        str = Split(Right(String.Concat(dt.Rows(i)("LinkPath")), 6), ".")

                        Select Case str(1)
                            Case "pdf"
                                img1.ImageUrl = "images/pdf_download.png"
                            Case "doc", "docx"
                                img1.ImageUrl = "images/word.png"
                            Case "ppt", "pptx"
                                img1.ImageUrl = "images/ppt.png"
                            Case "xls", "xlsx"
                                img1.ImageUrl = "images/xls.png"
                            Case Else
                                img1.ImageUrl = "images/pdf.png"
                        End Select

                        hlnkN.NavigateUrl = DocumentUpload & "/6/" & String.Concat(dt.Rows(i)("LinkPath"))

                    Case "CON"
                        img1.ImageUrl = "images/newspaper.jpg"
                        hlnkN.NavigateUrl = "News.aspx?id=" & .DataKeys(i).Value
                    Case Else
                        img1.ImageUrl = "images/newspaper.jpg"
                        hlnkN.NavigateUrl = "News.aspx?id=" & .DataKeys(i).Value
                End Select

                'Dim img2 As Image = .Rows(i).Cells(0).FindControl("imgNew")
                'img2.Visible = False
                'If DateDiff(DateInterval.Day, Today.Date, dt.Rows(i)("NewsDate")) <= 5 Then
                '    img2.Visible = True
                'End If
                '.Rows(i).Cells(2).Text = "Update เมื่อ" & DisplayDateTH(dt.Rows(i)("NewsDate"))
            Next

        End With

    End Sub


    Private Sub LoadMedia_6g()

        dt = ctlM.Media_Get(6, 2, 4, "Y")

        With grdMedia6g
            .Visible = True
            .DataSource = dt
            .DataBind()

            For i = 0 To .Rows.Count - 1
                Dim img1 As Image = .Rows(i).Cells(0).FindControl("imgNews")

                Dim hlnkN As HyperLink = .Rows(i).Cells(0).FindControl("hlnkMedia6g")
                hlnkN.Text = dt.Rows(i)("Title")

                Select Case dt.Rows(i)("MediaType")
                    Case "URL"
                        img1.ImageUrl = "images/www.png"
                        hlnkN.NavigateUrl = String.Concat(dt.Rows(i)("LinkPath"))
                    Case "UPL"
                        Dim str As String()
                        str = Split(Right(String.Concat(dt.Rows(i)("LinkPath")), 6), ".")

                        Select Case str(1)
                            Case "pdf"
                                img1.ImageUrl = "images/pdf_download.png"
                            Case "doc", "docx"
                                img1.ImageUrl = "images/word.png"
                            Case "ppt", "pptx"
                                img1.ImageUrl = "images/ppt.png"
                            Case "xls", "xlsx"
                                img1.ImageUrl = "images/xls.png"
                            Case Else
                                img1.ImageUrl = "images/pdf.png"
                        End Select

                        hlnkN.NavigateUrl = DocumentUpload & "/6/" & String.Concat(dt.Rows(i)("LinkPath"))

                    Case "CON"
                        img1.ImageUrl = "images/newspaper.jpg"
                        hlnkN.NavigateUrl = "News.aspx?id=" & .DataKeys(i).Value
                    Case Else
                        img1.ImageUrl = "images/newspaper.jpg"
                        hlnkN.NavigateUrl = "News.aspx?id=" & .DataKeys(i).Value
                End Select

                'Dim img2 As Image = .Rows(i).Cells(0).FindControl("imgNew")
                'img2.Visible = False
                'If DateDiff(DateInterval.Day, Today.Date, dt.Rows(i)("NewsDate")) <= 5 Then
                '    img2.Visible = True
                'End If
                '.Rows(i).Cells(2).Text = "Update เมื่อ" & DisplayDateTH(dt.Rows(i)("NewsDate"))
            Next

        End With

    End Sub
    Private Sub CheckUser()
        Dim ctlCfg As New SystemConfigController()

        dt = acc.User_CheckLogin(txtUserName.Text, enc.EncryptString(txtPassword.Text, True))

        If dt.Rows.Count > 0 Then
            If dt.Rows(0).Item("isPublic") = 0 Then
                lblAlert.Text = "ID ของท่านไม่สามารถใช้งานได้ชั่วคราว"
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ID ของท่านไม่สามารถใช้งานได้ชั่วคราว');", True)
                Exit Sub
            End If

            '--------------Start Set Cookie---------------------------------------------
            Response.Cookies.Clear()
            Request.Cookies.Clear()
            Dim ckLoginID As New HttpCookie("UserLoginID")
            Dim ckUserLogin As New HttpCookie("UserLogin")
            'Dim ckUserProfile As New HttpCookie("UserProfile")
            Dim ckPassword As New HttpCookie("Password")
            Dim ckNameOfUser As New HttpCookie("NameOfUser")
            'Dim ckLastLogin As New HttpCookie("LastLogin")
            Dim ckUserProfileID As New HttpCookie("UserProfileID")
            Dim ckProfileID As New HttpCookie("ProfileID")
            Dim ckLocationID As New HttpCookie("LocationID")
            Dim ckROLE_STD As New HttpCookie("ROLE_STD")
            Dim ckROLE_TCH As New HttpCookie("ROLE_TCH")
            Dim ckROLE_ADV As New HttpCookie("ROLE_ADV")
            Dim ckROLE_PCT As New HttpCookie("ROLE_PCT")
            Dim ckROLE_ADM As New HttpCookie("ROLE_ADM")
            Dim ckROLE_SPA As New HttpCookie("ROLE_SPA")
            Dim ckROLE_CO1 As New HttpCookie("ROLE_CO1")
            Dim ckEDUYEAR As New HttpCookie("EDUYEAR")
            Dim ckASSMYEAR As New HttpCookie("ASSMYEAR")
            'Dim ckAssessorUID As New HttpCookie("AssessorUID")
            'Dim ckLocationAssessment As New HttpCookie("LocationAssessment")
            'Dim ckAssessorRoleID As New HttpCookie("AssessorRoleID")
            'Dim ckCourseAssessment As New HttpCookie("CourseAssessment")


            ckLoginID.Value = dt.Rows(0).Item("UserID")
            ckUserLogin.Value = dt.Rows(0).Item("Username")
            'ckUserProfile.Value = dt.Rows(0).Item("Username")
            ckPassword.Value = enc.DecryptString(dt.Rows(0).Item("Password"), True)
            ckNameOfUser.Value = String.Concat(dt.Rows(0).Item("FirstName")) & " " & String.Concat(dt.Rows(0).Item("LastName"))
            'ckLastLogin.Value = String.Concat(dt.Rows(0).Item("LastLogin"))
            ckUserProfileID.Value = dt.Rows(0).Item("UserProfileID")
            ckProfileID.Value = dt.Rows(0).Item("ProfileID")
            ckLocationID.Value = String.Concat(dt.Rows(0).Item("LocationID"))

            ckROLE_STD.Value = False
            ckROLE_TCH.Value = False
            ckROLE_ADV.Value = False
            ckROLE_PCT.Value = False
            ckROLE_ADM.Value = False
            ckROLE_SPA.Value = False
            ckROLE_CO1.Value = False


            If ckUserProfileID.Value = 2 Then
                ckROLE_TCH.Value = True
            End If

            If ckUserProfileID.Value <> 1 Then
                Dim ctlC As New CourseController
                ckROLE_ADV.Value = ctlC.CoursesCoordinator_CheckStatus(ckProfileID.Value)
            End If

            'Dim ctlCfg As New SystemConfigController()
            ckEDUYEAR.Value = ctlCfg.SystemConfig_GetByCode(CFG_EDUYEAR)
            ckASSMYEAR.Value = ctlCfg.SystemConfig_GetByCode(CFG_ASSMYEAR)

            genLastLog()

            acc.User_GenLogfile(txtUserName.Text, ACTTYPE_LOG, "Users", "", "")

            Dim rd As New UserRoleController

            dt = rd.UserRole_GetActiveRoleByUID(ckLoginID.Value)
            If dt.Rows.Count > 0 Then

                For i = 0 To dt.Rows.Count - 1

                    Select Case dt.Rows(i)("RoleID")
                        Case 1
                            ckROLE_STD.Value = True
                        Case 2
                            ckROLE_TCH.Value = True
                            ckROLE_ADV.Value = True
                        Case 3
                            ckROLE_PCT.Value = True
                        Case 4
                            ckROLE_CO1.Value = True
                        Case 5
                            ckROLE_ADM.Value = True
                        Case 9
                            ckROLE_SPA.Value = True
                    End Select

                Next

            End If


            Response.Cookies.Add(ckLoginID)
            Response.Cookies.Add(ckUserLogin)
            'Response.Cookies.Add(ckUserProfile)
            Response.Cookies.Add(ckPassword)
            Response.Cookies.Add(ckNameOfUser)
            Response.Cookies.Add(ckUserProfileID)
            Response.Cookies.Add(ckProfileID)
            Response.Cookies.Add(ckLocationID)
            Response.Cookies.Add(ckROLE_STD)
            Response.Cookies.Add(ckROLE_TCH)
            Response.Cookies.Add(ckROLE_ADV)
            Response.Cookies.Add(ckROLE_PCT)
            Response.Cookies.Add(ckROLE_ADM)
            Response.Cookies.Add(ckROLE_SPA)
            Response.Cookies.Add(ckROLE_CO1)
            Response.Cookies.Add(ckEDUYEAR)
            Response.Cookies.Add(ckASSMYEAR)

            'Request.Cookies.Add(ckUsername)

            'Response.Cookies("UserLogin").Expires = DateTime.Now.AddDays(-1)
            'Response.Cookies("UserLoginID").Expires = DateTime.Now.AddDays(-1)
            '---------------------------------------------------------------

            'Dim rsuCookie As HttpCookie = New HttpCookie("rsu")

            'rsuCookie("UserLoginID") = dt.Rows(0).Item("UserID")
            'rsuCookie("UserLogin") = dt.Rows(0).Item("USERNAME")
            'rsuCookie("Password") = enc.DecryptString(dt.Rows(0).Item("PASSWORD"), True)
            'rsuCookie("LastLogin") = String.Concat(dt.Rows(0).Item("LastLogin"))
            'rsuCookie("NameOfUser") = String.Concat(dt.Rows(0).Item("FirstName")) & " " & String.Concat(dt.Rows(0).Item("LastName"))


            'rsuCookie("UserProfileID") = dt.Rows(0).Item("UserProfileID")
            'rsuCookie("ProfileID") = dt.Rows(0).Item("ProfileID")
            'rsuCookie("LocationID") = DBNull2Zero(dt.Rows(0).Item("LocationID"))

            'rsuCookie("ROLE_STD") = False
            'rsuCookie("ROLE_TCH") = False
            'rsuCookie("ROLE_ADV") = False
            'rsuCookie("ROLE_PCT") = False
            'rsuCookie("ROLE_ADM") = False
            'rsuCookie("ROLE_SPA") = False
            'rsuCookie("ROLE_CO1") = False

            'rsuCookie("EDUYEAR") = ctlCfg.SystemConfig_GetByCode(CFG_EDUYEAR)
            'rsuCookie("ASSMYEAR") = ctlCfg.SystemConfig_GetByCode(CFG_ASSMYEAR)

            'rsuCookie.Expires = acc.GET_DATE_SERVER().AddMinutes(60)
            'Response.Cookies.Add(rsuCookie)


            '------------------End set Cookie---------------------------------------------------------------------------------------------


            'If rsuCookie("UserProfileID") = 2 Then
            '    rsuCookie("ROLE_TCH") = True
            'End If

            'If rsuCookie("UserProfileID") <> 1 Then
            '    Dim ctlC As New CourseController
            '    rsuCookie("ROLE_ADV") = ctlC.CoursesCoordinator_CheckStatus(rsuCookie("ProfileID"))
            'End If

            'genLastLog()

            'acc.User_GenLogfile(txtUserName.Text, ACTTYPE_LOG, "Users", "", "")

            'Dim rd As New UserRoleController

            'dt = rd.UserRole_GetActiveRoleByUID(rsuCookie("UserLoginID"))
            'If dt.Rows.Count > 0 Then

            '    For i = 0 To dt.Rows.Count - 1

            '        Select Case dt.Rows(i)("RoleID")
            '            Case 1 'Student
            '                'Request.Cookies("ROLE_STD").Value = True
            '                rsuCookie("ROLE_STD") = True
            '            Case 2  'Faculty member
            '                'Request.Cookies("ROLE_TCH").Value = True
            '                'Request.Cookies("ROLE_ADV").Value = True
            '                rsuCookie("ROLE_TCH") = True
            '                rsuCookie("ROLE_ADV") = True
            '            Case 3 'Preceptor
            '                'Request.Cookies("ROLE_PCT").Value = True
            '                rsuCookie("ROLE_PCT") = True
            '            Case 4 'Admin แหล่งฝึกร้านยา
            '                'session("ROLE_CO-1") = True
            '                rsuCookie("ROLE_CO1") = True
            '            Case 5 'Administrator
            '                'Request.Cookies("ROLE_ADM").Value = True
            '                rsuCookie("ROLE_ADM") = True
            '            Case 9
            '                'Request.Cookies("ROLE_SPA").Value = True
            '                rsuCookie("ROLE_SPA") = True
            '        End Select
            '    Next

            'End If

            'If rsuCookie("UserProfileID") = 1 Then
            '    rsuCookie("ROLE_STD") = True
            'End If

            'Response.Cookies.Set(rsuCookie)
            'Request.Cookies.Set(rsuCookie)

            'Select Case Request.Cookies("UserProfileID").Value
            '    Case 1 'students
            '        Response.Redirect("Default.aspx")
            '    Case 2, 3 'Teacher
            '        Response.Redirect("Default.aspx")
            '    Case 4 'Admin
            Response.Redirect("Home.aspx?actionType=h")
            'End Select

        Else

            lblAlert.Text = "Username  หรือ Password ไม่ถูกต้อง"
            'ModalPopupExtender1.Show()

            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','Username  หรือ Password ไม่ถูกต้อง');", True)

            ' DisplayMessage(Me.Page, "Username  หรือ Password ไม่ถูกต้อง")
            Exit Sub
        End If
    End Sub
    Private Sub LoadUserOnline()

        lblUserOnlineCount.Text = Application("OnlineNow")
        Dim ctlU As New UserController
        dt = ctlU.Users_Online
        lblOnline.Text = "Last online: "
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1

                Select Case i Mod 5
                    Case 0
                        lblOnline.Text &= " <span class='label label-success'>" & dt.Rows(i)("Name") & "</span>"
                    Case 1
                        lblOnline.Text &= " <span class='label label-info'>" & dt.Rows(i)("Name") & "</span>"
                    Case 2
                        lblOnline.Text &= " <span class='label label-danger'>" & dt.Rows(i)("Name") & "</span>"
                    Case 3
                        lblOnline.Text &= " <span class='label label-primary'>" & dt.Rows(i)("Name") & "</span>"
                    Case Else
                        lblOnline.Text &= " <span class='label label-warning'>" & dt.Rows(i)("Name") & "</span>"
                End Select
                'If i Mod 2 = 0 Then
                '    lblOnline.Text &= " <span class='label label-success'>" & dt.Rows(i)("Name") & " : " & dt.Rows(i)("Name") & "</span>"
                '    'lblOnline.Text &= "<font color=#1878ca>" & "[" & dt.Rows(i)("Name") & " : " & dt.Rows(i)("Name") & "]" & "</font>"
                'Else
                '    lblOnline.Text &= " <span class='label label-warning'>" & dt.Rows(i)("Name") & " : " & dt.Rows(i)("Name") & "</span>"
                'End If

            Next

        Else
            lblOnline.Text = "ไม่มี User Online ในขณะนี้"
        End If

    End Sub

    Private Sub LoadEventList()
        dt = ctlE.Events_GetList

        With grdEvent
            .Visible = True
            .DataSource = dt
            .DataBind()

            For i = 0 To .Rows.Count - 1
                'Dim img1 As Image = .Rows(i).Cells(0).FindControl("imgNews")

                Dim hlnkD As HyperLink = .Rows(i).Cells(0).FindControl("hlnkEdate")
                Dim hlnkN As HyperLink = .Rows(i).Cells(0).FindControl("hlnkEname")
                hlnkN.Text = dt.Rows(i)("Title")
                hlnkD.Text = dt.Rows(i)("EventDate")

                If dt.Rows(i)("Details") <> "" Then
                    hlnkD.NavigateUrl = "EventDetail?eid=" & dt.Rows(i)("UID")
                    hlnkN.NavigateUrl = "EventDetail?eid=" & dt.Rows(i)("UID")
                End If


                'Select Case dt.Rows(i)("MediaType")
                '    Case "URL"
                '        img1.ImageUrl = "images/www.png"
                '        hlnkN.NavigateUrl = String.Concat(dt.Rows(i)("LinkPath"))
                '    Case "UPL"
                '        Dim str As String()
                '        str = Split(Right(String.Concat(dt.Rows(i)("LinkPath")), 6), ".")

                '        Select Case str(1)
                '            Case "pdf"
                '                img1.ImageUrl = "images/pdf_download.png"
                '            Case "doc", "docx"
                '                img1.ImageUrl = "images/word.png"
                '            Case "ppt", "pptx"
                '                img1.ImageUrl = "images/ppt.png"
                '            Case "xls", "xlsx"
                '                img1.ImageUrl = "images/xls.png"
                '            Case Else
                '                img1.ImageUrl = "images/pdf.png"
                '        End Select

                '        hlnkN.NavigateUrl = DocumentUpload & "/4/" & String.Concat(dt.Rows(i)("LinkPath"))

                '    Case "CON"
                '        img1.ImageUrl = "images/newspaper.jpg"
                '        hlnkN.NavigateUrl = "News.aspx?id=" & .DataKeys(i).Value
                '    Case Else
                '        img1.ImageUrl = "images/newspaper.jpg"
                '        hlnkN.NavigateUrl = "News.aspx?id=" & .DataKeys(i).Value
                'End Select

            Next

        End With

    End Sub

    Private Sub genLastLog() ' เก็บวันเวลาที่ User เข้าใช้ระบบครั้งล่าสุด
        acc.User_LastLog_Update(txtUserName.Text)
    End Sub

    Protected Sub cmdLogin_Click(sender As Object, e As EventArgs) Handles cmdLogin.Click

        'txtUserName.Text = "admin"
        'txtPassword.Text = "112233"

        If txtUserName.Text = "" Or txtPassword.Text = "" Then

            lblAlert.Text = "กรุณาป้อน Username  หรือ Password ให้ครบถ้วนก่อน"
            'ModalPopupExtender1.Show()

            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'Warning!','กรุณาป้อน Username  หรือ Password ให้ครบถ้วนก่อน');", True)



            ' DisplayMessage(Me.Page, "กรุณาป้อน Username  หรือ Password ให้ครบถ้วนก่อน ")
            txtUserName.Focus()
            Exit Sub
        End If

        Session.Contents.RemoveAll()
        CheckUser()

    End Sub

End Class
