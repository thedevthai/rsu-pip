﻿Public Class PsychologyAssessmentModify
    Inherits System.Web.UI.Page

    Dim ctlA As New AssessmentController
    Dim ctlStd As New StudentController
    Dim ctlM As New MasterController

    Dim dt As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("UserLogin").Value) Then
            Response.Redirect("Default.aspx")
        End If
        If Not IsPostBack Then
            'lblYear.Text = Request.Cookies("ASSMYEAR").Value

            LoadStudentInfo()
            LoadPsychology()
            'LoadPhaseInfo()
            LoadAssessorInfo()

            If (Not Request("id") Is Nothing) Then
                hdUID.Value = Request("id")
                LoadAssessmentPsychology()
                LoadPsychologyProblem()
            End If

        End If
    End Sub
    Private Sub LoadPsychology()
        chkPros.Items.Clear()

        dt = ctlM.Psychology_Get
        With chkPros
            .Enabled = True
            .DataSource = dt
            .DataTextField = "Descriptions"
            .DataValueField = "UID"
            .DataBind()
            .Visible = True
        End With
        chkPros.ClearSelection()
        dt = Nothing
    End Sub

    Private Sub LoadAssessorInfo()
        Dim ctlPs As New UserController
        lblAssessorName.Text = ctlPs.User_GetNameByUserID(Request.Cookies("UserLoginID").Value)
    End Sub
    Private Sub LoadStudentInfo()
        dt = ctlStd.GetStudent_ByID(Request("std"))
        If dt.Rows.Count > 0 Then
            lblStudentCode.Text = String.Concat(dt.Rows(0)("Student_Code"))
            lblStudentName.Text = String.Concat(dt.Rows(0)("StudentName"))
            lblMajorName.Text = String.Concat(dt.Rows(0)("MajorName"))
            lblNickName.Text = String.Concat(dt.Rows(0)("NickName"))
        End If
        dt = Nothing
    End Sub

    Protected Sub cmdBack_Click(sender As Object, e As EventArgs) Handles cmdBack.Click
        Response.Redirect("PsychologyAssessment.aspx?ActionType=psy")
    End Sub

    Private Sub LoadAssessmentPsychology()
        dt = ctlA.StudentPsychologyAssessment_GetByUID(StrNull2Zero(hdUID.Value))

        If dt.Rows.Count > 0 Then
            txtComment.Text = String.Concat(dt.Rows(0)("Comment"))
        End If
        dt = Nothing
    End Sub

    Private Sub LoadPsychologyProblem()
        chkPros.ClearSelection()

        dt = ctlM.StudentPsychologyProblem_Get(lblStudentCode.Text)

        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                For n = 0 To chkPros.Items.Count - 1
                    If dt.Rows(i)("ProsID") = chkPros.Items(n).Value Then
                        chkPros.Items(n).Selected = True
                    End If
                Next
            Next
        End If
        dt = Nothing

    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click
        ctlA.StudentPsychologyAssessment_Save(StrNull2Zero(hdUID.Value), lblStudentCode.Text, txtComment.Text, Request.Cookies("UserLoginID").Value)

        ctlA.StudentPsychologyProblem_Delete(lblStudentCode.Text)
        SavePsychology()
        'DisplayMessage(Me.Page, "บันทึกเรียบร้อย")

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)
    End Sub
    Private Sub SavePsychology()
        For i = 0 To chkPros.Items.Count - 1
            If chkPros.Items(i).Selected Then
                ctlA.StudentPsychologyProblem_Save(lblStudentCode.Text, StrNull2Zero(chkPros.Items(i).Value), Request.Cookies("UserLoginID").Value)
            End If
        Next
    End Sub

    Protected Sub cmdDelete_Click(sender As Object, e As EventArgs) Handles cmdDelete.Click
        ctlA.StudentPsychologyAssessment_Delete(StrNull2Zero(hdUID.Value))

        Response.Redirect("PsychologyAssessment.aspx?ActionType=psy")
    End Sub
End Class

