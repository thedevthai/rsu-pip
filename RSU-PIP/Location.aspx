﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Location.aspx.vb" Inherits=".Location" %>
<%@ Register assembly="DevExpress.Web.v17.2, Version=17.2.13.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">    
     <section class="content-header">
      <h1>แหล่งฝึก
          <small></small>
              
      </h1>
      <ol class="breadcrumb">
        <li><i class="fa fa-home"></i>  </li>       
      </ol>
    </section>

<section class="content">

 <!-- Small boxes (Stat box) -->


      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-7 connectedSortable">   
            
 <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-hospital-o"></i>

              <h3 class="box-title">ข้อมูลแหล่งฝึก</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">
                
<table width="100%" border="0">
                                                <tr>
                                                     <td width="150" align="left" class="texttopic">รหัส :  </td>
                                                  <td align="left" width="250"><asp:Label ID="lblID" runat="server"></asp:Label>&nbsp;<asp:Label ID="lblAuto" runat="server">(กำหนดให้อัตโนมัติด้วยโปรแกรม)</asp:Label></td> 
                                                    <td width="100" align="left" class="texttopic">      </td>
                                                    <td align="left" class="texttopic">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td align="left" class="texttopic">
                                                        ชื่อแหล่งฝึก:</td>
                                                    <td align="left" class="texttopic" colspan="3"><asp:TextBox ID="txtName" runat="server" Width="100%" ></asp:TextBox></td>
                                                    
                                                </tr>
                                                <tr>
                                                  <td align="left" class="texttopic">ที่อยู่ :</td>
                                                  <td colspan="3" align="left" class="texttopic">
                                                      <asp:TextBox ID="txtAddress" runat="server" Width="100%" ></asp:TextBox>                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" class="texttopic">
                                                        จังหวัด/เมือง :                                                        </td>
                                                    <td align="left" class="texttopic" colspan="3">
                                                        <table width="100%">
                                                            <tr>
                                                                <td>
            <asp:DropDownList CssClass="Objcontrol" ID="ddlProvince" runat="server"></asp:DropDownList>                                                    <asp:TextBox ID="txtStat" runat="server" Width="200px" ></asp:TextBox></td>
                                                                <td align="right">ประเทศ :</td>
                                                                <td><asp:TextBox ID="txtCountry" runat="server" Width="100px" >ประเทศไทย</asp:TextBox></td>
                                                                 <td align="right" class="texttopic">รหัสไปรษณีย์:</td>
                                                    <td align="left" class="texttopic">
                                                        <asp:TextBox ID="txtZipCode" runat="server" MaxLength="5" Width="50px" 
                                                        ></asp:TextBox></td>

                                                            </tr>

                                                        </table>
                                                    </td>                                                   
                                                </tr>
                                                <tr>
                                                    <td align="left" class="texttopic">โทรศัพท์ :                                                    </td>
                                                    <td align="left" class="texttopic" width="250">
                                                        <asp:TextBox ID="txtTel" runat="server" Width="100%"></asp:TextBox>                                                    </td>
                                                    <td align="right" class="texttopic">โทรสาร :</td>
                                                    <td align="left" class="texttopic">
                                                        <asp:TextBox ID="txtFax" runat="server" Width="100%"></asp:TextBox>                                                    </td>
                                                </tr>
                                                <tr>
                                                  <td align="left" class="texttopic">ชื่อผู้รับเช็ค :</td>
                                                  <td align="left" class="texttopic" colspan="3">
                                                        <asp:TextBox ID="txtReceiveName" runat="server" Width="100%"></asp:TextBox>                                                    </td>
                                                </tr>
                                                <tr>
                                                  <td align="left" class="texttopic">อีเมล์ (หลัก)</td>
                                                  <td align="left" class="texttopic" colspan="3">
                                                      <asp:TextBox ID="txtMail" runat="server" width="90%"></asp:TextBox>                                                    </td>
                                                </tr>
                                               <tr>
             
              <td align="left" valign="top">หัวจดหมาย/หนังสือราชการ</td>     
              <td align="left" valign="top" colspan="3">
              <asp:TextBox ID="txtLetterName" runat="server" Width="90%" MaxLength="100"></asp:TextBox> 
              </td>

            </tr>
                                               <tr>
             
              <td align="left" valign="top">Web site</td>     
              <td align="left" valign="top" colspan="3">
              <asp:TextBox ID="txtWebsite" runat="server" Width="90%" MaxLength="100"></asp:TextBox> 
                                                   </td>

            </tr>
    <tr>
             
              <td align="left" valign="top">Facebook</td>     
              <td align="left" valign="top" colspan="3">
              <asp:TextBox ID="txtFacebook" runat="server" Width="90%" MaxLength="100"></asp:TextBox> 
              </td>

            </tr>
    <tr>
             
              <td align="left" valign="top">ละติจูด (Lat)</td>     
              <td align="left" valign="top" colspan="3">
                  <table cellpadding="0" cellspacing="0">
                      <tr>
                          <td>
                              <asp:TextBox ID="txtLat" runat="server"></asp:TextBox>
                          </td>
                          <td>ลองติจูด (Lng)</td>
                          <td>
                              <asp:TextBox ID="txtLng" runat="server"></asp:TextBox>
                          </td>
                      </tr>
                  </table>
              </td>

            </tr>
  </table> 
    </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>
  
               <div class="box box-success">
            <div class="box-header">
              <i class="fa fa-university"></i>

              <h3 class="box-title">ประเภทแหล่งฝึก</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body"> 
       <table width="100%" border="0">
            <tr>
              <td width="100" class="texttopic" valign="top">ประเภท</td>
              <td align="left">
                  <asp:DropDownList ID="ddlLocationGroup" runat="server" AutoPostBack="True" CssClass="Objcontrol">
                  </asp:DropDownList>
                         </td>
            </tr>
</table>
                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                    <ContentTemplate>
 <table width="100%">
            <tr>
              <td width="100" class="texttopic" valign="top">
                  <asp:Label ID="lblType" runat="server" Text="กลุ่ม"></asp:Label>                </td>
              <td align="left">
                  <asp:DropDownList ID="ddlType" runat="server" CssClass="Objcontrol">
                  </asp:DropDownList>
                </td>
            </tr>
     </table>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ddlLocationGroup" EventName="SelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                    <ContentTemplate>
  <table width="100%">
      <tr>
              <td class="texttopic" width="100" valign="top">
                  <asp:Label ID="lblDepartment" runat="server" Text="สังกัด"></asp:Label>
                </td>
              <td align="left">
                      <asp:DropDownList ID="ddlDepartment" runat="server" AutoPostBack="True" CssClass="Objcontrol" Width="200px">
                        </asp:DropDownList>                </td>
            </tr>
           </table>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ddlLocationGroup" EventName="SelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                    <ContentTemplate>
               <table >
                    <tr>
                        <td style="width: 100px;">
                  <asp:Label ID="lblSpecDept" runat="server" Text="ระบุ"></asp:Label></td>
                        <td>
                  <asp:TextBox ID="txtDepartment" runat="server" Width="200px"></asp:TextBox>
                        </td>
                         
                    </tr>
                     
                </table>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ddlDepartment" EventName="SelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>
              


                <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                    <ContentTemplate>
                  <table >
                      <tr>
                          <td width="100">
                        <asp:Label ID="lblLevel" runat="server" Text="ประเภทร้านยา"></asp:Label>
                          </td>
                          <td>
                        <asp:DropDownList ID="ddlLevel" runat="server" CssClass="Objcontrol" Width="200px">
                            <asp:ListItem Selected="True" Value="1">ร้านยาทั่วไป</asp:ListItem>
                            <asp:ListItem Value="2">ร้านยาคุณภาพ</asp:ListItem>
                    </asp:DropDownList>                      </td>
                      </tr>
                  </table>
                
   
   </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ddlLocationGroup" EventName="SelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>


       <table  border="0"  >
 <tr>
              <td width="100">จำนวนเตียง</td>
              <td align="left"><asp:TextBox ID="txtBed" runat="server" Width="60px"></asp:TextBox></td>
              <td align="left" >จำนวนสาขา</td>
      <td>
        <asp:TextBox ID="txtBranch" runat="server" Width="60px"></asp:TextBox>
      </td>
      </tr>  

 <tr>
              <td  colspan="4">
              <table border="0">
  <tr>
    <td>จำนวนเภสัชกรที่ปฏิบัติงานเต็มเวลา (ต่อ 1 สาขา)</td>
    <td> <asp:TextBox ID="txtOfficerNo" runat="server" Width="60px"></asp:TextBox></td>
  </tr>
</table>

              </td>              
              </tr>  

                <tr>
                     <td class="texttopic"  width="100">เวลาเปิดทำการ</td>
                  <td colspan="3">
                      <asp:TextBox ID="txtOfficeHour" runat="server" Width="200px"></asp:TextBox>                    </td> 
                </tr>
              </table>          
            </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>


 <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-user-md"></i>

              <h3 class="box-title">ผู้ประสานงาน</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">
                  <table width="100%" >
                  <tr>
                      <td class="texttopic" width="150">ชื่อ-สกุล</td>
                      <td><asp:TextBox ID="txtCoName" runat="server" width="90%"></asp:TextBox></td>
                    </tr>
                     <tr>
                      <td class="texttopic" width="100">ตำแหน่ง</td>
                      <td>
                                                      <asp:TextBox ID="txtCoPosition" runat="server" width="90%"></asp:TextBox>                                                    </td>
                  </tr>
                     <tr>
                      <td class="texttopic" width="100">ส่วนงาน</td>
                      <td>
                                                      <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                                          <ContentTemplate>
                                                              <asp:DropDownList ID="ddlWork" runat="server" CssClass="Objcontrol">
                                                              </asp:DropDownList>
                                                              <asp:Label ID="lblNoWork" runat="server" CssClass="alert alert-error show" Text="ยังไม่ได้กำหนดส่วนงาน กรุณาไปกำหนดก่อน" Visible="False"></asp:Label>
                                                          </ContentTemplate>
                                                          <Triggers>
                                                              <asp:AsyncPostBackTrigger ControlID="ddlLocationGroup" EventName="SelectedIndexChanged" />
                                                          </Triggers>
                                                      </asp:UpdatePanel>
                         </td>
                  </tr>
                  <tr>
                      <td class="texttopic">อีเมล</td>
                      <td><asp:TextBox ID="txtCoMail" runat="server" width="90%"></asp:TextBox></td>
                     </tr>
                     <tr>  <td class="texttopic">เบอร์โทรศัพท์</td>
                      <td><asp:TextBox ID="txtCoTel" runat="server" width="90%"></asp:TextBox></td>
                  </tr>
                     <tr>  <td class="texttopic">สำหรับ นศ. ชั้นปีที่</td>
                      <td>
                          <table>
                              <tr>
                                  <td>
                                      <asp:DropDownList ID="ddlClassLevel" runat="server">
                                          <asp:ListItem Selected="True" Value="0">--- ทุกระดับชั้น ---</asp:ListItem>
                                          <asp:ListItem>1</asp:ListItem>
                                          <asp:ListItem>2</asp:ListItem>
                                          <asp:ListItem>3</asp:ListItem>
                                          <asp:ListItem>4</asp:ListItem>
                                          <asp:ListItem>5</asp:ListItem>
                                          <asp:ListItem>6</asp:ListItem>
                                      </asp:DropDownList>
                                  </td>
                                  <td>สาขา</td>
                                  <td>
                                      <asp:DropDownList ID="ddlMajor" runat="server">
                                      </asp:DropDownList>
                                  </td> 
                              </tr>
                          </table>
                         </td>
                  </tr>
                  <tr>
                      <td class="texttopic">รายวิชา</td>
                      <td><asp:DropDownList ID="ddlSubject" runat="server" CssClass="Objcontrol" Width="90%"></asp:DropDownList></td>
                  </tr>
                  <tr>
                      <td class="texttopic">&nbsp;</td>
                      <td class="mailbox-messages">
                          <asp:CheckBox ID="chkCoMain" runat="server" Text="ผู้ประสานงานหลัก" />
                      </td>
                  </tr>                     
                  <tr>
                      <td class="texttopic">&nbsp;</td>
                      <td>
                          <asp:Button ID="cmdAdd" runat="server" CssClass="btn btn-find" Text="เพิ่มผู้ประสานงาน" />
                      </td>
                      <td>&nbsp;</td>
                  </tr>
              </table>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                  <ContentTemplate>
                      <dx:ASPxGridView ID="grdCoordinator2" ClientInstanceName="grdCoordinator2" runat="server" KeyFieldName="UID" OnCustomCallback="grdCoordinator2_CustomCallback" OnCustomButtonCallback="grdCoordinator2_CustomButtonCallback" Width="100%"  AutoGenerateColumns="False" Theme="Office2010Blue">
                          <Settings GridLines="None"  GroupFormat="{0} {1} {2}" />
                          <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
                          <SettingsSearchPanel Visible="True" />                           

        <Columns>          
            <dx:GridViewCommandColumn ShowNewButton="true" VisibleIndex="8" ButtonRenderMode="Image" Caption="ลบ">
                <CustomButtons>
                    <dx:GridViewCommandColumnCustomButton ID="Del">
                        <Image ToolTip="ลบรายชื่อ" Url="images/delete.png" />
                    </dx:GridViewCommandColumnCustomButton>
                </CustomButtons>
            </dx:GridViewCommandColumn>

            <dx:GridViewDataColumn FieldName="Name" VisibleIndex="2" Caption="ชื่อ-นามสกุล"/>
            <dx:GridViewDataColumn FieldName="PositionName" VisibleIndex="3" Caption="ตำแหน่ง"/>
            <dx:GridViewDataColumn FieldName="WorkName" VisibleIndex="1" Caption="ส่วนงานที่ดูแล"/>
            <dx:GridViewDataColumn FieldName="Email" VisibleIndex="4" Caption="อีเมล"/>           
            <dx:GridViewDataColumn FieldName="Tel" VisibleIndex="5" Caption="เบอร์โทร"/>            
            <dx:GridViewDataColumn Caption="ชั้นปี" FieldName="ClassLevelName" VisibleIndex="6" />
             <dx:GridViewDataColumn Caption="สาขา" FieldName="MajorName" VisibleIndex="7" />
            
        </Columns>
                          <Styles>
                              <Header Font-Bold="True"  Font-Size="12px" HorizontalAlign="Center" CssClass="th">
                              </Header>
                              <GroupRow BackColor="#ecf0f5"   Font-Size="12px">
                              </GroupRow>
                              <Row  Font-Size="12px">
                              </Row>
                              <AlternatingRow BackColor="#ffdfef"></AlternatingRow>
                          </Styles>
    </dx:ASPxGridView>
                      <asp:GridView ID="grdCoordinator" runat="server" AutoGenerateColumns="False" CellPadding="2" CssClass="txtcontent" Font-Bold="False" GridLines="None" Width="100%" DataKeyNames="UID">
                          <RowStyle BackColor="White"/>
                          <columns>
                              <asp:BoundField DataField="Name" HeaderText="ชื่อ-สกุล">
                              <HeaderStyle HorizontalAlign="Left" />
                              <ItemStyle Width="200px" />
                              </asp:BoundField>
                              <asp:BoundField DataField="PositionName" HeaderText="ตำแหน่ง">
                              <HeaderStyle HorizontalAlign="Center" />
                              </asp:BoundField>
                              <asp:BoundField DataField="Email" HeaderText="อีเมล"></asp:BoundField>
                              <asp:BoundField HeaderText="โทรศัพท์" DataField="Tel" />
                              <asp:BoundField DataField="ClassLevelName" HeaderText="ชั้นปี" />
                              <asp:BoundField DataField="MajorName" HeaderText="สาขา" />
                              <asp:BoundField HeaderText="ประสานงานวิชา" DataField="SubjectName" />                             
                              <asp:TemplateField HeaderText="ลบ">
                                  <ItemTemplate>
                                      <asp:ImageButton ID="cmdDel" runat="server" ImageUrl="images/delete.png"  CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID") %>'  />
                                  </ItemTemplate>
                                  <ItemStyle HorizontalAlign="Center" />
                              </asp:TemplateField>
                          </columns>
                          <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                          <pagerstyle CssClass="dc_pagination dc_paginationC dc_paginationC11" HorizontalAlign="Center" />
                          <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                          <headerstyle CssClass="th" Font-Bold="True" />
                          <EditRowStyle BackColor="#2461BF" />
                          <AlternatingRowStyle BackColor="#ffdfef"/>
                      </asp:GridView>
                  </ContentTemplate>
                  <Triggers>
                      <asp:AsyncPostBackTrigger ControlID="cmdAdd" EventName="Click" />
                      <asp:AsyncPostBackTrigger ControlID="grdCoordinator" EventName="RowCommand" />
                      <asp:AsyncPostBackTrigger ControlID="grdCoordinator2" EventName="RowCommand" />
                  </Triggers>
              </asp:UpdatePanel>
    </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>
 
 </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        <section class="col-lg-5 connectedSortable">
         
      
             <div class="box box-success">
            <div class="box-header">
              <i class="fa fa-clock-o"></i>

              <h3 class="box-title">วัน-เวลาที่สามารถให้นักศึกษาฝึกปฏิบัติงาน</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">
   <table border="0"   width="100%" class="mailbox-messages">
            <tr>
              <td valign="top" width="30">วัน</td>
              <td width="200px">
              <asp:RadioButtonList ID="optDay" runat="server">
                  <asp:ListItem Value="1" Selected="True">จันทร์-ศุกร์</asp:ListItem>
                  <asp:ListItem Value="2">จันทร์-เสาร์</asp:ListItem>
                  <asp:ListItem Value="3">อื่นๆ (โปรดระบุ)</asp:ListItem>
                  </asp:RadioButtonList>                </td>
              <td  width="30"  valign="bottom">ระบุ</td>
              <td valign="bottom"><asp:TextBox ID="txtDay" runat="server" Width="100%"></asp:TextBox></td>
            </tr>
            <tr>
              <td  valign="top">เวลา</td>
              <td>
              <asp:RadioButtonList ID="optTime" runat="server" RepeatDirection="Vertical">
                  <asp:ListItem Value="1" Selected="True">08.00-16.00 น.</asp:ListItem>
                  <asp:ListItem Value="2">09.00-17.00 น.</asp:ListItem>
                  <asp:ListItem Value="3">อื่นๆ (โปรดระบุ)</asp:ListItem>
                  </asp:RadioButtonList>                </td>
              <td valign="bottom">ระบุ</td>
              <td valign="bottom">
                      <asp:TextBox ID="txtOfficeTime" runat="server" Width="100%"></asp:TextBox>                    </td>
            </tr>
            
          </table>
                 <asp:Panel ID="pnWorkBrunch" runat="server">
          
          <table width="100%" border="0"  >
            <tr>
              <td>
                  <asp:RadioButtonList ID="optWorkBrunch" runat="server" 
                      RepeatDirection="Horizontal">
                      <asp:ListItem Value="1">นักศึกษาต้องหมุนเวียนฝึกทุกสาขา</asp:ListItem>
                      <asp:ListItem Value="2" Selected="True">อยู่ประจำสาขาเดียว</asp:ListItem>
                  </asp:RadioButtonList>                </td>
            </tr>
            <tr>
              <td>ระบุรายละเอียด</td>
            </tr>
            <tr>
              <td><asp:TextBox ID="txtBrunchDetail" runat="server" Width="90%" Height="55px" 
                      TextMode="MultiLine"></asp:TextBox></td>
            </tr>
          </table>
             
              </asp:Panel>
    </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>

               <div class="box box-success">
            <div class="box-header">
              <i class="fa fa-cloud"></i>

              <h3 class="box-title">โซน/กลุ่ม</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">
                <table  Width="99%">
                    <tr>
                        <td>โซนมหาวิทยาลัย</td>
                        <td> 
                            <asp:DropDownList ID="ddlZone" runat="server" CssClass="Objcontrol" Width="95%">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>กลุ่มแหล่งฝึก</td>
                        <td>
                            
                            
                            
                            <asp:DropDownList ID="ddlOfficeGroup" runat="server" CssClass="Objcontrol" Width="95%">
                            </asp:DropDownList>
                            
                            
                            
                        </td>
                    </tr>
                </table>


        </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>

              <div class="box box-success">
            <div class="box-header">
              <i class="fa fa-dollar"></i>

              <h3 class="box-title">ค่าตอบแทน</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">


                <table class="mailbox-messages">
                    <tr>
                        <td>แหล่งฝึกประสงค์ต้องการรับค่าตอบแทนจากคณะ</td>
                        <td><asp:RadioButtonList ID="optReceive" runat="server" RepeatDirection="Horizontal" CssClass="mailbox-messages">
                                <asp:ListItem Value="Y">รับ</asp:ListItem>
                                <asp:ListItem Selected="True" Value="N">ไม่รับ</asp:ListItem>
                            </asp:RadioButtonList></td>
                    </tr>
                    <tr>
                        <td>แหล่งฝึกมีค่าตอบแทนให้นักศึกษา</td>
                        <td>
                            
                            <asp:RadioButtonList ID="optPay" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="Y">มี</asp:ListItem>
                                <asp:ListItem Selected="True" Value="N">ไม่มี</asp:ListItem>
                            </asp:RadioButtonList>
                            
                        </td>
                    </tr>
                </table>


        </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>
      

              <div class="box box-success">
            <div class="box-header">
              <i class="fa fa-medkit"></i>

              <h3 class="box-title"><asp:Label ID="lblStep5" runat="server" Text="งานที่สามารถให้นักศึกษาฝึกปฏิบัติได้"></asp:Label> </h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body mailbox-messages">
                <table width="100%">                                  
                        <tr>
          <td align="left" valign="top" class="mailbox-messages">              
              
              <asp:Panel ID="pnWorkList" runat="server">
            <!--<asp:ListItem Value="1">งานบริการผู้ป่วยนอกและผู้ป่วยใน</asp:ListItem>
                  <asp:ListItem Value="2">งานด้านเภสัชกรรมคลีนิก</asp:ListItem>
                  <asp:ListItem Value="3">งานด้านคลังเวชภัณฑ์</asp:ListItem>
                  <asp:ListItem Value="4">งานบริการวิชาการ</asp:ListItem>
                  <asp:ListItem Value="5">งานการผลิตยา</asp:ListItem>
                  <asp:ListItem Value="6">งานคุ้มครองผู้บริโภคในหน่วยงาน</asp:ListItem>
                  <asp:ListItem Value="7">ศึกษาดูงานคุ้มครองผู้บริโภค ณ สำนักงานสาธารณสุขจังหวัด</asp:ListItem>
                  <asp:ListItem Value="8">อื่นๆ (โปรดระบุ)</asp:ListItem>-->
                  <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                      <ContentTemplate>
                          <asp:CheckBoxList ID="chkWorkList" runat="server" CssClass="mailbox-messages">
                          </asp:CheckBoxList>
                      </ContentTemplate>
                      <Triggers>
                          <asp:AsyncPostBackTrigger ControlID="ddlLocationGroup" EventName="SelectedIndexChanged" />
                      </Triggers>
                  </asp:UpdatePanel>
         <div class="form-control no-border">**ถ้ามีงานอื่นๆนอกจากตัวเลือกข้างบน ให้ไปเพิ่มงานที่เมนู ส่วนงานของแหล่งฝึก ก่อน
         </div>   
            </asp:Panel>          </td>
      </tr>
        <tr>
          <td align="left" valign="top" >งานเด่นของแหล่งฝึก</td>
      </tr>
        <tr>
          <td align="left" valign="top" >
                                                      <asp:TextBox ID="txtTopWork" 
                  runat="server" Width="90%" TextMode="MultiLine" Height="55px"></asp:TextBox>                                                    </td>
      </tr>
         <tr>
          <td align="left" valign="top">สิ่งอื่นๆที่นักศึกษาควรทราบหรือเตรียมตัวก่อนมาฝึกปฏิบัติงาน</td>
      </tr>
         <tr>
          <td align="left" valign="top">
                                                      <asp:TextBox ID="txtRemark" 
                  runat="server" Width="90%" Height="55px" TextMode="MultiLine"></asp:TextBox>                                                    </td>
      </tr>
         

                </table>



    </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>   

             <div class="box box-success">
            <div class="box-header">
              <i class="fa fa-hotel"></i>

              <h3 class="box-title">การจัดหาที่พักสำหรับนักศึกษา</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">
              
              <table width="100%" border="0"  >
            <tr>
              <td>
                  <asp:RadioButton ID="optResidence1" runat="server" Text="แหล่งฝึกมีที่พักให้" 
                      AutoPostBack="True" />                </td>
            </tr>
            <tr>
              <td>
                  <asp:Panel ID="pnRes1" runat="server" HorizontalAlign="Left" class="block_step1">                  
                  <asp:RadioButtonList ID="optIsPay" runat="server" RepeatDirection="Horizontal">
                      <asp:ListItem Value="0" Selected="True">ไม่ต้องเสียค่าใช้จ่าย</asp:ListItem>
                      <asp:ListItem Value="1">เสียค่าเช่าโดยประมาณ</asp:ListItem>
                  </asp:RadioButtonList>
                  เดือนละ
                  <asp:TextBox ID="txtPay" runat="server" Width="60px" CssClass="NumberCenter"></asp:TextBox>
&nbsp;บาท<br /> นักศึกษาต้องเข้าพัก ณ ที่พักที่แหล่งฝึกจัดหาให้<asp:RadioButtonList ID="optForce" runat="server" RepeatDirection="Horizontal">
                          <asp:ListItem Value="1">ใช่</asp:ListItem>
                          <asp:ListItem Value="0" Selected="True">ไม่ใช่</asp:ListItem>
                      </asp:RadioButtonList>
                      <br />
                นักศึกษาต้องติดต่อเพื่อเข้าพักที่แหล่งฝึกจัดหาให้ล่วงหน้า 
                  <asp:TextBox ID="txtConfirm" runat="server" Width="50px" CssClass="NumberCenter"></asp:TextBox>
&nbsp;เดือน
</asp:Panel></td>
            </tr>
            <tr>
              <td>
                  <asp:RadioButton ID="optResidence2" runat="server" 
                      Text="แหล่งฝึกไม่มีที่พักให้" AutoPostBack="True" />                </td>
            </tr>
            <tr>
              <td>
                  <asp:RadioButton ID="optResidence3" runat="server" 
                      Text="แหล่งฝึกไม่มีที่พักให้ แต่สามารถแนะนำที่พักให้ได้" 
                      AutoPostBack="True" />                </td>
            </tr>
            <tr>
              <td>
                  <asp:Panel ID="pnRes3" runat="server" class="block_step1">
                
                  <table width="100%" border="0"  >
                <tr>
                  <td width="100">ชื่อที่พัก</td>
                  <td>
                                                      <asp:TextBox ID="txtApartmentName1" runat="server" Width="200px"></asp:TextBox>                                                    </td>
                  <td width="100">เบอร์โทร</td>
                  <td>
                                                        <asp:TextBox ID="txtApartmentTel1" runat="server" Width="200px"></asp:TextBox>                                                    </td>
                </tr>
                <tr>
                  <td>ที่อยู่</td>
                  <td colspan="3">
                                                        <asp:TextBox ID="txtApartmentAddress1" runat="server" 
                          Width="90%"></asp:TextBox>                                                    </td>
                </tr>
                <tr>
                  <td>การเดินทาง</td>
                  <td>
                                                      <asp:TextBox ID="txtTravel1" runat="server" Width="200px"></asp:TextBox>                                                    </td>
                  <td>ระยะทาง</td>
                  <td>
                                                      <asp:TextBox ID="txtDistance1" runat="server" Width="200px"></asp:TextBox>                                                    </td>
                </tr>
                      <tr>
                          <td>ค่าใช้จ่าย/อื่นๆ</td>
                          <td colspan="3">
                              <asp:TextBox ID="txtApartmentRemark1" runat="server" Width="90%"></asp:TextBox>
                          </td>
                      </tr>
              </table>

                      <br />

                         <table width="100%" border="0"  >
                <tr>
                  <td width="100">ชื่อที่พัก</td>
                  <td>
                                                      <asp:TextBox ID="txtApartmentName2" runat="server" Width="200px"></asp:TextBox>                                                    </td>
                  <td width="100">เบอร์โทร</td>
                  <td>
                                                        <asp:TextBox ID="txtApartmentTel2" runat="server" Width="200px"></asp:TextBox>                                                    </td>
                </tr>
                <tr>
                  <td>ที่อยู่</td>
                  <td colspan="3">
                                                        <asp:TextBox ID="txtApartmentAddress2" runat="server" 
                          Width="90%"></asp:TextBox>                                                    </td>
                </tr>
                <tr>
                  <td>การเดินทาง</td>
                  <td>
                                                      <asp:TextBox ID="txtTravel2" runat="server" Width="200px"></asp:TextBox>                                                    </td>
                  <td>ระยะทาง</td>
                  <td>
                                                      <asp:TextBox ID="txtDistance2" runat="server" Width="200px"></asp:TextBox>                                                    </td>
                </tr>
                              <tr>
                          <td>ค่าใช้จ่าย/อื่นๆ</td>
                          <td colspan="3">
                              <asp:TextBox ID="txtApartmentRemark2" runat="server" Width="90%"></asp:TextBox>
                          </td>
                      </tr>
              </table>

                      <br />
                         <table width="100%" border="0"  >
                <tr>
                  <td width="100">ชื่อที่พัก</td>
                  <td>
                                                      <asp:TextBox ID="txtApartmentName3" runat="server" Width="200px"></asp:TextBox>                                                    </td>
                  <td width="100">เบอร์โทร</td>
                  <td>
                                                        <asp:TextBox ID="txtApartmentTel3" runat="server" Width="200px"></asp:TextBox>                                                    </td>
                </tr>
                <tr>
                  <td>ที่อยู่</td>
                  <td colspan="3">
                                                        <asp:TextBox ID="txtApartmentAddress3" runat="server" 
                          Width="90%"></asp:TextBox>                                                    </td>
                </tr>
                <tr>
                  <td>การเดินทาง</td>
                  <td>
                                                      <asp:TextBox ID="txtTravel3" runat="server" Width="200px"></asp:TextBox>                                                    </td>
                  <td>ระยะทาง</td>
                  <td>
                                                      <asp:TextBox ID="txtDistance3" runat="server" Width="200px"></asp:TextBox>                                                    </td>
                </tr>
                              <tr>
                          <td>ค่าใช้จ่าย/อื่นๆ</td>
                          <td colspan="3">
                              <asp:TextBox ID="txtApartmentRemark3" runat="server" Width="90%"></asp:TextBox>
                          </td>
                      </tr>
              </table>
              
                </asp:Panel>              </td>
            </tr>
          </table>        
    
    </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>


        
      <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">เอกสารอัพโหลดสำหรับ นศ.</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">  
     
        <table width="100%">
             <tr>
                 <td width="100">File</td>
                 <td>
                     <asp:FileUpload ID="FileUpload1" runat="server" Width="300px" />
                 </td>
            </tr> 
            <tr>
                <td>Description</td>
                <td>
                    <asp:TextBox ID="txtDesc" runat="server" CssClass="OptionControl2" Width="90%"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td valign="top">&nbsp;</td>
                <td>
                    <asp:Button ID="cmdUploadFile" runat="server" CssClass="btn btn-save" Text="Upload" Width="100px" />
                    &nbsp;</td>
            </tr>
    </table>
 
                      <asp:Panel ID="pnDocumentAlert" runat="server" Width="100%">
                         <div class="alert alert-danger alert-dismissible">
                             <h4><i class="icon fa fa-ban"></i>Alert!</h4>
                             <p>
                                 <asp:Label ID="lblDocAlert" runat="server"></asp:Label>
                             </p>
                         </div>
                     </asp:Panel>
         
                              
                <asp:GridView ID="grdDocument" runat="server" AutoGenerateColumns="False" CellPadding="0" ForeColor="#333333" GridLines="None" Width="100%">
                    <RowStyle BackColor="White" VerticalAlign="Top" />
                    <columns>
                        <asp:HyperLinkField DataNavigateUrlFields="FilePath" DataTextField="Descriptions" HeaderText="รายการเอกสารอัพโหลด" Target="_blank" >
                        <HeaderStyle HorizontalAlign="Left" />
                        </asp:HyperLinkField>
                        <asp:TemplateField HeaderText="ลบ">
                            <ItemTemplate>
                                <asp:ImageButton ID="imgDel_Doc" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID") %>' CssClass="gridbutton" Height="20px" ImageUrl="images/delete.png" />
                            </ItemTemplate>
                            <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                        </asp:TemplateField>
                    </columns>
                    <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <pagerstyle CssClass="dc_pagination dc_paginationC dc_paginationC11" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <headerstyle CssClass="th" Font-Bold="True" VerticalAlign="Middle" />
                    <EditRowStyle BackColor="#2461BF" />
                    <AlternatingRowStyle BackColor="#F7F7F7" />
                </asp:GridView>
                
      </div>
      <!-- /.box -->  
</div> 

   </section>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->
    <div class="row">
 
           
        
        <div align="center" class="mailbox-messages"><asp:CheckBox ID="chkStatus" runat="server" Checked="True" Text="Active" />            </div>

         <div align="center">
              <asp:Label ID="lblValidate" runat="server" 
                  Visible="False" CssClass="alert alert-error show" Width="99%"></asp:Label>
        </div>

       <div align="center"><span class="texttopic">
            <asp:Button ID="cmdSave" runat="server" Text="บันทึก" CssClass="btn btn-save" Width="100px"></asp:Button>
            <asp:Button ID="cmdClear" runat="server" text="ยกเลิก" CssClass="btn btn-default" Width="100px"></asp:Button>
          </span>
      </div>
    </div>
      
    </section>
   
</asp:Content>
