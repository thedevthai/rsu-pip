﻿Public Class Homes
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlT As New StudentController
    Dim ctlU As New UserController
    Dim ctlM As New MediaController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Request.Cookies("UserLogin").Value Is Nothing Then
            Response.Redirect("Default.aspx?logout=YES")
        End If

        If Not IsPostBack Then

            If StrNull2Boolean(Request.Cookies("ROLE_ADM").Value) = True Then
                LoadMemberCount()
            End If

            LoadUserOnline()
            LoadNewsToGrid()

            LoadMedia_4a()
            LoadMedia_4b()
            LoadMedia_4c()

            LoadMedia_6a()
            LoadMedia_6b()
            LoadMedia_6c()
            LoadMedia_6d()
            LoadMedia_6e()
            LoadMedia_6f()
            LoadMedia_6g()


            'If Request.Cookies("UserProfileID").Value = 3 Then
            '    LoadVPNAccount()
            '    'LoadCreditBalance()
            'End If

        End If

    End Sub
    'Private Sub LoadVPNAccount()
    '    Dim ctlP As New PersonController
    '    Dim ctlV As New VPNController
    '    Dim LID As Integer = ctlP.Person_GetLocationByUserID(Request.Cookies("UserLoginID").Value)

    '    dt = ctlV.VPNAccount_GetByLocation(LID)
    '    If dt.Rows.Count > 0 Then
    '        lblVPNUser.Text = dt.Rows(0)("Username")
    '        lblVPNPass.Text = dt.Rows(0)("Password")
    '    End If
    'End Sub
    Private Sub LoadNewsToGrid()
        Dim ctlNews As New NewsController
        dt = ctlNews.News_GetFirstPage

        With grdData
            .Visible = True
            .DataSource = dt
            .DataBind()

            For i = 0 To .Rows.Count - 1
                Dim img1 As Image = .Rows(i).Cells(0).FindControl("imgNews")

                Dim hlnkN As HyperLink = .Rows(i).Cells(0).FindControl("hlnkNews")
                hlnkN.Text = dt.Rows(i)("Title")

                Select Case dt.Rows(i)("NewsType")
                    Case "URL"
                        img1.ImageUrl = "images/www.png"
                        hlnkN.NavigateUrl = String.Concat(dt.Rows(i)("LinkPath"))
                    Case "UPL"

                        Dim str As String()
                        str = Split(Right(String.Concat(dt.Rows(i)("LinkPath")), 6), ".")

                        Select Case str(1)
                            Case "pdf"
                                img1.ImageUrl = "images/pdf.png"
                            Case "doc", "docx"
                                img1.ImageUrl = "images/word.png"
                            Case "ppt", "pptx"
                                img1.ImageUrl = "images/ppt.png"
                            Case "xls", "xlsx"
                                img1.ImageUrl = "images/xls.png"
                            Case Else
                                img1.ImageUrl = "images/pdf.png"
                        End Select

                        hlnkN.NavigateUrl = tmpUpload & "/" & String.Concat(dt.Rows(i)("LinkPath"))
                    Case "CON"
                        img1.ImageUrl = "images/newspaper.jpg"
                        hlnkN.NavigateUrl = "News.aspx?id=" & .DataKeys(i).Value
                    Case Else
                        img1.ImageUrl = "images/newspaper.jpg"
                        hlnkN.NavigateUrl = "News.aspx?id=" & .DataKeys(i).Value
                End Select

            Next

        End With

    End Sub
    Private Sub LoadMedia_4a()

        dt = ctlM.Media_Get(4, 0, 1, "Y")
        If dt.Rows.Count > 0 Then

            With grdMedia4a
                .Visible = True
                .DataSource = dt
                .DataBind()

                For i = 0 To .Rows.Count - 1
                    Dim img1 As Image = .Rows(i).Cells(0).FindControl("imgNews")

                    Dim hlnkN As HyperLink = .Rows(i).Cells(0).FindControl("hlnkMedia4a")
                    hlnkN.Text = dt.Rows(i)("Title")

                    Select Case dt.Rows(i)("MediaType")
                        Case "URL"
                            img1.ImageUrl = "images/www.png"
                            hlnkN.NavigateUrl = String.Concat(dt.Rows(i)("LinkPath"))
                        Case "UPL"
                            Dim str As String()
                            str = Split(Right(String.Concat(dt.Rows(i)("LinkPath")), 6), ".")

                            Select Case str(1)
                                Case "pdf"
                                    img1.ImageUrl = "images/pdf_download.png"
                                Case "doc", "docx"
                                    img1.ImageUrl = "images/word.png"
                                Case "ppt", "pptx"
                                    img1.ImageUrl = "images/ppt.png"
                                Case "xls", "xlsx"
                                    img1.ImageUrl = "images/xls.png"
                                Case Else
                                    img1.ImageUrl = "images/pdf.png"
                            End Select

                            hlnkN.NavigateUrl = DocumentUpload & "/4/" & String.Concat(dt.Rows(i)("LinkPath"))

                        Case "CON"
                            img1.ImageUrl = "images/newspaper.jpg"
                            hlnkN.NavigateUrl = "News.aspx?id=" & .DataKeys(i).Value
                        Case Else
                            img1.ImageUrl = "images/newspaper.jpg"
                            hlnkN.NavigateUrl = "News.aspx?id=" & .DataKeys(i).Value
                    End Select

                    'Dim img2 As Image = .Rows(i).Cells(0).FindControl("imgNew")
                    'img2.Visible = False
                    'If DateDiff(DateInterval.Day, Today.Date, dt.Rows(i)("NewsDate")) <= 5 Then
                    '    img2.Visible = True
                    'End If
                    '.Rows(i).Cells(2).Text = "Update เมื่อ" & DisplayDateTH(dt.Rows(i)("NewsDate"))
                Next

            End With

        End If
    End Sub
    Private Sub LoadMedia_4b()

        dt = ctlM.Media_Get(4, 0, 2, "Y")
        If dt.Rows.Count > 0 Then


            With grdMedia4b
                .Visible = True
                .DataSource = dt
                .DataBind()

                For i = 0 To .Rows.Count - 1
                    Dim img1 As Image = .Rows(i).Cells(0).FindControl("imgNews")

                    Dim hlnkN As HyperLink = .Rows(i).Cells(0).FindControl("hlnkMedia4b")
                    hlnkN.Text = dt.Rows(i)("Title")

                    Select Case dt.Rows(i)("MediaType")
                        Case "URL"
                            img1.ImageUrl = "images/www.png"
                            hlnkN.NavigateUrl = String.Concat(dt.Rows(i)("LinkPath"))
                        Case "UPL"
                            Dim str As String()
                            str = Split(Right(String.Concat(dt.Rows(i)("LinkPath")), 6), ".")

                            Select Case str(1)
                                Case "pdf"
                                    img1.ImageUrl = "images/pdf_download.png"
                                Case "doc", "docx"
                                    img1.ImageUrl = "images/word.png"
                                Case "ppt", "pptx"
                                    img1.ImageUrl = "images/ppt.png"
                                Case "xls", "xlsx"
                                    img1.ImageUrl = "images/xls.png"
                                Case Else
                                    img1.ImageUrl = "images/pdf.png"
                            End Select

                            hlnkN.NavigateUrl = DocumentUpload & "/4/" & String.Concat(dt.Rows(i)("LinkPath"))

                        Case "CON"
                            img1.ImageUrl = "images/newspaper.jpg"
                            hlnkN.NavigateUrl = "News.aspx?id=" & .DataKeys(i).Value
                        Case Else
                            img1.ImageUrl = "images/newspaper.jpg"
                            hlnkN.NavigateUrl = "News.aspx?id=" & .DataKeys(i).Value
                    End Select

                    'Dim img2 As Image = .Rows(i).Cells(0).FindControl("imgNew")
                    'img2.Visible = False
                    'If DateDiff(DateInterval.Day, Today.Date, dt.Rows(i)("NewsDate")) <= 5 Then
                    '    img2.Visible = True
                    'End If
                    '.Rows(i).Cells(2).Text = "Update เมื่อ" & DisplayDateTH(dt.Rows(i)("NewsDate"))
                Next

            End With
        End If
    End Sub
    Private Sub LoadMedia_4c()

        dt = ctlM.Media_Get(4, 0, 3, "Y")
        If dt.Rows.Count > 0 Then
            With grdMedia4c
                .Visible = True
                .DataSource = dt
                .DataBind()

                For i = 0 To .Rows.Count - 1
                    Dim img1 As Image = .Rows(i).Cells(0).FindControl("imgNews")

                    Dim hlnkN As HyperLink = .Rows(i).Cells(0).FindControl("hlnkMedia4c")
                    hlnkN.Text = dt.Rows(i)("Title")

                    Select Case dt.Rows(i)("MediaType")
                        Case "URL"
                            img1.ImageUrl = "images/www.png"
                            hlnkN.NavigateUrl = String.Concat(dt.Rows(i)("LinkPath"))
                        Case "UPL"
                            Dim str As String()
                            str = Split(Right(String.Concat(dt.Rows(i)("LinkPath")), 6), ".")

                            Select Case str(1)
                                Case "pdf"
                                    img1.ImageUrl = "images/pdf_download.png"
                                Case "doc", "docx"
                                    img1.ImageUrl = "images/word.png"
                                Case "ppt", "pptx"
                                    img1.ImageUrl = "images/ppt.png"
                                Case "xls", "xlsx"
                                    img1.ImageUrl = "images/xls.png"
                                Case Else
                                    img1.ImageUrl = "images/pdf.png"
                            End Select

                            hlnkN.NavigateUrl = DocumentUpload & "/4/" & String.Concat(dt.Rows(i)("LinkPath"))

                        Case "CON"
                            img1.ImageUrl = "images/newspaper.jpg"
                            hlnkN.NavigateUrl = "News.aspx?id=" & .DataKeys(i).Value
                        Case Else
                            img1.ImageUrl = "images/newspaper.jpg"
                            hlnkN.NavigateUrl = "News.aspx?id=" & .DataKeys(i).Value
                    End Select

                    'Dim img2 As Image = .Rows(i).Cells(0).FindControl("imgNew")
                    'img2.Visible = False
                    'If DateDiff(DateInterval.Day, Today.Date, dt.Rows(i)("NewsDate")) <= 5 Then
                    '    img2.Visible = True
                    'End If
                    '.Rows(i).Cells(2).Text = "Update เมื่อ" & DisplayDateTH(dt.Rows(i)("NewsDate"))
                Next

            End With
        End If
    End Sub

    Private Sub LoadMedia_6a()

        dt = ctlM.Media_Get(6, 2, 1, "Y")
        If dt.Rows.Count > 0 Then


            With grdMedia6a
                .Visible = True
                .DataSource = dt
                .DataBind()

                For i = 0 To .Rows.Count - 1
                    Dim img1 As Image = .Rows(i).Cells(0).FindControl("imgNews")

                    Dim hlnkN As HyperLink = .Rows(i).Cells(0).FindControl("hlnkMedia6a")
                    hlnkN.Text = dt.Rows(i)("Title")

                    Select Case dt.Rows(i)("MediaType")
                        Case "URL"
                            img1.ImageUrl = "images/www.png"
                            hlnkN.NavigateUrl = String.Concat(dt.Rows(i)("LinkPath"))
                        Case "UPL"
                            Dim str As String()
                            str = Split(Right(String.Concat(dt.Rows(i)("LinkPath")), 6), ".")

                            Select Case str(1)
                                Case "pdf"
                                    img1.ImageUrl = "images/pdf_download.png"
                                Case "doc", "docx"
                                    img1.ImageUrl = "images/word.png"
                                Case "ppt", "pptx"
                                    img1.ImageUrl = "images/ppt.png"
                                Case "xls", "xlsx"
                                    img1.ImageUrl = "images/xls.png"
                                Case Else
                                    img1.ImageUrl = "images/pdf.png"
                            End Select

                            hlnkN.NavigateUrl = DocumentUpload & "/6/" & String.Concat(dt.Rows(i)("LinkPath"))

                        Case "CON"
                            img1.ImageUrl = "images/newspaper.jpg"
                            hlnkN.NavigateUrl = "News.aspx?id=" & .DataKeys(i).Value
                        Case Else
                            img1.ImageUrl = "images/newspaper.jpg"
                            hlnkN.NavigateUrl = "News.aspx?id=" & .DataKeys(i).Value
                    End Select

                    'Dim img2 As Image = .Rows(i).Cells(0).FindControl("imgNew")
                    'img2.Visible = False
                    'If DateDiff(DateInterval.Day, Today.Date, dt.Rows(i)("NewsDate")) <= 5 Then
                    '    img2.Visible = True
                    'End If
                    '.Rows(i).Cells(2).Text = "Update เมื่อ" & DisplayDateTH(dt.Rows(i)("NewsDate"))
                Next

            End With
        End If
    End Sub
    Private Sub LoadMedia_6b()

        dt = ctlM.Media_Get(6, 2, 2, "Y")
        If dt.Rows.Count > 0 Then


            With grdMedia6b
                .Visible = True
                .DataSource = dt
                .DataBind()

                For i = 0 To .Rows.Count - 1
                    Dim img1 As Image = .Rows(i).Cells(0).FindControl("imgNews")

                    Dim hlnkN As HyperLink = .Rows(i).Cells(0).FindControl("hlnkMedia6b")
                    hlnkN.Text = dt.Rows(i)("Title")

                    Select Case dt.Rows(i)("MediaType")
                        Case "URL"
                            img1.ImageUrl = "images/www.png"
                            hlnkN.NavigateUrl = String.Concat(dt.Rows(i)("LinkPath"))
                        Case "UPL"
                            Dim str As String()
                            str = Split(Right(String.Concat(dt.Rows(i)("LinkPath")), 6), ".")

                            Select Case str(1)
                                Case "pdf"
                                    img1.ImageUrl = "images/pdf_download.png"
                                Case "doc", "docx"
                                    img1.ImageUrl = "images/word.png"
                                Case "ppt", "pptx"
                                    img1.ImageUrl = "images/ppt.png"
                                Case "xls", "xlsx"
                                    img1.ImageUrl = "images/xls.png"
                                Case Else
                                    img1.ImageUrl = "images/pdf.png"
                            End Select

                            hlnkN.NavigateUrl = DocumentUpload & "/6/" & String.Concat(dt.Rows(i)("LinkPath"))

                        Case "CON"
                            img1.ImageUrl = "images/newspaper.jpg"
                            hlnkN.NavigateUrl = "News.aspx?id=" & .DataKeys(i).Value
                        Case Else
                            img1.ImageUrl = "images/newspaper.jpg"
                            hlnkN.NavigateUrl = "News.aspx?id=" & .DataKeys(i).Value
                    End Select

                    'Dim img2 As Image = .Rows(i).Cells(0).FindControl("imgNew")
                    'img2.Visible = False
                    'If DateDiff(DateInterval.Day, Today.Date, dt.Rows(i)("NewsDate")) <= 5 Then
                    '    img2.Visible = True
                    'End If
                    '.Rows(i).Cells(2).Text = "Update เมื่อ" & DisplayDateTH(dt.Rows(i)("NewsDate"))
                Next

            End With
        End If
    End Sub

    Private Sub LoadMedia_6c()

        dt = ctlM.Media_Get(6, 2, 3, "Y")
        If dt.Rows.Count > 0 Then


            With grdMedia6c
                .Visible = True
                .DataSource = dt
                .DataBind()

                For i = 0 To .Rows.Count - 1
                    Dim img1 As Image = .Rows(i).Cells(0).FindControl("imgNews")

                    Dim hlnkN As HyperLink = .Rows(i).Cells(0).FindControl("hlnkMedia6c")
                    hlnkN.Text = dt.Rows(i)("Title")

                    Select Case dt.Rows(i)("MediaType")
                        Case "URL"
                            img1.ImageUrl = "images/www.png"
                            hlnkN.NavigateUrl = String.Concat(dt.Rows(i)("LinkPath"))
                        Case "UPL"
                            Dim str As String()
                            str = Split(Right(String.Concat(dt.Rows(i)("LinkPath")), 6), ".")

                            Select Case str(1)
                                Case "pdf"
                                    img1.ImageUrl = "images/pdf_download.png"
                                Case "doc", "docx"
                                    img1.ImageUrl = "images/word.png"
                                Case "ppt", "pptx"
                                    img1.ImageUrl = "images/ppt.png"
                                Case "xls", "xlsx"
                                    img1.ImageUrl = "images/xls.png"
                                Case Else
                                    img1.ImageUrl = "images/pdf.png"
                            End Select

                            hlnkN.NavigateUrl = DocumentUpload & "/6/" & String.Concat(dt.Rows(i)("LinkPath"))

                        Case "CON"
                            img1.ImageUrl = "images/newspaper.jpg"
                            hlnkN.NavigateUrl = "News.aspx?id=" & .DataKeys(i).Value
                        Case Else
                            img1.ImageUrl = "images/newspaper.jpg"
                            hlnkN.NavigateUrl = "News.aspx?id=" & .DataKeys(i).Value
                    End Select

                    'Dim img2 As Image = .Rows(i).Cells(0).FindControl("imgNew")
                    'img2.Visible = False
                    'If DateDiff(DateInterval.Day, Today.Date, dt.Rows(i)("NewsDate")) <= 5 Then
                    '    img2.Visible = True
                    'End If
                    '.Rows(i).Cells(2).Text = "Update เมื่อ" & DisplayDateTH(dt.Rows(i)("NewsDate"))
                Next

            End With
        End If
    End Sub
    Private Sub LoadMedia_6d()

        dt = ctlM.Media_Get(6, 1, 1, "Y")
        If dt.Rows.Count > 0 Then


            With grdMedia6d
                .Visible = True
                .DataSource = dt
                .DataBind()

                For i = 0 To .Rows.Count - 1
                    Dim img1 As Image = .Rows(i).Cells(0).FindControl("imgNews")

                    Dim hlnkN As HyperLink = .Rows(i).Cells(0).FindControl("hlnkMedia6d")
                    hlnkN.Text = dt.Rows(i)("Title")

                    Select Case dt.Rows(i)("MediaType")
                        Case "URL"
                            img1.ImageUrl = "images/www.png"
                            hlnkN.NavigateUrl = String.Concat(dt.Rows(i)("LinkPath"))
                        Case "UPL"
                            Dim str As String()
                            str = Split(Right(String.Concat(dt.Rows(i)("LinkPath")), 6), ".")

                            Select Case str(1)
                                Case "pdf"
                                    img1.ImageUrl = "images/pdf_download.png"
                                Case "doc", "docx"
                                    img1.ImageUrl = "images/word.png"
                                Case "ppt", "pptx"
                                    img1.ImageUrl = "images/ppt.png"
                                Case "xls", "xlsx"
                                    img1.ImageUrl = "images/xls.png"
                                Case Else
                                    img1.ImageUrl = "images/pdf.png"
                            End Select

                            hlnkN.NavigateUrl = DocumentUpload & "/6/" & String.Concat(dt.Rows(i)("LinkPath"))

                        Case "CON"
                            img1.ImageUrl = "images/newspaper.jpg"
                            hlnkN.NavigateUrl = "News.aspx?id=" & .DataKeys(i).Value
                        Case Else
                            img1.ImageUrl = "images/newspaper.jpg"
                            hlnkN.NavigateUrl = "News.aspx?id=" & .DataKeys(i).Value
                    End Select

                    'Dim img2 As Image = .Rows(i).Cells(0).FindControl("imgNew")
                    'img2.Visible = False
                    'If DateDiff(DateInterval.Day, Today.Date, dt.Rows(i)("NewsDate")) <= 5 Then
                    '    img2.Visible = True
                    'End If
                    '.Rows(i).Cells(2).Text = "Update เมื่อ" & DisplayDateTH(dt.Rows(i)("NewsDate"))
                Next

            End With
        End If
    End Sub
    Private Sub LoadMedia_6e()

        dt = ctlM.Media_Get(6, 1, 2, "Y")
        If dt.Rows.Count > 0 Then


            With grdMedia6e
                .Visible = True
                .DataSource = dt
                .DataBind()

                For i = 0 To .Rows.Count - 1
                    Dim img1 As Image = .Rows(i).Cells(0).FindControl("imgNews")

                    Dim hlnkN As HyperLink = .Rows(i).Cells(0).FindControl("hlnkMedia6e")
                    hlnkN.Text = dt.Rows(i)("Title")

                    Select Case dt.Rows(i)("MediaType")
                        Case "URL"
                            img1.ImageUrl = "images/www.png"
                            hlnkN.NavigateUrl = String.Concat(dt.Rows(i)("LinkPath"))
                        Case "UPL"
                            Dim str As String()
                            str = Split(Right(String.Concat(dt.Rows(i)("LinkPath")), 6), ".")

                            Select Case str(1)
                                Case "pdf"
                                    img1.ImageUrl = "images/pdf_download.png"
                                Case "doc", "docx"
                                    img1.ImageUrl = "images/word.png"
                                Case "ppt", "pptx"
                                    img1.ImageUrl = "images/ppt.png"
                                Case "xls", "xlsx"
                                    img1.ImageUrl = "images/xls.png"
                                Case Else
                                    img1.ImageUrl = "images/pdf.png"
                            End Select

                            hlnkN.NavigateUrl = DocumentUpload & "/6/" & String.Concat(dt.Rows(i)("LinkPath"))

                        Case "CON"
                            img1.ImageUrl = "images/newspaper.jpg"
                            hlnkN.NavigateUrl = "News.aspx?id=" & .DataKeys(i).Value
                        Case Else
                            img1.ImageUrl = "images/newspaper.jpg"
                            hlnkN.NavigateUrl = "News.aspx?id=" & .DataKeys(i).Value
                    End Select

                    'Dim img2 As Image = .Rows(i).Cells(0).FindControl("imgNew")
                    'img2.Visible = False
                    'If DateDiff(DateInterval.Day, Today.Date, dt.Rows(i)("NewsDate")) <= 5 Then
                    '    img2.Visible = True
                    'End If
                    '.Rows(i).Cells(2).Text = "Update เมื่อ" & DisplayDateTH(dt.Rows(i)("NewsDate"))
                Next

            End With
        End If
    End Sub
    Private Sub LoadMedia_6f()

        dt = ctlM.Media_Get(6, 1, 3, "Y")
        If dt.Rows.Count > 0 Then


            With grdMedia6f
                .Visible = True
                .DataSource = dt
                .DataBind()

                For i = 0 To .Rows.Count - 1
                    Dim img1 As Image = .Rows(i).Cells(0).FindControl("imgNews")

                    Dim hlnkN As HyperLink = .Rows(i).Cells(0).FindControl("hlnkMedia6f")
                    hlnkN.Text = dt.Rows(i)("Title")

                    Select Case dt.Rows(i)("MediaType")
                        Case "URL"
                            img1.ImageUrl = "images/www.png"
                            hlnkN.NavigateUrl = String.Concat(dt.Rows(i)("LinkPath"))
                        Case "UPL"
                            Dim str As String()
                            str = Split(Right(String.Concat(dt.Rows(i)("LinkPath")), 6), ".")

                            Select Case str(1)
                                Case "pdf"
                                    img1.ImageUrl = "images/pdf_download.png"
                                Case "doc", "docx"
                                    img1.ImageUrl = "images/word.png"
                                Case "ppt", "pptx"
                                    img1.ImageUrl = "images/ppt.png"
                                Case "xls", "xlsx"
                                    img1.ImageUrl = "images/xls.png"
                                Case Else
                                    img1.ImageUrl = "images/pdf.png"
                            End Select

                            hlnkN.NavigateUrl = DocumentUpload & "/6/" & String.Concat(dt.Rows(i)("LinkPath"))

                        Case "CON"
                            img1.ImageUrl = "images/newspaper.jpg"
                            hlnkN.NavigateUrl = "News.aspx?id=" & .DataKeys(i).Value
                        Case Else
                            img1.ImageUrl = "images/newspaper.jpg"
                            hlnkN.NavigateUrl = "News.aspx?id=" & .DataKeys(i).Value
                    End Select

                    'Dim img2 As Image = .Rows(i).Cells(0).FindControl("imgNew")
                    'img2.Visible = False
                    'If DateDiff(DateInterval.Day, Today.Date, dt.Rows(i)("NewsDate")) <= 5 Then
                    '    img2.Visible = True
                    'End If
                    '.Rows(i).Cells(2).Text = "Update เมื่อ" & DisplayDateTH(dt.Rows(i)("NewsDate"))
                Next

            End With
        End If
    End Sub
    Private Sub LoadMedia_6g()

        dt = ctlM.Media_Get(6, 2, 4, "Y")
        If dt.Rows.Count > 0 Then


            With grdMedia6g
                .Visible = True
                .DataSource = dt
                .DataBind()

                For i = 0 To .Rows.Count - 1
                    Dim img1 As Image = .Rows(i).Cells(0).FindControl("imgNews")

                    Dim hlnkN As HyperLink = .Rows(i).Cells(0).FindControl("hlnkMedia6g")
                    hlnkN.Text = dt.Rows(i)("Title")

                    Select Case dt.Rows(i)("MediaType")
                        Case "URL"
                            img1.ImageUrl = "images/www.png"
                            hlnkN.NavigateUrl = String.Concat(dt.Rows(i)("LinkPath"))
                        Case "UPL"
                            Dim str As String()
                            str = Split(Right(String.Concat(dt.Rows(i)("LinkPath")), 6), ".")

                            Select Case str(1)
                                Case "pdf"
                                    img1.ImageUrl = "images/pdf_download.png"
                                Case "doc", "docx"
                                    img1.ImageUrl = "images/word.png"
                                Case "ppt", "pptx"
                                    img1.ImageUrl = "images/ppt.png"
                                Case "xls", "xlsx"
                                    img1.ImageUrl = "images/xls.png"
                                Case Else
                                    img1.ImageUrl = "images/pdf.png"
                            End Select

                            hlnkN.NavigateUrl = DocumentUpload & "/6/" & String.Concat(dt.Rows(i)("LinkPath"))

                        Case "CON"
                            img1.ImageUrl = "images/newspaper.jpg"
                            hlnkN.NavigateUrl = "News.aspx?id=" & .DataKeys(i).Value
                        Case Else
                            img1.ImageUrl = "images/newspaper.jpg"
                            hlnkN.NavigateUrl = "News.aspx?id=" & .DataKeys(i).Value
                    End Select

                    'Dim img2 As Image = .Rows(i).Cells(0).FindControl("imgNew")
                    'img2.Visible = False
                    'If DateDiff(DateInterval.Day, Today.Date, dt.Rows(i)("NewsDate")) <= 5 Then
                    '    img2.Visible = True
                    'End If
                    '.Rows(i).Cells(2).Text = "Update เมื่อ" & DisplayDateTH(dt.Rows(i)("NewsDate"))
                Next

            End With
        End If
    End Sub
    Private Sub LoadMemberCount()

        lblMember1.Text = String.Concat(ctlT.Student_GetCountByLevelClass(1).ToString("#,##0"))
        lblMember2.Text = String.Concat(ctlT.Student_GetCountByLevelClass(2).ToString("#,##0"))
        lblMember3.Text = String.Concat(ctlT.Student_GetCountByLevelClass(3).ToString("#,##0"))
        lblMember4.Text = String.Concat(ctlT.Student_GetCountByLevelClass(4).ToString("#,##0"))
        lblMember5.Text = String.Concat(ctlT.Student_GetCountByLevelClass(5).ToString("#,##0"))
        lblMember6.Text = String.Concat(ctlT.Student_GetCountByLevelClass(6).ToString("#,##0"))
        lblMember7.Text = String.Concat(ctlT.Student_GetCountByLevelClass(7).ToString("#,##0"))
        lblMember8.Text = String.Concat(ctlT.Student_GetCountByLevelClass(40).ToString("#,##0"))

    End Sub

    'Private Sub LoadCreditBalance()
    '    Dim ctlL As New LocationCreditController

    '    'Label2.Text = String.Concat(ctlL.Location_GetCreditBalance(Request.Cookies("LocationID").Value).ToString("#,##0"))

    'End Sub
    Private Sub LoadUserOnline()
        dt = ctlU.Users_Online
        lblOnline.Text = ""
        If dt.Rows.Count > 0 Then
            dtlMember.DataSource = dt
            dtlMember.DataBind()

            'For i = 0 To dt.Rows.Count - 1

            '    Select Case i Mod 5
            '        Case 0
            '            lblOnline.Text &= " <span class='label label-success'>" & dt.Rows(i)("Name") & "</span>"
            '        Case 1
            '            lblOnline.Text &= " <span class='label label-info'>" & dt.Rows(i)("Name") & "</span>"
            '        Case 2
            '            lblOnline.Text &= " <span class='label label-danger'>" & dt.Rows(i)("Name") & "</span>"
            '        Case 3
            '            lblOnline.Text &= " <span class='label label-primary'>" & dt.Rows(i)("Name") & "</span>"
            '        Case Else
            '            lblOnline.Text &= " <span class='label label-warning'>" & dt.Rows(i)("Name") & "</span>"
            '    End Select
            '    'If i Mod 2 = 0 Then
            '    '    lblOnline.Text &= " <span class='label label-success'>" & dt.Rows(i)("Username") & " : " & dt.Rows(i)("Name") & "</span>"
            '    '    'lblOnline.Text &= "<font color=#1878ca>" & "[" & dt.Rows(i)("Username") & " : " & dt.Rows(i)("Name") & "]" & "</font>"
            '    'Else
            '    '    lblOnline.Text &= " <span class='label label-warning'>" & dt.Rows(i)("Username") & " : " & dt.Rows(i)("Name") & "</span>"
            '    'End If

            'Next

        Else
            lblOnline.Text = "ไม่มี User Online ในขณะนี้"
        End If

    End Sub
End Class