﻿Public Class _404
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ctlS As New SystemConfigController



        If ctlS.SystemConfig_GetByCode("ALIVE") = "Y" Then
            Response.Redirect("Default.aspx")
        Else
            Dim dt As New DataTable
            dt = ctlS.SystemConfig_GetAllByCode("ALIVE")
            lblRemark.Text = String.Concat(dt.Rows(0)("Remark"))
        End If

    End Sub

End Class