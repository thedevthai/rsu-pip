﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="AssignMenu.aspx.vb" Inherits=".AssignMenu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
     <section class="content-header">
      <h1>การคัดเลือกแหล่งฝึกแบบสุ่ม (Random)
        <small>เลือกชั้นปี </small>
      </h1>    
    </section>

<section class="content">    
      
      <div class="row">
        <div class="col-md-6">
          <div class="small-box bg-blue">
            <div class="inner">
                <br />
              <h3>นักศึกษา ชั้นปีที่ 4</h3>

              <p></p>
            </div>
            <div class="icon">
              <i class="ion ion-load-a"></i>
            </div>
            <a href="AssignRandom1.aspx?ActionType=asg&ItemType=rnd" class="small-box-footer"><h4>Go <i class="fa fa-arrow-circle-right"></i></h4></a>
          </div>
        </div>
              
            <div class="col-md-6">
          <div class="small-box bg-purple">
            <div class="inner">
                  <br />
              <h3>นักศึกษา ชั้นปีที่ 6</h3>
              <p></p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="AssignRandom2.aspx?ActionType=asg&ItemType=rnd" class="small-box-footer"><h4>Go <i class="fa fa-arrow-circle-right"></i></h4></a>
          </div>
        </div>
      </div>
    
    </section>
</asp:Content>
