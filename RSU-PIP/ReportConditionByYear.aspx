﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="ReportConditionByYear.aspx.vb" Inherits=".ReportConditionByYear" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" type="text/css" href="css/jquery-ui.css">  
</asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  
    <div class="Page_Header">
        <asp:Label ID="lblReportHeader" runat="server"></asp:Label>
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr>
          <td align="center" valign="top">&nbsp;</td>
      </tr>
        <tr>
            <td align="left" valign="top">
             
            
<table border="0" align="center" cellPadding="1" cellSpacing="1">
<tr>
                                                    <td align="left">
                                                        ปีการศึกษา :                                                        </td>
                                                    <td align="left">
                                                        <asp:DropDownList ID="ddlYear" runat="server" 
                                                            CssClass="Objcontrol" AutoPostBack="True">                                                        </asp:DropDownList>                                                    </td>
</tr>
<tr>
  <td colspan="2" align="center" >
      <asp:Button ID="cmdPrint1" runat="server" CssClass="btn btn-save" Text="รายงานแบบสรุป" />
    &nbsp;<asp:Button ID="cmdPrint2" runat="server" CssClass="btn btn-save" Text="รายงานแบบ Table" />
    </td>
  </tr>
  </table>      </td>
      </tr>
       <tr>
          <td  align="center" valign="top"  >
              &nbsp;</td>
      </tr>
       <tr>
         <td  align="center" valign="top" >
              &nbsp;</td>
       </tr>
       <tr>
         <td  align="center" valign="top"  >
             <asp:Label ID="lblResult" runat="server" CssClass="OptionPanels" 
                 Text="ไม่มีข้อมูลการเลือกแหล่งฝึก" Width="95%"></asp:Label>
           </td>
       </tr>
        <tr>
         <td    align="left" valign="top" >&nbsp;</td>
       </tr>

       <tr>
         <td   align="center" valign="top" >&nbsp;</td>
      </tr>
       <tr>
         <td height="200"  align="left" valign="top" >&nbsp;</td>
       </tr>
    
    </table>
    
</asp:Content>
