﻿Public Class PracticeHistory
    Inherits System.Web.UI.Page
    Dim ctlA As New AssessmentController
    Dim ctlU As New UserController
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Request.Cookies("ROLE_ADM").Value = True Or Request.Cookies("ROLE_SPA").Value = True Then
                LoadStudentData(Request("std"))
                LoadFoundationHistory(Request("std"))
                LoadPraticeHistory(Request("std"))

                grdFoundation.Columns(4).Visible = True
                grdData.Columns(5).Visible = True
            Else
                LoadStudentData(Request.Cookies("UserLogin").Value)
                LoadFoundationHistory(Request.Cookies("UserLogin").Value)
                LoadPraticeHistory(Request.Cookies("UserLogin").Value)

                grdFoundation.Columns(4).Visible = False
                grdData.Columns(5).Visible = False
            End If
        End If
    End Sub

    Private Sub LoadStudentData(stdcode As String)
        Dim dtS As New DataTable
        Dim ctlS As New StudentController
        dtS = ctlS.GetStudent_ByID(stdcode)
        If dtS.Rows.Count > 0 Then
            With dtS.Rows(0)
                txtCode.Text = String.Concat(.Item("Student_Code"))
                txtName.Text = String.Concat(.Item("StudentName"))
                txtMajorName.Text = String.Concat(.Item("MajorName"))
                txtLevelClass.Text = String.Concat(.Item("ClassName"))

                lnkAdd.NavigateUrl = "PracticeHistoryModify.aspx?ActionType=std&ItemType=pthistory&stdcode=" & txtCode.Text
            End With
        End If
        dtS = Nothing
    End Sub
    Private Sub LoadFoundationHistory(stdcode As String)

        Dim dtA As New DataTable

        dtA = ctlA.PracticeHistory_GetFoundation(stdcode)
        If dtA.Rows.Count > 0 Then
            With grdFoundation
                .Visible = True
                .DataSource = dtA
                .DataBind()
            End With
            lblNotF.Visible = False
        Else
            lblNotF.Visible = True
        End If
        dtA = Nothing
    End Sub
    Private Sub LoadPraticeHistory(stdcode As String)

        Dim dtA As New DataTable

        dtA = ctlA.PracticeHistory_GetByStudent(stdcode)
        If dtA.Rows.Count > 0 Then
            With grdData
                .Visible = True
                .DataSource = dtA
                .DataBind()
            End With
            lblNot.Visible = False
        Else
            lblNot.Visible = True
        End If
        dtA = Nothing
    End Sub
    Private Sub grdFoundation_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdFoundation.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit1"
                    'Response.Redirect("Student_Bio.aspx?ActionType=std&std=" & e.CommandArgument())
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Modify", "window.location='PracticeHistoryModify.aspx?ActionType=std&id=" & e.CommandArgument() & "';", True)
                Case "imgDel1"
                    If ctlA.Assessment_DeleteByID(e.CommandArgument) Then
                        ctlU.User_GenLogfile(Request.Cookies("UserLogin").Value, "DEL", "Assessment from PracticeHistory", "ลบประวัติการฝึก ID:" & e.CommandArgument & "stdcode=" & Request("std"), "")

                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ลบข้อมูลเรียบร้อย');", True)

                        'DisplayMessage(Me, "ลบข้อมูลเรียบร้อย")
                    Else

                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ไม่สามารถลบข้อมูลได้ กรุณาตรวจสอบและลองใหม่อีกครั้ง');", True)
                        'DisplayMessage(Me, "ไม่สามารถลบข้อมูลได้ กรุณาตรวจสอบและลองใหม่อีกครั้ง")
                    End If

                    LoadFoundationHistory(Request("std"))
            End Select

        End If
    End Sub

    Private Sub grdData_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdData.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit2"
                    'Response.Redirect("Student_Bio.aspx?ActionType=std&std=" & e.CommandArgument())
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Modify", "window.location='PracticeHistoryModify.aspx?ActionType=std&ItemType=pthistory&id=" & e.CommandArgument() & "';", True)
                Case "imgDel2"
                    If ctlA.Assessment_DeleteByID(e.CommandArgument) Then
                        ctlU.User_GenLogfile(Request.Cookies("UserLogin").Value, "DEL", "Assessment from PracticeHistory", "ลบประวัติการฝึก ID:" & e.CommandArgument & "stdcode=" & Request("std"), "")

                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ลบข้อมูลเรียบร้อย');", True)

                        'DisplayMessage(Me, "ลบข้อมูลเรียบร้อย")
                    Else

                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ไม่สามารถลบข้อมูลได้ กรุณาตรวจสอบและลองใหม่อีกครั้ง');", True)
                        'DisplayMessage(Me, "ไม่สามารถลบข้อมูลได้ กรุณาตรวจสอบและลองใหม่อีกครั้ง")
                    End If

                    LoadPraticeHistory(Request("std"))
            End Select

        End If
    End Sub

    Private Sub grdFoundation_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdFoundation.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then
            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(4).FindControl("imgDel1")
            imgD.Attributes.Add("onClick", scriptString)

        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#ffdfef';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If
    End Sub

    Private Sub grdData_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdData.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then
            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(5).FindControl("imgDel2")
            imgD.Attributes.Add("onClick", scriptString)

        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#ffdfef';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If
    End Sub


End Class